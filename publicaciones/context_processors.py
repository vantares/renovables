# -*- coding: utf-8 -*-

from django.conf import settings
from publicaciones.models import Publicacion

def recientes_pub(request):
    return {
        'recientes_list': Publicacion.objects.all().order_by('-fecha')[:2],
    }