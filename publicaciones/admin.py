# -*- coding: UTF-8 -*-
from django.contrib import admin
from publicaciones.models import Publicacion, TipoPublicacion, Fuente, FuenteEnergia

class PublicacionAdmin(admin.ModelAdmin):
    list_display = ['titulo','codigo','tipo','energia','fuente','idioma']
    prepopulated_fields = {'slug': ("titulo",)}

class TipoPublicacionAdmin(admin.ModelAdmin):
    list_display = ['nombre']
    prepopulated_fields = {'slug': ("nombre",)}

class FuenteAdmin(admin.ModelAdmin):
    list_display = ['nombre']
    prepopulated_fields = {'slug': ("nombre",)}

class FuenteEnergiaAdmin(admin.ModelAdmin):
    list_display = ['nombre']
    prepopulated_fields = {'slug': ("nombre",)}

admin.site.register(Publicacion, PublicacionAdmin)
admin.site.register(TipoPublicacion, TipoPublicacionAdmin)
admin.site.register(Fuente,FuenteAdmin)
admin.site.register(FuenteEnergia, FuenteEnergiaAdmin)

