from django.db import models
from django.db.models import Q
from django.contrib.auth.models import User
from sorl.thumbnail import get_thumbnail

class TipoPublicacion(models.Model):
	nombre = models.CharField(max_length=200,verbose_name="Nombre")
	slug = models.SlugField(help_text="Usado como url unica(autorellenado)",blank=True,null=True);

	def __unicode__(self):
		return self.nombre

	class Meta:
		verbose_name = 'Tipo Publicacion'

	@models.permalink
	def get_absolute_url(self):
		if not self.slug:
			self.slug = slugify(self.titulo)[:50]
		return('tipopublicacion_detail', (), {
			'pk': self.pk,
			'slug': self.slug
		})

class Fuente(models.Model):
	nombre = models.CharField(max_length=200,verbose_name="Nombre")
	slug = models.SlugField(help_text="Usado como url unica(autorellenado)",blank=True,null=True);

	def __unicode__(self):
		return self.nombre

	class Meta:
		verbose_name = 'Fuente'

	@models.permalink
	def get_absolute_url(self):
		if not self.slug:
			self.slug = slugify(self.titulo)[:50]
		return('fuente_detail', (), {
			'pk': self.pk,
			'slug': self.slug
		})

class FuenteEnergia(models.Model):
	nombre = models.CharField(max_length=200,verbose_name="Nombre")
	slug = models.SlugField(help_text="Usado como url unica(autorellenado)",blank=True,null=True);

	def __unicode__(self):
		return self.nombre

	class Meta:
		verbose_name = 'Fuente Energia'

	@models.permalink
	def get_absolute_url(self):
		if not self.slug:
			self.slug = slugify(self.titulo)[:50]
		return('fuenteenergia_detail', (), {
			'pk': self.pk,
			'slug': self.slug
		})

class Publicacion(models.Model):
	IDIOMA = (
	("i", "Ingles"),
	("e", "Espanol"),
	)
	codigo = models.IntegerField(max_length=10, unique=True)
	titulo = models.CharField(max_length=400, verbose_name="Titulo Publicacion")
	slug = models.SlugField(help_text='Usado como url unica(autorellenado)', blank=True, null=True)
	descripcion = models.TextField(blank=True, null=True)
	fecha = models.DateField(verbose_name="Publicado el")
	autor = models.CharField(max_length=200)
	imagen = models.ImageField(upload_to='publicaciones')
	idioma = models.CharField(max_length=1, choices=IDIOMA, verbose_name="Idioma")
	tipo = models.ForeignKey(TipoPublicacion,related_name="tipo_publicacion",null=True,blank=True)	
	fuente = models.ForeignKey(Fuente,related_name="fuente",null=True,blank=True)
	energia = models.ForeignKey(FuenteEnergia,related_name="energia",null=True,blank=True)
	archivo = models.FileField(upload_to='publicaciones/archivos', blank=True, null=True)

	class Meta:
		verbose_name = 'Publicacion'
		verbose_name_plural = 'Publicaciones'
        ordering = ['fecha']

	@models.permalink
	def get_absolute_url(self):
		if not self.slug:
			self.slug = slugify(self.titulo)[:50]
		return('publicacion_detail',(), {
			'pk': self.pk,
			'slug': self.slug
		})        
