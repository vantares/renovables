# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Publicacion.archivo'
        db.add_column(u'publicaciones_publicacion', 'archivo',
                      self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Publicacion.archivo'
        db.delete_column(u'publicaciones_publicacion', 'archivo')


    models = {
        u'publicaciones.fuente': {
            'Meta': {'object_name': 'Fuente'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'})
        },
        u'publicaciones.fuenteenergia': {
            'Meta': {'object_name': 'FuenteEnergia'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'})
        },
        u'publicaciones.publicacion': {
            'Meta': {'object_name': 'Publicacion'},
            'archivo': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'autor': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'codigo': ('django.db.models.fields.IntegerField', [], {'unique': 'True', 'max_length': '10'}),
            'descripcion': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'energia': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'energia'", 'null': 'True', 'to': u"orm['publicaciones.FuenteEnergia']"}),
            'fecha': ('django.db.models.fields.DateField', [], {}),
            'fuente': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'fuente'", 'null': 'True', 'to': u"orm['publicaciones.Fuente']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'idioma': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'imagen': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'tipo': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'tipo_publicacion'", 'null': 'True', 'to': u"orm['publicaciones.TipoPublicacion']"}),
            'titulo': ('django.db.models.fields.CharField', [], {'max_length': '400'})
        },
        u'publicaciones.tipopublicacion': {
            'Meta': {'object_name': 'TipoPublicacion'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['publicaciones']