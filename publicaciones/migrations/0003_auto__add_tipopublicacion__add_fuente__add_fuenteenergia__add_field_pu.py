# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'TipoPublicacion'
        db.create_table(u'publicaciones_tipopublicacion', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50, null=True, blank=True)),
        ))
        db.send_create_signal(u'publicaciones', ['TipoPublicacion'])

        # Adding model 'Fuente'
        db.create_table(u'publicaciones_fuente', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50, null=True, blank=True)),
        ))
        db.send_create_signal(u'publicaciones', ['Fuente'])

        # Adding model 'FuenteEnergia'
        db.create_table(u'publicaciones_fuenteenergia', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50, null=True, blank=True)),
        ))
        db.send_create_signal(u'publicaciones', ['FuenteEnergia'])

        # Adding field 'Publicacion.idioma'
        db.add_column(u'publicaciones_publicacion', 'idioma',
                      self.gf('django.db.models.fields.CharField')(default=0, max_length=1),
                      keep_default=False)

        # Adding field 'Publicacion.tipo'
        db.add_column(u'publicaciones_publicacion', 'tipo',
                      self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='tipo_publicacion', null=True, to=orm['publicaciones.TipoPublicacion']),
                      keep_default=False)

        # Adding field 'Publicacion.fuente'
        db.add_column(u'publicaciones_publicacion', 'fuente',
                      self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='fuente', null=True, to=orm['publicaciones.Fuente']),
                      keep_default=False)

        # Adding field 'Publicacion.energia'
        db.add_column(u'publicaciones_publicacion', 'energia',
                      self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='energia', null=True, to=orm['publicaciones.FuenteEnergia']),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting model 'TipoPublicacion'
        db.delete_table(u'publicaciones_tipopublicacion')

        # Deleting model 'Fuente'
        db.delete_table(u'publicaciones_fuente')

        # Deleting model 'FuenteEnergia'
        db.delete_table(u'publicaciones_fuenteenergia')

        # Deleting field 'Publicacion.idioma'
        db.delete_column(u'publicaciones_publicacion', 'idioma')

        # Deleting field 'Publicacion.tipo'
        db.delete_column(u'publicaciones_publicacion', 'tipo_id')

        # Deleting field 'Publicacion.fuente'
        db.delete_column(u'publicaciones_publicacion', 'fuente_id')

        # Deleting field 'Publicacion.energia'
        db.delete_column(u'publicaciones_publicacion', 'energia_id')


    models = {
        u'publicaciones.fuente': {
            'Meta': {'object_name': 'Fuente'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'})
        },
        u'publicaciones.fuenteenergia': {
            'Meta': {'object_name': 'FuenteEnergia'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'})
        },
        u'publicaciones.publicacion': {
            'Meta': {'object_name': 'Publicacion'},
            'autor': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'codigo': ('django.db.models.fields.IntegerField', [], {'unique': 'True', 'max_length': '10'}),
            'descripcion': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'energia': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'energia'", 'null': 'True', 'to': u"orm['publicaciones.FuenteEnergia']"}),
            'fecha': ('django.db.models.fields.DateField', [], {}),
            'fuente': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'fuente'", 'null': 'True', 'to': u"orm['publicaciones.Fuente']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'idioma': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'imagen': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'tipo': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'tipo_publicacion'", 'null': 'True', 'to': u"orm['publicaciones.TipoPublicacion']"}),
            'titulo': ('django.db.models.fields.CharField', [], {'max_length': '400'})
        },
        u'publicaciones.tipopublicacion': {
            'Meta': {'object_name': 'TipoPublicacion'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['publicaciones']