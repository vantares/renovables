# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Publicacion.user'
        db.delete_column(u'publicaciones_publicacion', 'user_id')


    def backwards(self, orm):
        # Adding field 'Publicacion.user'
        db.add_column(u'publicaciones_publicacion', 'user',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=0, to=orm['auth.User'], unique=True),
                      keep_default=False)


    models = {
        u'publicaciones.publicacion': {
            'Meta': {'object_name': 'Publicacion'},
            'autor': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'codigo': ('django.db.models.fields.IntegerField', [], {'unique': 'True', 'max_length': '10'}),
            'descripcion': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'fecha': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imagen': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'titulo': ('django.db.models.fields.CharField', [], {'max_length': '400'})
        }
    }

    complete_apps = ['publicaciones']