# -*-  coding=utf-8 -*-

from django.db.models import Q
from django.views.generic import TemplateView
from django.shortcuts import redirect
from django.template import RequestContext
from django.http import HttpResponse
from django.shortcuts import render_to_response, get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.template.loader import render_to_string
from publicaciones.models import Publicacion, TipoPublicacion, Fuente, FuenteEnergia
from publicaciones.forms import PublicacionForm

@csrf_exempt
def search(request):
    form = PublicacionForm(request.POST)
    q_object_source = Q()
    if form.is_valid():
        print 'entro'
        energia = form.cleaned_data['energia']
        tipo = form.cleaned_data['tipo']
        fuente = form.cleaned_data['fuente']
        desde = form.cleaned_data['desde']
        hasta = form.cleaned_data['hasta']
        idioma = form.cleaned_data['idioma']
        if energia:
            print 'energia'
            q_object_source.add(Q(energia=energia),Q.OR)
        if tipo:
            print 'tipo'
            q_object_source.add(Q(tipo=tipo),Q.OR)
        if fuente:
            print 'fuente'
            q_object_source.add(Q(fuente=fuente),Q.OR)
        if desde and hasta:
            print 'desde y hasta'
            q_object_source.add(Q(fecha__gte=desde,fecha__lte=hasta),Q.OR)
        if idioma:
            print 'idioma'
            q_object_source.add(Q(idioma=idioma),Q.OR)

    resultado = Publicacion.objects.filter(q_object_source)
    contador = resultado.count()
    print resultado

    return render_to_response('biblioteca_virtual.html',{
                                                      'resultado':resultado,
                                                      'search_form':form,
                                                      'contador': contador,
                                                      },
                                                      context_instance=RequestContext(request))





