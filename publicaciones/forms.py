# -*- coding: UTF-8 -*-

from django import forms
from publicaciones.models import Publicacion, TipoPublicacion, Fuente, FuenteEnergia

IDIOMA = (
    ("n", "------"),
	("i", "Ingles"),
	("e", "Espanol"),
	)

class PublicacionForm(forms.Form):
    energia = forms.ModelChoiceField(queryset=FuenteEnergia.objects.all(),required=False)
    tipo = forms.ModelChoiceField(queryset=TipoPublicacion.objects.all(),required=False)
    fuente = forms.ModelChoiceField(queryset=Fuente.objects.all(),required=False)
    idioma =  forms.ChoiceField(choices=IDIOMA)
    desde = forms.DateField(label='Desde',required=False)
    hasta = forms.DateField(label='Hasta',required=False)
