# -*- coding: UTF-8 -*-
from django.db.models import Q
from tastypie import fields, utils
from tastypie.resources import ModelResource
from tastypie.constants import ALL, ALL_WITH_RELATIONS
from organizaciones.models import SistemaEjecucion, Organizacion, Proyecto
from nomencladores.models import Departamento, FuenteRenovable, Actividad
from renovable.models import KMLexternos
from renovable.utils import customquery_fetchassoc

class SourceResource(ModelResource):
    # systems=  fields.ToManyField('renovable.api.SystemResource', 'sistemas', null = True, blank= True)
    class Meta:
        queryset = FuenteRenovable.objects.all()
        resource_name = 'source'
        filtering = {
            "id": ['in','exact'],
            "nombre": ALL,
        }

class ActivityResource(ModelResource):
    class Meta:
        queryset = Actividad.objects.all()
        resource_name = 'activity'

class OrganizationResource(ModelResource):
    fuentes = fields.ToManyField(SourceResource, 'fuente', full = True, null = True, blank = True)
    actividades = fields.ToManyField(ActivityResource, 'actividad', full = True, null = True, blank = True)
    latitud= fields.CharField(attribute='get_lat')
    longitud= fields.CharField(attribute='get_lan')
    popupimage = fields.CharField(attribute='popupimage')
    get_absolute_url = fields.CharField(attribute='get_absolute_url')

    class Meta:
        queryset = Organizacion.objects.all()
        resource_name = 'organization'
        filtering = {
            "id": ALL,
            "nombre": ALL,
            "fuentes": ALL_WITH_RELATIONS,
        }

    def build_filters(self, filters=None):
        if filters is None:
            filters = {}
        orm_filters = super(OrganizationResource, self).build_filters(filters)

        if "alternative" in filters:
            alternative = True
        else:
            alternative = False

        qObjects = Q()

        polygon_qobject= Q()
        if "poly" in filters:
            polygon= filters['poly']
            if polygon is not None:
                geo_query= """
                select id 
                from (
                    select 
                        id, 
                        latitud, 
                        longitud, 
                        ST_PointFromText( 'POINT('||longitud||' '||latitud||')', 4326)::Geometry as punto 
                    from organizaciones_organizacion
                ) as geometria_organizacion 
                where ST_Within(punto,ST_Transform(ST_GeomFromText('%s',900913),4326))
                """ % polygon
                print(geo_query)
                sysinside= customquery_fetchassoc(geo_query)
                syspk_list= [sys['id'] for sys in sysinside]
                polygon_qobject= Q(pk__in= syspk_list)

        sources_qobject = Q() 
        if "src" in filters:
            src = filters['src']
            sources = src.split(",")
            if not alternative:
                orm_filters["fuente__id__in"] = [int(i) for i in sources]
            else:
                sources_qobject = Q(fuente__id__in=sources)
        
        keywords_qobject = Q()
        if "keywords" in filters:
            keywords = filters["keywords"]
            if not alternative:
                orm_filters["nombre__icontains"] = keywords
            else:
                keywords_qobject = Q(nombre__icontains = keywords)
                
        qObjects.add(sources_qobject | keywords_qobject | polygon_qobject, Q.OR)
        
        if alternative:
            orm_filters.update({"alternative": qObjects})
            
        return orm_filters

    def apply_filters(self, request, applicable_filters):
        if 'alternative' in applicable_filters:
            alternative = applicable_filters.pop( 'alternative' )
        else:
            alternative = None
        default_filtered = super( ProjectResource, self ).apply_filters( request, applicable_filters )
        filtered = default_filtered.filter( alternative ).distinct() if alternative else default_filtered
        print(alternative)
        return filtered        

# class OrganizationResource(ModelResource):

#     fuentes = fields.ToManyField(SourceResource, 'fuente', full = True, null = True, blank = True)
#     actividades = fields.ToManyField(ActivityResource, 'actividad', full = True, null = True, blank = True)
#     popupimage = fields.CharField(attribute='popupimage')
#     get_absolute_url = fields.CharField(attribute='get_absolute_url')

#     class Meta:
#         queryset = Organizacion.objects.all()
#         resource_name = 'organization'
#         filtering = {
#             "id": ALL_WITH_RELATIONS,
#             "nombre": ALL,
#         }

#     def build_filters(self, filters=None):
#         if filters is None:
#             filters = {}
#         orm_filters = super(OrganizationResource, self).build_filters(filters)
#         if "src" in filters:
#             fuentes= filters['src']
#             sqs = fuentes.split(",")
#             orm_filters["fuente__id__in"] = [int(i) for i in sqs]
#         return orm_filters

class DepartmentResource(ModelResource):
    class Meta:
        queryset = Departamento.objects.all()
        resource_name = 'department'

class SystemResource(ModelResource):
    organizacion = fields.ToOneField(OrganizationResource, 'organizacion', full = True, null = True, blank = True)
    departamento = fields.ToOneField(DepartmentResource, 'departamento', full = True, null = True, blank = True)
    fuentes = fields.ToManyField(SourceResource, 'fuente', full = True, null = True, blank = True)
    imagen = fields.CharField(attribute='get_imagen')
    latitud= fields.CharField(attribute='get_lat')
    longitud= fields.CharField(attribute='get_lan')
    get_absolute_url = fields.CharField(attribute='get_absolute_url')
    popupimage = fields.CharField(attribute='popupimage')

    class Meta:
        queryset = SistemaEjecucion.objects.all()
        resource_name = 'system'
        limit = 50
        filtering = {
            "id": ALL,
            "nombre": ALL,
            "fuentes": ALL_WITH_RELATIONS,
        }
        
    def build_filters(self, filters=None):
        if filters is None:
            filters = {}
        orm_filters = super(SystemResource, self).build_filters(filters)
        if "alternative" in filters:
            alternative= True
        else:
            alternative = False
        
        qObjects = Q()
        
        polygon_qobject= Q()
        if "poly" in filters:
            polygon= filters['poly']
            print(polygon)
            if polygon is not None:
                geo_query= """
                select id 
                from (
                    select 
                        id, 
                        latitud, 
                        longitud, 
                        ST_PointFromText( 'POINT('||longitud||' '||latitud||')', 4326)::Geometry as punto 
                    from organizaciones_sistemaejecucion
                ) as geometria_sistemas 
                where ST_Within(punto,ST_Transform(ST_GeomFromText('%s',900913),4326))
                """ % polygon
                print(geo_query)
                sysinside= customquery_fetchassoc(geo_query)
                syspk_list= [sys['id'] for sys in sysinside]
                polygon_qobject= Q(pk__in= syspk_list)
        
        sources_qobject = Q() 
        if "src" in filters:
            src = filters['src']
            sources = src.split(",")
            if not alternative:
                orm_filters["fuente__id__in"] = [int(i) for i in sources]
            else:
                sources_qobject = Q(fuente__id__in=sources)
        
        keywords_qobject = Q()
        if "keywords" in filters:
            keywords = filters["keywords"]
            if not alternative:
                orm_filters["nombre__icontains"] = keywords
            else:
                keywords_qobject = Q(nombre__icontains = keywords)
                
        qObjects.add(sources_qobject | keywords_qobject | polygon_qobject, Q.OR)
        
        if alternative:
            orm_filters.update({"alternative": qObjects})
            
        return orm_filters
    
    
    def apply_filters(self, request, applicable_filters):
        if 'alternative' in applicable_filters:
            alternative = applicable_filters.pop( 'alternative' )
        else:
            alternative = None
        default_filtered = super( SystemResource, self ).apply_filters( request, applicable_filters )
        filtered = default_filtered.filter( alternative ).distinct() if alternative else default_filtered
        print(alternative)
        return filtered

class ProjectResource(ModelResource):
    organizacion = fields.ToOneField(OrganizationResource, 'organizacion', full = True, null = True, blank = True)
    departamento = fields.ToOneField(DepartmentResource, 'departamento', full = True, null = True, blank = True)
    fuentes = fields.ToManyField(SourceResource, 'fuente', full = True, null = True, blank = True)
    latitud= fields.CharField(attribute='get_lat')
    longitud= fields.CharField(attribute='get_lan')
    popupimage = fields.CharField(attribute='popupimage')
    get_absolute_url = fields.CharField(attribute='get_absolute_url')

    class Meta:
        queryset = Proyecto.objects.all()
        resource_name = 'project'
        filtering = {
            "id": ALL,
            "nombre": ALL,
            "fuentes": ALL_WITH_RELATIONS,
        }

    def build_filters(self, filters=None):
        if filters is None:
            filters = {}
        orm_filters = super(ProjectResource, self).build_filters(filters)
        
        if "alternative" in filters:
            alternative= True
        else:
            alternative = False
        
        qObjects = Q()
        
        polygon_qobject= Q()
        if "poly" in filters:
            polygon= filters['poly']
            if polygon is not None:
                geo_query= """
                select id from (
                    select 
                        organizacion_id as id, 
                        latitud, 
                        longitud, 
                        ST_PointFromText( 'POINT('||longitud||' '||latitud||')', 4326)::Geometry as punto 
                    from organizaciones_proyecto
                ) as geometria 
                where ST_Within(punto,ST_Transform(ST_GeomFromText('%s',900913),4326))
                """ % polygon
                sysinside= customquery_fetchassoc(geo_query)
                syspk_list= [sys['id'] for sys in sysinside]
                polygon_qobject= Q(pk__in= syspk_list)
        
        sources_qobject = Q() 
        if "src" in filters:
            src = filters['src']
            sources = src.split(",")
            if not alternative:
                orm_filters["fuente__id__in"] = [int(i) for i in sources]
            else:
                sources_qobject = Q(fuente__id__in=sources)
        
        keywords_qobject = Q()
        if "keywords" in filters:
            keywords = filters["keywords"]
            if not alternative:
                orm_filters["nombre__icontains"] = keywords
            else:
                keywords_qobject = Q(nombre__icontains = keywords)

        qObjects.add(sources_qobject | keywords_qobject | polygon_qobject, Q.OR)
        
        if alternative:
            orm_filters.update({"alternative": qObjects})
        
        return orm_filters
    
    def apply_filters(self, request, applicable_filters):
        if 'alternative' in applicable_filters:
            alternative = applicable_filters.pop( 'alternative' )
        else:
            alternative = None
        default_filtered = super( ProjectResource, self ).apply_filters( request, applicable_filters )
        filtered = default_filtered.filter( alternative ).distinct() if alternative else default_filtered
        print(alternative)
        return filtered

class ExternalKMLResource(ModelResource):
    def build_filters(self, filters=None):
        if filters is None:
            filters = {}
        orm_filters = super(ExternalKMLResource, self).build_filters(filters)
        if "lyrs" in filters:
            fuentes= filters['lyrs']
            sqs = fuentes.split(",")
            orm_filters["id__in"] = [int(i) for i in sqs]
        return orm_filters

    class Meta:
        queryset = KMLexternos.objects.all()
        resource_name = 'externalkml'
        filtering = {
            "id": ALL,
            "nombre": ALL,
        }
