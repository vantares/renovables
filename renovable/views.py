# -*- coding: UTF-8 -*-
from django.template import RequestContext
from django.http import HttpResponse
from django.shortcuts import render_to_response, get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import *
from django.db.models import Q
from django.views.generic import ListView, DetailView, TemplateView
from nomencladores.models import *
from indicadores.models import IndicadoresSectoriales,Indicador,ValorIndicador
from organizaciones.models import Organizacion,SistemaEjecucion,Proyecto,FuenteRenovable,Contacto
from renovable.models import KMLexternos
import csv,codecs,cStringIO
from renovable.forms import GmapForm 
from renovable.utils import customquery_fetchassoc


def home(request):
    indicadores= IndicadoresSectoriales.objects.all().order_by('-anho')[:10]
    fuentes=FuenteRenovable.objects.all().order_by('-id')
    potencia={}
    for fuente in fuentes:
        potenciafuente=0
        sistemaspotencia=SistemaEjecucion.objects.filter(fuente=fuente)
        for sispotencia in sistemaspotencia:
            potenciafuente+= float(sispotencia.potencia) if sispotencia.potencia != "" else 0
        potencia[fuente.id]= float(potenciafuente / 1000000) if potenciafuente != "" else 0
    kmls=KMLexternos.objects.all()
    organizaciones= Organizacion.objects.filter(longitud__isnull=False,latitud__isnull=False)

    sistemas = False
    proyectos = False
# Descomentar estas lineas para mostrar inicialmente los sistemas y proyectos    
#    sistemas=SistemaEjecucion.objects.filter(longitud__isnull=False,latitud__isnull=False)
#    proyectos=Proyecto.objects.filter(Q(longitud__isnull=False,latitud__isnull=False) | Q(poligono__isnull=False))

    anhoselect=IndicadoresSectoriales.objects.filter(indicador__isnull=False).distinct().latest('anho')
    indicador=anhoselect.indicador.all()
    indicador=indicador[0]
    voloresindicador=ValorIndicador.objects.filter(indicador=indicador).order_by('-anho')[:10]
    objsindi = sorted(indicadores, key=lambda o: o.anho)

    objsvalores = sorted(voloresindicador, key=lambda o: o.id)

    return render_to_response("index.html",{'indicadores':objsindi,
                                            'potencia':potencia,
                                            'anhoselect':anhoselect,
                                            'voloresindicador':objsvalores,
                                            'fuentes':fuentes,
                                            'sistemas':sistemas,
                                            'proyectos':proyectos,
                                            'kmls':kmls,
                                            'organizaciones':organizaciones,
                                            'indicador':indicador},
                                            context_instance=RequestContext(request))

@csrf_exempt
def cargar_indicador(request):
    prod_id = request.POST['prod_id']
    indicador = Indicador.objects.get(id=prod_id)
    voloresindicador=ValorIndicador.objects.filter(indicador=indicador).order_by('-anho')[:10]
    objsvalores = sorted(voloresindicador, key=lambda o: o.id)
    return render_to_response('indicador.html',{'indicador':indicador,'voloresindicador':objsvalores}, context_instance=RequestContext(request))

def cargar_fuentesrenovables(request):
    fuentes=FuenteRenovable.objects.all().order_by('id')
    return render_to_response('fuentesrenovables.html',{'fuentes':fuentes}, context_instance=RequestContext(request))

def cargar_kmlexternos(request):
    kml=KMLexternos.objects.all().order_by('id')
    return render_to_response('kmlexternos.html',{'kmls':kml}, context_instance=RequestContext(request))


@csrf_exempt
def buscar(request):
    organizaciones_Post = request.POST['organizaciones']
    proyectos_Post = request.POST['proyectos']
    sistemas_Post = request.POST['sistemas']
    arrayfuentes=request.POST['arrayfuentes']
    q_object_source = Q()
    q_object_position = Q()
    if arrayfuentes !='0':
        fuentes=arrayfuentes.split(',')
        for fuente in fuentes:
            q_object_source.add(Q(fuente=int(fuente)),Q.OR)
    q_object_position.add(Q(longitud__isnull=False),Q.AND)
    q_object_position.add(Q(latitud__isnull=False),Q.AND)
    if organizaciones_Post=='1':
        organizaciones= Organizacion.objects.filter(q_object_position & q_object_source)
        if organizaciones.count()==0:
            organizaciones=False
    else:
        organizaciones=False

    if proyectos_Post=='1':
        q_object_position_2 = Q(poligono__isnull=False)
        proyectos= Proyecto.objects.filter((q_object_position | q_object_position_2) & q_object_source)
        if proyectos.count()==0:
            proyectos=False
    else:
        proyectos=False
    if sistemas_Post=='1':
        sistemas= SistemaEjecucion.objects.filter(q_object_position & q_object_source)
        if sistemas.count()==0:
            sistemas=False
    else:
        sistemas=False

    arrkml = request.POST['arrkml']
    q_objectkml = Q()
    if arrkml !='0':
        kmls=arrkml.split(',')
        for kml in kmls:
            q_objectkml.add(Q(id=int(kml)),Q.OR)
        kml= KMLexternos.objects.filter(q_objectkml)
    else:
        kml=False
    return render_to_response('mapa.html',{
                                        'sistemas':sistemas,
                                        'organizaciones':organizaciones,
                                        'proyectos':proyectos,
                                        'kml':kml
                                        },
                                        context_instance=RequestContext(request))


@csrf_exempt
def loadkml(request):
    arrkml = request.POST['arrkml']
    q_object = Q()
    if arrkml !='0':
        kmls=arrkml.split(',')
        for kml in kmls:
            q_object.add(Q(id=int(kml)),Q.OR)
    kml= KMLexternos.objects.filter(q_object)
    return render_to_response('mapa.html',{
        'kml':kml
    },
        context_instance=RequestContext(request))


@login_required
def buscador(request):
    area_search= GmapForm(request.POST)
    tipoentidad=TipoEntidad.objects.all()
    fuenteRenovable=FuenteRenovable.objects.all()
    organizaciones = Organizacion.objects.all();
    actividades = Actividad.objects.all()
    materiaPrima=MateriaPrima.objects.all()
    vegetal=Vegetal.objects.all()
    resultadosorganizacion=Organizacion.objects.all()
    return render_to_response('buscador.html',{
        'tipoentidad':tipoentidad,
        'fuenteRenovable':fuenteRenovable,
        'actividades':actividades,
        'resultadosorganizacion':resultadosorganizacion,
        'form':area_search,
        'fuentes':fuenteRenovable,
        'organizaciones': organizaciones,
        'materiaPrima': materiaPrima,
        },
        context_instance=RequestContext(request))

@csrf_exempt
def filtrarorganizaciones(request):
    area_search= GmapForm(request.POST)
    #agregando poly
    q_objectPolygon = Q()
    geo_query= ""
    if area_search.is_valid():
        poligono = area_search.cleaned_data['poly']
        if poligono is not None:
            # coordenadas contenidas en el area de busqueda
            geo_query= """
            select id 
            from (
                select 
                    id, 
                    latitud, 
                    longitud, 
                    ST_PointFromText( 'POINT('||longitud||' '||latitud||')', 4326)::Geometry as punto 
                from organizaciones_organizacion
                where latitud is not null and longitud is not null
            ) as geometria_organizacion 
            where ST_Within(punto,ST_Transform(ST_GeomFromText('%s',900913),4326))
            """ % poligono
            # centroide del departamento dentro del area de busqueda
            geo_query_2= """
            select id 
            from (
                select 
                s.id, 
                d.latitud,                                                                          
                d.longitud,                                                                           
                ST_PointFromText( 'POINT('||d.longitud||' '||d.latitud||')', 4326)::Geometry as punto 
            from organizaciones_organizacion s left join nomencladores_depto d on s.departamento_id = d.id 
            where s.latitud is null and s.longitud is null and s.departamento_id is not null
            ) as geometria_organizacion 
            where ST_Within(punto,ST_Transform(ST_GeomFromText('%s',900913),4326))
            """ % poligono
            sysinside= customquery_fetchassoc(geo_query)
            sysdeptoinside= customquery_fetchassoc(geo_query_2)
            syspk_list= [sys['id'] for sys in sysinside]
            syspk_list_2= [sys['id'] for sys in sysdeptoinside]
            syspk_list.extend(x for x in syspk_list_2 if x not in syspk_list)
            q_objectPolygon.add(Q(id__in= syspk_list),Q.OR)

    #datos iguales
    q_objectClaves = Q()
    organizaciones=Organizacion.objects.all()
    if 'arrclaves' in request.POST and request.POST['arrclaves']!="Buscar":
        claves=request.POST['arrclaves']
        q_objectClaves.add(Q(nombre__icontains=claves),Q.OR)

    q_objectFuente = Q()
    if 'arrFuente' in request.POST and request.POST['arrFuente']!="0":
        list= request.POST['arrFuente']
        lista=list.split(',')
        for id in lista:
            q_objectFuente.add(Q(fuente=int(id)),Q.OR)

    q_objectTipoentidad = Q()
    if 'arrTipoentidad' in request.POST and request.POST['arrTipoentidad']!="0":
        list= request.POST['arrTipoentidad']
        lista=list.split(',')
        for id in lista:
            q_objectTipoentidad.add(Q(tipo=int(id)),Q.OR)

    q_objectActividad = Q()
    if 'arrActividad' in request.POST and request.POST['arrActividad']!="0":
        list= request.POST['arrActividad']
        lista=list.split(',')
        for id in lista:
            q_objectActividad.add(Q(actividad=int(id)),Q.OR)

    organizaciones=organizaciones.filter(q_objectFuente & q_objectActividad & q_objectTipoentidad & q_objectClaves & q_objectPolygon).distinct('nombre')
    return render_to_response("resultadosorganizaciones.html",{ 'resultadosorganizacion':organizaciones, 'form':area_search},
                            context_instance=RequestContext(request))

    
@csrf_exempt
def filtrarsistemas(request):
    area_search= GmapForm(request.POST)
    q_objectPolygon = Q()
    geo_query= ""
    if area_search.is_valid():
        poligono = area_search.cleaned_data['poly']
        if poligono is not None:
            # coordenadas contenidas en el area de busqueda
            geo_query= """
            select id 
            from (
                select 
                    id, 
                    latitud, 
                    longitud, 
                    ST_PointFromText( 'POINT('||longitud||' '||latitud||')', 4326)::Geometry as punto 
                from organizaciones_sistemaejecucion 
                where latitud is not null and longitud is not null
            ) as geometria_sistemas 
            where ST_Within(punto,ST_Transform(ST_GeomFromText('%s',900913),4326))
            """ % poligono
            # centroide del departamento dentro del area de busqueda
            geo_query_2= """
            select id 
            from (
                select 
                s.id, 
                d.latitud,                                                                          
                d.longitud,                                                                           
                ST_PointFromText( 'POINT('||d.longitud||' '||d.latitud||')', 4326)::Geometry as punto 
            from organizaciones_sistemaejecucion s left join nomencladores_depto d on s.departamento_id = d.id 
            where s.latitud is null and s.longitud is null and s.departamento_id is not null
            ) as geometria_sistemas 
            where ST_Within(punto,ST_Transform(ST_GeomFromText('%s',900913),4326))
            """ % poligono
            sysinside= customquery_fetchassoc(geo_query)
            sysdeptoinside= customquery_fetchassoc(geo_query_2)
            syspk_list= [sys['id'] for sys in sysinside]
            syspk_list_2= [sys['id'] for sys in sysdeptoinside]
            syspk_list.extend(x for x in syspk_list_2 if x not in syspk_list)
            q_objectPolygon.add(Q(id__in= syspk_list),Q.OR)
        
    sistemas=SistemaEjecucion.objects.all()
    q_objectOrganizaciones = Q()
    if 'fltr_org' in  request.POST and request.POST['fltr_org'] !="":
        organizaciones= request.POST['fltr_org'].split(',')
        idorg= [int(org) for org in organizaciones]
        q_objectOrganizaciones.add(Q(organizacion__in=idorg),Q.OR)

    q_objectClaves = Q()
    if 'claves' in request.POST and request.POST['claves']!="Buscar":
        claves=request.POST['claves']
        q_objectClaves.add(Q(nombre__icontains=claves),Q.OR)

    q_objectFuente = Q()
    if 'arrFuente' in request.POST and request.POST['arrFuente']!="0":
        list= request.POST['arrFuente']
        lista=list.split(',')
        for id in lista:
            q_objectFuente.add(Q(fuente=int(id)),Q.OR)

    q_objectVegetal = Q()
    if 'arrVegetal' in request.POST and request.POST['arrVegetal']!="0":
        list= request.POST['arrVegetal']
        lista=list.split(',')
        for id in lista:
            q_objectVegetal.add(Q(vegetal=int(id)),Q.OR)

    q_objectMateria = Q()
    if 'arrMateria' in request.POST and request.POST['arrMateria']!="0":
        list= request.POST['arrMateria']
        lista=list.split(',')
        for id in lista:
            q_objectMateria.add(Q(materia_prima=int(id)),Q.OR)

    sistemas=sistemas.filter(q_objectFuente & q_objectMateria & q_objectVegetal & q_objectClaves & q_objectOrganizaciones & q_objectPolygon).distinct('nombre')
    return render_to_response("resultadossistemas.html",{ 'resultadossistemas':sistemas, 'form':area_search },
        context_instance=RequestContext(request))

@csrf_exempt
def filtrarproyecto(request):
    area_search= GmapForm(request.POST)
    q_objectPolygon = Q()
    geo_query= ""    

    if area_search.is_valid():
        poligono = area_search.cleaned_data['poly']
        
        if poligono is not None:
            # coordenadas contenidas en area de busqueda
            geo_query= """
            select id from (
                select 
                    id, 
                    latitud, 
                    longitud, 
                    ST_PointFromText( 'POINT('||longitud||' '||latitud||')', 4326)::Geometry as punto 
                from organizaciones_proyecto
            ) as geometria 
            where ST_Within(punto,ST_Transform(ST_GeomFromText('%s',900913),4326))
            """ % poligono
            
            
            # centro del departamento contenido en el area de busqueda
            geo_query_2= """
            select id from (
                select 
                    p.id, 
                    d.latitud, 
                    d.longitud, 
                    ST_PointFromText( 'POINT('||d.longitud||' '||d.latitud||')', 4326)::Geometry as punto 
                from organizaciones_proyecto p left join nomencladores_depto d on p.departamento_id = d.id 
                where p.latitud is null and p.longitud is null and p.departamento_id is not null
            ) as geometria 
            where ST_Within(punto,ST_Transform(ST_GeomFromText('%s',900913),4326))
            """ % poligono
            # interseccion de areas poligonales
            geo_query_3= """
            select p.id 
            from organizaciones_proyecto p
            where ST_Intersects(p.poligono,ST_Transform(ST_GeomFromText('%s',900913),4326))
            """ % poligono
            
            try:
                sysinside= customquery_fetchassoc(geo_query)            
                sysinside_2= customquery_fetchassoc(geo_query_2)
                sysinside_3= customquery_fetchassoc(geo_query_3)
                syspk_list= [sys['id'] for sys in sysinside]
                syspk_list_2= [sys['id'] for sys in sysinside_2]
                syspk_list_3= [sys['id'] for sys in sysinside_3]
                syspk_list.extend(x for x in syspk_list_2 if x not in syspk_list)
                syspk_list.extend(x for x in syspk_list_3 if x not in syspk_list)
                q_objectPolygon.add(Q(pk__in= syspk_list),Q.OR)
            except:
                pass

    proyectos=Proyecto.objects.all()
    q_objectClaves = Q()
    if 'claves' in request.POST and request.POST['claves']!="Buscar":
        claves=request.POST['claves']
        q_objectClaves.add(Q(nombre__icontains=claves),Q.OR)

    q_objectFuente = Q()
    if 'arrFuente' in request.POST and request.POST['arrFuente']!="0":
        list= request.POST['arrFuente']
        lista=list.split(',')
        for id in lista:
            q_objectFuente.add(Q(fuente=int(id)),Q.OR)
    if 'presupuesto' in request.POST and request.POST['presupuesto']!="-1":
        presupuesto=request.POST['presupuesto']
        if presupuesto =='1':
            proyectos=proyectos.filter(Q(monto__lt=1000)&Q(monto=1000))
        if presupuesto =='2':
            proyectos=proyectos.filter(Q(monto__lt=10000)&Q(monto__gt=1000)|Q(monto=1000))
        if presupuesto =='3':
            proyectos=proyectos.filter(Q(monto__lt=100000)&Q(monto__gt=10000)|Q(monto=10000))
        if presupuesto =='4':
            proyectos=proyectos.filter(Q(monto__lt=1000000)&Q(monto__gt=100000)|Q(monto=100000))
        if presupuesto =='5':
            proyectos=proyectos.filter(Q(monto__lt=10000000)&Q(monto__gt=1000000)|Q(monto=1000000))
        if presupuesto =='6':
            proyectos=proyectos.filter(Q(monto__gt=10000000)&Q(monto=10000000))


    proyectos=proyectos.filter(q_objectFuente & q_objectClaves & q_objectPolygon)
    return render_to_response("resultadosproyectos.html",{ 'resultadosproyectos':proyectos, 'form':area_search},
        context_instance=RequestContext(request))

@csrf_exempt
def filtrarcontacto(request):
    contactos=Contacto.objects.all()
    q_objectClaves = Q()
    if 'claves' in request.POST and request.POST['claves']!="Buscar":
        claves=request.POST['claves']
        q_objectClaves.add(Q(nombre__icontains=claves),Q.OR)

    q_objectOrganizacion = Q()
    if 'fltr_org' in  request.POST and request.POST['fltr_org'] !="":
        organizaciones= request.POST['fltr_org'].split(',')
        idorg= [int(org) for org in organizaciones]
        q_objectOrganizacion.add(Q(organizacion__in=idorg),Q.OR)

    q_objectFuente = Q()
    if 'arrFuente' in request.POST and request.POST['arrFuente']!="0":
        list= request.POST['arrFuente']
        lista=list.split(',')
        for id in lista:
            q_objectFuente.add(Q(organizacion__fuente=int(id)),Q.OR)

    contactos=contactos.filter(q_objectClaves & q_objectOrganizacion & q_objectFuente)

    contactos=contactos.filter(q_objectClaves)
    return render_to_response("resultadoscontacto.html",{ 'resultadoscontactos':contactos},
        context_instance=RequestContext(request))

@csrf_exempt
def busquedaorganizacion(request):
    fuenteRenovable=FuenteRenovable.objects.all()
    actividad=Actividad.objects.all()
    return render_to_response("busquedaorganizacion.html",{'fuenteRenovable':fuenteRenovable,'actividad':actividad},
        context_instance=RequestContext(request))

@csrf_exempt
def busquedaproyecto(request):
    fuenteRenovable=FuenteRenovable.objects.all()
    return render_to_response("busquedaproyecto.html",{'fuenteRenovable':fuenteRenovable},
        context_instance=RequestContext(request))

@csrf_exempt
def busquedasistemas(request):
    fuenteRenovable=FuenteRenovable.objects.all()
    materiaPrima=MateriaPrima.objects.all()
    vegetal=Vegetal.objects.all()
    organizaciones = Organizacion.objects.all()
    return render_to_response("busquedasistemas.html",{
        'organizaciones': organizaciones,
        'materiaPrima':materiaPrima,
        'fuenteRenovable':fuenteRenovable,
        'vegetal':vegetal
        },
        context_instance=RequestContext(request))

@csrf_exempt
def busquedacontacto(request):
    fuenteRenovable=FuenteRenovable.objects.all()
    organizaciones=Organizacion.objects.all()
    return render_to_response("busquedacontacto.html",{'organizaciones':organizaciones, 'fuenteRenovable':fuenteRenovable},
        context_instance=RequestContext(request))

@login_required
def exportarorganizaciones(request):
    # Create the HttpResponse object with the appropriate CSV header.
    q_objectClaves = Q()
    organizaciones=Organizacion.objects.all()
    if 'arrclaves' in request.POST and request.POST['arrclaves']!="Buscar":
        claves=request.POST['arrclaves']
        q_objectClaves.add(Q(nombre__icontains=claves),Q.OR)

    q_objectFuente = Q()
    if 'arrFuente' in request.POST and request.POST['arrFuente']!="0":
        list= request.POST['arrFuente']
        lista=list.split(',')
        for id in lista:
            q_objectFuente.add(Q(fuente=int(id)),Q.OR)

    q_objectTipoentidad = Q()
    if 'arrTipoentidad' in request.POST and request.POST['arrTipoentidad']!="0":
        list= request.POST['arrTipoentidad']
        lista=list.split(',')
        for id in lista:
            q_objectTipoentidad.add(Q(tipo=int(id)),Q.OR)

    q_objectActividad = Q()
    if 'arrActividad' in request.POST and request.POST['arrActividad']!="0":
        list= request.POST['arrActividad']
        lista=list.split(',')
        for id in lista:
            q_objectActividad.add(Q(actividad=int(id)),Q.OR)

    organizaciones=organizaciones.filter(q_objectFuente & q_objectActividad & q_objectTipoentidad & q_objectClaves).distinct('nombre')

    response = HttpResponse(mimetype='text/csv',content_type="charset=Windows-1252")
    if request.POST['tipo']=="1":
        response['Content-Disposition'] = 'attachment; filename=organizaciones.csv'
        writer = csv.writer(response)
        fila1=[u'Organización'.encode('Windows-1252'),
               u'Tipo'.encode('Windows-1252'),
               u'Fuentes Renovables'.encode('Windows-1252'),
               u'Actividad'.encode('Windows-1252'),
               u'Responsable'.encode('Windows-1252'),
               u'¿Es consesionario  en Nicaragua?'.encode('Windows-1252'),
               u'¿Es miembro  en Renovables?'.encode('Windows-1252'),
               u'¿Es miembro de asociación GREMIAL?'.encode('Windows-1252'),
               u'Otra asociacion'.encode('Windows-1252'),
               u'Teléfono'.encode('Windows-1252'),
               u'Web'.encode('Windows-1252'),
               u'Dirección'.encode('Windows-1252'),
               u'Pais'.encode('Windows-1252'),
               u'Latitud'.encode('Windows-1252'),
               u'Longitud'.encode('Windows-1252')]

        writer.writerow(fila1)
        for organizacion in organizaciones:
            fuentes=''
            actividades=''
            contactos=''
            for fuente in organizacion.fuente.all():
                fuentes+=fuente.nombre+','
            if organizacion.actividad.all():
                for actividad in organizacion.actividad.all():
                    actividades+=actividad.nombre+","
            if organizacion.gremial:
                gremial='Si'
            else:
                gremial='No'
            if organizacion.renovable:
                renovable='Si'
            else:
                renovable='No'
            if organizacion.nic:
                nic='Si'
            else:
                nic='No'
            if organizacion.tipo:
                tipo=organizacion.tipo.nombre
            else:
                tipo='-'

            if organizacion.asociacion:
                asociacion=organizacion.asociacion
            else:
                asociacion='-'

            if organizacion.telefono:
                telefono=organizacion.telefono
            else:
                telefono='-'
            if organizacion.web:
                web=organizacion.web
            else:
                web='-'
            if organizacion.direccion:
                direccion=organizacion.direccion
            else:
                direccion='-'
            if organizacion.pais:
                pais=organizacion.pais
            else:
                pais='-'
            if organizacion.get_responsable():
                responsable=organizacion.get_responsable()
                email=responsable.get_mail()
                responsable=responsable.contacto.nombre+', Cargo:'+responsable.contacto.cargo
                if email:
                    responsable+=', Email:'+email.email
            else:
                responsable='-'

            if organizacion.nombre:
                nombre=organizacion.nombre
            else:
                nombre='-'
                
            latitud=str(organizacion.latitud) if organizacion.latitud is not None else '-'                
            longitud=str(organizacion.longitud) if organizacion.longitud is not None else '-'
                            
            writer.writerow([nombre.encode('Windows-1252'),
                             tipo.encode('Windows-1252'),
                             fuentes.encode('Windows-1252'),
                             actividades.encode('Windows-1252'),
                             responsable.encode('Windows-1252'),
                             nic.encode('Windows-1252'),
                             renovable.encode('Windows-1252'),
                             gremial.encode('Windows-1252'),
                             asociacion.encode('Windows-1252'),
                             telefono.encode('Windows-1252'),
                             web.encode('Windows-1252'),
                             direccion.encode('Windows-1252'),
                             pais.encode('Windows-1252'),
                             latitud.encode('Windows-1252'),
                             longitud.encode('Windows-1252')
                             ])

        return response
    if request.POST['tipo']=="2":
        response['Content-Disposition'] = 'attachment; filename=email_organizaciones.csv'
        writer = csv.writer(response)
        fila1=[u'Email'.encode('Windows-1252')]
        writer.writerow(fila1)
        for organizacion in organizaciones:
            email=''
            email_list = []
            responsable = organizacion.get_responsable()
            contactos = organizacion.get_contactos()
            organizacion_mails = organizacion.get_email()
            if responsable:
                if responsable.contacto.get_mail():
                    email_list.append(str(responsable.contacto.get_mail()))
            if(organizacion_mails):
                for orgmail in organizacion_mails:
                    email_list.append(str(orgmail.email))
            if(contactos):
                for orgcontact in contactos:
                    contactmail= orgcontact.get_mail()
                    if(contactmail):
                        email_list.append(str(contactmail))
            email = ','.join(email_list)
            if(email.strip() != ""):
                writer.writerow([email.encode('Windows-1252')])
        return response

@login_required
def exportarproyecto(request):
    # Create the HttpResponse object with the appropriate CSV header.
    proyectos=Proyecto.objects.all().order_by('nombre')

    q_objectClaves = Q()
    if 'arrclaves' in request.POST and request.POST['arrclaves']!="Buscar":
        claves=request.POST['claves']
        q_objectClaves.add(Q(nombre__icontains=claves),Q.OR)

    q_objectFuente = Q()
    if 'arrFuente' in request.POST and request.POST['arrFuente']!="0":
        list= request.POST['arrFuente']
        lista=list.split(',')
        for id in lista:
            q_objectFuente.add(Q(fuente=int(id)),Q.OR)
    if 'arrpresupuesto' in request.POST and request.POST['arrpresupuesto']!="-1":
        presupuesto=request.POST['arrpresupuesto']
        if presupuesto =='1':
            proyectos=proyectos.filter(Q(monto__lt=1000)&Q(monto=1000))
        if presupuesto =='2':
            proyectos=proyectos.filter(Q(monto__lt=10000)&Q(monto__gt=1000)|Q(monto=1000))
        if presupuesto =='3':
            proyectos=proyectos.filter(Q(monto__lt=100000)&Q(monto__gt=10000)|Q(monto=10000))
        if presupuesto =='4':
            proyectos=proyectos.filter(Q(monto__lt=1000000)&Q(monto__gt=100000)|Q(monto=100000))
        if presupuesto =='5':
            proyectos=proyectos.filter(Q(monto__lt=10000000)&Q(monto__gt=1000000)|Q(monto=1000000))
        if presupuesto =='6':
            proyectos=proyectos.filter(Q(monto__gt=10000000)&Q(monto=10000000))

    proyectos=proyectos.filter(q_objectFuente & q_objectClaves)

    response = HttpResponse(mimetype='text/csv',content_type="charset=Windows-1252")
    if request.POST['tipo']=="1":
        response['Content-Disposition'] = 'attachment; filename=proyectos.csv'
        writer = csv.writer(response)
        fila1=[u'Nombre del Proyecto'.encode('Windows-1252'),
               u'Organización'.encode('Windows-1252'),
               u'Fuentes Renovables'.encode('Windows-1252'),
               u'Monto'.encode('Windows-1252'),
               u'Web'.encode('Windows-1252'),
               u'Donante'.encode('Windows-1252'),
               u'Beneficiarios'.encode('Windows-1252'),
               u'Latitud'.encode('Windows-1252'),
               u'Longitud'.encode('Windows-1252')]


        writer.writerow(fila1)
        for proyecto in proyectos:
            fuentes=''
            beneficiariotext=''
            for fuente in proyecto.fuente.all():
                fuentes+=fuente.nombre+","
            if proyecto.get_beneficiarios():
                beneficiarios=proyecto.get_beneficiarios()
                for beneficiario in beneficiarios:
                    beneficiariotext+=str(beneficiario.anho)+': '+str(beneficiario.numero)+' personas,  '
            else:
                beneficiariotext='-'

            if proyecto.nombre:
                nombre=proyecto.nombre
            else:
                nombre= '-'

            if proyecto.organizacion:
                organizacion=proyecto.organizacion.nombre
            else:
                organizacion= '-'

            if proyecto.monto:
                monto= str(proyecto.monto)
            else:
                monto= '-'

            if proyecto.enlace:
                enlace=proyecto.enlace
            else:
                enlace= '-'

            if proyecto.donante:
                donante=proyecto.donante
            else:
                donante= '-'
                
            latitud=str(proyecto.latitud) if proyecto.latitud is not None else '-'                
            longitud=str(proyecto.longitud) if proyecto.longitud is not None else '-'
            
            
            writer.writerow([nombre.encode('Windows-1252'),
                             organizacion.encode('Windows-1252'),
                             fuentes.encode('Windows-1252'),
                             monto.encode('Windows-1252'),
                             enlace.encode('Windows-1252'),
                             donante.encode('Windows-1252'),
                             beneficiariotext.encode('Windows-1252'),
                             latitud.encode('Windows-1252'),
                             longitud.encode('Windows-1252')
            ])

        return response
    if request.POST['tipo']=="2":
        response['Content-Disposition'] = 'attachment; filename=email_proyectos.csv'
        writer = csv.writer(response)
        fila1=[u'Email'.encode('Windows-1252')]
        writer.writerow(fila1)
        proyectos=proyectos.distinct('organizacion')
        for proyecto in proyectos:
            email=''
            responsable=proyecto.organizacion.get_responsable()
            if responsable:
                if responsable.contacto.get_mail():
                    email=str(responsable.contacto.get_mail())
            if(email.strip() != ""):
                writer.writerow([email.encode('Windows-1252')])
        return response

@login_required
def exportarsistemas(request):
    sistemas=SistemaEjecucion.objects.all().order_by('nombre')

    q_objectClaves = Q()
    if 'claves' in request.POST and request.POST['claves']!="Buscar":
        claves=request.POST['claves']
        q_objectClaves.add(Q(nombre__icontains=claves),Q.OR)

    q_objectFuente = Q()
    if 'arrFuente' in request.POST and request.POST['arrFuente']!="0":
        list= request.POST['arrFuente']
        lista=list.split(',')
        for id in lista:
            q_objectFuente.add(Q(fuente=int(id)),Q.OR)

    q_objectVegetal = Q()
    if 'arrVegetal' in request.POST and request.POST['arrVegetal']!="0":
        list= request.POST['arrVegetal']
        lista=list.split(',')
        for id in lista:
            q_objectVegetal.add(Q(vegetal=int(id)),Q.OR)

    q_objectMateria = Q()
    if 'arrMateria' in request.POST and request.POST['arrMateria']!="0":
        list= request.POST['arrMateria']
        lista=list.split(',')
        for id in lista:
            q_objectMateria.add(Q(materia_prima=int(id)),Q.OR)

    sistemas=sistemas.filter(q_objectFuente & q_objectMateria & q_objectVegetal & q_objectClaves).distinct('nombre')

    response = HttpResponse(mimetype='text/csv',content_type="charset=Windows-1252")
    if request.POST['tipo']=="1":
        response['Content-Disposition'] = 'attachment; filename=sistemas.csv'
        writer = csv.writer(response)
        fila1=[u'Identificador del sistema'.encode('Windows-1252'),
               u'Organización'.encode('Windows-1252'),
               u'Cantidad de sistema instalados'.encode('Windows-1252'),
               u'Fuentes Renovables'.encode('Windows-1252'),
               u'Vegetal que usa'.encode('Windows-1252'),
               u'Materia Prima'.encode('Windows-1252'),
               u'Interconexión a SIN'.encode('Windows-1252'),
               u'Produce Biocombustible'.encode('Windows-1252'),
               u'Energia producida anualmente en MWh'.encode('Windows-1252'),
               u'Tarifa promedio anual por MWh'.encode('Windows-1252'),
               u'Potencia en W'.encode('Windows-1252'),
               u'Produce Calor'.encode('Windows-1252'),
               u'Latitud'.encode('Windows-1252'),
               u'Longitud'.encode('Windows-1252')
               ]

        writer.writerow(fila1)
        for sistema in sistemas:
            fuentes=''
            for fuente in sistema.fuente.all():
                fuentes+=fuente.nombre+","
            if sistema.interconexion:
                interconexion= 'Si'
            else:
                interconexion= 'No'
            if sistema.calor:
                calor= 'Si'
            else:
                calor= 'No'
            if sistema.biocombustible:
                biocombustible= 'Si'
            else:
                biocombustible= 'No'
            if sistema.vegetal:
                vegetal= sistema.vegetal.nombre
            else:
                vegetal= '-'

            if sistema.nombre:
                nombre=sistema.nombre
            else:
                nombre= '-'

            if sistema.organizacion:
                organizacion=sistema.organizacion.nombre
            else:
                organizacion= '-'

            if sistema.cantidad_sistemas:
                instancias=str(sistema.cantidad_sistemas)
            else:
                instancias= '-'

            if sistema.materia_prima:
                materia=sistema.materia_prima.nombre
            else:
                materia= '-'

            if sistema.energia_producida:
                energia=str(sistema.energia_producida)
            else:
                energia= '-'

            if sistema.tarifa:
                tarifa=str(sistema.tarifa)
            else:
                tarifa= '-'

            if sistema.potencia:
                potencia=str(sistema.potencia)
            else:
                potencia= '-'

            latitud = '-'                
            if sistema.latitud is not None: 

                latitud=str(sistema.latitud) 
            elif sistema.departamento is not None:
                latitud = str(sistema.departamento.latitud)

            longitud = '-'                
            if sistema.longitud is not None: 
                longitud=str(sistema.longitud) 
            elif sistema.departamento is not None:
                longitud = str(sistema.departamento.longitud)

            writer.writerow([nombre.encode('Windows-1252'),
                             organizacion.encode('Windows-1252'),
                             instancias.encode('Windows-1252'),
                             fuentes.encode('Windows-1252'),
                             vegetal.encode('Windows-1252'),
                             materia.encode('Windows-1252'),
                             interconexion.encode('Windows-1252'),
                             biocombustible.encode('Windows-1252'),
                             energia.encode('Windows-1252'),
                             tarifa.encode('Windows-1252'),
                             potencia.encode('Windows-1252'),
                             calor.encode('Windows-1252'),
                             latitud.encode('Windows-1252'),
                             longitud.encode('Windows-1252')
            ])

        return response

    if request.POST['tipo']=="2":
        response['Content-Disposition'] = 'attachment; filename=email_sistemas.csv'
        writer = csv.writer(response)
        fila1=[u'Email'.encode('Windows-1252')]
        writer.writerow(fila1)
        sistemas=sistemas.distinct('organizacion')
        mail_list= []
        for sistema in sistemas:
            email=''
            responsable=sistema.organizacion.get_responsable()
            if responsable:
                if responsable.contacto.get_mail():
                    email= str(responsable.contacto.get_mail())
                    if(email.strip() != ""):
                        mail_list.append(email)
        distinct_emails = set(mail_list)
        for email in distinct_emails:
                writer.writerow([email.encode('Windows-1252')])
        return response

@login_required
def exportarcontactos(request):
    contactos=Contacto.objects.all().order_by('nombre')
    q_objectClaves = Q()
    if 'claves' in request.POST and request.POST['claves']!="Buscar":
        claves=request.POST['claves']
        q_objectClaves.add(Q(nombre__icontains=claves),Q.OR)


    q_objectOrganizacion = Q()
    if 'fltr_org' in  request.POST and request.POST['fltr_org'] !="":
        organizaciones= request.POST['fltr_org'].split(',')
        idorg= [int(org) for org in organizaciones]
        q_objectOrganizacion.add(Q(organizacion__in=idorg),Q.OR)

    contactos=contactos.filter(q_objectClaves & q_objectOrganizacion)

    response = HttpResponse(mimetype='text/csv',content_type="charset=Windows-1252")
    if request.POST['tipo']=="1":
        response['Content-Disposition'] = 'attachment; filename=contactos.csv'
        writer = csv.writer(response)
        fila1=[u'Nombre'.encode('Windows-1252'),
               u'Organización'.encode('Windows-1252'),
               u'Categoría'.encode('Windows-1252'),
               u'Cargo'.encode('Windows-1252'),
               u'Skype'.encode('Windows-1252'),
               u'email'.encode('Windows-1252'),
               u'Página Web'.encode('Windows-1252'),
               u'Dirección'.encode('Windows-1252'),
               u'Teléfonos'.encode('Windows-1252'),
               u'Área de Trabajo'.encode('Windows-1252'),
               u'País'.encode('Windows-1252')
        ]

        writer.writerow(fila1)
        for contacto in contactos:
            if contacto.nombre:
                nombre=contacto.nombre
            else:
                nombre= u''

            if contacto.get_mails():
                mails=",".join([str(mail.email) for mail in contacto.get_mails()])
            else:
                mails= u''

            if contacto.organizacion:
                organizacion=contacto.organizacion.nombre
            else:
                organizacion= u''

            if contacto.categoria:
                categoria=contacto.categoria.nombre
            else:
                categoria= u''

            if contacto.cargo:
                cargo=contacto.cargo
            else:
                cargo= u''

            if contacto.skype:
                skype=contacto.skype
            else:
                skype= u''

            if contacto.web:
                web=contacto.web
            else:
                web= u''

            if contacto.direccion:
                direccion=contacto.direccion
            else:
                direccion= u''

            if contacto.telefono:
                telefono=contacto.telefono
            else:
                telefono= u''

            if contacto.area:
                area=contacto.area
            else:
                area= u''

            if contacto.pais:
                pais = str(contacto.pais)
            else:
                pais= u''

            writer.writerow([nombre.encode('Windows-1252'),
                             organizacion.encode('Windows-1252'),
                             categoria.encode('Windows-1252'),
                             cargo.encode('Windows-1252'),
                             skype.encode('Windows-1252'),
                             mails.encode('Windows-1252'),
                             web.encode('Windows-1252'),
                             direccion.encode('Windows-1252'),
                             telefono.encode('Windows-1252'),
                             area.encode('Windows-1252'),
                             pais.encode('Windows-1252')
            ])

        return response
    if request.POST['tipo']=="2":
        response['Content-Disposition'] = 'attachment; filename=email_contactos.csv'
        writer = csv.writer(response)
        fila1=[u'Email'.encode('Windows-1252')]
        writer.writerow(fila1)
        contact_mail_list = []
        for contacto in contactos:
            if contacto.get_mails():
                mails=''
                mail_list= []
                for mail in contacto.get_mails():
                    mail_list.append(str(mail.email))
                mails= ','.join(mail_list)
                contact_mail_list.append(mails)
        for mails in set(contact_mail_list):
            if(mails.strip() != ""):
                writer.writerow([mails.encode('Windows-1252')])
        return response

@login_required
def detalleorganizacion(request,pk):
    organizacion=get_object_or_404(Organizacion,pk=pk)

    fuentes=FuenteRenovable.objects.all().order_by('id')
    kmls=KMLexternos.objects.all()
    return render_to_response("detalleorganizacion.html",{
                            'organizacion':organizacion,

                            'fuentes':fuentes,
                            'kmls':kmls},context_instance=RequestContext(request))


#@login_required
def detalleproyecto(request,pk):
    proyecto=get_object_or_404(Proyecto,pk=pk)
    return render_to_response("detalleproyecto.html",{'proyecto':proyecto},context_instance=RequestContext(request))

@login_required
def detallesistema(request,pk):
    sistema=get_object_or_404(SistemaEjecucion,pk=pk)
    return render_to_response("detallesistema.html",{'sistema':sistema},context_instance=RequestContext(request))

@login_required
def detallecontacto(request,pk):
    contacto=get_object_or_404(Contacto,pk=pk)
    return render_to_response("detallecontacto.html",{'contacto':contacto},context_instance=RequestContext(request))

class OrganizacionList(ListView):
    model = Organizacion
    title = 'Organizaciones'
    template_name = 'organizaciones_list.html'
    context_object_name = 'data'
