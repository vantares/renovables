from django.template import Library, Node,TemplateSyntaxError
from indicadores.models import ValorIndicador
from organizaciones.models import SistemaEjecucion
from django.utils.encoding import force_unicode
import re

register = Library()

@register.filter
def mod(value, arg):
    return value % arg

@register.filter
def in_list(value,arg):
    if arg:
        respuesta= str(value) in arg
    else:
        return False
    return respuesta

@register.filter
def intspace(value):

    orig = force_unicode(value)
    new = re.sub("^(-?\d+)(\d{3})", '\g<1> \g<2>', orig)
    if orig == new:
        return new
    else:
        return intspace(new)

class valueIndicador(Node):
    def __init__(self, anho,indicador):
        self.anho = anho
        self.indicador=indicador

    def render(self, context):
        context['valorIndicador'] = ValorIndicador.objects.get(anho=self.anho,indicador=self.indicador)
        return ''

@register.tag
def get_valueIndicador(parser, token):
    bits = token.contents.split()
    if len(bits) != 3:
        raise TemplateSyntaxError, "get_valueIndicador necesita dos argumentos"
    return valueIndicador(bits[1],bits[2])


