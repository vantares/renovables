from django.db import models
from django.contrib.flatpages.models import FlatPage

# Create your models here.
class KMLexternos(models.Model):
    nombre = models.CharField(max_length=200,verbose_name='Nombre')
    cargo = models.FileField(upload_to='kml')


    class Meta:
        verbose_name = "KML Externo"
        verbose_name_plural = "KML Externos"
        ordering = ('nombre',)
    def __unicode__(self):
        return unicode(self.nombre)

class ExtendedFlatPage(FlatPage):
    foto = models.ImageField( upload_to = 'pagina', blank = True , null = True)

    class Meta:
        verbose_name="Pagina"
        verbose_name_plural= "Paginas"
