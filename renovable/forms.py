import floppyforms as forms
from organizaciones.forms import GMapPolygonWidget

class SearchGmapPolyWidget(GMapPolygonWidget):
    map_width= 690
    map_height= 420
    template_name = 'floppyforms/gis/custom_gmap.html'
    #template_name = 'floppyforms/gis/google.html'

class GmapForm(forms.Form):
    poly = forms.gis.PolygonField(widget=SearchGmapPolyWidget, required=False )


class AdminGmapPolygonWidget(GMapPolygonWidget):
    map_srid = 3857
    display_wkt= False
    template_name = 'floppyforms/gis/custom_google.html'
