from renovable.models import KMLexternos, ExtendedFlatPage
from django.contrib import admin
from django.db import models

from django import forms
from django.contrib.flatpages.models import FlatPage
from django.contrib.flatpages.admin import FlatpageForm, FlatPageAdmin
from tinymce.widgets import TinyMCE


admin.site.register(KMLexternos)

class ExtendedFlatPageForm(FlatpageForm):
    class Meta:
        model = ExtendedFlatPage

class ExtendedFlatPageAdmin(FlatPageAdmin):
    form = ExtendedFlatPageForm
    formfield_overrides = {
        models.TextField:{'widget': TinyMCE(attrs={'cols': 30, 'rows': 30}) }
    }
    fieldsets = ((None, {'fields': ('url', 'title', 'content',  'foto', 'sites')}),)

admin.site.unregister( FlatPage )
admin.site.register(ExtendedFlatPage, ExtendedFlatPageAdmin)
