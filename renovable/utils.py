from django.db import connection, transaction

#=====================================================
# funciones reutilizadas
#=====================================================
def dictfetchall(cursor):
    "Returns all rows from a cursor as a dict"
    desc = cursor.description
    return [
        dict(zip([col[0] for col in desc], row))
        for row in cursor.fetchall()
    ]

def customquery(sql):
    cursor = connection.cursor()
    return cursor.execute(sql)
    
def customquery_fetchassoc(sql):
    cursor = connection.cursor()
    cursor.execute(sql)
    return dictfetchall(cursor)

