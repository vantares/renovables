--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- Name: plpgsql; Type: PROCEDURAL LANGUAGE; Schema: -; Owner: postgres
--

CREATE OR REPLACE PROCEDURAL LANGUAGE plpgsql;


ALTER PROCEDURAL LANGUAGE plpgsql OWNER TO postgres;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO renovable;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: renovable
--

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO renovable;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: renovable
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: renovable
--

SELECT pg_catalog.setval('auth_group_id_seq', 1, true);


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO renovable;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: renovable
--

CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO renovable;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: renovable
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: renovable
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 36, true);


--
-- Name: auth_message; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE auth_message (
    id integer NOT NULL,
    user_id integer NOT NULL,
    message text NOT NULL
);


ALTER TABLE public.auth_message OWNER TO renovable;

--
-- Name: auth_message_id_seq; Type: SEQUENCE; Schema: public; Owner: renovable
--

CREATE SEQUENCE auth_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_message_id_seq OWNER TO renovable;

--
-- Name: auth_message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: renovable
--

ALTER SEQUENCE auth_message_id_seq OWNED BY auth_message.id;


--
-- Name: auth_message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: renovable
--

SELECT pg_catalog.setval('auth_message_id_seq', 1, false);


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO renovable;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: renovable
--

CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO renovable;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: renovable
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: renovable
--

SELECT pg_catalog.setval('auth_permission_id_seq', 105, true);


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    username character varying(30) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(75) NOT NULL,
    password character varying(128) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    is_superuser boolean NOT NULL,
    last_login timestamp with time zone NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO renovable;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO renovable;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: renovable
--

CREATE SEQUENCE auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO renovable;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: renovable
--

ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: renovable
--

SELECT pg_catalog.setval('auth_user_groups_id_seq', 1, true);


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: renovable
--

CREATE SEQUENCE auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO renovable;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: renovable
--

ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: renovable
--

SELECT pg_catalog.setval('auth_user_id_seq', 5, true);


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO renovable;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: renovable
--

CREATE SEQUENCE auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO renovable;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: renovable
--

ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: renovable
--

SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 1, false);


--
-- Name: csvimport_csvimport; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE csvimport_csvimport (
    id integer NOT NULL,
    model_name character varying(255) NOT NULL,
    field_list character varying(255) NOT NULL,
    upload_file character varying(100) NOT NULL,
    file_name character varying(255) NOT NULL,
    encoding character varying(32) NOT NULL,
    upload_method character varying(50) NOT NULL,
    error_log text NOT NULL,
    import_date date NOT NULL,
    import_user character varying(255) NOT NULL
);


ALTER TABLE public.csvimport_csvimport OWNER TO renovable;

--
-- Name: csvimport_csvimport_id_seq; Type: SEQUENCE; Schema: public; Owner: renovable
--

CREATE SEQUENCE csvimport_csvimport_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.csvimport_csvimport_id_seq OWNER TO renovable;

--
-- Name: csvimport_csvimport_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: renovable
--

ALTER SEQUENCE csvimport_csvimport_id_seq OWNED BY csvimport_csvimport.id;


--
-- Name: csvimport_csvimport_id_seq; Type: SEQUENCE SET; Schema: public; Owner: renovable
--

SELECT pg_catalog.setval('csvimport_csvimport_id_seq', 19, true);


--
-- Name: csvimport_importmodel; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE csvimport_importmodel (
    id integer NOT NULL,
    csvimport_id integer NOT NULL,
    numeric_id integer NOT NULL,
    natural_key character varying(100) NOT NULL,
    CONSTRAINT csvimport_importmodel_numeric_id_check CHECK ((numeric_id >= 0))
);


ALTER TABLE public.csvimport_importmodel OWNER TO renovable;

--
-- Name: csvimport_importmodel_id_seq; Type: SEQUENCE; Schema: public; Owner: renovable
--

CREATE SEQUENCE csvimport_importmodel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.csvimport_importmodel_id_seq OWNER TO renovable;

--
-- Name: csvimport_importmodel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: renovable
--

ALTER SEQUENCE csvimport_importmodel_id_seq OWNED BY csvimport_importmodel.id;


--
-- Name: csvimport_importmodel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: renovable
--

SELECT pg_catalog.setval('csvimport_importmodel_id_seq', 1, false);


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    user_id integer NOT NULL,
    content_type_id integer,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO renovable;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: renovable
--

CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO renovable;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: renovable
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: renovable
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 472, true);


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO renovable;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: renovable
--

CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO renovable;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: renovable
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: renovable
--

SELECT pg_catalog.setval('django_content_type_id_seq', 35, true);


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO renovable;

--
-- Name: django_site; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE django_site (
    id integer NOT NULL,
    domain character varying(100) NOT NULL,
    name character varying(50) NOT NULL
);


ALTER TABLE public.django_site OWNER TO renovable;

--
-- Name: django_site_id_seq; Type: SEQUENCE; Schema: public; Owner: renovable
--

CREATE SEQUENCE django_site_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_site_id_seq OWNER TO renovable;

--
-- Name: django_site_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: renovable
--

ALTER SEQUENCE django_site_id_seq OWNED BY django_site.id;


--
-- Name: django_site_id_seq; Type: SEQUENCE SET; Schema: public; Owner: renovable
--

SELECT pg_catalog.setval('django_site_id_seq', 1, true);


--
-- Name: indicadores_indicador; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE indicadores_indicador (
    id integer NOT NULL,
    nombre character varying(200) NOT NULL,
    tipoindicador_id integer NOT NULL,
    nombrelargo character varying(500) NOT NULL,
    explicacion text,
    fuentes text,
    unidad character varying(500) NOT NULL,
    icono character varying(100)
);


ALTER TABLE public.indicadores_indicador OWNER TO renovable;

--
-- Name: indicadores_indicador_id_seq; Type: SEQUENCE; Schema: public; Owner: renovable
--

CREATE SEQUENCE indicadores_indicador_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.indicadores_indicador_id_seq OWNER TO renovable;

--
-- Name: indicadores_indicador_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: renovable
--

ALTER SEQUENCE indicadores_indicador_id_seq OWNED BY indicadores_indicador.id;


--
-- Name: indicadores_indicador_id_seq; Type: SEQUENCE SET; Schema: public; Owner: renovable
--

SELECT pg_catalog.setval('indicadores_indicador_id_seq', 9, true);


--
-- Name: indicadores_indicadoressectoriales; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE indicadores_indicadoressectoriales (
    anho integer NOT NULL
);


ALTER TABLE public.indicadores_indicadoressectoriales OWNER TO renovable;

--
-- Name: indicadores_tipoindicador; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE indicadores_tipoindicador (
    id integer NOT NULL,
    nombre character varying(200) NOT NULL
);


ALTER TABLE public.indicadores_tipoindicador OWNER TO renovable;

--
-- Name: indicadores_tipoindicador_id_seq; Type: SEQUENCE; Schema: public; Owner: renovable
--

CREATE SEQUENCE indicadores_tipoindicador_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.indicadores_tipoindicador_id_seq OWNER TO renovable;

--
-- Name: indicadores_tipoindicador_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: renovable
--

ALTER SEQUENCE indicadores_tipoindicador_id_seq OWNED BY indicadores_tipoindicador.id;


--
-- Name: indicadores_tipoindicador_id_seq; Type: SEQUENCE SET; Schema: public; Owner: renovable
--

SELECT pg_catalog.setval('indicadores_tipoindicador_id_seq', 5, true);


--
-- Name: indicadores_valorindicador; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE indicadores_valorindicador (
    id integer NOT NULL,
    anho_id integer NOT NULL,
    indicador_id integer NOT NULL,
    valor numeric(16,4)
);


ALTER TABLE public.indicadores_valorindicador OWNER TO renovable;

--
-- Name: indicadores_valorindicador_id_seq; Type: SEQUENCE; Schema: public; Owner: renovable
--

CREATE SEQUENCE indicadores_valorindicador_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.indicadores_valorindicador_id_seq OWNER TO renovable;

--
-- Name: indicadores_valorindicador_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: renovable
--

ALTER SEQUENCE indicadores_valorindicador_id_seq OWNED BY indicadores_valorindicador.id;


--
-- Name: indicadores_valorindicador_id_seq; Type: SEQUENCE SET; Schema: public; Owner: renovable
--

SELECT pg_catalog.setval('indicadores_valorindicador_id_seq', 108, true);


--
-- Name: nomencladores_actividad; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE nomencladores_actividad (
    id integer NOT NULL,
    nombre character varying(200) NOT NULL
);


ALTER TABLE public.nomencladores_actividad OWNER TO renovable;

--
-- Name: nomencladores_actividad_id_seq; Type: SEQUENCE; Schema: public; Owner: renovable
--

CREATE SEQUENCE nomencladores_actividad_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.nomencladores_actividad_id_seq OWNER TO renovable;

--
-- Name: nomencladores_actividad_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: renovable
--

ALTER SEQUENCE nomencladores_actividad_id_seq OWNED BY nomencladores_actividad.id;


--
-- Name: nomencladores_actividad_id_seq; Type: SEQUENCE SET; Schema: public; Owner: renovable
--

SELECT pg_catalog.setval('nomencladores_actividad_id_seq', 11, true);


--
-- Name: nomencladores_categoria; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE nomencladores_categoria (
    id integer NOT NULL,
    nombre character varying(200) NOT NULL
);


ALTER TABLE public.nomencladores_categoria OWNER TO renovable;

--
-- Name: nomencladores_categoria_id_seq; Type: SEQUENCE; Schema: public; Owner: renovable
--

CREATE SEQUENCE nomencladores_categoria_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.nomencladores_categoria_id_seq OWNER TO renovable;

--
-- Name: nomencladores_categoria_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: renovable
--

ALTER SEQUENCE nomencladores_categoria_id_seq OWNED BY nomencladores_categoria.id;


--
-- Name: nomencladores_categoria_id_seq; Type: SEQUENCE SET; Schema: public; Owner: renovable
--

SELECT pg_catalog.setval('nomencladores_categoria_id_seq', 1, false);


--
-- Name: nomencladores_fuenterenovable; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE nomencladores_fuenterenovable (
    id integer NOT NULL,
    nombre character varying(200) NOT NULL,
    imagen character varying(100)
);


ALTER TABLE public.nomencladores_fuenterenovable OWNER TO renovable;

--
-- Name: nomencladores_fuenterenovable_id_seq; Type: SEQUENCE; Schema: public; Owner: renovable
--

CREATE SEQUENCE nomencladores_fuenterenovable_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.nomencladores_fuenterenovable_id_seq OWNER TO renovable;

--
-- Name: nomencladores_fuenterenovable_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: renovable
--

ALTER SEQUENCE nomencladores_fuenterenovable_id_seq OWNED BY nomencladores_fuenterenovable.id;


--
-- Name: nomencladores_fuenterenovable_id_seq; Type: SEQUENCE SET; Schema: public; Owner: renovable
--

SELECT pg_catalog.setval('nomencladores_fuenterenovable_id_seq', 7, true);


--
-- Name: nomencladores_materiaprima; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE nomencladores_materiaprima (
    id integer NOT NULL,
    nombre character varying(200) NOT NULL
);


ALTER TABLE public.nomencladores_materiaprima OWNER TO renovable;

--
-- Name: nomencladores_materiaprima_id_seq; Type: SEQUENCE; Schema: public; Owner: renovable
--

CREATE SEQUENCE nomencladores_materiaprima_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.nomencladores_materiaprima_id_seq OWNER TO renovable;

--
-- Name: nomencladores_materiaprima_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: renovable
--

ALTER SEQUENCE nomencladores_materiaprima_id_seq OWNED BY nomencladores_materiaprima.id;


--
-- Name: nomencladores_materiaprima_id_seq; Type: SEQUENCE SET; Schema: public; Owner: renovable
--

SELECT pg_catalog.setval('nomencladores_materiaprima_id_seq', 3, true);


--
-- Name: nomencladores_pais; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE nomencladores_pais (
    id integer NOT NULL,
    nombre character varying(200) NOT NULL
);


ALTER TABLE public.nomencladores_pais OWNER TO renovable;

--
-- Name: nomencladores_pais_id_seq; Type: SEQUENCE; Schema: public; Owner: renovable
--

CREATE SEQUENCE nomencladores_pais_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.nomencladores_pais_id_seq OWNER TO renovable;

--
-- Name: nomencladores_pais_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: renovable
--

ALTER SEQUENCE nomencladores_pais_id_seq OWNED BY nomencladores_pais.id;


--
-- Name: nomencladores_pais_id_seq; Type: SEQUENCE SET; Schema: public; Owner: renovable
--

SELECT pg_catalog.setval('nomencladores_pais_id_seq', 1, true);


--
-- Name: nomencladores_tipoentidad; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE nomencladores_tipoentidad (
    id integer NOT NULL,
    nombre character varying(200) NOT NULL
);


ALTER TABLE public.nomencladores_tipoentidad OWNER TO renovable;

--
-- Name: nomencladores_tipoentidad_id_seq; Type: SEQUENCE; Schema: public; Owner: renovable
--

CREATE SEQUENCE nomencladores_tipoentidad_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.nomencladores_tipoentidad_id_seq OWNER TO renovable;

--
-- Name: nomencladores_tipoentidad_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: renovable
--

ALTER SEQUENCE nomencladores_tipoentidad_id_seq OWNED BY nomencladores_tipoentidad.id;


--
-- Name: nomencladores_tipoentidad_id_seq; Type: SEQUENCE SET; Schema: public; Owner: renovable
--

SELECT pg_catalog.setval('nomencladores_tipoentidad_id_seq', 4, true);


--
-- Name: nomencladores_vegetal; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE nomencladores_vegetal (
    id integer NOT NULL,
    nombre character varying(200) NOT NULL
);


ALTER TABLE public.nomencladores_vegetal OWNER TO renovable;

--
-- Name: nomencladores_vegetal_id_seq; Type: SEQUENCE; Schema: public; Owner: renovable
--

CREATE SEQUENCE nomencladores_vegetal_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.nomencladores_vegetal_id_seq OWNER TO renovable;

--
-- Name: nomencladores_vegetal_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: renovable
--

ALTER SEQUENCE nomencladores_vegetal_id_seq OWNED BY nomencladores_vegetal.id;


--
-- Name: nomencladores_vegetal_id_seq; Type: SEQUENCE SET; Schema: public; Owner: renovable
--

SELECT pg_catalog.setval('nomencladores_vegetal_id_seq', 1, false);


--
-- Name: organizaciones_beneficiarios; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE organizaciones_beneficiarios (
    id integer NOT NULL,
    organizacion_id integer NOT NULL,
    anho integer NOT NULL,
    numero integer NOT NULL
);


ALTER TABLE public.organizaciones_beneficiarios OWNER TO renovable;

--
-- Name: organizaciones_beneficiarios_id_seq; Type: SEQUENCE; Schema: public; Owner: renovable
--

CREATE SEQUENCE organizaciones_beneficiarios_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.organizaciones_beneficiarios_id_seq OWNER TO renovable;

--
-- Name: organizaciones_beneficiarios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: renovable
--

ALTER SEQUENCE organizaciones_beneficiarios_id_seq OWNED BY organizaciones_beneficiarios.id;


--
-- Name: organizaciones_beneficiarios_id_seq; Type: SEQUENCE SET; Schema: public; Owner: renovable
--

SELECT pg_catalog.setval('organizaciones_beneficiarios_id_seq', 1, false);


--
-- Name: organizaciones_beneficiariosproyecto; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE organizaciones_beneficiariosproyecto (
    id integer NOT NULL,
    proyecto_id integer NOT NULL,
    anho integer NOT NULL,
    numero integer NOT NULL
);


ALTER TABLE public.organizaciones_beneficiariosproyecto OWNER TO renovable;

--
-- Name: organizaciones_beneficiariosproyecto_id_seq; Type: SEQUENCE; Schema: public; Owner: renovable
--

CREATE SEQUENCE organizaciones_beneficiariosproyecto_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.organizaciones_beneficiariosproyecto_id_seq OWNER TO renovable;

--
-- Name: organizaciones_beneficiariosproyecto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: renovable
--

ALTER SEQUENCE organizaciones_beneficiariosproyecto_id_seq OWNED BY organizaciones_beneficiariosproyecto.id;


--
-- Name: organizaciones_beneficiariosproyecto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: renovable
--

SELECT pg_catalog.setval('organizaciones_beneficiariosproyecto_id_seq', 1, false);


--
-- Name: organizaciones_contacto; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE organizaciones_contacto (
    id integer NOT NULL,
    organizacion_id integer NOT NULL,
    categoria_id integer,
    nombre character varying(200) NOT NULL,
    cargo character varying(200) NOT NULL,
    skype character varying(200),
    web character varying(200),
    direccion character varying(200) NOT NULL,
    telefono character varying(300),
    area character varying(200) DEFAULT ''::character varying NOT NULL,
    pais_id integer DEFAULT 1 NOT NULL,
    correo character varying(80)
);


ALTER TABLE public.organizaciones_contacto OWNER TO renovable;

--
-- Name: organizaciones_contacto_id_seq; Type: SEQUENCE; Schema: public; Owner: renovable
--

CREATE SEQUENCE organizaciones_contacto_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.organizaciones_contacto_id_seq OWNER TO renovable;

--
-- Name: organizaciones_contacto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: renovable
--

ALTER SEQUENCE organizaciones_contacto_id_seq OWNED BY organizaciones_contacto.id;


--
-- Name: organizaciones_contacto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: renovable
--

SELECT pg_catalog.setval('organizaciones_contacto_id_seq', 279, true);


--
-- Name: organizaciones_email; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE organizaciones_email (
    id integer NOT NULL,
    contacto_id integer NOT NULL,
    email character varying(75) NOT NULL
);


ALTER TABLE public.organizaciones_email OWNER TO renovable;

--
-- Name: organizaciones_email_id_seq; Type: SEQUENCE; Schema: public; Owner: renovable
--

CREATE SEQUENCE organizaciones_email_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.organizaciones_email_id_seq OWNER TO renovable;

--
-- Name: organizaciones_email_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: renovable
--

ALTER SEQUENCE organizaciones_email_id_seq OWNED BY organizaciones_email.id;


--
-- Name: organizaciones_email_id_seq; Type: SEQUENCE SET; Schema: public; Owner: renovable
--

SELECT pg_catalog.setval('organizaciones_email_id_seq', 1, false);


--
-- Name: organizaciones_empleados; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE organizaciones_empleados (
    id integer NOT NULL,
    organizacion_id integer NOT NULL,
    anho integer NOT NULL,
    numero integer NOT NULL
);


ALTER TABLE public.organizaciones_empleados OWNER TO renovable;

--
-- Name: organizaciones_empleados_id_seq; Type: SEQUENCE; Schema: public; Owner: renovable
--

CREATE SEQUENCE organizaciones_empleados_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.organizaciones_empleados_id_seq OWNER TO renovable;

--
-- Name: organizaciones_empleados_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: renovable
--

ALTER SEQUENCE organizaciones_empleados_id_seq OWNED BY organizaciones_empleados.id;


--
-- Name: organizaciones_empleados_id_seq; Type: SEQUENCE SET; Schema: public; Owner: renovable
--

SELECT pg_catalog.setval('organizaciones_empleados_id_seq', 1, false);


--
-- Name: organizaciones_galeriafotosproyecto; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE organizaciones_galeriafotosproyecto (
    id integer NOT NULL,
    proyecto_id integer NOT NULL,
    imagen character varying(100) NOT NULL
);


ALTER TABLE public.organizaciones_galeriafotosproyecto OWNER TO renovable;

--
-- Name: organizaciones_galeriafotosproyecto_id_seq; Type: SEQUENCE; Schema: public; Owner: renovable
--

CREATE SEQUENCE organizaciones_galeriafotosproyecto_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.organizaciones_galeriafotosproyecto_id_seq OWNER TO renovable;

--
-- Name: organizaciones_galeriafotosproyecto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: renovable
--

ALTER SEQUENCE organizaciones_galeriafotosproyecto_id_seq OWNED BY organizaciones_galeriafotosproyecto.id;


--
-- Name: organizaciones_galeriafotosproyecto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: renovable
--

SELECT pg_catalog.setval('organizaciones_galeriafotosproyecto_id_seq', 1, false);


--
-- Name: organizaciones_galeriafotossistema; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE organizaciones_galeriafotossistema (
    id integer NOT NULL,
    sistema_id integer NOT NULL,
    imagen character varying(100) NOT NULL
);


ALTER TABLE public.organizaciones_galeriafotossistema OWNER TO renovable;

--
-- Name: organizaciones_galeriafotossistema_id_seq; Type: SEQUENCE; Schema: public; Owner: renovable
--

CREATE SEQUENCE organizaciones_galeriafotossistema_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.organizaciones_galeriafotossistema_id_seq OWNER TO renovable;

--
-- Name: organizaciones_galeriafotossistema_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: renovable
--

ALTER SEQUENCE organizaciones_galeriafotossistema_id_seq OWNED BY organizaciones_galeriafotossistema.id;


--
-- Name: organizaciones_galeriafotossistema_id_seq; Type: SEQUENCE SET; Schema: public; Owner: renovable
--

SELECT pg_catalog.setval('organizaciones_galeriafotossistema_id_seq', 1, false);


--
-- Name: organizaciones_organizacion; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE organizaciones_organizacion (
    id integer NOT NULL,
    tipo_id integer,
    nombre character varying(400) NOT NULL,
    nic boolean DEFAULT false NOT NULL,
    gremial boolean DEFAULT false NOT NULL,
    longitud numeric(15,10),
    latitud numeric(15,10),
    logo character varying(100),
    renovable boolean NOT NULL,
    asociacion character varying(400),
    telefono character varying(400),
    web character varying(200),
    direccion character varying(400),
    pais character varying(400),
    user_id_id integer NOT NULL,
    geolocation character varying(100)
);


ALTER TABLE public.organizaciones_organizacion OWNER TO renovable;

--
-- Name: organizaciones_organizacion_actividad; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE organizaciones_organizacion_actividad (
    id integer NOT NULL,
    organizacion_id integer NOT NULL,
    actividad_id integer NOT NULL
);


ALTER TABLE public.organizaciones_organizacion_actividad OWNER TO renovable;

--
-- Name: organizaciones_organizacion_actividad_id_seq; Type: SEQUENCE; Schema: public; Owner: renovable
--

CREATE SEQUENCE organizaciones_organizacion_actividad_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.organizaciones_organizacion_actividad_id_seq OWNER TO renovable;

--
-- Name: organizaciones_organizacion_actividad_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: renovable
--

ALTER SEQUENCE organizaciones_organizacion_actividad_id_seq OWNED BY organizaciones_organizacion_actividad.id;


--
-- Name: organizaciones_organizacion_actividad_id_seq; Type: SEQUENCE SET; Schema: public; Owner: renovable
--

SELECT pg_catalog.setval('organizaciones_organizacion_actividad_id_seq', 13, true);


--
-- Name: organizaciones_organizacion_fuente; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE organizaciones_organizacion_fuente (
    id integer NOT NULL,
    organizacion_id integer NOT NULL,
    fuenterenovable_id integer NOT NULL
);


ALTER TABLE public.organizaciones_organizacion_fuente OWNER TO renovable;

--
-- Name: organizaciones_organizacion_fuente_id_seq; Type: SEQUENCE; Schema: public; Owner: renovable
--

CREATE SEQUENCE organizaciones_organizacion_fuente_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.organizaciones_organizacion_fuente_id_seq OWNER TO renovable;

--
-- Name: organizaciones_organizacion_fuente_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: renovable
--

ALTER SEQUENCE organizaciones_organizacion_fuente_id_seq OWNED BY organizaciones_organizacion_fuente.id;


--
-- Name: organizaciones_organizacion_fuente_id_seq; Type: SEQUENCE SET; Schema: public; Owner: renovable
--

SELECT pg_catalog.setval('organizaciones_organizacion_fuente_id_seq', 264, true);


--
-- Name: organizaciones_organizacion_id_seq; Type: SEQUENCE; Schema: public; Owner: renovable
--

CREATE SEQUENCE organizaciones_organizacion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.organizaciones_organizacion_id_seq OWNER TO renovable;

--
-- Name: organizaciones_organizacion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: renovable
--

ALTER SEQUENCE organizaciones_organizacion_id_seq OWNED BY organizaciones_organizacion.id;


--
-- Name: organizaciones_organizacion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: renovable
--

SELECT pg_catalog.setval('organizaciones_organizacion_id_seq', 82, true);


--
-- Name: organizaciones_proyecto; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE organizaciones_proyecto (
    nombre character varying(200) NOT NULL,
    organizacion_id integer NOT NULL,
    monto integer NOT NULL,
    longitud numeric(15,10),
    latitud numeric(15,10),
    enlace character varying(200),
    donante character varying(400),
    geolocation character varying(100)
);


ALTER TABLE public.organizaciones_proyecto OWNER TO renovable;

--
-- Name: organizaciones_proyecto_fuente; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE organizaciones_proyecto_fuente (
    id integer NOT NULL,
    proyecto_id integer NOT NULL,
    fuenterenovable_id integer NOT NULL
);


ALTER TABLE public.organizaciones_proyecto_fuente OWNER TO renovable;

--
-- Name: organizaciones_proyecto_fuente_id_seq; Type: SEQUENCE; Schema: public; Owner: renovable
--

CREATE SEQUENCE organizaciones_proyecto_fuente_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.organizaciones_proyecto_fuente_id_seq OWNER TO renovable;

--
-- Name: organizaciones_proyecto_fuente_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: renovable
--

ALTER SEQUENCE organizaciones_proyecto_fuente_id_seq OWNED BY organizaciones_proyecto_fuente.id;


--
-- Name: organizaciones_proyecto_fuente_id_seq; Type: SEQUENCE SET; Schema: public; Owner: renovable
--

SELECT pg_catalog.setval('organizaciones_proyecto_fuente_id_seq', 1, false);


--
-- Name: organizaciones_responsable; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE organizaciones_responsable (
    organizacion_id integer NOT NULL,
    contacto_id integer NOT NULL
);


ALTER TABLE public.organizaciones_responsable OWNER TO renovable;

--
-- Name: organizaciones_sistemaejecucion; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE organizaciones_sistemaejecucion (
    id integer NOT NULL,
    nombre character varying(200) NOT NULL,
    instancias integer NOT NULL,
    vegetal_id integer,
    materia_prima_id integer,
    biocombustible boolean DEFAULT false NOT NULL,
    longitud numeric(15,10),
    latitud numeric(15,10),
    potencia character varying(200),
    interconexion boolean DEFAULT false NOT NULL,
    energia_producida integer NOT NULL,
    tarifa integer NOT NULL,
    calor boolean DEFAULT false NOT NULL,
    organizacion_id integer,
    tmp integer,
    geolocation character varying(100)
);


ALTER TABLE public.organizaciones_sistemaejecucion OWNER TO renovable;

--
-- Name: organizaciones_sistemaejecucion_fuente; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE organizaciones_sistemaejecucion_fuente (
    id integer NOT NULL,
    sistemaejecucion_id integer NOT NULL,
    fuenterenovable_id integer NOT NULL
);


ALTER TABLE public.organizaciones_sistemaejecucion_fuente OWNER TO renovable;

--
-- Name: organizaciones_sistemaejecucion_fuente_id_seq; Type: SEQUENCE; Schema: public; Owner: renovable
--

CREATE SEQUENCE organizaciones_sistemaejecucion_fuente_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.organizaciones_sistemaejecucion_fuente_id_seq OWNER TO renovable;

--
-- Name: organizaciones_sistemaejecucion_fuente_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: renovable
--

ALTER SEQUENCE organizaciones_sistemaejecucion_fuente_id_seq OWNED BY organizaciones_sistemaejecucion_fuente.id;


--
-- Name: organizaciones_sistemaejecucion_fuente_id_seq; Type: SEQUENCE SET; Schema: public; Owner: renovable
--

SELECT pg_catalog.setval('organizaciones_sistemaejecucion_fuente_id_seq', 814, true);


--
-- Name: organizaciones_sistemaejecucion_id_seq; Type: SEQUENCE; Schema: public; Owner: renovable
--

CREATE SEQUENCE organizaciones_sistemaejecucion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.organizaciones_sistemaejecucion_id_seq OWNER TO renovable;

--
-- Name: organizaciones_sistemaejecucion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: renovable
--

ALTER SEQUENCE organizaciones_sistemaejecucion_id_seq OWNED BY organizaciones_sistemaejecucion.id;


--
-- Name: organizaciones_sistemaejecucion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: renovable
--

SELECT pg_catalog.setval('organizaciones_sistemaejecucion_id_seq', 1123, true);


--
-- Name: organizaciones_usuariosbeneficiarios; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE organizaciones_usuariosbeneficiarios (
    id integer NOT NULL,
    sistema_id integer NOT NULL,
    anho integer NOT NULL,
    numero integer NOT NULL
);


ALTER TABLE public.organizaciones_usuariosbeneficiarios OWNER TO renovable;

--
-- Name: organizaciones_usuariosbeneficiarios_id_seq; Type: SEQUENCE; Schema: public; Owner: renovable
--

CREATE SEQUENCE organizaciones_usuariosbeneficiarios_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.organizaciones_usuariosbeneficiarios_id_seq OWNER TO renovable;

--
-- Name: organizaciones_usuariosbeneficiarios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: renovable
--

ALTER SEQUENCE organizaciones_usuariosbeneficiarios_id_seq OWNED BY organizaciones_usuariosbeneficiarios.id;


--
-- Name: organizaciones_usuariosbeneficiarios_id_seq; Type: SEQUENCE SET; Schema: public; Owner: renovable
--

SELECT pg_catalog.setval('organizaciones_usuariosbeneficiarios_id_seq', 1, true);


--
-- Name: south_migrationhistory; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE south_migrationhistory (
    id integer NOT NULL,
    app_name character varying(255) NOT NULL,
    migration character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.south_migrationhistory OWNER TO renovable;

--
-- Name: south_migrationhistory_id_seq; Type: SEQUENCE; Schema: public; Owner: renovable
--

CREATE SEQUENCE south_migrationhistory_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.south_migrationhistory_id_seq OWNER TO renovable;

--
-- Name: south_migrationhistory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: renovable
--

ALTER SEQUENCE south_migrationhistory_id_seq OWNED BY south_migrationhistory.id;


--
-- Name: south_migrationhistory_id_seq; Type: SEQUENCE SET; Schema: public; Owner: renovable
--

SELECT pg_catalog.setval('south_migrationhistory_id_seq', 21, true);


--
-- Name: thumbnail_kvstore; Type: TABLE; Schema: public; Owner: renovable; Tablespace: 
--

CREATE TABLE thumbnail_kvstore (
    key character varying(200) NOT NULL,
    value text NOT NULL
);


ALTER TABLE public.thumbnail_kvstore OWNER TO renovable;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: renovable
--

ALTER TABLE auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: renovable
--

ALTER TABLE auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: renovable
--

ALTER TABLE auth_message ALTER COLUMN id SET DEFAULT nextval('auth_message_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: renovable
--

ALTER TABLE auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: renovable
--

ALTER TABLE auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: renovable
--

ALTER TABLE auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: renovable
--

ALTER TABLE auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: renovable
--

ALTER TABLE csvimport_csvimport ALTER COLUMN id SET DEFAULT nextval('csvimport_csvimport_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: renovable
--

ALTER TABLE csvimport_importmodel ALTER COLUMN id SET DEFAULT nextval('csvimport_importmodel_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: renovable
--

ALTER TABLE django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: renovable
--

ALTER TABLE django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: renovable
--

ALTER TABLE django_site ALTER COLUMN id SET DEFAULT nextval('django_site_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: renovable
--

ALTER TABLE indicadores_indicador ALTER COLUMN id SET DEFAULT nextval('indicadores_indicador_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: renovable
--

ALTER TABLE indicadores_tipoindicador ALTER COLUMN id SET DEFAULT nextval('indicadores_tipoindicador_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: renovable
--

ALTER TABLE indicadores_valorindicador ALTER COLUMN id SET DEFAULT nextval('indicadores_valorindicador_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: renovable
--

ALTER TABLE nomencladores_actividad ALTER COLUMN id SET DEFAULT nextval('nomencladores_actividad_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: renovable
--

ALTER TABLE nomencladores_categoria ALTER COLUMN id SET DEFAULT nextval('nomencladores_categoria_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: renovable
--

ALTER TABLE nomencladores_fuenterenovable ALTER COLUMN id SET DEFAULT nextval('nomencladores_fuenterenovable_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: renovable
--

ALTER TABLE nomencladores_materiaprima ALTER COLUMN id SET DEFAULT nextval('nomencladores_materiaprima_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: renovable
--

ALTER TABLE nomencladores_pais ALTER COLUMN id SET DEFAULT nextval('nomencladores_pais_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: renovable
--

ALTER TABLE nomencladores_tipoentidad ALTER COLUMN id SET DEFAULT nextval('nomencladores_tipoentidad_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: renovable
--

ALTER TABLE nomencladores_vegetal ALTER COLUMN id SET DEFAULT nextval('nomencladores_vegetal_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: renovable
--

ALTER TABLE organizaciones_beneficiarios ALTER COLUMN id SET DEFAULT nextval('organizaciones_beneficiarios_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: renovable
--

ALTER TABLE organizaciones_beneficiariosproyecto ALTER COLUMN id SET DEFAULT nextval('organizaciones_beneficiariosproyecto_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: renovable
--

ALTER TABLE organizaciones_contacto ALTER COLUMN id SET DEFAULT nextval('organizaciones_contacto_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: renovable
--

ALTER TABLE organizaciones_email ALTER COLUMN id SET DEFAULT nextval('organizaciones_email_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: renovable
--

ALTER TABLE organizaciones_empleados ALTER COLUMN id SET DEFAULT nextval('organizaciones_empleados_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: renovable
--

ALTER TABLE organizaciones_galeriafotosproyecto ALTER COLUMN id SET DEFAULT nextval('organizaciones_galeriafotosproyecto_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: renovable
--

ALTER TABLE organizaciones_galeriafotossistema ALTER COLUMN id SET DEFAULT nextval('organizaciones_galeriafotossistema_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: renovable
--

ALTER TABLE organizaciones_organizacion ALTER COLUMN id SET DEFAULT nextval('organizaciones_organizacion_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: renovable
--

ALTER TABLE organizaciones_organizacion_actividad ALTER COLUMN id SET DEFAULT nextval('organizaciones_organizacion_actividad_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: renovable
--

ALTER TABLE organizaciones_organizacion_fuente ALTER COLUMN id SET DEFAULT nextval('organizaciones_organizacion_fuente_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: renovable
--

ALTER TABLE organizaciones_proyecto_fuente ALTER COLUMN id SET DEFAULT nextval('organizaciones_proyecto_fuente_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: renovable
--

ALTER TABLE organizaciones_sistemaejecucion ALTER COLUMN id SET DEFAULT nextval('organizaciones_sistemaejecucion_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: renovable
--

ALTER TABLE organizaciones_sistemaejecucion_fuente ALTER COLUMN id SET DEFAULT nextval('organizaciones_sistemaejecucion_fuente_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: renovable
--

ALTER TABLE organizaciones_usuariosbeneficiarios ALTER COLUMN id SET DEFAULT nextval('organizaciones_usuariosbeneficiarios_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: renovable
--

ALTER TABLE south_migrationhistory ALTER COLUMN id SET DEFAULT nextval('south_migrationhistory_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY auth_group (id, name) FROM stdin;
1	Organizaciones
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY auth_group_permissions (id, group_id, permission_id) FROM stdin;
1	1	25
2	1	26
3	1	27
4	1	28
5	1	29
6	1	30
7	1	31
8	1	32
9	1	33
10	1	34
11	1	35
12	1	36
13	1	58
14	1	59
15	1	60
16	1	61
17	1	62
18	1	63
19	1	64
20	1	65
21	1	66
22	1	67
23	1	68
24	1	69
25	1	70
26	1	71
27	1	72
28	1	73
29	1	74
30	1	75
31	1	76
32	1	77
33	1	78
34	1	79
35	1	80
36	1	81
\.


--
-- Data for Name: auth_message; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY auth_message (id, user_id, message) FROM stdin;
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add permission	1	add_permission
2	Can change permission	1	change_permission
3	Can delete permission	1	delete_permission
4	Can add group	2	add_group
5	Can change group	2	change_group
6	Can delete group	2	delete_group
7	Can add user	3	add_user
8	Can change user	3	change_user
9	Can delete user	3	delete_user
10	Can add message	4	add_message
11	Can change message	4	change_message
12	Can delete message	4	delete_message
13	Can add content type	5	add_contenttype
14	Can change content type	5	change_contenttype
15	Can delete content type	5	delete_contenttype
16	Can add session	6	add_session
17	Can change session	6	change_session
18	Can delete session	6	delete_session
19	Can add site	7	add_site
20	Can change site	7	change_site
21	Can delete site	7	delete_site
22	Can add log entry	8	add_logentry
23	Can change log entry	8	change_logentry
24	Can delete log entry	8	delete_logentry
25	Can add Sistema de Ejecución	9	add_sistemaejecucion
26	Can change Sistema de Ejecución	9	change_sistemaejecucion
27	Can delete Sistema de Ejecución	9	delete_sistemaejecucion
28	Can add Organización	10	add_organizacion
29	Can change Organización	10	change_organizacion
30	Can delete Organización	10	delete_organizacion
31	Can add Contacto	11	add_contacto
32	Can change Contacto	11	change_contacto
33	Can delete Contacto	11	delete_contacto
34	Can add email	12	add_email
35	Can change email	12	change_email
36	Can delete email	12	delete_email
37	Can add Pais	13	add_pais
38	Can change Pais	13	change_pais
39	Can delete Pais	13	delete_pais
40	Can add Categoria	14	add_categoria
41	Can change Categoria	14	change_categoria
42	Can delete Categoria	14	delete_categoria
43	Can add Tipo de Entidad	15	add_tipoentidad
44	Can change Tipo de Entidad	15	change_tipoentidad
45	Can delete Tipo de Entidad	15	delete_tipoentidad
46	Can add Materia Prima	16	add_materiaprima
47	Can change Materia Prima	16	change_materiaprima
48	Can delete Materia Prima	16	delete_materiaprima
49	Can add Vegetal	17	add_vegetal
50	Can change Vegetal	17	change_vegetal
51	Can delete Vegetal	17	delete_vegetal
52	Can add Fuente Renovable	18	add_fuenterenovable
53	Can change Fuente Renovable	18	change_fuenterenovable
54	Can delete Fuente Renovable	18	delete_fuenterenovable
55	Can add Indicador sectorial	19	add_indicadoressectoriales
56	Can change Indicador sectorial	19	change_indicadoressectoriales
57	Can delete Indicador sectorial	19	delete_indicadoressectoriales
58	Can add Imagen	20	add_galeriafotossistema
59	Can change Imagen	20	change_galeriafotossistema
60	Can delete Imagen	20	delete_galeriafotossistema
61	Can add Usuarios Beneficiados	21	add_usuariosbeneficiarios
62	Can change Usuarios Beneficiados	21	change_usuariosbeneficiarios
63	Can delete Usuarios Beneficiados	21	delete_usuariosbeneficiarios
64	Can add Beneficiarios	22	add_beneficiarios
65	Can change Beneficiarios	22	change_beneficiarios
66	Can delete Beneficiarios	22	delete_beneficiarios
67	Can add Empleados	23	add_empleados
68	Can change Empleados	23	change_empleados
69	Can delete Empleados	23	delete_empleados
70	Can add Responsable de la Organización	24	add_responsable
71	Can change Responsable de la Organización	24	change_responsable
72	Can delete Responsable de la Organización	24	delete_responsable
73	Can add Proyecto de la Organización	25	add_proyecto
74	Can change Proyecto de la Organización	25	change_proyecto
75	Can delete Proyecto de la Organización	25	delete_proyecto
76	Can add Imagen	26	add_galeriafotosproyecto
77	Can change Imagen	26	change_galeriafotosproyecto
78	Can delete Imagen	26	delete_galeriafotosproyecto
79	Can add Beneficiarios	27	add_beneficiariosproyecto
80	Can change Beneficiarios	27	change_beneficiariosproyecto
81	Can delete Beneficiarios	27	delete_beneficiariosproyecto
82	Can add Tipo de Actividad	28	add_actividad
83	Can change Tipo de Actividad	28	change_actividad
84	Can delete Tipo de Actividad	28	delete_actividad
85	Can add kv store	29	add_kvstore
86	Can change kv store	29	change_kvstore
87	Can delete kv store	29	delete_kvstore
88	Can add migration history	30	add_migrationhistory
89	Can change migration history	30	change_migrationhistory
90	Can delete migration history	30	delete_migrationhistory
91	Can add Tipo de Indicador	31	add_tipoindicador
92	Can change Tipo de Indicador	31	change_tipoindicador
93	Can delete Tipo de Indicador	31	delete_tipoindicador
94	Can add Indicador	32	add_indicador
95	Can change Indicador	32	change_indicador
96	Can delete Indicador	32	delete_indicador
97	Can add valor indicador	33	add_valorindicador
98	Can change valor indicador	33	change_valorindicador
99	Can delete valor indicador	33	delete_valorindicador
100	Can add csv import	34	add_csvimport
101	Can change csv import	34	change_csvimport
102	Can delete csv import	34	delete_csvimport
103	Can add import model	35	add_importmodel
104	Can change import model	35	change_importmodel
105	Can delete import model	35	delete_importmodel
\.


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY auth_user (id, username, first_name, last_name, email, password, is_staff, is_active, is_superuser, last_login, date_joined) FROM stdin;
2	renovables			maribel@gmail.com	sha1$09347$cddd847f6a2e77439908fd8679cb305aa843bd29	t	t	t	2012-01-06 23:03:53.104951+00	2012-01-06 22:40:15.975723+00
3	maribel			maribel@guegue.net	sha1$eb5fa$08885f1a985c05fb5e202c4683f51ea553a64ffa	t	t	t	2012-04-01 04:57:48.469883+00	2012-03-28 15:24:39.497843+00
1	admin			javier.wilson@gmail.com	sha1$3caa4$8e748823ea987d578d6e1d8f9bfb85260103a96e	t	t	t	2012-03-29 14:57:40.015984+00	2011-11-23 20:24:33.925597+00
4	alejandro				sha1$12207$c42481dfcf8fee5a92c69926dc09e4d1dfbc5bb7	t	t	t	2012-03-30 16:49:48.68183+00	2012-03-29 19:30:26+00
5	organizacion1				sha1$52ea2$91263fa2f0fd94eaef65093c9f6083c9606b28f0	t	t	f	2012-03-30 21:10:55.088578+00	2012-03-30 20:29:00+00
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY auth_user_groups (id, user_id, group_id) FROM stdin;
1	5	1
\.


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Data for Name: csvimport_csvimport; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY csvimport_csvimport (id, model_name, field_list, upload_file, file_name, encoding, upload_method, error_log, import_date, import_user) FROM stdin;
19	organizaciones.Contacto	column1=organizacion(Organizacion|nombre),column2=nombre,column3=cargo,column4=emails,column5=telefono,column6=direccion	csv/Contactos_v05032012_18.csv	csv/Contactos_v05032012_18.csv		manual	Using manually entered mapping list	2012-03-15	admin
\.


--
-- Data for Name: csvimport_importmodel; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY csvimport_importmodel (id, csvimport_id, numeric_id, natural_key) FROM stdin;
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY django_admin_log (id, action_time, user_id, content_type_id, object_id, object_repr, action_flag, change_message) FROM stdin;
1	2011-11-24 19:18:57.877981+00	1	15	1	ONG nacional	1	
2	2011-11-24 19:19:03.581031+00	1	15	2	Empresa Nacional	1	
3	2011-11-24 19:34:42.750233+00	1	15	3	Empresa Internacional	1	
4	2012-01-06 23:04:23.596884+00	2	16	1	carbon	1	
5	2012-02-17 15:39:00.081484+00	1	28	1	Construccion	1	
6	2012-02-17 15:45:21.863366+00	1	18	1	N/A	1	
7	2012-02-17 15:59:37.262565+00	1	18	2	fuente prueba	1	
8	2012-02-17 18:31:22.195748+00	1	10	1	aaaaa	1	
9	2012-02-17 19:34:45.569319+00	1	10	1	ACN, S.A	2	Modificado/a tipo y nombre.
10	2012-02-17 19:35:39.038014+00	1	28	2	Generación	1	
11	2012-02-17 19:40:13.323449+00	1	18	3	Eólico	1	
12	2012-02-17 19:51:40.532018+00	1	10	2	AMAYO, S.A	1	
13	2012-02-17 19:54:34.231414+00	1	18	4	Hidroeléctrico	1	
14	2012-02-17 19:55:30.618804+00	1	10	3	APRODELBO	1	
15	2012-02-17 19:56:59.664156+00	1	18	5	Solar	1	
16	2012-02-17 19:57:08.014746+00	1	18	6	Biomasa	1	
17	2012-02-17 19:58:26.457884+00	1	10	4	ASOFENIX	1	
18	2012-02-17 20:02:55.758448+00	1	10	5	ATDER – BL	1	
19	2012-02-17 20:03:47.541069+00	1	10	3	APRODELBO	2	Modificado/a tipo.
20	2012-02-17 20:09:34.585202+00	1	10	6	BLUE POWER & ENERGY	1	
21	2012-02-17 20:11:22.034916+00	1	10	7	blueEnergy	1	
22	2012-02-17 20:12:22.728228+00	1	18	7	Geotérmico	1	
23	2012-02-17 20:12:58.348732+00	1	10	8	Cerro Colorado Power, SA	1	
24	2012-02-17 20:14:48.895836+00	1	10	9	Cerro Frio, SA	1	
25	2012-02-17 20:17:27.459537+00	1	10	10	CHN, S.A	1	
26	2012-02-17 21:36:27.010535+00	1	10	2	AMAYO, S.A	2	Modificado/a logo.
27	2012-02-17 21:38:21.03882+00	1	10	2	AMAYO, S.A	2	Modificado/a logo.
28	2012-02-17 21:38:27.216638+00	1	10	11	Prueba Eliminar	1	
29	2012-02-17 21:47:10.948501+00	1	10	7	blueEnergy	2	Modificado/a logo.
30	2012-02-17 21:47:41.086156+00	1	10	5	ATDER – BL	2	Modificado/a logo.
31	2012-02-17 21:48:20.867802+00	1	10	10	CHN, S.A	2	Modificado/a logo.
32	2012-02-17 21:54:57.672263+00	1	10	11	Prueba Eliminar	3	
33	2012-02-17 21:57:48.051474+00	1	10	12	ECAMI, SA	1	
34	2012-02-17 22:00:27.600513+00	1	10	13	EMEEAW - Wiwilí	1	
35	2012-02-17 22:02:58.474577+00	1	10	14	ENICALSA	1	
36	2012-02-17 22:04:20.054071+00	1	10	15	EOLONICA, SA	1	
37	2012-02-17 22:06:20.360187+00	1	10	16	Geonica - ENEL (It)	1	
38	2012-02-17 22:08:53.775426+00	1	10	17	GIER	1	
39	2012-02-17 22:12:48.301713+00	1	10	18	Grupo Fénix (UNI - PFAE)	1	
40	2012-02-17 22:14:18.004592+00	1	10	19	Hidro Pantasma - Grupo Saret	1	
41	2012-02-20 14:44:46.622383+00	1	10	20	HISMOW, SA	1	
42	2012-02-20 14:46:19.694019+00	1	10	21	Ingenio Monte Rosa - Pantaleon	1	
43	2012-02-20 14:47:56.723839+00	1	10	22	Ingenio San Antonio	1	
44	2012-02-20 14:48:30.026839+00	1	15	4	Academia	1	
45	2012-02-20 14:50:14.583547+00	1	10	23	IPLS	1	
46	2012-02-20 14:55:31.478576+00	1	10	24	Mesoamérica Energy - GlobalEq, SA	1	
47	2012-02-20 14:57:16.467957+00	1	10	25	Mi Fogon, SA	1	
48	2012-02-20 14:58:42.119536+00	1	10	26	MOLINA & ASOCIADOS	1	
49	2012-02-20 15:01:13.280969+00	1	10	27	Mujeres solares de Totogalpa	1	
50	2012-02-20 15:03:55.513845+00	1	10	28	ORMAT Momotombo Power Company Inc.	1	
51	2012-02-20 15:05:15.576113+00	1	10	29	PCH Bilampi - Wanawas	1	
52	2012-02-20 15:06:33.669093+00	1	10	30	PCH Casa Quedama	1	
53	2012-02-20 15:07:13.271389+00	1	10	29	PCH Bilampi - Wanawas	2	Modificado/a longitud y latitud.
54	2012-02-20 15:08:12.374051+00	1	10	31	PCH El Naranjo	1	
55	2012-02-20 15:09:11.631498+00	1	10	32	PCH La Florida	1	
56	2012-02-20 15:09:49.304813+00	1	10	33	PCH Las Nubes - El Naranjo	1	
57	2012-02-20 15:43:08.439789+00	1	10	34	PCH Mini Central El Trebol	1	
58	2012-02-20 15:44:54.010739+00	1	10	35	PCH Rio Bravo - Puerto Viejo	1	
59	2012-02-20 15:45:22.056091+00	1	10	36	PCH Salto Negro	1	
60	2012-02-20 15:47:01.509119+00	1	10	37	POLARIS, SA	1	
61	2012-02-20 15:48:26.95744+00	1	10	38	Proleña	1	
62	2012-02-20 15:51:01.85858+00	1	10	39	PRONICARAGUA	1	
63	2012-02-20 16:05:39.657792+00	1	10	40	RENOVABLES	1	
64	2012-02-20 16:07:07.485947+00	1	10	41	SUNISOLAR, SA	1	
65	2012-02-20 16:09:06.804698+00	1	10	42	TICHANA POWER, SA	1	
66	2012-02-20 16:28:50.422621+00	1	10	43	ULSA	1	
67	2012-02-20 16:31:54.478932+00	1	10	44	UNA	1	
68	2012-02-20 16:33:46.042862+00	1	10	45	UNI	1	
69	2012-02-20 16:34:23.707262+00	1	10	44	UNA	2	Modificado/a longitud y latitud.
70	2012-02-20 16:35:30.949839+00	1	10	46	UTN	1	
71	2012-02-20 16:40:58.409107+00	1	10	4	ASOFENIX	2	Modificado/a logo.
72	2012-02-21 17:21:08.808197+00	1	10	1	ACN, S.A	2	Modificado/a telefono y pais.
73	2012-02-21 17:23:14.987163+00	1	10	2	AMAYO, S.A	2	Modificado/a renovable, telefono, direccion y pais.
74	2012-02-21 17:24:57.899993+00	1	10	3	APRODELBO	2	Modificado/a renovable, telefono y direccion.
75	2012-02-21 17:26:36.846841+00	1	10	4	ASOFENIX	2	Modificado/a renovable, telefono, web, direccion y pais.
76	2012-02-21 17:28:07.707836+00	1	10	5	ATDER – BL	2	Modificado/a renovable, telefono, web y pais.
77	2012-02-21 17:29:53.094813+00	1	10	6	BLUE POWER & ENERGY	2	Modificado/a renovable, telefono, web, direccion y pais.
78	2012-02-21 17:31:10.952151+00	1	10	7	blueEnergy	2	Modificado/a renovable, telefono, web, direccion y pais.
79	2012-02-21 17:32:48.848064+00	1	10	8	Cerro Colorado Power, SA	2	Modificado/a renovable, telefono y pais.
80	2012-02-21 17:34:19.249442+00	1	10	9	Cerro Frio, SA	2	Modificado/a renovable, telefono y pais.
81	2012-02-21 17:35:23.868502+00	1	10	10	CHN, S.A	2	Modificado/a renovable y pais.
82	2012-02-21 17:37:06.23132+00	1	10	12	ECAMI, SA	2	Modificado/a renovable, telefono, web, direccion y pais.
83	2012-02-21 17:38:55.583212+00	1	10	13	EMEEAW - Wiwilí	2	Modificado/a renovable, telefono y pais.
84	2012-02-21 17:40:44.443283+00	1	10	14	ENICALSA	2	Modificado/a renovable, telefono, web y pais.
85	2012-02-21 17:41:54.633975+00	1	10	15	EOLONICA, SA	2	Modificado/a renovable, telefono y pais.
86	2012-02-21 17:44:29.8293+00	1	10	16	Geonica - ENEL (It)	2	Modificado/a telefono, web y pais.
87	2012-02-21 17:45:36.965692+00	1	10	17	GIER	2	Modificado/a renovable, telefono y pais.
88	2012-02-21 17:46:49.635089+00	1	10	16	Geonica - ENEL (It)	2	Modificado/a renovable.
89	2012-02-21 17:48:34.365256+00	1	10	18	Grupo Fénix (UNI - PFAE)	2	Modificado/a renovable, telefono y pais.
90	2012-02-21 17:50:33.090985+00	1	10	19	Hidro Pantasma - Grupo Saret	2	Modificado/a telefono y pais.
91	2012-02-21 17:51:54.302376+00	1	10	20	HISMOW, SA	2	Modificado/a renovable, telefono y pais.
377	2012-03-30 17:13:31.147776+00	3	11	266	Jaime Munoz	3	
92	2012-02-21 17:54:18.040227+00	1	10	21	Ingenio Monte Rosa - Pantaleon	2	Modificado/a telefono, direccion y pais.
93	2012-02-21 17:56:23.758564+00	1	10	22	Ingenio San Antonio	2	Modificado/a telefono, direccion y pais.
94	2012-02-21 17:57:10.366332+00	1	10	21	Ingenio Monte Rosa - Pantaleon	2	No ha cambiado ningún campo.
95	2012-02-21 17:58:42.612862+00	1	10	23	IPLS	2	Modificado/a renovable, telefono, web, direccion y pais.
96	2012-02-21 18:00:23.86032+00	1	10	24	Mesoamérica Energy - GlobalEq, SA	2	Modificado/a renovable, telefono, web, direccion y pais.
97	2012-02-21 18:01:23.176166+00	1	10	25	Mi Fogon, SA	2	Modificado/a renovable y pais.
98	2012-02-21 18:02:53.497467+00	1	10	26	MOLINA & ASOCIADOS	2	Modificado/a renovable, telefono, web, direccion y pais.
99	2012-02-21 18:04:14.787945+00	1	10	27	Mujeres solares de Totogalpa	2	Modificado/a renovable, telefono, web, direccion y pais.
100	2012-02-21 18:05:59.750782+00	1	10	28	ORMAT Momotombo Power Company Inc.	2	Modificado/a telefono, direccion y pais.
101	2012-02-21 18:07:01.776875+00	1	10	29	PCH Bilampi - Wanawas	2	Modificado/a renovable, telefono, direccion y pais.
102	2012-02-21 18:08:20.454363+00	1	10	30	PCH Casa Quedama	2	Modificado/a renovable y pais.
103	2012-02-21 18:09:51.035762+00	1	10	31	PCH El Naranjo	2	Modificado/a renovable, telefono, direccion y pais.
104	2012-02-21 18:11:39.623454+00	1	10	32	PCH La Florida	2	Modificado/a renovable, telefono, direccion y pais.
105	2012-02-21 18:12:27.667574+00	1	10	33	PCH Las Nubes - El Naranjo	2	Modificado/a renovable y pais.
106	2012-02-21 18:12:54.345066+00	1	10	34	PCH Mini Central El Trebol	2	Modificado/a renovable y pais.
107	2012-02-21 18:13:39.851996+00	1	10	35	PCH Rio Bravo - Puerto Viejo	2	Modificado/a renovable, telefono y pais.
108	2012-02-21 18:14:35.552849+00	1	10	36	PCH Salto Negro	2	Modificado/a renovable, telefono y pais.
109	2012-02-21 18:15:12.745088+00	1	10	37	POLARIS, SA	2	Modificado/a renovable y pais.
110	2012-02-21 18:16:24.702823+00	1	10	38	Proleña	2	Modificado/a renovable, telefono, web, direccion y pais.
111	2012-02-21 18:17:29.890979+00	1	10	39	PRONICARAGUA	2	Modificado/a renovable, telefono, web y pais.
112	2012-02-21 18:18:45.80239+00	1	10	40	RENOVABLES	2	Modificado/a renovable, telefono, web, direccion y pais.
113	2012-02-21 18:19:53.259854+00	1	10	41	SUNISOLAR, SA	2	Modificado/a renovable, telefono, web, direccion y pais.
114	2012-02-21 18:20:44.98679+00	1	10	42	TICHANA POWER, SA	2	Modificado/a direccion y pais.
115	2012-02-21 18:21:52.556542+00	1	10	43	ULSA	2	Modificado/a renovable, telefono, web, direccion y pais.
116	2012-02-21 18:23:13.03496+00	1	10	44	UNA	2	Modificado/a renovable, telefono, web y pais.
117	2012-02-21 18:24:40.131126+00	1	10	45	UNI	2	Modificado/a renovable, telefono, web, direccion y pais.
118	2012-02-21 18:25:41.423907+00	1	10	46	UTN	2	Modificado/a renovable, telefono, web y pais.
119	2012-02-29 16:56:14.807329+00	1	19	2002	2002	1	
120	2012-02-29 17:11:23.140636+00	1	19	2004	2004	1	
121	2012-02-29 18:30:35.106384+00	1	19	2004	2004	2	Modificado/a energia_nacional, poblacion, tCO2eq, tCO2ahorradas, mwh, tarifa, tarifarenovable, beneficiarios, personas y empleos.
122	2012-02-29 18:31:14.02413+00	1	19	2004	2004	2	Modificado/a tCO2eq.
123	2012-02-29 18:32:33.333456+00	1	19	2004	2004	2	Modificado/a mwh.
124	2012-02-29 18:33:17.790389+00	1	19	2004	2004	2	Modificado/a mwh.
125	2012-02-29 18:37:08.618681+00	1	19	2002	2002	2	Modificado/a energia_nacional, poblacion, tCO2eq, tCO2ahorradas, mwh, tarifa, tarifarenovable, beneficiarios, personas y empleos.
126	2012-02-29 18:40:20.744436+00	1	19	2000	2000	1	
127	2012-02-29 18:46:02.754944+00	1	19	2001	2001	1	
128	2012-02-29 18:51:02.817685+00	1	19	2003	2003	1	
129	2012-02-29 18:52:03.037492+00	1	19	2003	2003	2	Modificado/a personas.
130	2012-02-29 18:53:10.847358+00	1	19	2003	2003	2	Modificado/a personas.
131	2012-02-29 19:28:45.222274+00	1	19	2002	2002	1	
132	2012-02-29 19:29:17.009951+00	1	19	2000	2000	1	
133	2012-02-29 19:30:01.769617+00	1	19	2000	2000	2	No ha cambiado ningún campo.
134	2012-02-29 19:36:42.350213+00	1	19	2001	2001	2	Modificado/a anho, energia_nacional, poblacion, tCO2eq, tCO2ahorradas, mwh, tarifa, tarifarenovable, beneficiarios, personas y empleos.
135	2012-02-29 19:37:07.284278+00	1	19	2001	2001	2	No ha cambiado ningún campo.
136	2012-02-29 19:39:53.756695+00	1	19	2002	2002	2	Modificado/a energia_nacional, poblacion, tCO2eq, tCO2ahorradas, mwh, tarifa, tarifarenovable, beneficiarios, personas y empleos.
137	2012-02-29 19:43:14.95294+00	1	19	2003	2003	1	
138	2012-02-29 19:46:50.905635+00	1	19	2004	2004	1	
139	2012-02-29 19:49:01.87685+00	1	19	2005	2005	1	
140	2012-02-29 19:51:19.41431+00	1	19	2006	2006	1	
141	2012-02-29 19:55:09.084521+00	1	19	2007	2007	1	
142	2012-02-29 19:57:08.93167+00	1	19	2008	2008	1	
143	2012-02-29 19:59:27.1485+00	1	19	2009	2009	1	
144	2012-02-29 20:01:53.819572+00	1	19	2010	2010	1	
145	2012-02-29 20:04:02.276744+00	1	19	2011	2011	1	
146	2012-02-29 22:27:31.582632+00	1	10	37	POLARIS, SA	2	Modificado/a actividad y fuente.
147	2012-02-29 22:31:29.285526+00	1	28	3	Asesoría Legal	1	
148	2012-02-29 22:31:40.460356+00	1	28	4	Distribución de energía	1	
149	2012-02-29 22:31:53.032658+00	1	28	5	Educación / Capacitación	1	
150	2012-02-29 22:32:04.540133+00	1	28	6	Fabricación de Tecnología	1	
151	2012-02-29 22:32:15.06177+00	1	28	7	Gobierno	1	
152	2012-02-29 22:32:34.006111+00	1	28	8	Investigación	1	
153	2012-02-29 22:32:43.944001+00	1	28	9	Mantenimiento	1	
154	2012-02-29 22:33:00.838707+00	1	28	10	Organización gremial	1	
155	2012-02-29 22:33:11.221063+00	1	28	11	Servicios energéticos	1	
156	2012-02-29 22:36:14.507018+00	1	10	38	Proleña	2	Modificado/a actividad, telefono y direccion.
157	2012-02-29 22:44:40.969089+00	1	28	1	Construcción	2	Modificado/a nombre.
158	2012-02-29 22:47:01.607882+00	1	10	46	UTN	2	Modificado/a actividad y telefono.
159	2012-02-29 22:47:18.346758+00	1	10	45	UNI	2	Modificado/a telefono y direccion.
160	2012-02-29 22:55:38.490546+00	1	10	45	UNI	2	Modificado/a actividad.
161	2012-02-29 22:55:54.366586+00	1	10	44	UNA	2	Modificado/a actividad y telefono.
162	2012-03-01 01:08:31.078633+00	1	10	43	ULSA	2	Modificado/a actividad, telefono y direccion.
163	2012-03-01 15:13:27.899324+00	1	10	46	UTN	2	Modificado/a telefono.
164	2012-03-01 15:13:54.428964+00	1	10	46	UTN	2	No ha cambiado ningún campo.
165	2012-03-01 15:15:06.607526+00	1	10	42	TICHANA POWER, SA	2	Modificado/a actividad y direccion.
166	2012-03-01 15:15:36.064972+00	1	10	41	SUNISOLAR, SA	2	Modificado/a actividad, telefono y direccion.
167	2012-03-01 15:18:32.360176+00	1	10	7	blueEnergy	2	Modificado/a telefono.
168	2012-03-01 15:21:29.990533+00	1	10	40	RENOVABLES	2	Modificado/a actividad, telefono, direccion, longitud y latitud.
169	2012-03-01 15:22:06.692841+00	1	10	39	PRONICARAGUA	2	Modificado/a actividad y telefono.
170	2012-03-01 15:22:59.737588+00	1	10	36	PCH Salto Negro	2	Modificado/a actividad y telefono.
171	2012-03-01 15:23:19.362748+00	1	10	35	PCH Rio Bravo - Puerto Viejo	2	Modificado/a actividad y telefono.
172	2012-03-01 15:23:35.094286+00	1	10	34	PCH Mini Central El Trebol	2	Modificado/a actividad y renovable.
173	2012-03-01 15:23:51.567401+00	1	10	33	PCH Las Nubes - El Naranjo	2	Modificado/a actividad.
174	2012-03-01 15:24:10.013113+00	1	10	32	PCH La Florida	2	Modificado/a actividad, telefono y direccion.
175	2012-03-01 15:25:26.868963+00	1	10	31	PCH El Naranjo	2	Modificado/a telefono y direccion.
176	2012-03-01 15:25:50.094697+00	1	10	30	PCH Casa Quemada	2	Modificado/a nombre y actividad.
177	2012-03-01 15:26:10.988309+00	1	10	29	PCH Bilampi - Wanawas	2	Modificado/a actividad, telefono y direccion.
178	2012-03-01 15:26:31.363704+00	1	10	28	ORMAT Momotombo Power Company Inc.	2	Modificado/a actividad, telefono y direccion.
179	2012-03-01 15:26:56.679762+00	1	10	27	Mujeres solares de Totogalpa	2	Modificado/a actividad, telefono y direccion.
180	2012-03-01 15:27:19.587587+00	1	10	26	MOLINA & ASOCIADOS	2	Modificado/a actividad, telefono y direccion.
181	2012-03-01 15:27:37.576077+00	1	10	25	Mi Fogon, SA	2	Modificado/a actividad.
182	2012-03-01 15:28:06.11385+00	1	10	24	Mesoamerican Energy - GlobelEq, SA	2	Modificado/a nombre, actividad, telefono y direccion.
183	2012-03-01 15:28:20.396119+00	1	10	24	Mesoamerica Energy - GlobelEq, SA	2	Modificado/a nombre.
184	2012-03-01 15:28:44.474111+00	1	10	23	IPLS	2	Modificado/a actividad, telefono y direccion.
185	2012-03-01 15:28:59.772233+00	1	10	22	Ingenio San Antonio	2	Modificado/a actividad, telefono y direccion.
186	2012-03-01 15:29:21.101968+00	1	10	21	Ingenio Monte Rosa - Pantaleon	2	Modificado/a actividad, telefono y direccion.
187	2012-03-01 15:29:45.538285+00	1	10	20	HISMOW, SA	2	Modificado/a actividad y telefono.
188	2012-03-01 15:30:07.000697+00	1	10	19	Hidro Pantasma - Grupo Saret	2	Modificado/a actividad y telefono.
189	2012-03-01 15:30:28.669869+00	1	10	19	Hidro Pantasma - Grupo Saret	2	No ha cambiado ningún campo.
190	2012-03-01 15:31:59.928691+00	1	10	18	Grupo Fénix (UNI - PFAE)	2	Modificado/a actividad, telefono, web y direccion.
191	2012-03-01 15:33:09.673286+00	1	10	17	GIER	2	Modificado/a actividad y telefono.
192	2012-03-01 15:33:56.937846+00	1	10	16	Geonica - ENEL (It)	2	Modificado/a actividad y renovable.
193	2012-03-01 15:34:30.308357+00	1	10	15	EOLO de Nicaragua, SA	2	Modificado/a nombre, actividad y telefono.
194	2012-03-01 15:37:29.133556+00	1	10	14	ENICALSA	2	Modificado/a actividad, telefono y logo.
195	2012-03-01 15:38:21.896845+00	1	10	13	EMEEAW - Wiwilí	2	Modificado/a actividad y telefono.
196	2012-03-01 15:43:27.889234+00	1	10	12	ECAMI, SA	2	Modificado/a telefono, direccion y logo.
197	2012-03-01 15:46:15.881668+00	1	10	10	CHN, S.A	2	Modificado/a actividad, longitud y latitud.
198	2012-03-01 15:59:46.346507+00	1	10	9	Cerro Frio, SA	2	Modificado/a actividad, telefono, longitud y latitud.
199	2012-03-01 16:03:53.083664+00	1	10	8	Cerro Colorado Power, SA	2	Modificado/a actividad, telefono, longitud y latitud.
200	2012-03-01 16:04:27.74836+00	1	10	37	POLARIS, SA	2	Modificado/a longitud y latitud.
201	2012-03-01 16:05:54.005309+00	1	10	7	blueEnergy	2	Modificado/a longitud y latitud.
202	2012-03-01 16:06:09.132055+00	1	10	6	BLUE POWER & ENERGY	2	Modificado/a actividad y telefono.
203	2012-03-01 16:06:23.320428+00	1	10	7	blueEnergy	2	Modificado/a actividad.
204	2012-03-01 16:17:27.187792+00	1	10	5	ATDER – BL	2	Modificado/a actividad, telefono, longitud y latitud.
205	2012-03-01 16:17:58.190796+00	1	10	4	ASOFENIX	2	Modificado/a actividad y telefono.
206	2012-03-01 16:18:31.462364+00	1	10	3	APRODELBO	2	Modificado/a telefono.
207	2012-03-01 16:18:45.999212+00	1	10	2	AMAYO, S.A	2	Modificado/a telefono.
208	2012-03-01 16:18:59.562317+00	1	10	3	APRODELBO	2	Modificado/a logo.
209	2012-03-01 16:19:24.351875+00	1	10	1	ACN, S.A	2	Modificado/a telefono y fuente.
210	2012-03-01 17:20:12.473996+00	1	10	36	PCH Salto Negro - La Union S.A	2	Modificado/a nombre y direccion.
211	2012-03-01 17:20:58.947267+00	1	10	35	PCH Rio Bravo Puerto Viejo S.A	2	Modificado/a nombre y direccion.
212	2012-03-01 17:21:33.712406+00	1	10	35	PCH Rio Bravo Puerto Viejo S.A	2	Modificado/a telefono.
213	2012-03-01 17:22:01.837291+00	1	10	33	PCH Las Nubes El Naranjo S.A	2	Modificado/a nombre y direccion.
214	2012-03-01 17:22:28.90305+00	1	10	32	PCH Kubaly La Florida S.A	2	Modificado/a nombre y direccion.
215	2012-03-01 17:23:09.062202+00	1	10	31	PCH El Naranjo	3	
216	2012-03-01 17:24:30.97105+00	1	10	30	PCH HISAJOMA S.A - Casa Quemada	2	Modificado/a nombre y direccion.
217	2012-03-01 17:25:53.466014+00	1	10	29	PCH HIBIMUSUN S.A/  Bilampi - Wanawas	2	Modificado/a nombre y direccion.
218	2012-03-01 17:26:47.608668+00	1	10	47	PCH Salto Mollejones - Wapy S.A	1	
219	2012-03-01 17:45:18.005291+00	1	10	13	EMEEAW	2	Modificado/a nombre, longitud y latitud.
220	2012-03-01 17:54:12.561431+00	1	10	40	RENOVABLES	2	Modificado/a longitud y latitud.
221	2012-03-01 17:56:53.830367+00	1	10	37	POLARIS, SA	2	Modificado/a longitud y latitud.
222	2012-03-01 17:59:17.216222+00	1	10	10	CHN, S.A	2	Modificado/a longitud y latitud.
223	2012-03-01 17:59:41.461577+00	1	10	37	POLARIS, SA	2	Modificado/a longitud y latitud.
224	2012-03-01 18:00:14.179067+00	1	10	8	Cerro Colorado Power, SA	2	Modificado/a longitud y latitud.
225	2012-03-01 18:01:31.72355+00	1	10	9	Cerro Frio, SA	2	Modificado/a longitud y latitud.
226	2012-03-01 18:03:29.288688+00	1	10	30	PCH HISAJOMA S.A - Casa Quemada	2	Modificado/a longitud y latitud.
227	2012-03-01 18:04:54.95656+00	1	10	32	PCH Kubaly La Florida S.A	2	Modificado/a longitud y latitud.
228	2012-03-01 18:05:53.625185+00	1	10	35	PCH Rio Bravo Puerto Viejo S.A	2	Modificado/a longitud y latitud.
229	2012-03-01 18:06:46.562964+00	1	10	33	PCH Las Nubes El Naranjo S.A	2	Modificado/a longitud y latitud.
230	2012-03-01 18:08:05.313075+00	1	10	47	PCH Salto Mollejones - Wapy S.A	2	Modificado/a longitud y latitud.
231	2012-03-01 18:10:56.566075+00	1	10	7	blueEnergy	2	Modificado/a longitud y latitud.
232	2012-03-01 18:12:40.118074+00	1	10	3	APRODELBO	2	Modificado/a longitud y latitud.
233	2012-03-01 18:15:54.216829+00	1	10	5	ATDER – BL	2	Modificado/a longitud y latitud.
234	2012-03-01 18:22:19.548192+00	1	10	42	TICHANA POWER, SA	2	Modificado/a longitud y latitud.
235	2012-03-01 18:23:35.245938+00	1	10	26	MOLINA & ASOCIADOS	2	Modificado/a longitud y latitud.
236	2012-03-01 18:25:32.21817+00	1	10	25	Mi Fogon, SA	2	Modificado/a longitud y latitud.
237	2012-03-01 18:27:06.107628+00	1	10	24	Mesoamerica Energy - GlobelEq, SA	2	Modificado/a longitud y latitud.
238	2012-03-01 18:28:42.353111+00	1	10	16	Geonica - ENEL (It)	2	Modificado/a telefono, longitud y latitud.
239	2012-03-01 18:31:26.615495+00	1	10	15	EOLO de Nicaragua, SA	2	Modificado/a longitud y latitud.
240	2012-03-01 18:33:31.488945+00	1	10	12	ECAMI, SA	2	Modificado/a longitud y latitud.
241	2012-03-01 18:35:34.583216+00	1	10	6	BLUE POWER & ENERGY	2	Modificado/a longitud y latitud.
242	2012-03-05 22:17:42.079667+00	1	10	8	Cerro Colorado Power, SA	2	Modificado/a renovable.
243	2012-03-05 22:18:04.071322+00	1	10	6	BLUE POWER & ENERGY	2	Modificado/a renovable.
244	2012-03-06 15:40:44.471097+00	1	18	7	Geotérmico	2	Modificado/a imagen.
245	2012-03-06 15:41:01.138177+00	1	18	6	Biomasa	2	Modificado/a imagen.
246	2012-03-06 15:41:12.250778+00	1	18	5	Solar	2	Modificado/a imagen.
247	2012-03-06 15:41:25.16761+00	1	18	4	Hidroeléctrico	2	Modificado/a imagen.
248	2012-03-06 15:41:33.448939+00	1	18	3	Eólico	2	No ha cambiado ningún campo.
249	2012-03-06 15:41:44.186683+00	1	18	3	Eólico	2	Modificado/a imagen.
250	2012-03-06 15:42:01.638199+00	1	18	2	fuente prueba	3	
251	2012-03-14 15:44:16.394613+00	1	31	1	Acceso y Cobertura	1	
252	2012-03-14 15:48:52.454569+00	1	32	1	% Renovables en Nicaragua	1	
253	2012-03-14 15:50:21.218169+00	1	32	2	Población con Energía Renovable	1	
254	2012-03-14 15:51:04.199812+00	1	31	2	Impacto Ambiental	1	
255	2012-03-14 15:52:02.250105+00	1	32	3	Reducción de GEI (en tCO2)	1	
256	2012-03-14 15:53:14.400807+00	1	32	4	Reducción de GEI (en $)	1	
257	2012-03-14 15:53:50.088257+00	1	31	3	Económico	1	
258	2012-03-14 15:54:44.232995+00	1	32	5	Ahorro a la factura petrolera nacional	1	
259	2012-03-14 15:56:14.929534+00	1	32	6	Empleos generados	1	
260	2012-03-14 15:58:18.444915+00	1	31	4	Lucha Contra la Pobreza Energética	1	
261	2012-03-14 15:59:13.131681+00	1	32	7	Población beneficiada	1	
262	2012-03-14 16:13:16.870189+00	1	31	5	Precios	1	
263	2012-03-14 16:16:39.764552+00	1	32	8	Precio Actual Promedio	1	
264	2012-03-14 16:17:49.477293+00	1	32	9	Precio Estimado	1	
265	2012-03-14 16:24:14.953631+00	1	19	2000	2000	2	Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador.
266	2012-03-14 16:24:58.687499+00	1	19	2000	2000	2	No ha cambiado ningún campo.
267	2012-03-14 16:28:25.157629+00	1	19	2001	2001	2	Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador.
268	2012-03-14 16:31:16.872591+00	1	19	2002	2002	2	Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador.
269	2012-03-14 16:33:50.69978+00	1	19	2003	2003	2	Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador.
270	2012-03-14 16:37:14.029664+00	1	19	2004	2004	2	Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador.
271	2012-03-14 16:42:15.947389+00	1	19	2005	2005	2	Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador.
272	2012-03-14 16:44:58.132522+00	1	19	2006	2006	2	Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador.
273	2012-03-14 16:45:59.618401+00	1	19	2006	2006	2	No ha cambiado ningún campo.
274	2012-03-14 16:48:48.925195+00	1	19	2007	2007	2	Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador.
275	2012-03-14 16:51:36.26317+00	1	19	2008	2008	2	Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador.
276	2012-03-14 16:54:27.710207+00	1	19	2009	2009	2	Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador.
277	2012-03-14 16:58:47.966999+00	1	19	2010	2010	2	Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador.
278	2012-03-14 17:01:47.513567+00	1	19	2011	2011	2	Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador. Añadido/a "" valor indicador.
279	2012-03-14 17:05:54.724993+00	1	32	6	Empleos Generados	2	Modificado/a nombre y nombrelargo.
280	2012-03-14 17:06:15.984313+00	1	32	7	Población Beneficiada	2	Modificado/a nombre.
281	2012-03-14 17:06:58.344604+00	1	32	5	Ahorro a la Factura Petrolera Nacional	2	Modificado/a nombre y nombrelargo.
282	2012-03-14 17:29:46.1272+00	1	32	1	% Renovables en Nicaragua	2	Modificado/a icono.
283	2012-03-14 17:30:17.358411+00	1	32	2	Población con Energía Renovable	2	Modificado/a icono.
284	2012-03-14 17:30:51.404459+00	1	32	3	Reducción de GEI (en tCO2)	2	Modificado/a icono.
285	2012-03-14 17:31:35.364647+00	1	32	4	Reducción de GEI (en $)	2	Modificado/a icono.
286	2012-03-14 17:32:06.664458+00	1	32	5	Ahorro a la Factura Petrolera Nacional	2	Modificado/a icono.
287	2012-03-14 18:32:33.209884+00	1	32	6	Empleos Generados	2	Modificado/a icono.
288	2012-03-14 17:32:53.400175+00	1	32	7	Población Beneficiada	2	Modificado/a icono.
289	2012-03-15 13:47:02.089093+00	1	32	9	Precio actual promedio de la energía eléctrica en Nicaragua	2	Modificado/a nombre, nombrelargo, explicacion y fuentes.
378	2012-03-30 17:13:31.20112+00	3	11	184	Jaime Saborio	3	
290	2012-03-15 13:48:11.326533+00	1	32	8	Precio estimado de la energía eléctrica con una matriz 100% Renovable	2	Modificado/a nombre, nombrelargo y fuentes.
291	2012-03-15 14:50:01.342449+00	1	32	8	Precio actual promedio de la energía eléctrica en Nicaragua	2	Modificado/a nombre, nombrelargo, explicacion y fuentes.
292	2012-03-15 14:50:41.445098+00	1	32	9	Precio estimado de la energía eléctrica con una matriz 100% Renovable	2	Modificado/a nombre, nombrelargo, explicacion y fuentes.
293	2012-03-15 14:52:45.538772+00	1	19	2009	2009	2	Modificados indicador para "" valor indicador.
294	2012-03-15 20:47:15.026955+00	1	13	1	Nicaragua	1	
295	2012-03-15 21:59:58.443557+00	1	34	19	csv/Contactos_v05032012_18.csv	1	
296	2012-03-19 22:54:07.432929+00	1	10	66	Energía Tropical Sostenible Cerro Frío, S.A	2	Modificado/a tipo, nombre, renovable, longitud, latitud, fuente y actividad. Añadido/a "Energía Tropical Sostenible Cerro Frío, S.A" Responsable de la Organización.
297	2012-03-19 22:55:13.443111+00	1	10	9	Cerro Frio, SA	3	
298	2012-03-20 23:28:42.956763+00	1	10	63	BLUEPOWER & ENERGY	3	
299	2012-03-20 23:29:01.922626+00	1	10	60	ACN	3	
300	2012-03-21 00:29:16.310511+00	1	10	62	ATDER-BL	3	
301	2012-03-20 23:29:40.829982+00	1	10	81	UNI - PFAE	3	
302	2012-03-21 00:30:07.304583+00	1	10	61	AMAYO, SA	3	
303	2012-03-20 23:30:36.927509+00	1	10	69	ECAMI	3	
304	2012-03-20 23:30:50.387772+00	1	10	67	CHN	3	
305	2012-03-20 23:31:27.633694+00	1	10	65	Cerro Colorado Power	3	
306	2012-03-20 23:32:31.583039+00	1	10	80	SUNISOLAR	3	
307	2012-03-20 23:33:12.374037+00	1	10	74	HISMOW S.A.	3	
308	2012-03-21 00:39:02.066354+00	1	10	71	EOLO de Nicaragua	3	
309	2012-03-20 23:39:27.851003+00	1	10	78	PCH HIBIMUSUN S.A	3	
310	2012-03-21 00:40:00.475783+00	1	10	29	PCH HIBIMUSUN S.A	2	Modificado/a nombre y actividad.
311	2012-03-21 00:40:35.326272+00	1	10	73	Grupo Fénix – UNI - PFAE	3	
312	2012-03-21 00:43:02.922793+00	1	10	79	PCH HISAJOMA S.A	3	
313	2012-03-21 00:43:50.809863+00	1	10	30	PCH HISAJOMA S.A	2	Modificado/a nombre y actividad.
314	2012-03-21 00:48:03.096185+00	1	10	41	SUNI SOLAR, SA	2	Modificado/a nombre y actividad.
315	2012-03-26 02:28:17.454531+00	1	32	3	Reducción de GEI	2	Modificado/a nombre.
316	2012-03-26 02:28:26.433975+00	1	32	4	Reducción de GEI	2	Modificado/a nombre.
317	2012-03-26 02:52:31.384555+00	1	18	1	N/A	2	Modificado/a imagen.
318	2012-03-28 22:08:11.328544+00	3	16	2	BioDiesel	1	
319	2012-03-28 23:34:46.968664+00	3	16	3	N/A	1	
320	2012-03-28 23:47:24.590632+00	3	9	33	El Trebol	2	Modificado/a longitud y latitud.
321	2012-03-28 23:48:12.137399+00	3	9	33	El Trebol	2	Modificado/a longitud y latitud.
322	2012-03-29 13:14:28.526235+00	1	11	31	Lic. Karen Urcuyo Mejia (2)	2	Modificado/a nombre, direccion y area.
323	2012-03-29 19:30:26.696466+00	3	3	4	alejandro	1	
324	2012-03-29 18:30:50.659531+00	3	3	4	alejandro	2	Modificado/a is_staff y is_superuser.
325	2012-03-30 14:35:21.540428+00	1	10	46	UTN	2	Modificado/a actividad.
326	2012-03-30 14:35:36.565162+00	1	10	39	PRONICARAGUA	2	Modificado/a actividad.
327	2012-03-30 14:35:53.383602+00	1	10	26	MOLINA & ASOCIADOS	2	Modificado/a actividad.
328	2012-03-30 14:36:09.827743+00	1	10	40	RENOVABLES	2	Modificado/a actividad.
329	2012-03-30 15:56:28.91111+00	1	10	27	Mujeres solares de Totogalpa	2	Modificado/a longitud, latitud y actividad.
330	2012-03-30 15:58:39.831287+00	1	10	27	Mujeres solares de Totogalpa	2	Modificado/a longitud y latitud.
331	2012-03-30 16:59:16.188515+00	3	9	102	Yakalwás	2	Modificado/a nombre.
332	2012-03-30 16:59:35.007697+00	3	9	47	Reactor de Producción de Biodiesel	2	Modificado/a nombre.
333	2012-03-30 17:01:00.618973+00	3	9	42	Generador Eólico Autónomo	2	Modificado/a nombre.
334	2012-03-30 17:01:32.873801+00	3	9	41	Generador Eólico Conectado a la red	2	Modificado/a nombre.
335	2012-03-30 17:02:12.383309+00	3	9	40	Termosifón	2	Modificado/a nombre.
336	2012-03-30 17:02:28.504648+00	3	9	39	Sistema Autónomo Fotovoltaico	2	Modificado/a nombre.
337	2012-03-30 17:02:53.178065+00	3	9	26	Bilampí	2	Modificado/a nombre.
338	2012-03-30 17:03:21.08438+00	3	9	22	Benjamín Linder	2	Modificado/a nombre.
339	2012-03-30 17:05:51.353439+00	3	9	103	Bilamp	3	
340	2012-03-30 17:08:26.113946+00	3	11	247	Abdelia Aleman	3	
341	2012-03-30 17:13:31.094024+00	3	11	86	Agustín Barrera Hernández	3	
342	2012-03-30 17:13:31.097917+00	3	11	239	Alejandro Arguello	3	
343	2012-03-30 17:13:31.099456+00	3	11	175	Alejandro Calderon	3	
344	2012-03-30 17:13:31.100868+00	3	11	63	Alejo Carazo	3	
345	2012-03-30 17:13:31.102315+00	3	11	191	Alvaro  Molina	3	
346	2012-03-30 17:13:31.10375+00	3	11	219	Benito Montalvan Gonzalez	3	
347	2012-03-30 17:13:31.10517+00	3	11	187	Bismarck Castro	3	
348	2012-03-30 17:13:31.106593+00	3	11	154	Carla Tellez Meza	3	
349	2012-03-30 17:13:31.107995+00	3	11	251	Carlos Coronel	3	
350	2012-03-30 17:13:31.109432+00	3	11	236	Celso Jose Mejia Serrano	3	
351	2012-03-30 17:13:31.110816+00	3	11	234	Damaris Granja	3	
352	2012-03-30 17:13:31.112213+00	3	11	232	Damaris Granja Sequeira	3	
353	2012-03-30 17:13:31.113614+00	3	11	151	Darvis Rioz Rizo	3	
354	2012-03-30 17:13:31.115004+00	3	11	261	Daysi Largaespada	3	
355	2012-03-30 17:13:31.116385+00	3	11	245	Desiree Montealegre	3	
356	2012-03-30 17:13:31.118001+00	3	11	171	Edmundo Quintanilla	3	
357	2012-03-30 17:13:31.119429+00	3	11	213	Eliana Castro Chavarria	3	
358	2012-03-30 17:13:31.120826+00	3	11	230	Elida Leyla Sevilla Urbina	3	
359	2012-03-30 17:13:31.122203+00	3	11	153	Ernesto Lopez Ruiz	3	
360	2012-03-30 17:13:31.123578+00	3	11	214	Erwin Byron Zepeda Lopez	3	
361	2012-03-30 17:13:31.124993+00	3	11	276	Evelin Benavidez Cordobas	3	
362	2012-03-30 17:13:31.126392+00	3	11	228	Eveliyn Amador Mejia	3	
363	2012-03-30 17:13:31.127793+00	3	11	270	Felix Peralta	3	
364	2012-03-30 17:13:31.129195+00	3	11	244	Gabriel Sanchez	3	
365	2012-03-30 17:13:31.130595+00	3	11	167	Gerald Medina	3	
366	2012-03-30 17:13:31.131982+00	3	11	42	Gerald Medina	3	
367	2012-03-30 17:13:31.133361+00	3	11	207	German A Vallejos Ruiz	3	
368	2012-03-30 17:13:31.134744+00	3	11	275	Guadalupe Martinez	3	
369	2012-03-30 17:13:31.136142+00	3	11	160	Guillaume Craig	3	
370	2012-03-30 17:13:31.137521+00	3	11	181	Guillermo Rivera	3	
371	2012-03-30 17:13:31.138891+00	3	11	260	Gunther Klatte	3	
372	2012-03-30 17:13:31.140345+00	3	11	254	Gustavo Adolfo	3	
373	2012-03-30 17:13:31.141727+00	3	11	238	Gustavo Molina	3	
374	2012-03-30 17:13:31.143358+00	3	11	226	Henry Francisco Gutierrez	3	
375	2012-03-30 17:13:31.144857+00	3	11	265	Ing.Alejandro Quintana	3	
376	2012-03-30 17:13:31.14633+00	3	11	183	Ing. Jaime Vega	3	
379	2012-03-30 17:15:00.054165+00	3	11	222	Jamileth del Socorro Rocha	3	
380	2012-03-30 17:15:00.079898+00	3	11	242	Javier Chamorro	3	
381	2012-03-30 17:15:00.082283+00	3	11	70	Jeannette Lopez Salgado	3	
382	2012-03-30 17:15:00.08419+00	3	11	97	Joel Zeledón Flores	3	
383	2012-03-30 17:15:00.085863+00	3	11	39	José Antonio Moreno Moreno	3	
384	2012-03-30 17:15:00.087618+00	3	11	279	Jose Benito Rodriguez	3	
385	2012-03-30 17:15:00.08927+00	3	11	47	José Benito Rodríguez	3	
386	2012-03-30 17:15:00.090985+00	3	11	229	Jose Esteba Leiva Garcia	3	
387	2012-03-30 17:15:00.093816+00	3	11	237	Jose Leonardo Vargas	3	
388	2012-03-30 17:15:00.096016+00	3	11	269	Jose Maria Blanco	3	
389	2012-03-30 17:15:00.097679+00	3	11	227	Jose Rodolfo Canales	3	
390	2012-03-30 17:15:00.099205+00	3	11	51	Juan Baptista Ramirez / Lic. Rodrigo Mantica	3	
391	2012-03-30 17:15:00.100761+00	3	11	65	Juan Gutierrez	3	
392	2012-03-30 17:15:00.102292+00	3	11	216	Juan Ramon Rivas Rodriguez Francisco Javier Hernandez (pres.)	3	
393	2012-03-30 17:15:00.10391+00	3	11	278	Julia Gutierrez Zeledon	3	
394	2012-03-30 17:15:00.106546+00	3	11	231	Julio Osman Cruz Salas	3	
395	2012-03-30 17:15:00.108629+00	3	11	233	Justo Picado	3	
396	2012-03-30 17:15:00.110755+00	3	11	186	Karla Hernandez Chanto	3	
397	2012-03-30 17:17:07.789867+00	3	11	263	Keitelle Campos	3	
398	2012-03-30 17:17:07.793872+00	3	11	138	Keitelle Campos	3	
399	2012-03-30 17:17:07.795964+00	3	11	268	Lal Marandin	3	
400	2012-03-30 17:17:07.798461+00	3	11	252	Leonardo Coca	3	
401	2012-03-30 17:17:07.800389+00	3	11	241	Leonardo Mayorga	3	
402	2012-03-30 17:17:07.801994+00	3	11	31	Lic. Karen Urcuyo Mejia (2)	3	
403	2012-03-30 17:17:07.803564+00	3	11	155	Lilian de Oporta	3	
404	2012-03-30 17:17:07.805079+00	3	11	246	Lizeth Zuniga	3	
405	2012-03-30 17:17:07.806761+00	3	11	152	Manuel de Jesus Cano Gutierrez	3	
406	2012-03-30 17:18:05.454302+00	3	11	128	Marcia Bernarda Rubí	3	
407	2012-03-30 17:18:05.457834+00	3	11	64	Marco Amador	3	
408	2012-03-30 17:18:05.459863+00	3	11	55	Marco Zavala	3	
409	2012-03-30 17:18:45.359733+00	3	11	253	Marcia Bernarda Rubí	2	Modificado/a nombre, direccion y area.
410	2012-03-30 17:20:57.627788+00	3	11	67	Yali Molina	3	
411	2012-03-30 17:20:57.632712+00	3	11	256	Yader Barrera	3	
412	2012-03-30 17:20:57.634931+00	3	11	211	Santos Felicito Lopez Herrera	3	
413	2012-03-30 17:20:57.637032+00	3	11	262	Rosario Sotelo	3	
414	2012-03-30 17:20:57.639251+00	3	11	218	Rommel Loaisiga	3	
415	2012-03-30 17:20:57.640788+00	3	11	170	Rody Zelaya	3	
416	2012-03-30 17:23:58.270075+00	3	11	217	Pedro Munoz Mendez	3	
417	2012-03-30 17:23:58.274702+00	3	11	210	Oscar Armando Mendoza Sosa	3	
418	2012-03-30 17:23:58.276715+00	3	11	215	Oscar Armando Mendez	3	
419	2012-03-30 17:23:58.278244+00	3	11	194	Nombre pendiente	3	
420	2012-03-30 17:23:58.279735+00	3	11	274	Nicolas Osorno	3	
421	2012-03-30 17:23:58.281211+00	3	11	173	Nestor Saavedra	3	
422	2012-03-30 17:23:58.282715+00	3	11	255	Nestor Castro	3	
423	2012-03-30 17:23:58.284258+00	3	11	224	Nelson Cruz Aviles	3	
424	2012-03-30 17:23:58.285749+00	3	11	182	Milton Oconor	3	
425	2012-03-30 17:23:58.287246+00	3	11	221	Miguel Angel Garcia Jarquin	3	
426	2012-03-30 17:23:58.288679+00	3	11	193	Mayra Azucena Lopez Sanchez	3	
427	2012-03-30 17:23:58.290192+00	3	11	225	Martha Lorena Perez Vargas	3	
428	2012-03-30 17:23:58.291708+00	3	11	240	Marlyng Buitrago	3	
429	2012-03-30 17:23:58.293175+00	3	11	235	Maritza Amador Reyes	3	
430	2012-03-30 17:23:58.294672+00	3	11	257	Maria Virginia Moncada	3	
431	2012-03-30 17:23:58.296188+00	3	11	117	Maria Margarita Espinosa	3	
432	2012-03-30 17:24:11.852424+00	3	11	94		3	
433	2012-03-30 17:33:46.856131+00	3	11	205	Ana María Arauz Oporta	2	Modificado/a nombre y area.
434	2012-03-30 17:34:20.959111+00	3	11	204	Ali de Jesús Rivera Úbeda	2	Modificado/a nombre y area.
435	2012-03-30 17:35:13.168895+00	3	11	267	Félix Enrique Rosales	2	Modificado/a nombre y area.
436	2012-03-30 17:37:35.365169+00	3	11	172	José Benito Rodríguez	2	Modificado/a nombre, cargo, direccion y area.
437	2012-03-30 17:41:14.173606+00	3	11	202	Celso M Arauz Gómez	2	Modificado/a nombre, cargo y area.
438	2012-03-30 17:41:47.993002+00	3	11	203	Elvin José Castro Gómez	2	Modificado/a nombre, cargo y area.
439	2012-03-30 17:43:42.481329+00	3	11	259	Günther Klatte	2	Modificado/a nombre, direccion y area.
440	2012-03-30 17:43:57.271921+00	3	11	197	Félix Alonso Morales Pérez	2	Modificado/a nombre y area.
441	2012-03-30 17:45:00.268401+00	3	9	3	Kahkabila / Turbina	2	Modificado/a fuente.
442	2012-03-30 17:45:22.239888+00	3	9	5	Pearl Lagoon / Turbina	2	Modificado/a fuente.
443	2012-03-30 17:45:50.29205+00	3	9	7	Bluefields / Turbinas	2	Modificado/a fuente.
444	2012-03-30 17:46:05.642697+00	3	9	9	Monkey Point / Turbina	2	Modificado/a fuente.
445	2012-03-30 17:46:33.84701+00	3	9	22	Benjamín Linder	2	Modificado/a fuente.
446	2012-03-30 17:47:08.367578+00	3	9	23	El Bote	2	Modificado/a fuente.
447	2012-03-30 17:47:35.053955+00	3	9	102	Yakalwás	2	Modificado/a fuente.
448	2012-03-30 17:48:16.834637+00	3	9	26	Bilampí	2	Modificado/a fuente.
449	2012-03-30 17:48:28.96903+00	3	9	27	Casa Quemada	2	Modificado/a fuente.
450	2012-03-30 17:51:35.068039+00	3	9	28	La Florida	2	Añadido/a "2012" Usuarios Beneficiados.
451	2012-03-30 17:53:18.658447+00	3	9	28	La Florida	2	Modificado/a fuente.
452	2012-03-30 17:54:50.739269+00	3	9	29	Las Nubes 	2	Modificado/a fuente.
453	2012-03-30 17:55:11.417086+00	3	9	30	Rio Bravo	2	Modificado/a fuente.
454	2012-03-30 17:55:25.387589+00	3	9	31	Salto Mollejones	2	Modificado/a fuente.
455	2012-03-30 17:55:48.954486+00	3	9	32	Salto Negro	2	Modificado/a fuente.
456	2012-03-30 17:56:01.366584+00	3	9	33	El Trebol	2	Modificado/a fuente.
457	2012-03-30 17:56:15.568637+00	3	9	24	Parques Eolicos Amayo 1 y 2	2	Modificado/a fuente.
458	2012-03-30 17:56:32.23886+00	3	9	25	Planta San Jacinto Tizate	2	Modificado/a fuente.
459	2012-03-30 17:56:45.214904+00	3	9	34	Planta Momotombo	2	Modificado/a fuente.
460	2012-03-30 17:57:01.186068+00	3	9	36	La Cumplida	2	Modificado/a fuente.
461	2012-03-30 17:57:16.61087+00	3	9	37	Las Canas 	2	Modificado/a fuente.
462	2012-03-30 17:57:35.58517+00	3	9	41	Generador Eólico Conectado a la red	2	Modificado/a fuente.
463	2012-03-30 17:57:51.382787+00	3	9	42	Generador Eólico Autónomo	2	Modificado/a fuente.
464	2012-03-30 17:58:05.198096+00	3	9	47	Reactor de Producción de Biodiesel	2	Modificado/a fuente.
465	2012-03-30 17:58:37.331571+00	3	9	48	LAS FLORES MASAYA	2	Modificado/a fuente.
466	2012-03-30 19:53:53.605934+00	3	9	30	Rio Bravo	2	Modificado/a fuente.
467	2012-03-30 20:28:42.029574+00	3	2	1	Organizaciones	1	
468	2012-03-30 20:29:00.965292+00	3	3	5	organizacion1	1	
469	2012-03-30 20:29:16.522235+00	3	3	5	organizacion1	2	Modificado/a is_staff y groups.
470	2012-03-30 21:04:05.724368+00	3	10	82	UNIRSE	2	Modificado/a user_id, fuente y actividad.
471	2012-03-30 21:04:34.530381+00	5	10	82	UNIRSE	2	Modificado/a user_id.
472	2012-03-30 21:10:45.582057+00	3	10	82	UNIRSE	2	Modificado/a user_id.
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY django_content_type (id, name, app_label, model) FROM stdin;
1	permission	auth	permission
2	group	auth	group
3	user	auth	user
4	message	auth	message
5	content type	contenttypes	contenttype
6	session	sessions	session
7	site	sites	site
8	log entry	admin	logentry
9	Sistema de Ejecución	organizaciones	sistemaejecucion
10	Organización	organizaciones	organizacion
11	Contacto	organizaciones	contacto
12	email	organizaciones	email
13	Pais	nomencladores	pais
14	Categoria	nomencladores	categoria
15	Tipo de Entidad	nomencladores	tipoentidad
16	Materia Prima	nomencladores	materiaprima
17	Vegetal	nomencladores	vegetal
18	Fuente Renovable	nomencladores	fuenterenovable
19	Indicador sectorial	indicadores	indicadoressectoriales
20	Imagen	organizaciones	galeriafotossistema
21	Usuarios Beneficiados	organizaciones	usuariosbeneficiarios
22	Beneficiarios	organizaciones	beneficiarios
23	Empleados	organizaciones	empleados
24	Responsable de la Organización	organizaciones	responsable
25	Proyecto de la Organización	organizaciones	proyecto
26	Imagen	organizaciones	galeriafotosproyecto
27	Beneficiarios	organizaciones	beneficiariosproyecto
28	Tipo de Actividad	nomencladores	actividad
29	kv store	thumbnail	kvstore
30	migration history	south	migrationhistory
31	Tipo de Indicador	indicadores	tipoindicador
32	Indicador	indicadores	indicador
33	valor indicador	indicadores	valorindicador
34	csv import	csvimport	csvimport
35	import model	csvimport	importmodel
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY django_session (session_key, session_data, expire_date) FROM stdin;
2a2867223f679e7ade6a6c3cd2d81d97	YWM3ZjEyMjNiYzJkYzJhMDgwMzFjOTdjY2E0YWVkMmFlYzUwMWRlMzqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLAXUu\n	2011-12-07 20:27:51.56973+00
51f16945059d9a4fe8867ed6ff67b636	YWM3ZjEyMjNiYzJkYzJhMDgwMzFjOTdjY2E0YWVkMmFlYzUwMWRlMzqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLAXUu\n	2011-12-08 19:13:26.309027+00
59bb5ad18af1a00465b996970b234f08	YWM3ZjEyMjNiYzJkYzJhMDgwMzFjOTdjY2E0YWVkMmFlYzUwMWRlMzqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLAXUu\n	2011-12-16 21:54:34.156241+00
9689b738c4c367ad4511ef9958fdd4c9	YWM3ZjEyMjNiYzJkYzJhMDgwMzFjOTdjY2E0YWVkMmFlYzUwMWRlMzqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLAXUu\n	2011-12-16 21:54:43.656509+00
016a415c5157761b6747028915bb978e	ZmYyYmNhZWU4MGEwNWE3YjZkNDAyMzVmZTBiOTNmOTQwMDdhYjFlZDqAAn1xAVUKdGVzdGNvb2tp\nZXECVQZ3b3JrZWRxA3Mu\n	2011-12-31 00:41:12.962438+00
873c44e6f67517b48a3e5b946d35f060	ZmYyYmNhZWU4MGEwNWE3YjZkNDAyMzVmZTBiOTNmOTQwMDdhYjFlZDqAAn1xAVUKdGVzdGNvb2tp\nZXECVQZ3b3JrZWRxA3Mu\n	2012-01-20 21:48:29.019762+00
b569baf6b56b1ef3e34e2eb2f03ba4ea	ZmYyYmNhZWU4MGEwNWE3YjZkNDAyMzVmZTBiOTNmOTQwMDdhYjFlZDqAAn1xAVUKdGVzdGNvb2tp\nZXECVQZ3b3JrZWRxA3Mu\n	2012-01-20 22:34:10.294341+00
48cc56b7e3db282fa887cd2a003a3eff	NmNlMmJjYWM3NDgxYmRkYjFhYzU1OTUyNzdmMmQ2NDgwNWQyMWJhNDqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLAnUu\n	2012-01-20 22:40:24.892445+00
d25bc6632d968f9f70fada1f5355f01c	NmNlMmJjYWM3NDgxYmRkYjFhYzU1OTUyNzdmMmQ2NDgwNWQyMWJhNDqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLAnUu\n	2012-01-20 23:03:53.109247+00
b7a39a987f0fa42b7aaa3ccbb0329ede	ZmYyYmNhZWU4MGEwNWE3YjZkNDAyMzVmZTBiOTNmOTQwMDdhYjFlZDqAAn1xAVUKdGVzdGNvb2tp\nZXECVQZ3b3JrZWRxA3Mu\n	2012-01-23 16:03:18.746198+00
e327c57d74be4d24f218b6bde511009a	MDE3ZWEwZDg1YzQ2NjllNzM3YjRhOGMxODViNTAzNTM1NDk4MGVmZjqAAn1xAVUKdGVzdGNvb2tp\nZVUGd29ya2VkcQJzLg==\n	2012-01-23 20:18:04.6781+00
c171ec90a78a15d618f14a62b1ad2625	YWM3ZjEyMjNiYzJkYzJhMDgwMzFjOTdjY2E0YWVkMmFlYzUwMWRlMzqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLAXUu\n	2012-02-15 16:03:17.076743+00
c31501dc30d4e9fbde9ffe222cd7852f	YWM3ZjEyMjNiYzJkYzJhMDgwMzFjOTdjY2E0YWVkMmFlYzUwMWRlMzqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLAXUu\n	2012-03-06 16:50:43.034014+00
9c925a318ec22f56458039217b269832	YWM3ZjEyMjNiYzJkYzJhMDgwMzFjOTdjY2E0YWVkMmFlYzUwMWRlMzqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLAXUu\n	2012-02-15 16:21:09.801898+00
342d8e40ac9942c7366eb5e4510049b3	M2VkMjc5MzBmMzVlYWQwOTBlOWY3OWZiYThiOGEwNzU4ZTI2MTVlZjqAAn1xAS4=\n	2012-03-06 18:25:57.981886+00
5a773adf649590b5c9b64f470fa9a954	ZmYyYmNhZWU4MGEwNWE3YjZkNDAyMzVmZTBiOTNmOTQwMDdhYjFlZDqAAn1xAVUKdGVzdGNvb2tp\nZXECVQZ3b3JrZWRxA3Mu\n	2012-03-07 23:34:30.641954+00
821dc0aae6627a97a18ae730c79a3d06	YWM3ZjEyMjNiYzJkYzJhMDgwMzFjOTdjY2E0YWVkMmFlYzUwMWRlMzqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLAXUu\n	2012-02-15 19:24:35.66086+00
f6e40cc6f5a37d1dd626943f95a0edf9	YWM3ZjEyMjNiYzJkYzJhMDgwMzFjOTdjY2E0YWVkMmFlYzUwMWRlMzqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLAXUu\n	2012-03-14 17:37:06.518211+00
b321cb0a88f6d6239859564f409b55ab	M2VkMjc5MzBmMzVlYWQwOTBlOWY3OWZiYThiOGEwNzU4ZTI2MTVlZjqAAn1xAS4=\n	2012-04-11 23:56:40.548583+00
5f26d311901e57c49b91727ca2e04076	M2VkMjc5MzBmMzVlYWQwOTBlOWY3OWZiYThiOGEwNzU4ZTI2MTVlZjqAAn1xAS4=\n	2012-02-15 22:00:46.857302+00
b71321fd87919492f6f0863ea5ed88e1	YWM3ZjEyMjNiYzJkYzJhMDgwMzFjOTdjY2E0YWVkMmFlYzUwMWRlMzqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLAXUu\n	2012-02-15 23:44:51.042812+00
77ca560bed5ed0438d12d6ad276fe876	ZmYyYmNhZWU4MGEwNWE3YjZkNDAyMzVmZTBiOTNmOTQwMDdhYjFlZDqAAn1xAVUKdGVzdGNvb2tp\nZXECVQZ3b3JrZWRxA3Mu\n	2012-03-02 15:33:08.940254+00
de60396317cfb57951952383f6bcfa3d	ZmYyYmNhZWU4MGEwNWE3YjZkNDAyMzVmZTBiOTNmOTQwMDdhYjFlZDqAAn1xAVUKdGVzdGNvb2tp\nZXECVQZ3b3JrZWRxA3Mu\n	2012-03-02 15:34:00.43463+00
e2924c39145e5910d4aaa51f9803bfe0	YWM3ZjEyMjNiYzJkYzJhMDgwMzFjOTdjY2E0YWVkMmFlYzUwMWRlMzqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLAXUu\n	2012-03-02 15:57:54.568044+00
e4c22b586b714530636f0f0ea576aa1f	M2VkMjc5MzBmMzVlYWQwOTBlOWY3OWZiYThiOGEwNzU4ZTI2MTVlZjqAAn1xAS4=\n	2012-03-14 20:54:20.704388+00
a07adf1e68503cb66880c5f39c0b94fb	ZmYyYmNhZWU4MGEwNWE3YjZkNDAyMzVmZTBiOTNmOTQwMDdhYjFlZDqAAn1xAVUKdGVzdGNvb2tp\nZXECVQZ3b3JrZWRxA3Mu\n	2012-03-14 21:50:47.889411+00
abc6abad6a1c1ae6bb25b600b8d102a1	YWM3ZjEyMjNiYzJkYzJhMDgwMzFjOTdjY2E0YWVkMmFlYzUwMWRlMzqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLAXUu\n	2012-03-02 18:29:47.948358+00
1735021d24bd03c00734023d5c3562bf	YWM3ZjEyMjNiYzJkYzJhMDgwMzFjOTdjY2E0YWVkMmFlYzUwMWRlMzqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLAXUu\n	2012-03-20 15:36:20.796858+00
9ab4f98aed5ae3ae8fa9c3427f5915fd	YWM3ZjEyMjNiYzJkYzJhMDgwMzFjOTdjY2E0YWVkMmFlYzUwMWRlMzqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLAXUu\n	2012-03-28 15:23:31.349791+00
fa700ac7d44af43e01acc49749f043cd	M2VkMjc5MzBmMzVlYWQwOTBlOWY3OWZiYThiOGEwNzU4ZTI2MTVlZjqAAn1xAS4=\n	2012-03-02 23:29:50.737667+00
aabc99ea4be7c3ec62bc097aaf6087ec	YWM3ZjEyMjNiYzJkYzJhMDgwMzFjOTdjY2E0YWVkMmFlYzUwMWRlMzqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLAXUu\n	2012-03-29 13:41:52.531402+00
f7ec85984cb5f23b983d52327981ffb3	YWM3ZjEyMjNiYzJkYzJhMDgwMzFjOTdjY2E0YWVkMmFlYzUwMWRlMzqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLAXUu\n	2012-03-29 22:16:58.601399+00
e4f07aebdc88b87c6da926de7575d4d3	M2VkMjc5MzBmMzVlYWQwOTBlOWY3OWZiYThiOGEwNzU4ZTI2MTVlZjqAAn1xAS4=\n	2012-03-29 21:19:08.291788+00
ad414fa150a898ae5517f78de13f726c	YWM3ZjEyMjNiYzJkYzJhMDgwMzFjOTdjY2E0YWVkMmFlYzUwMWRlMzqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLAXUu\n	2012-04-02 22:50:24.241117+00
84ca3193d876032ddca5ade45587e0d0	ZmYyYmNhZWU4MGEwNWE3YjZkNDAyMzVmZTBiOTNmOTQwMDdhYjFlZDqAAn1xAVUKdGVzdGNvb2tp\nZXECVQZ3b3JrZWRxA3Mu\n	2012-04-02 23:12:52.869537+00
5cdea12b0da793491c63bd8aa4d1ced5	ZmYyYmNhZWU4MGEwNWE3YjZkNDAyMzVmZTBiOTNmOTQwMDdhYjFlZDqAAn1xAVUKdGVzdGNvb2tp\nZXECVQZ3b3JrZWRxA3Mu\n	2012-04-08 15:40:45.397228+00
a671780e9c22b2120efe56543b1d47e0	YWM3ZjEyMjNiYzJkYzJhMDgwMzFjOTdjY2E0YWVkMmFlYzUwMWRlMzqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLAXUu\n	2012-04-09 02:26:10.457378+00
4e813e7a76083b58d62971d01801a427	MmFhZTEwNTg2Y2Q1ZmY4MDI4ZWViZjllYTc0OGNjMWU5MmY0Yzk1ZTqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLA3Uu\n	2012-04-15 04:57:48.474626+00
2a6724842c5822a21204cb317d8eb781	YWM3ZjEyMjNiYzJkYzJhMDgwMzFjOTdjY2E0YWVkMmFlYzUwMWRlMzqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLAXUu\n	2012-04-11 23:56:57.189872+00
b131b035d3207a23d356662af36de49c	ZTcwMzYxMGY5MWFjMjk4MTM3ZGQ2OGRhY2Q0ZjM2YjVkZWFhNGMxODqAAn1xAShVCnRlc3Rjb29r\naWVxAlUGd29ya2VkcQNVDV9hdXRoX3VzZXJfaWRLAVUSX2F1dGhfdXNlcl9iYWNrZW5kVSlkamFu\nZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHUu\n	2012-04-12 00:18:47.611855+00
d2d47085fc9dd6b5118692e99e47146f	ZTcwMzYxMGY5MWFjMjk4MTM3ZGQ2OGRhY2Q0ZjM2YjVkZWFhNGMxODqAAn1xAShVCnRlc3Rjb29r\naWVxAlUGd29ya2VkcQNVDV9hdXRoX3VzZXJfaWRLAVUSX2F1dGhfdXNlcl9iYWNrZW5kVSlkamFu\nZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHUu\n	2012-04-12 15:01:08.008659+00
67456a347d10607f0ce368998707933c	MDE3ZWEwZDg1YzQ2NjllNzM3YjRhOGMxODViNTAzNTM1NDk4MGVmZjqAAn1xAVUKdGVzdGNvb2tp\nZVUGd29ya2VkcQJzLg==\n	2012-04-13 02:04:05.651367+00
9aa78ff7ecada9e4e93195ecdbc2ab93	YWYwNzRkZmMwY2EyOGI1NDMzNDVkODIzZGJkNmNlMGVlNWEyMjFkODqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLBHUu\n	2012-04-13 16:49:48.687374+00
\.


--
-- Data for Name: django_site; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY django_site (id, domain, name) FROM stdin;
1	example.com	example.com
\.


--
-- Data for Name: indicadores_indicador; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY indicadores_indicador (id, nombre, tipoindicador_id, nombrelargo, explicacion, fuentes, unidad, icono) FROM stdin;
1	% Renovables en Nicaragua	1	% de Energía Renovable en la Matriz Energética Nacional	% de la energía bruta generada con fuentes renovables en Nicaragua en total (electricidad, transporte y usos térmicos).	INE, Asociación Renovables	% de renovables	indicador/icono1.png
2	Población con Energía Renovable	1	Población nicaragüense con acceso a energía renovable	Estimación de la parte de la población nicaragüense con acceso a la energia limpia. Se calcula con base al % de energía renovable en la matriz energética nacional.	INIDE, Asociación Renovables	Personas	indicador/icono2.png
5	Ahorro a la Factura Petrolera Nacional	3	Ahorro a la Factura Petrolera Nacional	Es la cantidad monetaria del precio de las toneladas de petróleo ahorradas anualmente (12 meses) gracias a la producción de energía a base de renovables en Nicaragua. 	BCN, Asociación Renovables	En USD $	indicador/icono5.png
6	Empleos Generados	3	Empleos Generados por el Sector Energía Renovable en Nicaragua	Cantidad de empleos directos de las instituciones y empresas del sector + estimación de los empleos indirectos relacionados. 	Asociación Renovables	Personas	indicador/icono6.png
7	Población Beneficiada	4	Beneficiarios/as de programas en Energía Renovable en Nicaragua	Personas beneficiadas por los programas de Energía renovable: programas del Gobierno, programa de RSE de las empresas del sector, beneficiarios de proyectos ONG, participantes a talleres, foros, eventos de EERR en Nicaragua. 	MEM, Asociación Renovables	Personas	indicador/icono7.png
8	Precio actual promedio de la energía eléctrica en Nicaragua	5	Precio actual promedio de la energía eléctrica en Nicaragua		Fuente: BCN	C$ / kWh	
9	Precio estimado de la energía eléctrica con una matriz 100% Renovable	5	Precio estimado de la energía eléctrica con una matriz 100% Renovable		Fuente: Asociación Renovables	C$ / kWh	
3	Reducción de GEI	2	tCO2 ahorradas en Nicaragua	Son las toneladas equivalentes de CO2 que se ahorran anualmente por la sustitución con fuentes fósiles de los GWh producidos anualmente con fuentes renovables.	INE, Asociación Renovables	Toneladas de CO2eq	indicador/icono3.png
4	Reducción de GEI	2	Valor monetario de la reducción anual de emisiones de GEI	Se calcula con el valor mencionado arriba en toneladas por el precio de la tCO2 en el mercado del CO2 (valor promedio anual)	Bloomberg, USG, Asociación Renovables	En USD $	indicador/icono4.png
\.


--
-- Data for Name: indicadores_indicadoressectoriales; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY indicadores_indicadoressectoriales (anho) FROM stdin;
2000
2001
2002
2003
2004
2005
2006
2007
2008
2009
2010
2011
\.


--
-- Data for Name: indicadores_tipoindicador; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY indicadores_tipoindicador (id, nombre) FROM stdin;
1	Acceso y Cobertura
2	Impacto Ambiental
3	Económico
4	Lucha Contra la Pobreza Energética
5	Precios
\.


--
-- Data for Name: indicadores_valorindicador; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY indicadores_valorindicador (id, anho_id, indicador_id, valor) FROM stdin;
1	2000	1	18.3300
2	2000	2	934463.0000
3	2000	3	373320.0000
4	2000	4	12144286.0000
5	2000	5	218318850.0000
6	2000	6	900.0000
7	2000	7	10000.0000
8	2000	8	4.1000
9	2000	9	3.4000
10	2001	1	18.2300
11	2001	2	942218.0000
12	2001	3	405630.0000
13	2001	4	13584265.0000
14	2001	5	202882680.0000
15	2001	6	1100.0000
16	2001	7	15000.0000
17	2001	8	4.2000
18	2001	9	3.4000
19	2002	1	24.6100
20	2002	2	1289343.0000
21	2002	3	580284.0000
22	2002	4	18463824.0000
23	2002	5	186291950.0000
24	2002	6	1300.0000
25	2002	7	20000.0000
26	2002	8	4.3000
27	2002	9	3.4000
28	2003	1	27.0700
29	2003	2	1437282.0000
30	2003	3	675441.0000
31	2003	4	17934174.0000
32	2003	5	247024080.0000
33	2003	6	1500.0000
34	2003	7	25000.0000
35	2003	8	4.4000
36	2003	9	3.4000
37	2004	1	27.7300
38	2004	2	1491874.0000
39	2004	3	729099.0000
40	2004	4	17604460.0000
41	2004	5	287647960.0000
42	2004	6	1700.0000
43	2004	7	30000.0000
44	2004	8	4.5000
45	2004	9	3.4000
46	2005	1	34.6100
47	2005	2	1886383.0000
48	2005	3	950391.0000
49	2005	4	22931699.0000
50	2005	5	383503890.0000
51	2005	6	1900.0000
52	2005	7	35000.0000
53	2005	8	4.6000
54	2005	9	3.4000
55	2006	1	30.0200
56	2006	2	1657885.0000
57	2006	3	847638.0000
58	2006	4	20261430.0000
59	2006	5	475873200.0000
60	2006	6	2100.0000
61	2006	7	40000.0000
62	2006	8	4.7000
63	2006	9	3.4000
64	2007	1	28.9900
65	2007	2	1622135.0000
66	2007	3	837432.0000
67	2007	4	15295905.0000
68	2007	5	520474650.0000
69	2007	6	2300.0000
70	2007	7	45000.0000
71	2007	8	4.8000
72	2007	9	3.4000
73	2008	1	35.5400
74	2008	2	2014727.0000
75	2008	3	1074978.0000
76	2008	4	16701068.0000
77	2008	5	681901740.0000
78	2008	6	2500.0000
79	2008	7	50000.0000
80	2008	8	4.9000
81	2008	9	3.4000
82	2009	1	30.9100
83	2009	2	1774945.0000
84	2009	3	961029.0000
85	2009	4	9811232.0000
86	2009	5	453995880.0000
87	2009	6	2700.0000
88	2009	7	55000.0000
89	2009	8	5.0000
91	2010	1	36.9800
92	2010	2	2150572.0000
93	2010	3	1217970.0000
94	2010	4	14016715.0000
95	2010	5	575042040.0000
96	2010	6	2900.0000
97	2010	7	60000.0000
98	2010	8	5.1000
99	2010	9	3.4000
100	2011	1	38.8000
101	2011	2	2284350.0000
102	2011	3	1350000.0000
103	2011	4	12226108.0000
104	2011	5	721760000.0000
105	2011	6	3100.0000
106	2011	7	65000.0000
107	2011	8	5.2000
108	2011	9	3.4000
90	2009	9	3.4000
\.


--
-- Data for Name: nomencladores_actividad; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY nomencladores_actividad (id, nombre) FROM stdin;
2	Generación
3	Asesoría Legal
4	Distribución de energía
5	Educación / Capacitación
6	Fabricación de Tecnología
7	Gobierno
8	Investigación
9	Mantenimiento
10	Organización gremial
11	Servicios energéticos
1	Construcción
\.


--
-- Data for Name: nomencladores_categoria; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY nomencladores_categoria (id, nombre) FROM stdin;
\.


--
-- Data for Name: nomencladores_fuenterenovable; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY nomencladores_fuenterenovable (id, nombre, imagen) FROM stdin;
7	Geotérmico	fuente-renovable/iconmapa1.png
6	Biomasa	fuente-renovable/iconmapa3.png
5	Solar	fuente-renovable/iconmapa5.png
4	Hidroeléctrico	fuente-renovable/iconmapa2.png
3	Eólico	fuente-renovable/iconmapa4.png
1	N/A	fuente-renovable/otra.png
\.


--
-- Data for Name: nomencladores_materiaprima; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY nomencladores_materiaprima (id, nombre) FROM stdin;
1	carbon
2	BioDiesel
3	N/A
\.


--
-- Data for Name: nomencladores_pais; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY nomencladores_pais (id, nombre) FROM stdin;
1	Nicaragua
\.


--
-- Data for Name: nomencladores_tipoentidad; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY nomencladores_tipoentidad (id, nombre) FROM stdin;
1	ONG nacional
2	Empresa Nacional
3	Empresa Internacional
4	Academia
\.


--
-- Data for Name: nomencladores_vegetal; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY nomencladores_vegetal (id, nombre) FROM stdin;
\.


--
-- Data for Name: organizaciones_beneficiarios; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY organizaciones_beneficiarios (id, organizacion_id, anho, numero) FROM stdin;
\.


--
-- Data for Name: organizaciones_beneficiariosproyecto; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY organizaciones_beneficiariosproyecto (id, proyecto_id, anho, numero) FROM stdin;
\.


--
-- Data for Name: organizaciones_contacto; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY organizaciones_contacto (id, organizacion_id, categoria_id, nombre, cargo, skype, web, direccion, telefono, area, pais_id, correo) FROM stdin;
142	1	\N	Roberto Sosa	Consultor/Facilitador	\N	\N	CHF International | Nicaragua, Edificio Discover II, Villa Fontana, Modulo 5B, frente al Club Terraza	claro 88513926 / movistar 83960005/ (+505) 2278.1239 / 2278.5645		1	robertososa09@gmail.com
143	1	\N	Adriana Romero		\N	\N	Agricorp 2c Lago, Rotulo de Comanic Oficinas de Music &More	888-02225		1	adrianaromero87@hotmail.com
144	1	\N	Moises Lopez	Gerente General	\N	\N		88531220		1	
145	1	\N	Carlos Cortez	Proyectos	\N	\N		89886687		1	caceo6@hotmail.com
146	2	\N	Sean Porter	Gerente General	\N	\N		89276279		1	sean.porter@aeienergy.com
147	2	\N	Mariana Barrios Jackmann	Coordinadora	\N	\N	De Pizza Hut Villa Fontana, 1c abajo.	89603500/ (505)-89276278		1	mariana.barrios@aeienergy.com
148	2	\N	Xochylt Jerez	Asistente de Gerencia	\N	\N		(505) 88315947		1	
198	29	\N	Yolanda M.Romero Bracamonte		\N	\N		89112497		1	
199	29	\N	Gustavo Soza Gonzalez	Promotor Ambiental	\N	\N	Wanawas - Rio Blanco, Matagalpa	86028690		1	gustavosozagonzales@yahoo.es
200	29	\N	Ivett Arauz	Vocal	\N	\N	La Isla - Rio Blanco, Matagalpa	89034568		1	
201	29	\N	Miguel Molinarez Duarte	Vocal	\N	\N	Wanawas - Rio Blanco, Matagalpa	84973901		1	
206	30	\N	German A Vallejos Ruiz	Contador	\N	\N	Pantasma, Jinotega	86057484		1	
205	29	\N	Ana María Arauz Oporta	Secretaria JD			La Isla - Rio Blanco, Matagalpa	89199378	 	1	
208	30	\N	Rosa Argentina Sobalvarro Pineda	Vise Presidente	\N	\N	Pantasma, Jinotega	89212533		1	
209	30	\N	Enrique Montenegro		\N	\N	Pantasma, Jinotega	84317368		1	
212	32	\N	Agustin Barrera Hernandez	Gerente	\N	\N	Kubaly Central, RAAN	89333886		1	
204	30	\N	Ali de Jesús Rivera Úbeda	Presidente			Pantasma, Jinotega	86319989/86712022	 	1	aliriveraubeda@yahoo.com
267	5	\N	Félix Enrique Rosales	Coordinador de Proyectos			Aleyda 86172076	27829071/84327952/27722030	 	1	frosher5@yahoo.com,  atder.ecner@gmail.com
202	29	\N	Celso M Arauz Gómez	 			La Isla - Rio Blanco, Matagalpa	89115990	 	1	
203	30	\N	Elvin José Castro Gómez	 			Pantasma, Jinotega	84065869	 	1	pchhisajomasa@gmail.com
220	34	\N			\N	\N				1	pchminicentraleltrebol@gmail.com
197	29	\N	Félix Alonso Morales Pérez	Gerente, Presidente			Bilampi - Rio Blanco, Matagalpa	(505) 8931-9847	 	1	mfelixalonso@yahoo.com
223	35	\N	Joel Zeledon Flores		\N	\N	Puerto Viejo, RAAN	86375659		1	jozflores83@yahoo.com
271	66	\N	Lic. Karen Urcuyo Mejia	Responsable Administrativa	\N	\N		2276-8840 Ext. 107 - 131 / 88833133		1	cerrofrio_admon@turbonett.com.ni, kurcuyo@gmail.com
272	10	\N	Oscar Sanchez	Asesor Juridico	\N	\N		2253-8440 /49		1	osanchez@chnenergia.com
273	10	\N	Ruth Chavarria	Asistente de Gerencia	\N	\N		505-22538440 -49 / 505-8739-6761		1	rchavarria@chnenergia.com
277	13	\N	Jose Antonio Moreno Moreno	Presidente de la SA	\N	\N	Wiwili, Jinotega	84373113		1	josemorenomoreno@yahoo.com
149	2	\N	Cesar Zamora		\N	\N				1	Cesar.Zamora@aeienergy.com
150	2	\N	Roberto Villavicencio	Contabilidad	\N	\N		2293-5033		1	roberto.villavicencio@aeienergy.com
253	43	\N	Marcia Bernarda Rubí	Docente			 	2311 - 20382 83224186	 	1	mbrubi@yahoo.es
259	18	\N	Günther Klatte	Coordinador Tecnico			 	89870021	 	1	guentherklatte@gmx.de
156	5	\N	Doribel Morales	Administradora	\N	\N	El Cua, Jinotega	27829071		1	
157	5	\N	Abner Taleno		\N	\N	El Cua, Jinotega	27819017		1	atatderbl@hotmail.com
158	5	\N	Rebeca Leaf	Presidenta	\N	\N	El Cua, Jinotega	27829017/27722030		1	atder.bl@gmail.com
159	5	\N	Aleyda Morales Quintero	Administradora ATDER- Matagalpa	\N	\N	Matagalpa, Matagalpa	2772-2030		1	amqatder@ibw.com.ni
161	6	\N	Alvaro Baltodano		\N	\N				1	alvarobaltodano@gmail.com
162	6	\N	Carlos Melendez	Gerente General	\N	\N	Estatua Montoya 2c abajo, 20vrs al lago, Managua	2250-2507, 2268-0637		1	carlos.melendez@bluepowerenergy.net
163	8	\N	Moises Lopez	Project Manager	\N	\N		8853-1220		1	mlopez@cerrocoloradopower.com
164	10	\N	Roberto Abreu de Aguiar	Gerente General	\N	\N				1	raaguiar@chnenergia.com
243	39	\N	Maria Margarita Espinosa	Sector Energia	\N	\N		22706400/88505899		1	mespinosa@pronicaragua.org.ni
248	41	\N	Rodolfo Raudez	Representante Legal	\N	\N		8435 3056, 2278 2630 y 2278 6644		1	rodolfo.raudez@sunisolar.com, rodolfo.raudez@gmail.com
249	41	\N	Douglas Gonzalez	Coordinador Tecnico	\N	\N		22783133 (UNI)		1	douglas.gonzalez@sunisolar.com
250	41	\N	Karen Rojas	Contabilidad	\N	\N				1	karen.rojas@sunisolar.com
258	18	\N	Suyen Cordoba	Coordinadora	\N	\N		83799104		1	suyen.cordoba@gmail.com, suyen.cordoba@uni.edu.ni
264	46	\N	Keitelle Campos	Director de Proyectos	\N	\N		22781400		1	keycampos@yahoo.es
165	12	\N	Luis Lacayo	Gerente	\N	\N		88513219		1	ecamigg@ibw.com.ni, ecami.invest@ibw.com.ni
166	70	\N	Gerald Medina	Representante	\N	\N		2268-0089/ 84926690		1	geraldm3_09@hotmail.com
172	17	\N	José Benito Rodríguez	 			 	88937464 23114902	 	1	benitoleon06@gmail.com
168	15	\N	Gabriel Tapia	Manager	\N	\N		8979-2339		1	tapia@arctas.com
169	15	\N	Paola Perez	Admin	\N	\N		88876890		1	perezbelli@gmail.com
174	18	\N	Susan Kinne	Directora	\N	\N		22783133/ 86249350/Planta telefonica UNI:22781453		1	susankinnefenix@gmail.com
176	19	\N	Juan Baptista Ramirez / Lic. Rodrigo Mantica	gerente Proyecto Hidro Pantasma	\N	\N	Managua	505 2266-4157		1	rodrigo.mantica@hidropantasma.com
177	20	\N	Miguel Gonzalez Arauz	Presidente	\N	\N	Colegio Violeta Barrios de Chamorro 1/2 C. al Norte, Wapy.	(505) 8775-7211		1	mgonzalez6430@yahoo.com, hismow@gmail.com
178	20	\N	Socorro Sevilla G.	Gerente	\N	\N		88553454 / 86041273		1	hismowsa@yahoo.es
179	20	\N	Rodolfo		\N	\N		84379583		1	jrodolfoc11@yahoo.com
180	75	\N	Marco Zavala	Presidente	\N	\N				1	presidente@inde.org.ni
12	3	\N	José Luis Olivas	Gerente	\N	\N	Frente a Gasolinera Petronica. Sector B. San José de Bocay a 117 KM	89486901 / 27829069		1	\N
13	3	\N	Darvis Rioz Rizo	Contador	\N	\N	San Jose de Bocay, Jinotega	27829069/ 84101846		1	\N
14	3	\N	Manuel de Jesus Cano Gutierrez	Presidente	\N	\N	San Jose de Bocay, Jinotega	27829069/83609189		1	\N
15	3	\N	Ernesto Lopez Ruiz	Tecnico Electricista	\N	\N	San Jose de Bocay, Jinotega	27819069/87179702		1	\N
16	3	\N	Carla Tellez Meza	Fiscal	\N	\N	San Jose de Bocay, Jinotega	27829069		1	\N
17	4	\N	Jaime Muñoz	Director	\N	\N	Km 10 1/2 carretera norte, Entrada al nuevo CARNIC, Reparto Monte Criste ,Casa#374 esquinera, preguntar por oficinas de Asofenix	22513009/ 83319795/ 8447-2259		1	\N
18	4	\N	Lilian de Oporta	Apoyo	\N	\N		86668468		1	\N
24	7	\N	Guillaume Craig	Director	\N	\N	Vuelta San Pedro, Bluefields, RAAS	86070246, 2278-7522		1	\N
25	7	\N	Lâl Marandin	Oficina Managua	\N	\N	Shell Plaza El Sol 2c al sur, 202, Colonial Los Robles, Managua	22784182/ 86070146		1	\N
28	64	\N	José María Blanco	Director Regional	\N	\N				1	\N
29	64	\N	Félix Peralta	Técnico	\N	\N				1	\N
35	68	\N	Nicolás Osorno	Director de Desarrollo Empresarial e Innovación, (CONICYT)	\N	\N				1	\N
36	68	\N	Guadalupe Martínez	Secretaria Ejecutiva	\N	\N				1	\N
38	13	\N	Evelin Benavidez Córdobas	Tesorero	\N	\N	Wiwili, Jinotega	84487293		1	\N
40	13	\N	Julia Gutiérrez Zeledón	Miembro	\N	\N	Wiwili, Jinotega	86154257		1	\N
41	14	\N	José Benito Rodríguez		\N	\N		88937464 23114902		1	\N
45	16	\N	Rody Zelaya		\N	\N		22773372		1	\N
46	72	\N	Edmundo Quintanilla	Consultor Senior	\N	\N				1	\N
48	17	\N	Néstor Saavedra		\N	\N		88240968		1	\N
50	19	\N	Alejandro Calderon	Grupo Saret	\N	\N				1	\N
56	75	\N	Guillermo Rivera	Director Ejecutivo	\N	\N				1	\N
57	21	\N	Milton Oconor	Gerente de Fábrica	\N	\N	Km 148.5 Carretera el Viejo Potosi	22775100		1	\N
58	22	\N	Ing. Jaime Vega	Gerente Area de Producción	\N	\N	Empalme Km 121 carretera león-Chinandega, izquierda hasta el portón de Chichigalpa 5Km al sur	23429120 ext 3152		1	\N
59	23	\N	Jaime Saborío	Director	\N	\N				1	\N
60	23	\N	Evert Mendoza	Docente- Cursos Libres	\N	\N		2311-2584		1	\N
61	76	\N	Karla Hernandez Chanto		\N	\N		2283-8449 Ext.640 2283-8472		1	\N
62	76	\N	Bismarck Castro	Ingeniero Proyectos	\N	\N	Villa Fontana, Edificio Discover, Managua, Nic. M D-1	89208063 - 86880356-22703186		1	\N
66	26	\N	Alvaro  Molina	Gerente	\N	\N	Los Robles, Managua	Office: +(505) 2278-6045/ 2278-0919/ Celular # 8882-0184, Fax +(505) 2278-0832, Celular # 8883-7470		1	\N
68	27	\N	Mayra Azucena Lopez Sanchez		\N	\N	Centro Solar KM 212.5 carretera Managua-Sabana Grande. Totogalpa	8333 0197/   8360 8507		1	\N
69	28	\N	Nombre pendiente	Gerente Comercial	\N	\N	EDIFICIO LOS ROBLES ANTES(HSBC)TERCER PISO MODULO No.2 MANAGUA	2277-4762/22787937		1	\N
84	32	\N	Oscar Armando Mendoza Sosa	Presidente de la SA	\N	\N				1	\N
85	32	\N	Santos Felicito López Herrera	Tesorero, Promotor Ambiental	\N	\N	Kubaly Central, RAAN	89105532		1	\N
87	32	\N	Eliana Castro Chavarría	Cajera	\N	\N	Kubaly Central, RAAN	83634332		1	\N
88	32	\N	Erwin Byron Zepeda López	Contador	\N	\N	Kubaly Central, RAAN	84325988		1	\N
89	32	\N	Oscar Armando Méndez	Presidente	\N	\N	Kubaly Central, RAAN			1	\N
90	33	\N	Juan Ramon Rivas Rodriguez Francisco Javier Hernández (pres.)	Presidente de la SA	\N	\N		27929097 público/86468986 / 86601370		1	\N
91	33	\N	Pedro Muñoz Mendez	2do vocal	\N	\N				1	\N
92	33	\N	Rommel Loaisiga	Gerente	\N	\N	El Naranjo, RAAN	89346534		1	\N
93	33	\N	Benito Montalván González	Tesorero	\N	\N	El Naranjo, RAAN	87047496		1	\N
95	35	\N	Miguel Ángel García Jarquín	Presidente de la SA	\N	\N	Puerto Viejo-Waslala	Telefono Público para citar a la persona:27929104/84282861-86618776		1	\N
96	35	\N	Jamileth del Socorro Rocha	Cajera	\N	\N	Puerto Viejo, RAAN	84966553		1	\N
98	35	\N	Nelson Cruz Avilés	Gerente	\N	\N	Puerto Viejo, RAAN	84282861		1	\N
99	35	\N	Martha Lorena Pérez Vargas	Secretaria JD	\N	\N	Puerto Viejo, RAAN	86570508		1	\N
100	47	\N	Henry Francisco Gutiérrez	Secretario	\N	\N	Wapy - El Rama, El Rama - RAAS	86260927		1	\N
101	47	\N	José Rodolfo Canales	Promotor Ambiental	\N	\N	Wapy - El Rama, El Rama - RAAS	84377344		1	\N
102	47	\N	Eveliyn Amador Mejía	Fiscal	\N	\N	Wapy - El Rama, El Rama - RAAS	89441381		1	\N
103	47	\N	José Esteba Leiva García		\N	\N	Wapy - El Rama, El Rama - RAAS			1	\N
104	47	\N	Elida Leyla Sevilla Urbina		\N	\N	Wapy - El Rama, El Rama - RAAS	86628145		1	\N
105	36	\N	Julio Osmán Cruz Salas	Gerente, Promotor Ambiental	\N	\N		84973647		1	\N
106	36	\N	Damaris Granja Sequeira		\N	\N				1	\N
107	36	\N	Justo Picado	Presidente	\N	\N	El Guabo, Chontales	86241377		1	\N
108	36	\N	Damaris Granja	Gerente	\N	\N	El Guabo, Chontales	84434202		1	\N
109	36	\N	Maritza Amador Reyes	Secretaria JD	\N	\N	El Guabo, Chontales	86969481		1	\N
110	36	\N	Celso Jose Mejia Serrano	Tesorero	\N	\N	El Guabo, Chontales			1	\N
111	36	\N	Jose Leonardo Vargas	Vocal	\N	\N	El Guabo, Chontales			1	\N
112	37	\N	Gustavo Molina	Relaciones Publicas	\N	\N	Edificio No.7, Suite 735, tercer piso, Ofiplaza	88802225 /  2253-8340		1	\N
113	37	\N	Alejandro Arguello		\N	\N		88050002		1	\N
114	38	\N	Marlyng Buitrago	Gerente	\N	\N	Del nuevo PALI las Americas 75 varas al sur.	88834144 / 22806405 / 22806406		1	\N
115	38	\N	Leonardo Mayorga	Responsable Area	\N	\N		88835728		1	\N
116	39	\N	Javier Chamorro	Director Ejecutivo	\N	\N	Km 4.5 carretera Masaya del restaurante Tip-Top Los Robles 1c oeste frente a casa naranja			1	\N
118	39	\N	Gabriel Sánchez	Coordinador de Inteligencia de Mercadeo y Comunicaciones	\N	\N	Km 4.5 Cta Masaya 1c oeste, Edificio César, Managua	2270-6400 ext 109 / 88631053 /Skype: gabo.sanchezg		1	\N
119	39	\N	Desirée Montealegre	Analista de Inteligencia de Mercado	\N	\N	Km 4.5 Cta Masaya 1c oeste, Edificio César, Managua	2270-6400 ext 109 / 8850-6600 /Skype: montealegre.desiree		1	\N
120	39	\N	Janeth Jiménez	Asistente Javier Asesora de Inversiones/Turismo	\N	\N		2270 6400 ext 105 8387 5645 skype:janeth-jimenez		1	\N
121	40	\N	Lizeth Zúniga	Directora Ejecitiva	\N	\N	Shell Plaza El Sol 2c Sur Casa 202	2278-4182 / 83546179		1	\N
122	40	\N	Abdelia Aleman	Asistente Ejecutiva	\N	\N	Shell Plaza El Sol 2c Sur Casa 202	8713-0790/ 2278-4182		1	\N
126	42	\N	Carlos Coronel	Gerente	\N	\N	Tichana, Ometepe	84658378		1	\N
127	43	\N	Leonardo Coca	mercadeo.academico@ulsa.edu.ni	\N	\N				1	\N
129	43	\N	Gustavo Adolfo	Docente	\N	\N				1	\N
130	43	\N	Nestor Castro	ViceRector Acedemico	\N	\N		8426-3302		1	\N
131	44	\N	Yader Barrera	Docente, Facultad de Recursos Naturales y el ambiente	\N	\N		84579053		1	\N
132	45	\N	Maria Virginia Moncada	Profesora	\N	\N		/22672054/89880406		1	\N
135	82	\N	Matthias Dietrich	Director Ejecutivo	\N	\N		226613-38 / 8883-0493		1	\N
136	82	\N	Daysi Largaespada	Asistente	\N	\N				1	\N
137	46	\N	Rosario Sotelo		\N	\N	Sede Regional Ruben Dario. Costado Sur del Estadio Heróes y Martires. Bo. San Felipe	22781400/ 22785480/ 22786338		1	\N
139	46	\N	Ing.Alejandro Quintana		\N	\N		2278-1400, 2278-6338 / 2278-5480		1	\N
185	23	\N	Evert Mendoza	Docente- Cursos Libres	\N	\N		2311-2584		1	etonio61@yahoo.com
188	24	\N	Alejo Carazo	Asesor Legal	\N	\N	Villa Fontana, Edificio Discover, Managua, Nic. M D-1	89666611 - 8336-9969 -22703186		1	acarazo@mesoamericaenergy.com
189	24	\N	Marco Amador	Director de Desarrollo	\N	\N		8854-4000 / 2270-3186		1	marco.amador@mesoamericaenergy.com
190	77	\N	Juan Gutierrez	Presidente de la SA	\N	\N	Entrada Villa Soberana 1 1/2 C arriba.Calle los Italianos. Leon, Nic	23150475 /8698- 8884		1	mifogonleon@yahoo.com
192	26	\N	Yali Molina	Presidente	\N	\N				1	ymolina@central-law.com
195	28	\N	Jeannette Lopez Salgado	Especialista Ambiental	\N	\N		Tel: 22781024,  88861719		1	jelopez@ormat.com, jvlopezs@hotmail.com
196	29	\N	Reynaldo Alarcon Espinoza	Presidente de la SA	\N	\N	Wanawas - Rio Blanco, Matagalpa	Wanawas 27789026  / 27780295		1	pchbilampi@gmail.com
\.


--
-- Data for Name: organizaciones_email; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY organizaciones_email (id, contacto_id, email) FROM stdin;
\.


--
-- Data for Name: organizaciones_empleados; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY organizaciones_empleados (id, organizacion_id, anho, numero) FROM stdin;
\.


--
-- Data for Name: organizaciones_galeriafotosproyecto; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY organizaciones_galeriafotosproyecto (id, proyecto_id, imagen) FROM stdin;
\.


--
-- Data for Name: organizaciones_galeriafotossistema; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY organizaciones_galeriafotossistema (id, sistema_id, imagen) FROM stdin;
\.


--
-- Data for Name: organizaciones_organizacion; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY organizaciones_organizacion (id, tipo_id, nombre, nic, gremial, longitud, latitud, logo, renovable, asociacion, telefono, web, direccion, pais, user_id_id, geolocation) FROM stdin;
36	2	PCH Salto Negro - La Union S.A	f	f	\N	\N		t				El Guabo, Chontales	Nicaragua	1	\N
2	2	AMAYO, S.A	f	f	-86.2673950000	12.1256000000	/var/www/renovables/media/organizacion/Amayo.jpeg	t				Edificio frente a CLARO Villa Fontana. Primer piso.	Nicaragua	1	\N
5	1	ATDER – BL	f	f	-85.6717871509	13.3673608627	/var/www/renovables/media/organizacion/ATDER.jpg	t			http://www.atder-bl.org/		Nicaragua	1	\N
4	1	ASOFENIX	f	f	-86.1874010000	12.1497670000	/var/www/renovables/media/organizacion/AsoFenix.jpg	t			http://www.asofenix.org/	Km 10 1/2 carretera norte, Entrada al nuevo CARNIC, Reparto Monte Criste ,Casa#374 esquinera, preguntar por oficinas de Asofenix	Nicaragua	1	\N
6	2	BLUE POWER & ENERGY	f	f	-86.2900920000	12.1483740000		f			http://www.bluepowerenergy.com/	Estatua Montoya 2c abajo, 20vrs al lago, Managua	Nicaragua	1	\N
47	2	PCH Salto Mollejones - Wapy S.A	f	f	-84.3510061077	12.4233218000		t				Wapy, El Rama	Nicaragua	1	\N
14	2	ENICALSA	f	f	-86.8762820000	12.4399920000	/var/www/renovables/media/organizacion/enicalsa.jpg	t			http://www.enicalsa.com/		Nicaragua	1	\N
19	2	Hidro Pantasma - Grupo Saret	f	f	\N	\N		f					Nicaragua	1	\N
18	1	Grupo Fénix (UNI - PFAE)	f	f	-86.2701420000	12.1299630000	/var/www/renovables/media/organizacion/GFenix.jpg	t			http://www.grupofenix.org/	Managua, UNI	Nicaragua	1	\N
17	1	GIER	f	f	-86.8434910000	12.4791520000	/var/www/renovables/media/organizacion/GIER.jpg	t					Nicaragua	1	\N
21	2	Ingenio Monte Rosa - Pantaleon	f	f	\N	\N		f				Chinandega	Nicaragua	1	\N
23	4	IPLS	f	f	\N	\N	/var/www/renovables/media/organizacion/IPLS.png	t			http://www.ipls-lasalle.org/		Nicaragua	1	\N
28	2	ORMAT Momotombo Power Company Inc.	f	f	\N	\N		f					Nicaragua	1	\N
34	2	PCH Mini Central El Trebol	f	f	\N	\N		f					Nicaragua	1	\N
38	1	Proleña	f	f	-86.2354660000	12.1262710000	/var/www/renovables/media/organizacion/prolena.jpg	t		27929104 / 84282861 / 86618776	http://www.prolenaecofogon.org/		Nicaragua	1	\N
1	2	ACN, S.A	f	f	\N	\N		f					Nicaragua	1	\N
43	4	ULSA	f	f	-86.8743060000	12.4198130000	/var/www/renovables/media/organizacion/ULSA.png	t			http://www.ulsa.edu.ni/		Nicaragua	1	\N
45	4	UNI	f	f	\N	\N	/var/www/renovables/media/organizacion/UNI.jpg	t			http://www.uni.edu.ni/		Nicaragua	1	\N
44	4	UNA	f	f	-86.1631090000	12.1447320000	/var/www/renovables/media/organizacion/UNA.jpg	t			http://www.una.edu.ni/		Nicaragua	1	\N
22	2	Ingenio San Antonio	f	f	\N	\N		f					Nicaragua	1	\N
20	2	HISMOW, SA	f	f	-84.2184450000	12.2702310000	/var/www/renovables/media/organizacion/HISMOW.jpg	t					Nicaragua	1	\N
13	2	EMEEAW	f	f	-85.8178544341	13.6217029754		t					Nicaragua	1	\N
10	2	CHN, S.A	f	f	-86.2824050000	12.1339910000	/var/www/renovables/media/organizacion/CHN.jpg	t					Nicaragua	1	\N
37	2	POLARIS, SA	f	f	-86.2843410000	12.1215400000	/var/www/renovables/media/organizacion/LOGO_POLARIS.gif	t					Nicaragua	1	\N
32	2	PCH Kubaly La Florida S.A	f	f	-85.3615422480	13.4970953137		t				Kubaly Central, RAAN	Nicaragua	1	\N
35	2	PCH Rio Bravo Puerto Viejo S.A	f	f	-85.2445188164	13.5191066106		t		Telefono Público para citar a la persona: 2792-9104/ 842		Puerto Viejo, RAAN	Nicaragua	1	\N
33	2	PCH Las Nubes El Naranjo S.A	f	f	-85.1977788907	13.5791680812		t				El Naranjo, RAAN	Nicaragua	1	\N
7	1	blueEnergy	f	f	-83.7734450000	12.0065070000	/var/www/renovables/media/organizacion/bE.jpg	t		2278-4182 / 2572-2468	http://www.blueEnergygroup.org/	Vuelta San Pedro, Bluefields, RAAS	Nicaragua	1	\N
3	1	APRODELBO	t	f	-85.5392005660	13.5414313221	/var/www/renovables/media/organizacion/Aprodelbo.jpg	t				Frente a Gasolinera Petronica. Sector B. San José de Bocay a 117 KM		1	\N
42	2	TICHANA POWER, SA	f	f	-85.5019570000	11.4045650000		f					Nicaragua	1	\N
25	2	Mi Fogon, SA	f	f	-86.8818570000	12.4297660000		t					Nicaragua	1	\N
24	2	Mesoamerica Energy - GlobelEq, SA	f	f	-86.2628570000	12.1012370000	/var/www/renovables/media/organizacion/GLobeleq_Logo_CMYK.jpg	t			http://www.globeleqmesoamericaenergy.com/		Nicaragua	1	\N
16	3	Geonica - ENEL (It)	f	f	-86.2563500000	12.1154620000		f			http://www.enelgreenpower.com/en-GB/ela/power_plants/ongoing/nicaragua/		Nicaragua	1	\N
15	2	EOLO de Nicaragua, SA	f	f	-86.2659730000	12.1220280000	/var/www/renovables/media/organizacion/logo_eolonica.jpg	t					Nicaragua	1	\N
12	2	ECAMI, SA	f	f	-86.2423190000	12.0882260000	/var/www/renovables/media/organizacion/Ecami.jpg	t			http://www.ecamisa.com/		Nicaragua	1	\N
8	2	Cerro Colorado Power, SA	f	f	-86.2843410000	12.1215400000		f					Nicaragua	1	\N
64	\N	BUN-CA	f	f	\N	\N		f	\N	\N	\N	\N	\N	1	\N
68	\N	CONICYT	f	f	\N	\N		f	\N	\N	\N	\N	\N	1	\N
70	\N	ENOVA	f	f	\N	\N		f	\N	\N	\N	\N	\N	1	\N
72	\N	GESAWORLD	f	f	\N	\N		f	\N	\N	\N	\N	\N	1	\N
75	\N	INDE	f	f	\N	\N		f	\N	\N	\N	\N	\N	1	\N
76	\N	Mesoamérica Energy - GlobalEq	f	f	\N	\N		f	\N	\N	\N	\N	\N	1	\N
77	\N	Mi Fogon	f	f	\N	\N		f	\N	\N	\N	\N	\N	1	\N
29	2	PCH HIBIMUSUN S.A	f	f	-85.0396730000	12.9363070000	/var/www/renovables/media/organizacion/PCH_Bilampi.jpg	t				Bilampi, Rio Blanco, Wanawas Matagalpa	Nicaragua	1	\N
30	2	PCH HISAJOMA S.A	f	f	-85.8285702544	13.3653421828		t				Pantasma, Jinotega	Nicaragua	1	\N
41	2	SUNI SOLAR, SA	f	f	-86.2738340000	12.1223270000	/var/www/renovables/media/organizacion/SuniSolar.jpeg	t			http://www.sunisolar.com/		Nicaragua	1	\N
46	4	UTN	f	f	-86.2680820000	12.1306350000	/var/www/renovables/media/organizacion/UTN.jpg	t		2278-1400, 2278-6338 / 2278-5480	http://www.utn.edu.ni/		Nicaragua	1	\N
39	1	PRONICARAGUA	f	f	-86.2660220000	12.1244250000		t			http://pronicaragua.org/		Nicaragua	1	\N
26	2	MOLINA & ASOCIADOS	f	f	-86.2604860000	12.1228150000		t			http://www.molinalegal.com/		Nicaragua	1	\N
40	1	RENOVABLES	f	f	-86.2619930000	12.1287470000	/var/www/renovables/media/organizacion/Logo_chico.jpg	t			http://www.renovables.org.ni/	Plaza El Sol, 2c al Sur, #202, Los Robles Managua	Nicaragua	1	\N
27	1	Mujeres solares de Totogalpa	f	f	-86.4922070000	13.5623570000	/var/www/renovables/media/organizacion/LOGO_MUJERES_SOLARES.png	t			http://www.mujeressolares.org/		Nicaragua	1	\N
66	2	Energía Tropical Sostenible Cerro Frío, S.A	f	f	-85.8571240000	13.0002510000		t						1	\N
82	\N	UNIRSE	f	f	\N	\N		f						5	\N
\.


--
-- Data for Name: organizaciones_organizacion_actividad; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY organizaciones_organizacion_actividad (id, organizacion_id, actividad_id) FROM stdin;
1	66	2
2	29	2
3	30	2
4	41	11
5	46	5
6	39	10
7	26	3
8	40	10
10	27	6
13	82	2
\.


--
-- Data for Name: organizaciones_organizacion_fuente; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY organizaciones_organizacion_fuente (id, organizacion_id, fuenterenovable_id) FROM stdin;
251	66	4
252	29	4
253	30	4
254	41	3
255	41	5
256	46	1
257	39	1
258	26	1
259	40	1
261	27	5
264	82	1
140	38	6
143	45	1
144	44	1
145	43	1
204	4	3
158	34	4
164	28	7
205	4	4
170	23	1
171	22	6
172	21	6
173	20	4
206	4	5
175	19	4
176	18	5
177	18	6
178	17	3
179	17	4
207	4	6
209	2	3
211	1	3
212	1	4
213	36	4
180	17	5
221	13	4
181	17	6
184	14	3
224	10	4
225	37	7
185	14	4
229	32	4
230	35	4
231	33	4
232	47	4
233	7	3
234	7	5
235	7	6
236	3	4
237	5	4
186	14	5
238	42	4
240	25	6
241	24	3
242	16	7
243	15	3
244	12	3
245	12	4
246	12	5
247	12	6
249	8	7
250	6	3
\.


--
-- Data for Name: organizaciones_proyecto; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY organizaciones_proyecto (nombre, organizacion_id, monto, longitud, latitud, enlace, donante, geolocation) FROM stdin;
\.


--
-- Data for Name: organizaciones_proyecto_fuente; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY organizaciones_proyecto_fuente (id, proyecto_id, fuenterenovable_id) FROM stdin;
\.


--
-- Data for Name: organizaciones_responsable; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY organizaciones_responsable (organizacion_id, contacto_id) FROM stdin;
\.


--
-- Data for Name: organizaciones_sistemaejecucion; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY organizaciones_sistemaejecucion (id, nombre, instancias, vegetal_id, materia_prima_id, biocombustible, longitud, latitud, potencia, interconexion, energia_producida, tarifa, calor, organizacion_id, tmp, geolocation) FROM stdin;
2	Set Net Point / FV	1	\N	3	f	-83.4869580000	12.4563200000	500	f	1085	0	f	7	\N	\N
4	Kahkabila / FV	1	\N	3	f	-83.7260770000	12.3978920000	600	f	1193	0	f	7	\N	\N
6	Pearl Lagoon / FV	1	\N	3	f	-83.6735380000	12.3380100000	240	f	540	0	f	7	\N	\N
8	Bluefields / FV	1	\N	3	f	-83.7688210000	12.0046920000	1300	f	3136	0	f	7	\N	\N
10	Monkey Point / PV	1	\N	3	f	-83.6608890000	11.5974230000	1387	f	1937	0	f	7	\N	\N
11	Monkey Point / LED	43	\N	3	f	-83.6608890000	11.5974230000	10	f	54	0	f	7	\N	\N
12	Bangkukuk (Punta de Aguila) / FV	1	\N	3	f	-83.7264630000	11.5673210000	300	f	489	0	f	7	\N	\N
13	Bangkukuk (Punta de Aguila) / LED	35	\N	3	f	-83.7264630000	11.5673210000	10	f	47	0	f	7	\N	\N
14	Sumu Kaat / LED	30	\N	3	f	-84.0625760000	11.7577980000	10	f	44	0	f	7	\N	\N
15	Tiktik Kaanu / FV	1	\N	3	f	-83.9561460000	11.8942280000	10	f	1796	0	f	7	\N	\N
16	Tiktik Kaanu / LED	30	\N	3	f	-83.9561460000	11.8942280000	10	f	44	0	f	7	\N	\N
17	Wiring Cay / LED	12	\N	3	f	-83.6717890000	11.6988870000	10	f	18	0	f	7	\N	\N
18	Corn River / LED	27	\N	3	f	-83.8768390000	11.2939040000	10	f	39	0	f	7	\N	\N
19	Rama Cay / LED	11	\N	3	f	-83.8084320000	11.8811260000	10	f	16	0	f	7	\N	\N
20	Indian River / LED	23	\N	3	f	-83.9273070000	11.1123800000	10	f	34	0	f	7	\N	\N
21	Graytown / LED	2	\N	3	f	-83.7367630000	10.9489450000	10	f	3	0	f	7	\N	\N
1123	Santa Maria del Cedros	1	\N	\N	f	-85.5630655122	13.4612239100	80	f	0	0	f	12	39	\N
35	Sistema Solar	1	\N	3	f	-86.4922070000	13.5623560000	960	f	0	0	f	27	\N	\N
38	Sistema Fotovoltaico Conectado a la red	1	\N	3	f	-86.9045138880	12.4356611111	1000	t	0	0	f	23	\N	\N
44	Puerto Viejo- Rio Bravo	33	\N	3	f	-85.2104656380	13.5210033278	2640	t	0	0	f	12	\N	\N
45	El naranjo	33	\N	3	f	-85.1573169594	13.5664050229	2640	t	0	0	f	12	\N	\N
46	Wiwli	105	\N	3	f	-85.8178544341	13.6217029754	8400	t	0	0	f	12	\N	\N
49	NACIONAL	1	\N	3	f	-86.2865870000	12.1566970000	92400	f	202	0	f	41	\N	\N
50	MANAGUA	1	\N	3	f	-86.2865870000	12.1566970000	3170	f	7	0	f	41	\N	\N
51	JINOTEGA	1	\N	3	f	-86.0078570000	13.1259110000	7500	f	16	0	f	41	\N	\N
52	SAN JUAN DE NICARAGUA	1	\N	3	f	-83.7377120000	10.9458730000	3150	f	7	0	f	41	\N	\N
53	MADRIZ	1	\N	3	f	-86.5475460000	13.5178380000	27040	f	59	0	f	41	\N	\N
54	RAAN / EuroSolar	1	\N	3	f	-84.8987500000	13.7380400000	1120	f	2	0	f	41	\N	\N
57	RAAN / EuroSolar2	1	\N	3	f	-84.9166500000	13.5628300000	1120	f	2	0	f	41	\N	\N
58	RAAN / EuroSolar3	1	\N	3	f	-85.3028600000	13.5923100000	1120	f	2	0	f	41	\N	\N
59	RAAN / EuroSolar4	1	\N	3	f	-84.8634500000	13.8331600000	1120	f	2	0	f	41	\N	\N
60	RAAN / EuroSolar5	1	\N	3	f	-85.1452800000	13.5807500000	1120	f	2	0	f	41	\N	\N
61	RAAN / EuroSolar6	1	\N	3	f	-84.6106700000	13.6212300000	1120	f	2	0	f	41	\N	\N
62	RAAN / EuroSolar7	1	\N	3	f	-84.3475000000	13.9626900000	1120	f	2	0	f	41	\N	\N
63	RAAN / EuroSolar8	1	\N	3	f	-84.1355400000	14.0154000000	1120	f	2	0	f	41	\N	\N
64	RAAN / EuroSolar9	1	\N	3	f	-84.3375200000	13.8562700000	1120	f	2	0	f	41	\N	\N
65	RAAN / EuroSolar10	1	\N	3	f	-84.4248000000	14.0903200000	1120	f	2	0	f	41	\N	\N
66	RAAN / EuroSolar11	1	\N	3	f	-84.2916200000	13.5867100000	1120	f	2	0	f	41	\N	\N
67	RAAN / EuroSolar12	1	\N	3	f	-84.4079100000	13.5852700000	1120	f	2	0	f	41	\N	\N
68	RAAN / EuroSolar13	1	\N	3	f	-84.0729300000	14.4787500000	1120	f	2	0	f	41	\N	\N
69	RAAN / EuroSolar14	1	\N	3	f	-83.4274400000	14.1851800000	1120	f	2	0	f	41	\N	\N
70	RAAN / EuroSolar15	1	\N	3	f	-83.2874600000	14.4711200000	1120	f	2	0	f	41	\N	\N
24	Parques Eolicos Amayo 1 y 2	30	\N	3	f	-85.7215120000	11.3282430000	63000000	t	160300	0	f	2	\N	\N
40	Termosifón	1	\N	3	f	-86.9052305555	12.4366305555	400	f	0	0	f	23	\N	\N
39	Sistema Autónomo Fotovoltaico	3	\N	3	f	-86.9044750000	12.4360222222	3600	f	0	0	f	23	\N	\N
3	Kahkabila / Turbina	1	\N	3	f	-83.7260770000	12.3978920000	1000	f	1193	0	f	7	\N	\N
5	Pearl Lagoon / Turbina	1	\N	3	f	-83.6735380000	12.3380100000	500	f	540	0	f	7	\N	\N
7	Bluefields / Turbinas	3	\N	3	f	-83.7688210000	12.0046920000	3000	f	3136	0	f	7	\N	\N
9	Monkey Point / Turbina	1	\N	3	f	-83.6608890000	11.5974230000	1000	f	1937	0	f	7	\N	\N
22	Benjamín Linder	1	\N	3	f	-85.5392005661	13.5414335809	170	t	756	0	f	3	\N	\N
23	El Bote	1	\N	3	f	-85.5655404144	13.3944535931	900	t	3271	0	f	5	\N	\N
26	Bilampí	1	\N	3	f	-85.2318170015	13.0308356385	250	f	0	0	f	29	\N	\N
27	Casa Quemada	1	\N	3	f	-85.8244674518	13.3392776633	300	f	0	0	f	30	\N	\N
29	Las Nubes 	1	\N	3	f	-85.1573169594	13.5664050229	700	f	0	0	f	33	\N	\N
28	La Florida	1	\N	3	f	-85.3560173611	13.4486122484	350	f	0	0	f	32	\N	\N
31	Salto Mollejones	1	\N	3	f	-84.3478512786	12.4217325749	170	f	0	0	f	47	\N	\N
32	Salto Negro	1	\N	3	f	-84.8481221129	12.1781580279	210	f	0	0	f	36	\N	\N
33	El Trebol	1	\N	3	f	-84.8481221129	12.1781580279	0	t	0	0	f	34	\N	\N
25	Planta San Jacinto Tizate	1	\N	3	f	-86.8033220000	12.5971190000	10000000	t	68300	0	f	37	\N	\N
34	Planta Momotombo	1	\N	3	f	-86.5513230000	12.4097540000	77500000	t	199620	0	f	28	\N	\N
36	La Cumplida	1	\N	3	f	-85.8571230000	13.3000251000	250000	t	0	0	f	66	\N	\N
37	Las Canas 	1	\N	3	f	-85.8571240000	13.3000252000	1400000	t	0	0	f	66	\N	\N
41	Generador Eólico Conectado a la red	1	\N	3	f	-86.9035944444	12.4383611111	2250	t	0	0	f	23	\N	\N
42	Generador Eólico Autónomo	2	\N	3	f	-86.9035944444	12.4383611111	1400	f	0	0	f	23	\N	\N
47	Reactor de Producción de Biodiesel	2	\N	2	f	-86.9035944444	12.4383611111	0	f	6324	0	f	23	\N	\N
48	LAS FLORES MASAYA	1	\N	3	f	-86.0851870000	11.9604320000	2100	f	5	0	f	41	\N	\N
30	Rio Bravo	1	\N	3	f	-85.2104656380	13.5210033278	200	f	0	0	f	35	\N	\N
71	RAAN / EuroSolar16	1	\N	3	f	-83.7753100000	13.8733100000	1120	f	2	0	f	41	\N	\N
72	RAAN / EuroSolar17	1	\N	3	f	-83.6045700000	13.3168500000	1120	f	2	0	f	41	\N	\N
73	RAAN / EuroSolar18	1	\N	3	f	-83.5654700000	13.4066400000	1120	f	2	0	f	41	\N	\N
74	RAAN / EuroSolar19	1	\N	3	f	-83.5340000000	13.5569700000	1120	f	2	0	f	41	\N	\N
75	RAAN / EuroSolar20	1	\N	3	f	-83.5966200000	13.1693700000	1120	f	2	0	f	41	\N	\N
76	RAAN / EuroSolar21	1	\N	3	f	-83.7155800000	14.8070900000	1120	f	2	0	f	41	\N	\N
77	RAAN / EuroSolar22	1	\N	3	f	-84.7037400000	14.6635800000	1120	f	2	0	f	41	\N	\N
78	RAAN / EuroSolar23	1	\N	3	f	-84.7496800000	14.7247500000	1120	f	2	0	f	41	\N	\N
79	RAAN / EuroSolar24	1	\N	3	f	-84.4026600000	14.6613500000	1120	f	2	0	f	41	\N	\N
80	RAAN / EuroSolar25	1	\N	3	f	-84.9907300000	14.7283500000	1120	f	2	0	f	41	\N	\N
81	RAAN / EuroSolar26	1	\N	3	f	-85.0242200000	14.5933400000	1120	f	2	0	f	41	\N	\N
82	RAAN / EuroSolar27	1	\N	3	f	-84.1872500000	14.7127100000	1120	f	2	0	f	41	\N	\N
83	RAAN / EuroSolar28	1	\N	3	f	-83.8372200000	14.7686400000	1120	f	2	0	f	41	\N	\N
84	RAAN / EuroSolar29	1	\N	3	f	-83.7423900000	14.8000600000	1120	f	2	0	f	41	\N	\N
85	RAAN / EuroSolar30	1	\N	3	f	-84.1058300000	14.5589500000	1120	f	2	0	f	41	\N	\N
86	RAAN / EuroSolar31	1	\N	3	f	-84.4644000000	14.6143000000	1120	f	2	0	f	41	\N	\N
87	RAAN / EuroSolar32	1	\N	3	f	-83.8822200000	14.7579800000	1120	f	2	0	f	41	\N	\N
88	RAAN / EuroSolar33	1	\N	3	f	-84.7831460000	14.7893840000	1120	f	2	0	f	41	\N	\N
89	RAAN / EuroSolar34	1	\N	3	f	-83.9479700000	14.3809000000	1120	f	2	0	f	41	\N	\N
90	RAAN / EuroSolar35	1	\N	3	f	-84.1490800000	14.4659200000	1120	f	2	0	f	41	\N	\N
91	RAAN / EuroSolar36	1	\N	3	f	-83.7773400000	14.7837100000	1120	f	2	0	f	41	\N	\N
92	RAAN / EuroSolar37	1	\N	3	f	-83.4106500000	14.7464500000	1120	f	2	0	f	41	\N	\N
95	RAAN / EuroSolar41	1	\N	3	f	-83.9100800000	14.7459600000	1120	f	2	0	f	41	\N	\N
97	SAN JUAN DE NICARAGUA 2	1	\N	3	f	-83.7377120000	10.9458730000	4900	f	9	0	f	41	\N	\N
99	La Florida2	34	\N	3	f	-85.3560173611	13.4486122484	2720	t	0	0	f	12	\N	\N
102	Yakalwás	1	\N	3	f	-85.7207962534	13.6345490105	1480	t	0	0	f	13	\N	\N
104	Comunidad Los Limones	1	\N	\N	f	-87.2847128407	12.8188753443	80	f	0	0	f	12	1	\N
105	Comunidad Los Limones	1	\N	\N	f	-87.2847128407	12.8188753443	80	f	0	0	f	12	2	\N
106	Comunidad Los Limones	1	\N	\N	f	-87.2847128407	12.8188753443	80	f	0	0	f	12	3	\N
107	Comunidad Los Limones	1	\N	\N	f	-87.2847128407	12.8188753443	80	f	0	0	f	12	4	\N
108	Comunidad Los Limones	1	\N	\N	f	-87.2873566741	12.8181490513	80	f	0	0	f	12	5	\N
109	Comunidad Los Limones	1	\N	\N	f	-87.2861239949	12.8200131988	80	f	0	0	f	12	6	\N
110	Comunidad Los Limones	1	\N	\N	f	-87.2836601218	12.8251159712	80	f	0	0	f	12	7	\N
111	Comunidad Los Limones	1	\N	\N	f	-87.2837450311	12.8186503211	80	f	0	0	f	12	8	\N
112	Comunidad Los Limones	1	\N	\N	f	-87.2955125663	12.8109239159	80	f	0	0	f	12	9	\N
113	Comunidad Los Limones	1	\N	\N	f	-87.2837629868	12.8264541853	80	f	0	0	f	12	10	\N
114	Comunidad Los Limones	1	\N	\N	f	-87.2837629868	12.8264541853	80	f	0	0	f	12	11	\N
115	Comunidad Los Limones	1	\N	\N	f	-87.2835864824	12.8251883925	80	f	0	0	f	12	12	\N
116	Comunidad Los Limones	1	\N	\N	f	-87.2853963263	12.8202852713	80	f	0	0	f	12	13	\N
117	Comunidad Los Limones	1	\N	\N	f	-87.2720730028	12.8130109283	80	f	0	0	f	12	14	\N
118	Comunidad Los Limones	1	\N	\N	f	-87.2720730028	12.8130109283	80	f	0	0	f	12	15	\N
119	Comunidad Los Limones	1	\N	\N	f	-87.2720730028	12.8130109283	80	f	0	0	f	12	16	\N
120	Comunidad Los Limones	1	\N	\N	f	-87.2720730028	12.8130109283	80	f	0	0	f	12	17	\N
121	Comunidad Los Limones	1	\N	\N	f	-87.2720730028	12.8130109283	80	f	0	0	f	12	18	\N
122	Comunidad Los Limones	1	\N	\N	f	-87.2896187189	12.8139507407	80	f	0	0	f	12	19	\N
123	Comunidad Los Limones	1	\N	\N	f	-87.2897761224	12.8146106874	80	f	0	0	f	12	20	\N
124	Comunidad Los Limones	1	\N	\N	f	-87.2897761224	12.8146106874	80	f	0	0	f	12	21	\N
125	Comunidad Los Limones	1	\N	\N	f	-87.2897761224	12.8146106874	80	f	0	0	f	12	22	\N
126	Comunidad Los Limones	1	\N	\N	f	-87.2897761224	12.8146106874	80	f	0	0	f	12	23	\N
127	Comunidad Los Limones	1	\N	\N	f	-87.2843222453	12.8239035296	80	f	0	0	f	12	24	\N
128	Comunidad Los Limones	1	\N	\N	f	-87.2964525429	12.8110042439	80	f	0	0	f	12	25	\N
129	Comunidad Los Limones	1	\N	\N	f	-87.2837153514	12.8250616552	80	f	0	0	f	12	26	\N
130	Comunidad Los Limones	1	\N	\N	f	-87.2837153514	12.8250616552	80	f	0	0	f	12	27	\N
131	Comunidad Los Limones	1	\N	\N	f	-87.2837153514	12.8250616552	80	f	0	0	f	12	28	\N
132	Comunidad Los Limones	1	\N	\N	f	-87.2842335177	12.8187402213	80	f	0	0	f	12	29	\N
133	Comunidad Los Limones	1	\N	\N	f	-87.2842335177	12.8187402213	80	f	0	0	f	12	30	\N
134	Comunidad Los Limones	1	\N	\N	f	-87.2842335177	12.8187402213	80	f	0	0	f	12	31	\N
135	Comunidad Los Limones	1	\N	\N	f	-87.2782551770	12.8122448755	80	f	0	0	f	12	32	\N
136	Comunidad Los Limones	1	\N	\N	f	-87.2845435984	12.8240751022	80	f	0	0	f	12	33	\N
137	Comunidad Los Limones	1	\N	\N	f	-87.2845435984	12.8240751022	80	f	0	0	f	12	34	\N
138	Comunidad Los Limones	1	\N	\N	f	-87.2850494574	12.8232154945	80	f	0	0	f	12	35	\N
139	Comunidad Los Limones	1	\N	\N	f	-87.2780712324	12.8125615661	80	f	0	0	f	12	36	\N
140	Comunidad Los Limones	1	\N	\N	f	-87.2780712324	12.8125615661	80	f	0	0	f	12	37	\N
141	Comunidad Los Limones	1	\N	\N	f	-87.2780712324	12.8125615661	80	f	0	0	f	12	38	\N
142	Comunidad Los Limones	1	\N	\N	f	-87.2719074306	12.8132823819	80	f	0	0	f	12	39	\N
143	Comunidad Los Limones	1	\N	\N	f	-87.2830530009	12.8178281769	80	f	0	0	f	12	40	\N
144	Comunidad Los Limones	1	\N	\N	f	-87.2830530009	12.8178281769	80	f	0	0	f	12	41	\N
145	Comunidad Los Limones	1	\N	\N	f	-87.2848357642	12.8216603750	80	f	0	0	f	12	42	\N
146	Comunidad Los Limones	1	\N	\N	f	-87.2825990465	12.8156493635	80	f	0	0	f	12	43	\N
147	Comunidad Los Limones	1	\N	\N	f	-87.2944710415	12.8106809289	80	f	0	0	f	12	44	\N
148	Comunidad Los Limones	1	\N	\N	f	-87.2837153514	12.8250616552	80	f	0	0	f	12	45	\N
149	Comunidad Los Limones	1	\N	\N	f	-87.2826266403	12.8156041202	80	f	0	0	f	12	46	\N
150	Comunidad Los Limones	1	\N	\N	f	-87.2826266403	12.8156041202	80	f	0	0	f	12	47	\N
151	Comunidad Los Limones	1	\N	\N	f	-87.2897840656	12.8134984217	80	f	0	0	f	12	48	\N
152	Comunidad Los Limones	1	\N	\N	f	-87.2897671248	12.8148005949	80	f	0	0	f	12	49	\N
153	Comunidad Los Limones	1	\N	\N	f	-87.2897671248	12.8148005949	80	f	0	0	f	12	50	\N
154	Comunidad Los Limones	1	\N	\N	f	-87.2817831672	12.8111650361	80	f	0	0	f	12	51	\N
155	Comunidad Los Limones	1	\N	\N	f	-87.2972450134	12.8110214359	80	f	0	0	f	12	52	\N
156	Comunidad Los Limones	1	\N	\N	f	-86.5589794291	127.3444753230	80	f	0	0	f	12	53	\N
157	Comunidad Los Limones	1	\N	\N	f	-86.5589794291	127.3444753230	80	f	0	0	f	12	54	\N
158	Comunidad Los Limones	1	\N	\N	f	-87.2873842877	12.8181218930	80	f	0	0	f	12	55	\N
159	Comunidad Los Limones	1	\N	\N	f	-87.2873842877	12.8181218930	80	f	0	0	f	12	56	\N
160	Comunidad Los Limones	1	\N	\N	f	-87.2835589689	12.8253059778	80	f	0	0	f	12	57	\N
161	Comunidad Los Limones	1	\N	\N	f	-87.2966000071	12.8110312060	80	f	0	0	f	12	58	\N
162	Comunidad Los Limones	1	\N	\N	f	-87.2966000071	12.8110312060	80	f	0	0	f	12	59	\N
163	Comunidad Los Limones	1	\N	\N	f	-87.2966000071	12.8110312060	80	f	0	0	f	12	60	\N
164	Comunidad Los Limones	1	\N	\N	f	-87.2854331959	12.8202942740	80	f	0	0	f	12	61	\N
165	Comunidad Los Limones	1	\N	\N	f	-87.2868790710	12.8195331115	80	f	0	0	f	12	62	\N
166	Comunidad Los Limones	1	\N	\N	f	-87.2868790710	12.8195331115	80	f	0	0	f	12	63	\N
167	Comunidad Los Limones	1	\N	\N	f	-87.2868790710	12.8195331115	80	f	0	0	f	12	64	\N
168	Comunidad Los Limones	1	\N	\N	f	-87.2868790710	12.8195331115	80	f	0	0	f	12	65	\N
169	Comunidad Los Limones	1	\N	\N	f	-87.2868790710	12.8195331115	80	f	0	0	f	12	66	\N
170	Comunidad Los Limones	1	\N	\N	f	-87.2868790710	12.8195331115	80	f	0	0	f	12	67	\N
171	Comunidad Los Limones	1	\N	\N	f	-87.2840583652	12.8186771113	80	f	0	0	f	12	68	\N
172	Comunidad Los Limones	1	\N	\N	f	-87.2833855724	12.8185783665	80	f	0	0	f	12	69	\N
173	Comunidad Los Limones	1	\N	\N	f	-87.2968672284	12.8110309048	80	f	0	0	f	12	70	\N
174	Comunidad Los Limones	1	\N	\N	f	-87.2968672284	12.8110309048	80	f	0	0	f	12	71	\N
175	Comunidad Los Limones	1	\N	\N	f	-87.2820415265	12.8114812556	80	f	0	0	f	12	72	\N
176	Comunidad Los Limones	1	\N	\N	f	-87.2820415265	12.8114812556	80	f	0	0	f	12	73	\N
177	Comunidad Los Limones	1	\N	\N	f	-87.2820415265	12.8114812556	80	f	0	0	f	12	74	\N
178	Comunidad Los Limones	1	\N	\N	f	-87.2796568424	12.8048373771	80	f	0	0	f	12	75	\N
179	Comunidad Los Limones	1	\N	\N	f	-87.2971436960	12.8110577212	80	f	0	0	f	12	76	\N
180	Comunidad Los Limones	1	\N	\N	f	-87.2866948355	12.8195875687	80	f	0	0	f	12	77	\N
181	Comunidad Los Limones	1	\N	\N	f	-87.2755998130	12.8108098710	80	f	0	0	f	12	78	\N
182	Comunidad Los Limones	1	\N	\N	f	-87.2817739527	12.8111650459	80	f	0	0	f	12	79	\N
183	Comunidad Los Limones	1	\N	\N	f	-87.2817739527	12.8111650459	80	f	0	0	f	12	80	\N
184	Comunidad Tecometepé	1	\N	\N	f	-87.2484627017	12.7917385330	80	f	0	0	f	12	1	\N
185	Comunidad Tecometepé	1	\N	\N	f	-87.2465264472	12.7903568044	80	f	0	0	f	12	2	\N
186	Comunidad Tecometepé	1	\N	\N	f	-87.2465264472	12.7903568044	80	f	0	0	f	12	3	\N
187	Comunidad Tecometepé	1	\N	\N	f	-87.2555869277	12.7936307067	80	f	0	0	f	12	4	\N
188	Comunidad Tecometepé	1	\N	\N	f	-87.2432762136	12.7830803772	80	f	0	0	f	12	5	\N
189	Comunidad Tecometepé	1	\N	\N	f	-87.2432762136	12.7830803772	80	f	0	0	f	12	6	\N
190	Comunidad Tecometepé	1	\N	\N	f	-87.2432762136	12.7830803772	80	f	0	0	f	12	7	\N
191	Comunidad Tecometepé	1	\N	\N	f	-87.2508058088	12.7852255101	80	f	0	0	f	12	8	\N
192	Comunidad Tecometepé	1	\N	\N	f	-87.2542418663	12.7938038201	80	f	0	0	f	12	9	\N
193	Comunidad Tecometepé	1	\N	\N	f	-87.2542418663	12.7938038201	80	f	0	0	f	12	10	\N
194	Comunidad Tecometepé	1	\N	\N	f	-87.2542418663	12.7938038201	80	f	0	0	f	12	11	\N
195	Comunidad Tecometepé	1	\N	\N	f	-87.2424756007	12.7840939069	80	f	0	0	f	12	12	\N
196	Comunidad Tecometepé	1	\N	\N	f	-87.2424756007	12.7840939069	80	f	0	0	f	12	13	\N
197	Comunidad Tecometepé	1	\N	\N	f	-87.2424756007	12.7840939069	80	f	0	0	f	12	14	\N
198	Comunidad Tecometepé	1	\N	\N	f	-87.2424756007	12.7840939069	80	f	0	0	f	12	15	\N
199	Comunidad Tecometepé	1	\N	\N	f	-87.2424756007	12.7840939069	80	f	0	0	f	12	16	\N
200	Comunidad Tecometepé	1	\N	\N	f	-87.2424756007	12.7840939069	80	f	0	0	f	12	17	\N
201	Comunidad Tecometepé	1	\N	\N	f	-87.2424756007	12.7840939069	80	f	0	0	f	12	18	\N
202	Comunidad Tecometepé	1	\N	\N	f	-87.2424756007	12.7840939069	80	f	0	0	f	12	19	\N
203	Comunidad Tecometepé	1	\N	\N	f	-87.2424756007	12.7840939069	80	f	0	0	f	12	20	\N
204	Comunidad Tecometepé	1	\N	\N	f	-87.2561107719	12.7922918673	80	f	0	0	f	12	21	\N
205	Comunidad Tecometepé	1	\N	\N	f	-87.2609748601	12.8004346352	80	f	0	0	f	12	22	\N
206	Comunidad Tecometepé	1	\N	\N	f	-87.2400411992	12.7722048542	80	f	0	0	f	12	23	\N
207	Comunidad Tecometepé	1	\N	\N	f	-87.2426055234	12.7850704090	80	f	0	0	f	12	24	\N
208	Comunidad Tecometepé	1	\N	\N	f	-87.2506242746	12.7880018177	80	f	0	0	f	12	25	\N
209	Comunidad Tecometepé	1	\N	\N	f	-87.2443647867	12.7845081322	80	f	0	0	f	12	26	\N
210	Comunidad Tecometepé	1	\N	\N	f	-87.2491120884	12.7868457738	80	f	0	0	f	12	27	\N
211	Comunidad Tecometepé	1	\N	\N	f	-87.2589700283	12.8042164908	80	f	0	0	f	12	28	\N
212	Comunidad Tecometepé	1	\N	\N	f	-87.2588679705	12.7944956118	80	f	0	0	f	12	29	\N
213	Comunidad Tecometepé	1	\N	\N	f	-87.1489226222	12.7720549393	80	f	0	0	f	12	30	\N
214	Comunidad Tecometepé	1	\N	\N	f	-87.2589052052	12.8039000576	80	f	0	0	f	12	31	\N
215	Comunidad Tecometepé	1	\N	\N	f	-87.2434957777	12.7910649578	80	f	0	0	f	12	32	\N
216	Comunidad Tecometepé	1	\N	\N	f	-86.5768312638	127.1893074540	80	f	0	0	f	12	33	\N
217	Comunidad Tecometepé	1	\N	\N	f	-87.2582410762	12.8032134591	80	f	0	0	f	12	34	\N
218	Comunidad Tecometepé	1	\N	\N	f	-87.2582410762	12.8032134591	80	f	0	0	f	12	35	\N
219	Comunidad Tecometepé	1	\N	\N	f	-87.2629332445	12.7963453565	80	f	0	0	f	12	36	\N
220	Comunidad Tecometepé	1	\N	\N	f	-87.2529037383	12.7916890967	80	f	0	0	f	12	37	\N
221	Comunidad Tecometepé	1	\N	\N	f	-87.2529037383	12.7916890967	80	f	0	0	f	12	38	\N
222	Comunidad Tecometepé	1	\N	\N	f	-87.2410407029	12.7769333278	80	f	0	0	f	12	39	\N
223	Comunidad Tecometepé	1	\N	\N	f	-87.2410407029	12.7769333278	80	f	0	0	f	12	40	\N
224	Comunidad Tecometepé	1	\N	\N	f	-87.2520998439	12.7893839572	80	f	0	0	f	12	41	\N
225	Comunidad Tecometepé	1	\N	\N	f	-87.2601557722	12.8013849360	80	f	0	0	f	12	42	\N
226	Comunidad Tecometepé	1	\N	\N	f	-87.2589236430	12.8039090823	80	f	0	0	f	12	43	\N
227	Comunidad Santa Anita	1	\N	\N	f	-87.2589236430	12.8039090823	80	f	0	0	f	12	1	\N
228	Comunidad Santa Anita	1	\N	\N	f	-87.2589236430	12.8039090823	80	f	0	0	f	12	2	\N
229	Comunidad Santa Anita	1	\N	\N	f	-86.3852402568	12.6412694945	80	f	0	0	f	12	3	\N
230	Comunidad Santa Anita	1	\N	\N	f	-86.3852402568	12.6412694945	80	f	0	0	f	12	4	\N
231	Comunidad Santa Anita	1	\N	\N	f	-86.4026506882	12.6579744274	80	f	0	0	f	12	5	\N
232	Comunidad Santa Anita	1	\N	\N	f	-86.3835543075	12.6493134111	80	f	0	0	f	12	6	\N
233	Comunidad Santa Anita	1	\N	\N	f	-86.3856265388	12.6414602774	80	f	0	0	f	12	7	\N
234	Comunidad Cerro Las Delicias	1	\N	\N	f	-86.3900229860	12.7517612506	80	f	0	0	f	12	1	\N
235	Comunidad Cerro Las Delicias	1	\N	\N	f	-86.3911377435	12.7517186080	80	f	0	0	f	12	2	\N
236	Comunidad Cerro Las Delicias	1	\N	\N	f	-86.3880609653	12.7517024621	80	f	0	0	f	12	3	\N
237	Comunidad Cerro Las Delicias	1	\N	\N	f	-86.3884934506	12.7519023960	80	f	0	0	f	12	4	\N
238	El Chaguitillo	1	\N	\N	f	-86.3565663662	12.7548648576	80	f	0	0	f	12	1	\N
239	El Ojoche	1	\N	\N	f	-86.3600055841	12.7462919925	80	f	0	0	f	12	1	\N
240	El Ojoche	1	\N	\N	f	-86.3644150498	12.7474871781	80	f	0	0	f	12	2	\N
241	El Ojoche	1	\N	\N	f	-86.3731774310	12.7429597640	80	f	0	0	f	12	3	\N
242	El Tempiscal	1	\N	\N	f	-86.3411446483	12.7408835672	80	f	0	0	f	12	1	\N
243	El Tempiscal	1	\N	\N	f	-86.3411446483	12.7408835672	80	f	0	0	f	12	2	\N
244	El Tempiscal	1	\N	\N	f	-86.3394829721	12.7422719459	80	f	0	0	f	12	3	\N
245	El Tempiscal	1	\N	\N	f	-86.3404972477	12.7418856552	80	f	0	0	f	12	4	\N
246	El Tempiscal	1	\N	\N	f	-86.3412548826	12.7410013922	80	f	0	0	f	12	5	\N
247	El Tempiscal	1	\N	\N	f	-86.3371846672	12.7440475395	80	f	0	0	f	12	6	\N
248	El Tempiscal	1	\N	\N	f	-86.3371846672	12.7440475395	80	f	0	0	f	12	7	\N
249	El Tempiscal	1	\N	\N	f	-86.3371846672	12.7440475395	80	f	0	0	f	12	8	\N
250	El Tempiscal	1	\N	\N	f	-86.3371846672	12.7440475395	80	f	0	0	f	12	9	\N
251	El Tempiscal	1	\N	\N	f	-86.3408577831	12.7413892244	80	f	0	0	f	12	10	\N
252	El Tempiscal	1	\N	\N	f	-86.3374614619	12.7438764284	80	f	0	0	f	12	11	\N
253	El Tempiscal	1	\N	\N	f	-86.3374614619	12.7438764284	80	f	0	0	f	12	12	\N
254	El Tempiscal	1	\N	\N	f	-86.3407721892	12.7388661950	80	f	0	0	f	12	13	\N
255	El Tempiscal	1	\N	\N	f	-86.3372765012	12.7441562777	80	f	0	0	f	12	14	\N
256	El Tempiscal	1	\N	\N	f	-86.3372765012	12.7441562777	80	f	0	0	f	12	15	\N
257	Las Pilas	1	\N	\N	f	-86.3356744823	12.7048994015	80	f	0	0	f	12	1	\N
258	Las Pilas	1	\N	\N	f	-86.3356744823	12.7048994015	80	f	0	0	f	12	2	\N
259	Las Pilas	1	\N	\N	f	-86.3361930160	12.7109319575	80	f	0	0	f	12	3	\N
260	Las Pilas	1	\N	\N	f	-86.3307242137	12.7171212646	80	f	0	0	f	12	4	\N
261	Las Cañas	1	\N	\N	f	-86.3680740444	12.7020578553	80	f	0	0	f	12	1	\N
262	Las Cañas	1	\N	\N	f	-86.3620980159	12.7015010113	80	f	0	0	f	12	2	\N
263	Las Cañas	1	\N	\N	f	-86.3694650707	12.7056781333	80	f	0	0	f	12	3	\N
264	Las Cañas	1	\N	\N	f	-86.3694650707	12.7056781333	80	f	0	0	f	12	4	\N
265	Las Cañas	1	\N	\N	f	-86.3685614234	12.7023664579	80	f	0	0	f	12	5	\N
266	Las Cañas	1	\N	\N	f	-86.3703017590	12.7062678782	80	f	0	0	f	12	6	\N
267	Las Cañas	1	\N	\N	f	-86.3682180122	12.7034326461	80	f	0	0	f	12	7	\N
268	Las Cañas	1	\N	\N	f	-86.3717945167	12.7059911024	80	f	0	0	f	12	8	\N
269	Las Cañas	1	\N	\N	f	-86.3591124647	12.7094330489	80	f	0	0	f	12	9	\N
270	Las Cañas	1	\N	\N	f	-86.3692281235	12.7046557764	80	f	0	0	f	12	10	\N
271	Las Cañas	1	\N	\N	f	-86.3580418408	12.7066453998	80	f	0	0	f	12	11	\N
272	Las Cañas	1	\N	\N	f	-86.3585597720	12.7057876244	80	f	0	0	f	12	12	\N
273	Las Cañas	1	\N	\N	f	-86.3685614234	12.7023664579	80	f	0	0	f	12	13	\N
274	Las Cañas	1	\N	\N	f	-86.3610329051	12.7075659048	80	f	0	0	f	12	14	\N
275	Las Cañas	1	\N	\N	f	-86.3581199488	12.7048823202	80	f	0	0	f	12	15	\N
276	Las Cañas	1	\N	\N	f	-86.3684928670	12.7040210578	80	f	0	0	f	12	16	\N
277	Las Cañas	1	\N	\N	f	-86.3684940949	12.7035237276	80	f	0	0	f	12	17	\N
278	Las Cañas	1	\N	\N	f	-86.3684940949	12.7035237276	80	f	0	0	f	12	18	\N
279	Las Cañas	1	\N	\N	f	-86.3584309644	12.7057330585	80	f	0	0	f	12	19	\N
280	Caña Terranova	1	\N	\N	f	-86.3339061758	12.6623686625	80	f	0	0	f	12	1	\N
281	Caña Terranova	1	\N	\N	f	-86.3339061758	12.6623686625	80	f	0	0	f	12	2	\N
282	Caña Terranova	1	\N	\N	f	-86.3341369787	12.6621431803	80	f	0	0	f	12	3	\N
283	Tule Norte	1	\N	\N	f	-86.3134422037	12.6975818032	80	f	0	0	f	12	1	\N
284	Tule Norte	1	\N	\N	f	-86.3180325159	12.6996281638	80	f	0	0	f	12	2	\N
285	Tule Norte	1	\N	\N	f	-86.3131577409	12.6971922468	80	f	0	0	f	12	3	\N
286	Tule Norte	1	\N	\N	f	-86.3167211891	12.6974907988	80	f	0	0	f	12	4	\N
287	Tule Norte	1	\N	\N	f	-86.3168685231	12.6975002203	80	f	0	0	f	12	5	\N
288	Tule Norte	1	\N	\N	f	-86.3167575216	12.6976807814	80	f	0	0	f	12	6	\N
289	Tule Norte	1	\N	\N	f	-86.3202693750	12.7070034113	80	f	0	0	f	12	7	\N
290	Tule Norte	1	\N	\N	f	-86.3113184787	12.6927567382	80	f	0	0	f	12	8	\N
291	Tule Norte	1	\N	\N	f	-86.3113184787	12.6927567382	80	f	0	0	f	12	9	\N
292	Tule Norte	1	\N	\N	f	-86.3199182077	12.7074455858	80	f	0	0	f	12	10	\N
293	Tule Norte	1	\N	\N	f	-86.3165554353	12.6974813297	80	f	0	0	f	12	11	\N
294	Tule Norte	1	\N	\N	f	-86.3167660073	12.6979520735	80	f	0	0	f	12	12	\N
295	Tule Norte	1	\N	\N	f	-86.3179593423	12.6994380866	80	f	0	0	f	12	13	\N
296	Tule Norte	1	\N	\N	f	-86.3164438297	12.6978879475	80	f	0	0	f	12	14	\N
297	Tule Norte	1	\N	\N	f	-86.3164438297	12.6978879475	80	f	0	0	f	12	15	\N
298	Tule Norte	1	\N	\N	f	-86.3168951873	12.6978619826	80	f	0	0	f	12	16	\N
299	Tule Norte	1	\N	\N	f	-86.3170065025	12.6975638718	80	f	0	0	f	12	17	\N
300	Tule Norte	1	\N	\N	f	-86.3179863214	12.6996822991	80	f	0	0	f	12	18	\N
301	Tule Norte	1	\N	\N	f	-86.3168497171	12.6976448494	80	f	0	0	f	12	19	\N
302	Tule Norte	1	\N	\N	f	-86.3166415697	12.6997150092	80	f	0	0	f	12	20	\N
303	Tule Norte	1	\N	\N	f	-86.3160839428	12.6981492483	80	f	0	0	f	12	21	\N
304	Tule Norte	1	\N	\N	f	-86.3132106219	12.6980785324	80	f	0	0	f	12	22	\N
305	Tule Norte	1	\N	\N	f	-86.3170110062	12.6993271396	80	f	0	0	f	12	23	\N
306	Tule Norte	1	\N	\N	f	-86.3163602159	12.6981590024	80	f	0	0	f	12	24	\N
307	Abra Puerto Niuevo	1	\N	\N	f	-86.4033080265	12.7590797735	80	f	0	0	f	12	1	\N
308	Abra Puerto Niuevo	1	\N	\N	f	-86.4044380896	12.7603844340	80	f	0	0	f	12	2	\N
309	Abra Puerto Niuevo	1	\N	\N	f	-86.4056754737	12.7591303210	80	f	0	0	f	12	3	\N
310	Abra Puerto Niuevo	1	\N	\N	f	-86.3991184893	12.7621085365	80	f	0	0	f	12	4	\N
311	Abra Puerto Niuevo	1	\N	\N	f	-86.4049490144	12.7585680548	80	f	0	0	f	12	5	\N
312	Abra Puerto Niuevo	1	\N	\N	f	-86.4031125764	12.7599293212	80	f	0	0	f	12	6	\N
313	Abra Puerto Niuevo	1	\N	\N	f	-86.4025956271	12.7603802747	80	f	0	0	f	12	7	\N
314	Abra Puerto Niuevo	1	\N	\N	f	-86.4032762149	12.7608610617	80	f	0	0	f	12	8	\N
315	Los Llanos de los Pedros	1	\N	\N	f	-86.4032762149	12.7608610617	80	f	0	0	f	12	1	\N
316	Los Llanos de los Pedros	1	\N	\N	f	-86.4032762149	12.7608610617	80	f	0	0	f	12	2	\N
317	Los Llanos de los Pedros	1	\N	\N	f	-85.2096752704	12.1442085000	80	f	0	0	f	12	3	\N
318	Los Llanos de los Pedros	1	\N	\N	f	-85.2001428014	12.1409376466	80	f	0	0	f	12	4	\N
319	Los Llanos de los Pedros	1	\N	\N	f	-85.2023801677	12.1415668763	80	f	0	0	f	12	5	\N
320	Los Llanos de los Pedros	1	\N	\N	f	-85.2023801677	12.1415668763	80	f	0	0	f	12	6	\N
321	Los Llanos de los Pedros	1	\N	\N	f	-85.2187677608	12.1390787245	80	f	0	0	f	12	7	\N
322	Los Llanos de los Pedros	1	\N	\N	f	-85.2187677608	12.1390787245	80	f	0	0	f	12	8	\N
323	Los Llanos de los Pedros	1	\N	\N	f	-85.2187677608	12.1390787245	80	f	0	0	f	12	9	\N
324	Los Llanos de los Pedros	1	\N	\N	f	-85.2187677608	12.1390787245	80	f	0	0	f	12	10	\N
325	Los Llanos de los Pedros	1	\N	\N	f	-85.2187677608	12.1390787245	80	f	0	0	f	12	11	\N
326	Los Llanos de los Pedros	1	\N	\N	f	-85.2116660121	12.1473851301	80	f	0	0	f	12	12	\N
327	Los Llanos de los Pedros	1	\N	\N	f	-85.2116660121	12.1473851301	80	f	0	0	f	12	13	\N
328	Los Llanos de los Pedros	1	\N	\N	f	-85.2191251369	12.1336845733	80	f	0	0	f	12	14	\N
329	Los Llanos de los Pedros	1	\N	\N	f	-85.2084438805	12.1318890761	80	f	0	0	f	12	15	\N
330	Los Llanos de los Pedros	1	\N	\N	f	-85.2188150789	12.1374881154	80	f	0	0	f	12	16	\N
331	Los Llanos de los Pedros	1	\N	\N	f	-85.2188150789	12.1374881154	80	f	0	0	f	12	17	\N
332	Los Llanos de los Pedros	1	\N	\N	f	-85.2121654572	12.1303584093	80	f	0	0	f	12	18	\N
333	Los Llanos de los Pedros	1	\N	\N	f	-85.2201043532	12.1356343262	80	f	0	0	f	12	19	\N
334	Los Llanos de los Pedros	1	\N	\N	f	-85.2201043532	12.1356343262	80	f	0	0	f	12	20	\N
335	Los Llanos de los Pedros	1	\N	\N	f	-85.2201043532	12.1356343262	80	f	0	0	f	12	21	\N
336	Los Llanos de los Pedros	1	\N	\N	f	-85.1994729490	12.1380859361	80	f	0	0	f	12	22	\N
337	Los Llanos de los Pedros	1	\N	\N	f	-85.1998636507	12.1387302641	80	f	0	0	f	12	23	\N
338	Los Llanos de los Pedros	1	\N	\N	f	-85.2113439944	12.1474553589	80	f	0	0	f	12	24	\N
339	Los Llanos de los Pedros	1	\N	\N	f	-85.2113439944	12.1474553589	80	f	0	0	f	12	25	\N
340	Los Llanos de los Pedros	1	\N	\N	f	-85.2002491740	12.1387779708	80	f	0	0	f	12	26	\N
341	Los Llanos de los Pedros	1	\N	\N	f	-85.2002491740	12.1387779708	80	f	0	0	f	12	27	\N
342	Los Llanos de los Pedros	1	\N	\N	f	-85.2196195800	12.1380808520	80	f	0	0	f	12	28	\N
343	Los Llanos de los Pedros	1	\N	\N	f	-85.1998538149	12.1401945500	80	f	0	0	f	12	29	\N
344	Los Llanos de los Pedros	1	\N	\N	f	-85.2197860687	12.1337611437	80	f	0	0	f	12	30	\N
345	Los Llanos de los Pedros	1	\N	\N	f	-85.2197860687	12.1337611437	80	f	0	0	f	12	31	\N
346	Los Llanos de los Pedros	1	\N	\N	f	-85.2209617755	12.1420939007	80	f	0	0	f	12	32	\N
347	Los Llanos de los Pedros	1	\N	\N	f	-85.2106546216	12.1461311644	80	f	0	0	f	12	33	\N
348	Los Llanos de los Pedros	1	\N	\N	f	-85.2116116638	12.1458933025	80	f	0	0	f	12	34	\N
349	Los Llanos de los Pedros	1	\N	\N	f	-85.2116116638	12.1458933025	80	f	0	0	f	12	35	\N
350	Los Llanos de los Pedros	1	\N	\N	f	-85.2102208081	12.1326779578	80	f	0	0	f	12	36	\N
351	Los Llanos de los Pedros	1	\N	\N	f	-85.2111588253	12.1325303603	80	f	0	0	f	12	37	\N
352	Los Llanos de los Pedros	1	\N	\N	f	-85.2179438046	12.1386485669	80	f	0	0	f	12	38	\N
353	Los Llanos de los Pedros	1	\N	\N	f	-85.2179438046	12.1386485669	80	f	0	0	f	12	39	\N
354	Los Llanos de los Pedros	1	\N	\N	f	-85.2179438046	12.1386485669	80	f	0	0	f	12	40	\N
355	Los Llanos de los Pedros	1	\N	\N	f	-85.2179438046	12.1386485669	80	f	0	0	f	12	41	\N
356	Los Llanos de los Pedros	1	\N	\N	f	-85.2191573792	12.1343627276	80	f	0	0	f	12	42	\N
357	Los Llanos de los Pedros	1	\N	\N	f	-85.2191573792	12.1343627276	80	f	0	0	f	12	43	\N
358	Los Llanos de los Pedros	1	\N	\N	f	-85.2191573792	12.1343627276	80	f	0	0	f	12	44	\N
359	Los Llanos de los Pedros	1	\N	\N	f	-85.2180569178	12.1395984206	80	f	0	0	f	12	45	\N
360	Los Llanos de los Pedros	1	\N	\N	f	-85.2203353257	12.1340539369	80	f	0	0	f	12	46	\N
361	Los Llanos de los Pedros	1	\N	\N	f	-85.2075690210	12.1390786378	80	f	0	0	f	12	47	\N
362	Los Llanos de los Pedros	1	\N	\N	f	-85.2124233805	12.1481583690	80	f	0	0	f	12	48	\N
363	Los Llanos de los Pedros	1	\N	\N	f	-85.2004546336	12.1396470719	80	f	0	0	f	12	49	\N
364	Los Llanos de los Pedros	1	\N	\N	f	-85.1998141369	12.1392632542	80	f	0	0	f	12	50	\N
365	Los Llanos de los Pedros	1	\N	\N	f	-85.2111233468	12.1323403065	80	f	0	0	f	12	51	\N
366	Los Llanos de los Pedros	1	\N	\N	f	-85.2201987970	12.1352462447	80	f	0	0	f	12	52	\N
367	Los Llanos de los Pedros	1	\N	\N	f	-85.2201987970	12.1352462447	80	f	0	0	f	12	53	\N
368	San Bartolo	1	\N	\N	f	-85.2351736847	12.0792257956	80	f	0	0	f	12	1	\N
369	San Bartolo	1	\N	\N	f	-85.2351732157	12.0806992135	80	f	0	0	f	12	2	\N
370	San Bartolo	1	\N	\N	f	-85.2343813892	12.0795913739	80	f	0	0	f	12	3	\N
371	San Bartolo	1	\N	\N	f	-85.2343813892	12.0795913739	80	f	0	0	f	12	4	\N
372	San Bartolo	1	\N	\N	f	-85.2335353512	12.0783475972	80	f	0	0	f	12	5	\N
373	San Bartolo	1	\N	\N	f	-85.2341892257	12.0794816793	80	f	0	0	f	12	6	\N
374	San Bartolo	1	\N	\N	f	-85.2071427065	12.0832042852	80	f	0	0	f	12	7	\N
375	San Bartolo	1	\N	\N	f	-85.2359512044	12.1133272993	80	f	0	0	f	12	8	\N
376	San Bartolo	1	\N	\N	f	-85.2397097366	12.0808274454	80	f	0	0	f	12	9	\N
377	San Bartolo	1	\N	\N	f	-85.2103296598	12.0846168999	80	f	0	0	f	12	10	\N
378	San Bartolo	1	\N	\N	f	-85.2021236250	12.1044411409	80	f	0	0	f	12	11	\N
379	San Bartolo	1	\N	\N	f	-85.2021236250	12.1044411409	80	f	0	0	f	12	12	\N
380	San Bartolo	1	\N	\N	f	-85.2114038297	12.0860701093	80	f	0	0	f	12	13	\N
381	San Bartolo	1	\N	\N	f	-85.2445888218	12.1227470127	80	f	0	0	f	12	14	\N
382	San Bartolo	1	\N	\N	f	-85.2138581303	12.0913196656	80	f	0	0	f	12	15	\N
383	San Bartolo	1	\N	\N	f	-85.2094944182	12.0831471506	80	f	0	0	f	12	16	\N
384	San Bartolo	1	\N	\N	f	-85.2350670880	12.0800748209	80	f	0	0	f	12	17	\N
385	San Bartolo	1	\N	\N	f	-85.2088215680	12.1041952581	80	f	0	0	f	12	18	\N
386	Cunaga y Sacahuacal	1	\N	\N	f	-85.1789402515	12.0744425497	80	f	0	0	f	12	1	\N
387	Cunaga y Sacahuacal	1	\N	\N	f	-85.1395310498	12.0649977786	80	f	0	0	f	12	2	\N
388	Cunaga y Sacahuacal	1	\N	\N	f	-85.1543520518	12.0987850254	80	f	0	0	f	12	3	\N
389	Cunaga y Sacahuacal	1	\N	\N	f	-85.1871053162	12.0757976047	80	f	0	0	f	12	4	\N
390	Cunaga y Sacahuacal	1	\N	\N	f	-85.1414044182	12.0863241895	80	f	0	0	f	12	5	\N
391	Cunaga y Sacahuacal	1	\N	\N	f	-85.1414044182	12.0863241895	80	f	0	0	f	12	6	\N
392	Cunaga y Sacahuacal	1	\N	\N	f	-85.1892135123	12.0682274792	80	f	0	0	f	12	7	\N
393	Cunaga y Sacahuacal	1	\N	\N	f	-85.1892135123	12.0682274792	80	f	0	0	f	12	8	\N
394	Cunaga y Sacahuacal	1	\N	\N	f	-85.2114038297	12.0860701093	80	f	0	0	f	12	9	\N
395	Cunaga y Sacahuacal	1	\N	\N	f	-85.1388782853	12.0664306025	80	f	0	0	f	12	10	\N
396	Cunaga y Sacahuacal	1	\N	\N	f	-85.1414044182	12.0863241895	80	f	0	0	f	12	11	\N
397	Cunaga y Sacahuacal	1	\N	\N	f	-85.1414044182	12.0863241895	80	f	0	0	f	12	12	\N
398	Cunaga y Sacahuacal	1	\N	\N	f	-85.1573754674	12.0650081860	80	f	0	0	f	12	13	\N
399	Cunaga y Sacahuacal	1	\N	\N	f	-85.1573905699	12.1017248439	80	f	0	0	f	12	14	\N
400	Cunaga y Sacahuacal	1	\N	\N	f	-85.1390034103	12.0655998563	80	f	0	0	f	12	15	\N
401	Cunaga y Sacahuacal	1	\N	\N	f	-85.1799613341	12.0714844029	80	f	0	0	f	12	16	\N
402	Cunaga y Sacahuacal	1	\N	\N	f	-85.1697354667	12.0747345708	80	f	0	0	f	12	17	\N
403	Cunaga y Sacahuacal	1	\N	\N	f	-85.1344490606	12.0667986173	80	f	0	0	f	12	18	\N
404	Cunaga y Sacahuacal	1	\N	\N	f	-85.1344490606	12.0667986173	80	f	0	0	f	12	19	\N
405	El Rama - Wapy	50	\N	\N	f	-84.3548056746	12.5099524922	4000	f	0	0	f	12	1	\N
406	Hacienda Santa Maria	1	\N	\N	f	-85.8659397787	11.7652580644	80	f	0	0	f	12	1	\N
407	Isla El Armado.	1	\N	\N	f	-85.8254899095	11.7716042946	80	f	0	0	f	12	1	\N
408	Comunidad Sonzapote	1	\N	\N	f	-85.8134997023	11.7578744320	80	f	0	0	f	12	2	\N
409	Comunidad Sonzapote	1	\N	\N	f	-85.8078888021	11.7652471632	80	f	0	0	f	12	3	\N
410	Comunidad Sonzapote	1	\N	\N	f	-85.8077688869	11.7653913291	80	f	0	0	f	12	4	\N
411	Comunidad Sonzapote	1	\N	\N	f	-85.8076115652	11.7656980896	80	f	0	0	f	12	5	\N
412	Comunidad Sonzapote	1	\N	\N	f	-85.8077575298	11.7658976184	80	f	0	0	f	12	6	\N
413	Comunidad Sonzapote	1	\N	\N	f	-85.8071790306	11.7659856144	80	f	0	0	f	12	7	\N
414	Comunidad Sonzapote	1	\N	\N	f	-85.8054995325	11.7681576331	80	f	0	0	f	12	8	\N
415	Comunidad Sonzapote	1	\N	\N	f	-85.8053779755	11.7686815431	80	f	0	0	f	12	9	\N
416	Comunidad Sonzapote	1	\N	\N	f	-85.8054604853	11.7686999726	80	f	0	0	f	12	10	\N
417	Comunidad Sonzapote, Vivienda en Construccion	1	\N	\N	f	-85.8057104339	11.7681946842	80	f	0	0	f	12	11	\N
418	Comunidad Sonzapote	1	\N	\N	f	-85.8121828680	11.7589539584	80	f	0	0	f	12	12	\N
419	Comunidad Sonzapote	1	\N	\N	f	-85.8103385936	11.7610529937	80	f	0	0	f	12	13	\N
420	Comunidad Sonzapote	1	\N	\N	f	-85.8103385936	11.7610529937	80	f	0	0	f	12	14	\N
421	Comunidad Sonzapote	1	\N	\N	f	-85.8103385936	11.7610529937	80	f	0	0	f	12	15	\N
422	Comunidad Sonzapote	1	\N	\N	f	-85.8103385936	11.7610529937	80	f	0	0	f	12	16	\N
423	Comunidad Sonzapote	1	\N	\N	f	-85.8104662059	11.7612524445	80	f	0	0	f	12	17	\N
424	Comunidad Sonzapote	1	\N	\N	f	-85.8104662059	11.7612524445	80	f	0	0	f	12	18	\N
425	Comunidad Sonzapote	1	\N	\N	f	-85.8104662059	11.7612524445	80	f	0	0	f	12	19	\N
426	Comnunidad Tarcas.	1	\N	\N	f	-85.7981185020	11.7095543540	80	f	0	0	f	12	1	\N
427	Comunidad Cañas	1	\N	\N	f	-85.8157342860	11.7115988937	80	f	0	0	f	12	1	\N
428	Comunidad Cañas	1	\N	\N	f	-85.8156413467	11.7118788044	80	f	0	0	f	12	2	\N
429	Comunidad Cañas	1	\N	\N	f	-85.8156406536	11.7120415537	80	f	0	0	f	12	3	\N
430	Comunidad Cañas	1	\N	\N	f	-85.8157604623	11.7119154645	80	f	0	0	f	12	4	\N
431	Comunidad Cañas	1	\N	\N	f	-85.8160574744	11.7111119745	80	f	0	0	f	12	5	\N
432	Comunidad Cañas	1	\N	\N	f	-85.8160215844	11.7109219485	80	f	0	0	f	12	6	\N
433	Comunidad Cañas	1	\N	\N	f	-85.8159703473	11.7100265996	80	f	0	0	f	12	7	\N
434	Comunidad Cañas	1	\N	\N	f	-85.8159052789	11.7102252497	80	f	0	0	f	12	8	\N
435	Comunidad Cañas	1	\N	\N	f	-85.8157483111	11.7104596867	80	f	0	0	f	12	9	\N
436	Comunidad Cañas	1	\N	\N	f	-85.8157615272	11.7095103538	80	f	0	0	f	12	10	\N
437	Comunidad Cañas	1	\N	\N	f	-85.8161651671	11.7095210653	80	f	0	0	f	12	11	\N
438	Comunidad Cañas	1	\N	\N	f	-85.8165502272	11.7095859502	80	f	0	0	f	12	12	\N
439	Comunidad Cañas.	1	\N	\N	f	-85.8169814678	11.7095786912	80	f	0	0	f	12	13	\N
440	Comunidad Cañas.	1	\N	\N	f	-85.8172834963	11.7097517334	80	f	0	0	f	12	14	\N
441	Comunidad Cañas.	1	\N	\N	f	-85.8171177014	11.7099047587	80	f	0	0	f	12	15	\N
442	Comunidad Cañas.	1	\N	\N	f	-85.8172179291	11.7100679252	80	f	0	0	f	12	16	\N
443	Comunidad Caña	1	\N	\N	f	-85.8173827247	11.7101499823	80	f	0	0	f	12	17	\N
444	Comunidad Cañas.	1	\N	\N	f	-85.8174558135	11.7102226186	80	f	0	0	f	12	18	\N
445	Comunidad Cañas.	1	\N	\N	f	-85.8173622998	11.7106381546	80	f	0	0	f	12	19	\N
446	Comunidad Guinea	1	\N	\N	f	-85.8259487904	11.7130605467	80	f	0	0	f	12	1	\N
447	Comunidad Guinea	1	\N	\N	f	-85.8256373866	11.7129326834	80	f	0	0	f	12	2	\N
448	Comunidad Guinea	1	\N	\N	f	-85.8252879860	11.7131120855	80	f	0	0	f	12	3	\N
449	Comunidad Guinea	1	\N	\N	f	-85.8253453635	11.7125607704	80	f	0	0	f	12	4	\N
450	Comunidad Guinea	1	\N	\N	f	-85.8257031744	11.7125622390	80	f	0	0	f	12	5	\N
451	Comunidad Guinea	1	\N	\N	f	-85.8249612893	11.7122608137	80	f	0	0	f	12	6	\N
452	Comunidad Guinea	1	\N	\N	f	-85.8250890466	11.7124240909	80	f	0	0	f	12	7	\N
453	Comunidad Guinea	1	\N	\N	f	-85.8250048322	11.7128125431	80	f	0	0	f	12	8	\N
454	Comunidad Guinea	1	\N	\N	f	-85.8249208851	11.7131377036	80	f	0	0	f	12	9	\N
455	Comunidad Guinea	1	\N	\N	f	-85.8248104453	11.7132186264	80	f	0	0	f	12	10	\N
456	Comunidad Guinea	1	\N	\N	f	-85.8244538185	11.7129368653	80	f	0	0	f	12	11	\N
457	Comunidad Guinea	1	\N	\N	f	-85.8244370361	11.7125660821	80	f	0	0	f	12	12	\N
458	Comunidad Guinea	1	\N	\N	f	-85.8239132790	11.7127538080	80	f	0	0	f	12	13	\N
459	Comunidad Guinea	1	\N	\N	f	-85.8253301927	11.7139802728	80	f	0	0	f	12	14	\N
460	Comunidad Guinea	1	\N	\N	f	-85.8256701525	11.7138641248	80	f	0	0	f	12	15	\N
461	Comunidad Guinea	1	\N	\N	f	-85.8259647349	11.7136302465	80	f	0	0	f	12	16	\N
462	Comunidad Guinea	1	\N	\N	f	-85.8262484248	11.7138032048	80	f	0	0	f	12	17	\N
463	Comunidad Guinea	1	\N	\N	f	-85.8263969750	11.7133878906	80	f	0	0	f	12	18	\N
464	La Guaba	1	\N	\N	f	-85.8332098237	11.7121679791	80	f	0	0	f	12	1	\N
465	Sector Cascabelito	1	\N	\N	f	-85.8421346389	11.7105134002	80	f	0	0	f	12	1	\N
466	Sector Cascabelito	1	\N	\N	f	-85.8442784218	11.7112634983	80	f	0	0	f	12	2	\N
467	Sector Cascabelito	1	\N	\N	f	-85.8468299527	11.7110387019	80	f	0	0	f	12	3	\N
468	Punta La Aurora.	1	\N	\N	f	-85.8574714622	11.7091192867	80	f	0	0	f	12	1	\N
469	Comunidad Terron Colorado.	1	\N	\N	f	-85.8740563051	11.7031088050	80	f	0	0	f	12	1	\N
470	Comunidad Terron Colorado.	1	\N	\N	f	-85.8740563051	11.7031088050	80	f	0	0	f	12	2	\N
471	Comunidad Terron Colorado.	1	\N	\N	f	-85.8740563051	11.7031088050	80	f	0	0	f	12	3	\N
472	Comunidad Terron Colorado.	1	\N	\N	f	-85.8740563051	11.7031088050	80	f	0	0	f	12	4	\N
473	Comunidad Terron Colorado.	1	\N	\N	f	-85.8785572286	11.7040577883	80	f	0	0	f	12	5	\N
474	Comunidad Terron Colorado.	1	\N	\N	f	-85.8787053632	11.7060023928	80	f	0	0	f	12	6	\N
475	Comunidad Terron Colorado.	1	\N	\N	f	-85.8761065930	11.7111189980	80	f	0	0	f	12	7	\N
476	Comunidad San Miguel	1	\N	\N	f	-85.8765977205	11.7121878779	80	f	0	0	f	12	1	\N
477	Comunidad San Miguel	1	\N	\N	f	-85.8766777007	11.7128301713	80	f	0	0	f	12	2	\N
478	Comunidad San Miguel	1	\N	\N	f	-85.8766865103	11.7129206255	80	f	0	0	f	12	3	\N
479	Comunidad San Miguel (Punta Caliente)	1	\N	\N	f	-85.8767436487	11.7146749907	80	f	0	0	f	12	4	\N
480	Comunidad San Miguel	1	\N	\N	f	-85.8771697647	11.7182120708	80	f	0	0	f	12	5	\N
481	Comunidad San Miguel	1	\N	\N	f	-85.8771697647	11.7182120708	80	f	0	0	f	12	6	\N
482	Isla Pedrera	1	\N	\N	f	-85.8773624965	11.7250123816	80	f	0	0	f	12	1	\N
483	Isla Pedrera	1	\N	\N	f	-85.8777548393	11.7255564395	80	f	0	0	f	12	2	\N
484	Punta El Morro	1	\N	\N	f	-85.8816310521	11.7313494477	80	f	0	0	f	12	1	\N
485	Punta El Morro	1	\N	\N	f	-85.8813608113	11.7323791739	80	f	0	0	f	12	2	\N
486	Punta Los Robles	1	\N	\N	f	-85.8789399438	11.7410951730	80	f	0	0	f	12	1	\N
487	Punta Los Robles	1	\N	\N	f	-85.8765009725	11.7429120593	80	f	0	0	f	12	2	\N
488	Punta Los Robles	1	\N	\N	f	-85.8768478592	11.7433564796	80	f	0	0	f	12	3	\N
489	San Miguel Alvarez	1	\N	\N	f	-85.8768478592	11.7433564796	80	f	0	0	f	12	1	\N
490	Los Negritos	1	\N	\N	f	-85.8769381530	11.7437185127	80	f	0	0	f	12	1	\N
491	Los Negritos	1	\N	\N	f	-85.8770553521	11.7442343647	80	f	0	0	f	12	2	\N
492	Los Negritos	1	\N	\N	f	-85.8742716570	11.7450643004	80	f	0	0	f	12	3	\N
493	Cerro El Pilon	1	\N	\N	f	-85.8742716570	11.7450643004	80	f	0	0	f	12	1	\N
494	Cerro El Pilon	1	\N	\N	f	-85.8742441661	11.7450551500	80	f	0	0	f	12	2	\N
495	Cerro El Pilon	1	\N	\N	f	-85.8742441661	11.7450551500	80	f	0	0	f	12	3	\N
496	Cerro El Pilon	1	\N	\N	f	-85.8742441661	11.7450551500	80	f	0	0	f	12	4	\N
497	Cerro El Pilon	1	\N	\N	f	-85.8742441661	11.7450551500	80	f	0	0	f	12	5	\N
498	Cerro El Pilon	1	\N	\N	f	-85.8742441661	11.7450551500	80	f	0	0	f	12	6	\N
499	Cerro El Pilon	1	\N	\N	f	-85.8742441661	11.7450551500	80	f	0	0	f	12	7	\N
500	Cerro El Pilon	1	\N	\N	f	-85.8742441661	11.7450551500	80	f	0	0	f	12	8	\N
501	Cerro El Pilon	1	\N	\N	f	-85.8742441661	11.7450551500	80	f	0	0	f	12	9	\N
502	Punta Julieta	1	\N	\N	f	-85.8742441661	11.7450551500	80	f	0	0	f	12	1	\N
503	Isla El Muerto	1	\N	\N	f	-85.8695898696	11.7725422624	80	f	0	0	f	12	1	\N
504	Isla El Guanacaste	1	\N	\N	f	-85.8751368461	11.7557191128	80	f	0	0	f	12	1	\N
505	Isla El Molenillo	1	\N	\N	f	-85.8704011073	11.7491450007	80	f	0	0	f	12	1	\N
506	Comunidad San Fernando	1	\N	\N	f	-85.8681971346	11.7495612370	80	f	0	0	f	12	1	\N
507	Comunidad San Fernando	1	\N	\N	f	-85.8672344430	11.7516099311	80	f	0	0	f	12	2	\N
508	Comunidad El Bambu.	1	\N	\N	f	-85.8696772710	11.7601280651	80	f	0	0	f	12	1	\N
509	Comunidad El Bambu.	1	\N	\N	f	-85.8702107453	11.7620741917	80	f	0	0	f	12	2	\N
510	Comunidad El Bambu.	1	\N	\N	f	-85.8704017534	11.7624908769	80	f	0	0	f	12	3	\N
511	Comunidad El Bambu.	1	\N	\N	f	-85.8703911753	11.7628344277	80	f	0	0	f	12	4	\N
512	Comunidad El Bambu.	1	\N	\N	f	-85.8709857642	11.7632979214	80	f	0	0	f	12	5	\N
513	Comunidad El Bambu.	1	\N	\N	f	-85.8709857642	11.7632979214	80	f	0	0	f	12	6	\N
514	Comunidad El Bambu.	1	\N	\N	f	-85.8684836065	11.7670403906	80	f	0	0	f	12	7	\N
515	Comunidad Santa Maria	1	\N	\N	f	-85.8659192864	11.7635400215	80	f	0	0	f	12	1	\N
516	Comunidad Santa Maria	1	\N	\N	f	-85.8666993593	11.7635250407	80	f	0	0	f	12	2	\N
517	Comunidad La Perra.	1	\N	\N	f	-85.8377633004	11.7618985230	80	f	0	0	f	12	1	\N
518	Comunidad La Perra.	1	\N	\N	f	-85.8354649142	11.7607317896	80	f	0	0	f	12	2	\N
519	Comunidad La Perra.	1	\N	\N	f	-85.8398659265	11.7616087087	80	f	0	0	f	12	3	\N
520	Comunidad La Perra.	1	\N	\N	f	-85.8399744514	11.7619889071	80	f	0	0	f	12	4	\N
521	Comunidad La Perra.	1	\N	\N	f	-85.8427803289	11.7624976138	80	f	0	0	f	12	5	\N
522	Comunidad La Perra.	1	\N	\N	f	-85.8440589782	11.7639494967	80	f	0	0	f	12	6	\N
523	Isla Jesus Grande	1	\N	\N	f	-85.8519956463	11.7752658173	80	f	0	0	f	12	1	\N
524	Comunidad San Miguel	1	\N	\N	f	-85.8625770891	11.7684726170	80	f	0	0	f	12	1	\N
525	Comunidad San Miguel	1	\N	\N	f	-85.8625770891	11.7684726170	80	f	0	0	f	12	2	\N
526	Comunidad San Miguel	1	\N	\N	f	-85.8625770891	11.7684726170	80	f	0	0	f	12	3	\N
527	Comunidad San Miguel	1	\N	\N	f	-85.8625770891	11.7684726170	80	f	0	0	f	12	4	\N
528	Comunidad San Miguel	1	\N	\N	f	-85.8625770891	11.7684726170	80	f	0	0	f	12	5	\N
529	Comunidad San Miguel	1	\N	\N	f	-85.8625770891	11.7684726170	80	f	0	0	f	12	6	\N
530	Comunidad San Miguel	1	\N	\N	f	-85.8625770891	11.7684726170	80	f	0	0	f	12	7	\N
531	Comunidad San Miguel	1	\N	\N	f	-85.8625770891	11.7684726170	80	f	0	0	f	12	8	\N
532	Comunidad San Miguel	1	\N	\N	f	-85.8625770891	11.7684726170	80	f	0	0	f	12	9	\N
533	Comunidad San Miguel	1	\N	\N	f	-85.8625770891	11.7684726170	80	f	0	0	f	12	10	\N
534	Comunidad San Miguel	1	\N	\N	f	-85.8625770891	11.7684726170	80	f	0	0	f	12	11	\N
535	Comunidad San Miguel	1	\N	\N	f	-85.8625770891	11.7684726170	80	f	0	0	f	12	12	\N
536	Comunidad San Miguel	1	\N	\N	f	-85.8625770891	11.7684726170	80	f	0	0	f	12	13	\N
537	Comunidad San Miguel	1	\N	\N	f	-85.8625770891	11.7684726170	80	f	0	0	f	12	14	\N
538	Isla El Platano	1	\N	\N	f	-85.9023302080	11.7316105513	80	f	0	0	f	12	1	\N
539	Isla El Platano	1	\N	\N	f	-85.9023302080	11.7316105513	80	f	0	0	f	12	2	\N
540	Isla El Platano	1	\N	\N	f	-85.9023302080	11.7316105513	80	f	0	0	f	12	3	\N
541	Isla El Platano	1	\N	\N	f	-85.9023302080	11.7316105513	80	f	0	0	f	12	4	\N
542	Isla El Platano	1	\N	\N	f	-85.9023302080	11.7316105513	80	f	0	0	f	12	5	\N
543	Isla El Platano	1	\N	\N	f	-85.9023302080	11.7316105513	80	f	0	0	f	12	6	\N
544	Isla El Platano	1	\N	\N	f	-85.9023302080	11.7316105513	80	f	0	0	f	12	7	\N
545	Valle Los Rugama	1	\N	\N	f	-85.6374705135	13.3866055448	80	f	0	0	f	12	1	\N
546	Valle Los Rugama	1	\N	\N	f	-85.6394018115	13.4568577557	80	f	0	0	f	12	2	\N
547	Valle Los Rugama	1	\N	\N	f	-85.6500433865	13.4516989898	80	f	0	0	f	12	3	\N
548	Valle Los Rugama	1	\N	\N	f	-85.6377679665	13.3862545841	80	f	0	0	f	12	4	\N
549	Valle Los Rugama	1	\N	\N	f	-85.6511991747	13.4630234542	80	f	0	0	f	12	5	\N
550	Valle Los Rugama	1	\N	\N	f	-85.6505426360	13.4516112690	80	f	0	0	f	12	6	\N
551	Valle Los Rugama	1	\N	\N	f	-85.6499131712	13.4518610130	80	f	0	0	f	12	7	\N
552	Valle Los Rugama	1	\N	\N	f	-85.6451990828	13.4557861844	80	f	0	0	f	12	8	\N
553	Valle Los Rugama	1	\N	\N	f	-85.6458533236	13.4560609146	80	f	0	0	f	12	9	\N
554	Valle Los Rugama	1	\N	\N	f	-85.6417745845	13.4570333145	80	f	0	0	f	12	10	\N
555	Valle Los Rugama	1	\N	\N	f	-85.6504243946	13.4644747544	80	f	0	0	f	12	11	\N
556	Valle Los Rugama	1	\N	\N	f	-85.6484980323	13.4653864887	80	f	0	0	f	12	12	\N
557	Valle Los Rugama	1	\N	\N	f	-85.6357349875	13.4747824447	80	f	0	0	f	12	13	\N
558	Valle Los Rugama	1	\N	\N	f	-85.6361252012	13.4760140167	80	f	0	0	f	12	14	\N
559	Valle Los Rugama	1	\N	\N	f	-85.6383021080	13.4749229479	80	f	0	0	f	12	15	\N
560	Valle Los Rugama	1	\N	\N	f	-85.6365060133	13.4756454427	80	f	0	0	f	12	16	\N
561	Valle Los Rugama	1	\N	\N	f	-85.6369437483	13.4766422299	80	f	0	0	f	12	17	\N
562	Valle Los Rugama	1	\N	\N	f	-85.6453641641	13.4559949973	80	f	0	0	f	12	18	\N
563	Valle Los Rugama	1	\N	\N	f	-85.6461310681	13.4559448895	80	f	0	0	f	12	19	\N
564	Valle Los Rugama	1	\N	\N	f	-85.6497565645	13.4517878507	80	f	0	0	f	12	20	\N
565	Valle Los Rugama	1	\N	\N	f	-85.6399276561	13.4602416018	80	f	0	0	f	12	21	\N
566	Valle Los Rugama	1	\N	\N	f	-85.6393532823	13.4572733377	80	f	0	0	f	12	22	\N
567	Valle Los Rugama	1	\N	\N	f	-85.6543699699	13.4493265460	80	f	0	0	f	12	23	\N
568	Valle Los Rugama	1	\N	\N	f	-85.6392312209	13.4576252407	80	f	0	0	f	12	24	\N
569	Valle Los Rugama	1	\N	\N	f	-85.6422450174	13.4571443383	80	f	0	0	f	12	25	\N
570	Valle Los Rugama	1	\N	\N	f	-85.6502726443	13.4635337648	80	f	0	0	f	12	26	\N
571	Valle Los Rugama	1	\N	\N	f	-85.6393059783	13.4574719638	80	f	0	0	f	12	27	\N
572	Valle Los Rugama	1	\N	\N	f	-85.6537763412	13.4630011197	80	f	0	0	f	12	28	\N
573	Valle Los Rugama	1	\N	\N	f	-85.6534379797	13.4623936161	80	f	0	0	f	12	29	\N
574	Valle Los Rugama	1	\N	\N	f	-85.6349891133	13.4743715855	80	f	0	0	f	12	30	\N
575	Valle Los Rugama	1	\N	\N	f	-85.6405685280	13.4579760038	80	f	0	0	f	12	31	\N
576	Valle Los Rugama	1	\N	\N	f	-85.6403379294	13.4579205154	80	f	0	0	f	12	32	\N
577	Valle Los Rugama	1	\N	\N	f	-85.6484659984	13.4743812418	80	f	0	0	f	12	33	\N
578	Valle Los Rugama	1	\N	\N	f	-85.6400724273	13.4575032336	80	f	0	0	f	12	34	\N
579	Valle Los Zeledones	1	\N	\N	f	-85.6723027169	13.4550810628	80	f	0	0	f	12	1	\N
580	Valle Los Zeledones	1	\N	\N	f	-85.6645539239	13.4382975652	80	f	0	0	f	12	2	\N
581	Valle Los Zeledones	1	\N	\N	f	-85.6664217468	13.4462538118	80	f	0	0	f	12	3	\N
582	Valle Los Zeledones	1	\N	\N	f	-85.6840643421	13.4459670623	80	f	0	0	f	12	4	\N
583	Valle Los Zeledones	1	\N	\N	f	-85.6649553431	13.3504467208	80	f	0	0	f	12	5	\N
584	Valle Los Zeledones	1	\N	\N	f	-85.6830797111	13.4486830250	80	f	0	0	f	12	6	\N
585	Valle Los Zeledones	1	\N	\N	f	-85.6720602820	13.4387260241	80	f	0	0	f	12	7	\N
586	Valle Los Zeledones	1	\N	\N	f	-85.6738286002	13.4379488509	80	f	0	0	f	12	8	\N
587	Valle Los Zeledones	1	\N	\N	f	-85.6605148895	13.4388365865	80	f	0	0	f	12	9	\N
588	Valle Los Zeledones	1	\N	\N	f	-85.6627952819	13.4406567557	80	f	0	0	f	12	10	\N
589	Valle Los Zeledones	1	\N	\N	f	-85.6624505066	13.4412063745	80	f	0	0	f	12	11	\N
590	Valle Los Zeledones	1	\N	\N	f	-85.6664881808	13.4409204439	80	f	0	0	f	12	12	\N
591	Valle Los Zeledones	1	\N	\N	f	-85.6676738454	13.4536643753	80	f	0	0	f	12	13	\N
592	Valle Los Zeledones	1	\N	\N	f	-85.6680726978	13.4533591224	80	f	0	0	f	12	14	\N
593	Valle Los Zeledones	1	\N	\N	f	-85.6696786247	13.4502306749	80	f	0	0	f	12	15	\N
594	Valle Los Zeledones	1	\N	\N	f	-85.6663393243	13.4461268119	80	f	0	0	f	12	16	\N
595	Valle Los Zeledones	1	\N	\N	f	-85.6703466196	13.4396390761	80	f	0	0	f	12	17	\N
596	Valle Los Zeledones	1	\N	\N	f	-85.6755918468	13.4380937416	80	f	0	0	f	12	18	\N
597	Valle Los Zeledones	1	\N	\N	f	-85.6669180512	13.4567159545	80	f	0	0	f	12	19	\N
598	Valle Los Zeledones	1	\N	\N	f	-85.6669180512	13.4567159545	80	f	0	0	f	12	20	\N
599	Valle Los Zeledones	1	\N	\N	f	-85.6669180512	13.4567159545	80	f	0	0	f	12	21	\N
600	Valle Los Zeledones	1	\N	\N	f	-85.6635569917	13.4531995495	80	f	0	0	f	12	22	\N
601	Valle Los Zeledones	1	\N	\N	f	-85.6652039774	13.4409949949	80	f	0	0	f	12	23	\N
602	Valle Los Zeledones	1	\N	\N	f	-85.6690898193	13.4531565855	80	f	0	0	f	12	24	\N
603	Valle Los Zeledones	1	\N	\N	f	-85.6645524511	13.4402321603	80	f	0	0	f	12	25	\N
604	Valle Los Zeledones	1	\N	\N	f	-85.6711419102	13.4562139943	80	f	0	0	f	12	26	\N
605	Valle Los Zeledones	1	\N	\N	f	-85.6788608627	13.4364927228	80	f	0	0	f	12	27	\N
606	Valle Los Zeledones	1	\N	\N	f	-85.6664145455	13.4408748525	80	f	0	0	f	12	28	\N
607	Valle Los Zeledones	1	\N	\N	f	-85.6667314603	13.4537136193	80	f	0	0	f	12	29	\N
608	Valle Los Zeledones	1	\N	\N	f	-85.6671075444	13.4591939735	80	f	0	0	f	12	30	\N
609	Valle Los Zeledones	1	\N	\N	f	-85.6709850217	13.4394345242	80	f	0	0	f	12	31	\N
610	Valle Los Zeledones	1	\N	\N	f	-85.6630741982	13.4519855970	80	f	0	0	f	12	32	\N
611	Valle Los Zeledones	1	\N	\N	f	-85.6649364442	13.4409393340	80	f	0	0	f	12	33	\N
612	Valle Los Zeledones	1	\N	\N	f	-85.6715390188	13.4562251341	80	f	0	0	f	12	34	\N
613	Valle Los Zeledones	1	\N	\N	f	-85.6744609189	13.4539714675	80	f	0	0	f	12	35	\N
614	Valle Los Zeledones	1	\N	\N	f	-85.6674411515	13.4389456878	80	f	0	0	f	12	36	\N
615	Santa Maria del Cedros	1	\N	\N	f	-85.5636708761	13.4665789565	80	f	0	0	f	12	1	\N
616	Santa Maria del Cedros	1	\N	\N	f	-85.5461460881	13.4517974897	80	f	0	0	f	12	2	\N
617	Santa Maria del Cedros	1	\N	\N	f	-85.5492494219	13.4502515382	80	f	0	0	f	12	3	\N
618	Santa Maria del Cedros	1	\N	\N	f	-85.5710217742	13.4637010253	80	f	0	0	f	12	4	\N
619	Santa Maria del Cedros	1	\N	\N	f	-85.5740697020	13.4699558622	80	f	0	0	f	12	5	\N
620	Santa Maria del Cedros	1	\N	\N	f	-85.5571308574	13.4620486700	80	f	0	0	f	12	6	\N
621	Santa Maria del Cedros	1	\N	\N	f	-85.5576922007	13.4531386311	80	f	0	0	f	12	7	\N
622	Santa Maria del Cedros	1	\N	\N	f	-85.5549960672	13.4514688567	80	f	0	0	f	12	8	\N
623	Santa Maria del Cedros	1	\N	\N	f	-85.5608213754	13.4642665232	80	f	0	0	f	12	9	\N
624	Santa Maria del Cedros	1	\N	\N	f	-85.5608213754	13.4642665232	80	f	0	0	f	12	10	\N
625	Santa Maria del Cedros	1	\N	\N	f	-85.5598071493	13.4608798139	80	f	0	0	f	12	11	\N
626	Santa Maria del Cedros	1	\N	\N	f	-85.5504631839	13.4603559933	80	f	0	0	f	12	12	\N
627	Santa Maria del Cedros	1	\N	\N	f	-85.5615101943	13.4633755268	80	f	0	0	f	12	13	\N
628	Santa Maria del Cedros	1	\N	\N	f	-85.5739889310	13.4695576491	80	f	0	0	f	12	14	\N
629	Santa Maria del Cedros	1	\N	\N	f	-85.5578976094	13.4620259544	80	f	0	0	f	12	15	\N
630	Santa Maria del Cedros	1	\N	\N	f	-85.5568725198	13.4620019866	80	f	0	0	f	12	16	\N
631	Santa Maria del Cedros	1	\N	\N	f	-85.5568725198	13.4620019866	80	f	0	0	f	12	17	\N
632	Santa Maria del Cedros	1	\N	\N	f	-85.5742748303	13.4696315920	80	f	0	0	f	12	18	\N
633	Santa Maria del Cedros	1	\N	\N	f	-85.5461843593	13.4531084796	80	f	0	0	f	12	19	\N
634	Santa Maria del Cedros	1	\N	\N	f	-85.5723702845	13.4668184095	80	f	0	0	f	12	20	\N
635	Santa Maria del Cedros	1	\N	\N	f	-85.5753146136	13.4718613044	80	f	0	0	f	12	21	\N
636	Santa Maria del Cedros	1	\N	\N	f	-85.5681359931	13.4581612362	80	f	0	0	f	12	22	\N
637	Santa Maria del Cedros	1	\N	\N	f	-85.5693603451	13.4619559130	80	f	0	0	f	12	23	\N
638	Santa Maria del Cedros	1	\N	\N	f	-85.5600797596	13.4539026287	80	f	0	0	f	12	24	\N
639	Santa Maria del Cedros	1	\N	\N	f	-85.5493999103	13.4498004169	80	f	0	0	f	12	25	\N
640	Santa Maria del Cedros	1	\N	\N	f	-85.5646532357	13.4644782916	80	f	0	0	f	12	26	\N
641	Santa Maria del Cedros	1	\N	\N	f	-85.5590654809	13.4582540112	80	f	0	0	f	12	27	\N
642	Santa Maria del Cedros	1	\N	\N	f	-85.5496114445	13.4576210597	80	f	0	0	f	12	28	\N
643	Santa Maria del Cedros	1	\N	\N	f	-85.5587493249	13.4616963700	80	f	0	0	f	12	29	\N
644	Santa Maria del Cedros	1	\N	\N	f	-85.5625929982	13.4676575677	80	f	0	0	f	12	30	\N
645	Santa Maria del Cedros	1	\N	\N	f	-85.5458350417	13.4528352650	80	f	0	0	f	12	31	\N
646	Santa Maria del Cedros	1	\N	\N	f	-85.5725855056	13.4663495600	80	f	0	0	f	12	32	\N
647	Santa Maria del Cedros	1	\N	\N	f	-85.5720142526	13.4661112753	80	f	0	0	f	12	33	\N
648	Santa Maria del Cedros	1	\N	\N	f	-85.5495698915	13.4507144173	80	f	0	0	f	12	34	\N
649	Santa Maria del Cedros	1	\N	\N	f	-85.5747457538	13.4712162439	80	f	0	0	f	12	35	\N
650	Santa Maria del Cedros	1	\N	\N	f	-85.5747457538	13.4712162439	80	f	0	0	f	12	36	\N
651	Santa Maria del Cedros	1	\N	\N	f	-85.5747457538	13.4712162439	80	f	0	0	f	12	37	\N
652	Santa Maria del Cedros	1	\N	\N	f	-85.5577312137	13.4512495318	80	f	0	0	f	12	38	\N
\.


--
-- Data for Name: organizaciones_sistemaejecucion_fuente; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY organizaciones_sistemaejecucion_fuente (id, sistemaejecucion_id, fuenterenovable_id) FROM stdin;
251	32	4
252	33	4
253	24	3
254	25	7
255	34	7
256	36	4
257	37	4
258	41	3
259	42	3
260	47	6
261	48	3
262	48	5
263	30	4
264	1123	5
265	652	5
266	651	5
267	650	5
268	649	5
269	648	5
270	647	5
271	646	5
272	645	5
273	644	5
274	643	5
275	642	5
276	641	5
277	640	5
278	639	5
279	638	5
280	637	5
281	636	5
282	635	5
283	634	5
284	633	5
285	632	5
286	631	5
287	630	5
288	629	5
289	628	5
290	627	5
291	626	5
292	625	5
293	624	5
294	623	5
295	622	5
296	621	5
134	2	5
297	620	5
136	4	5
298	619	5
138	6	5
299	618	5
140	8	5
300	617	5
142	10	5
143	11	5
144	12	5
145	13	5
146	14	5
147	15	5
148	16	5
149	17	5
150	18	5
151	19	5
152	20	5
153	21	5
301	616	5
302	615	5
303	614	5
304	613	5
305	612	5
306	611	5
307	610	5
308	609	5
309	608	5
167	35	5
170	38	5
310	607	5
311	606	5
312	605	5
313	604	5
175	44	5
176	45	5
177	46	5
314	603	5
180	49	5
181	50	5
182	51	5
183	52	5
184	53	5
185	54	5
186	57	5
187	58	5
188	59	5
189	60	5
190	61	5
191	62	5
192	63	5
193	64	5
194	65	5
195	66	5
196	67	5
197	68	5
198	69	5
199	70	5
200	71	5
201	72	5
202	73	5
203	74	5
204	75	5
205	76	5
206	77	5
207	78	5
208	79	5
209	80	5
210	81	5
211	82	5
212	83	5
213	84	5
214	85	5
215	86	5
216	87	5
217	88	5
218	89	5
219	90	5
220	91	5
221	92	5
222	95	5
223	97	5
224	99	5
315	602	5
316	601	5
317	600	5
318	599	5
233	40	5
234	39	5
319	598	5
320	597	5
237	3	3
238	5	3
239	7	3
240	9	3
241	22	4
242	23	4
243	102	4
244	26	4
245	27	4
321	596	5
247	28	4
248	29	4
250	31	4
322	595	5
323	594	5
324	593	5
325	592	5
326	591	5
327	590	5
328	589	5
329	588	5
330	587	5
331	586	5
332	585	5
333	584	5
334	583	5
335	582	5
336	581	5
337	580	5
338	579	5
339	578	5
340	577	5
341	576	5
342	575	5
343	574	5
344	573	5
345	572	5
346	571	5
347	570	5
348	569	5
349	568	5
350	567	5
351	566	5
352	565	5
353	564	5
354	563	5
355	562	5
356	561	5
357	560	5
358	559	5
359	558	5
360	557	5
361	556	5
362	555	5
363	554	5
364	553	5
365	552	5
366	551	5
367	550	5
368	549	5
369	548	5
370	547	5
371	546	5
372	545	5
373	544	5
374	543	5
375	542	5
376	541	5
377	540	5
378	539	5
379	538	5
380	537	5
381	536	5
382	535	5
383	534	5
384	533	5
385	532	5
386	531	5
387	530	5
388	529	5
389	528	5
390	527	5
391	526	5
392	525	5
393	524	5
394	523	5
395	522	5
396	521	5
397	520	5
398	519	5
399	518	5
400	517	5
401	516	5
402	515	5
403	514	5
404	513	5
405	512	5
406	511	5
407	510	5
408	509	5
409	508	5
410	507	5
412	505	5
413	504	5
414	503	5
415	502	5
416	501	5
417	500	5
418	499	5
419	498	5
420	497	5
421	496	5
422	495	5
423	494	5
424	493	5
425	492	5
426	491	5
427	490	5
428	489	5
429	488	5
430	487	5
431	486	5
432	485	5
433	484	5
434	483	5
435	482	5
436	481	5
437	480	5
438	479	5
439	478	5
440	477	5
441	476	5
442	475	5
443	474	5
444	473	5
445	472	5
446	471	5
447	470	5
448	469	5
449	468	5
450	467	5
451	466	5
452	465	5
453	464	5
454	463	5
455	462	5
456	461	5
457	460	5
458	459	5
459	458	5
460	457	5
461	456	5
462	455	5
463	454	5
464	453	5
465	452	5
466	451	5
467	450	5
468	449	5
469	448	5
470	447	5
471	446	5
472	445	5
473	444	5
474	443	5
475	442	5
476	441	5
477	440	5
478	439	5
479	438	5
480	437	5
481	436	5
482	435	5
483	434	5
484	433	5
485	432	5
486	431	5
487	430	5
488	429	5
489	428	5
490	427	5
491	426	5
492	425	5
493	424	5
494	423	5
495	422	5
496	421	5
497	420	5
498	419	5
499	418	5
500	417	5
501	416	5
502	415	5
503	414	5
504	413	5
505	412	5
506	411	5
507	410	5
508	409	5
509	408	5
510	407	5
511	406	5
512	405	5
513	404	5
514	403	5
515	402	5
516	401	5
517	400	5
518	399	5
519	398	5
520	397	5
521	396	5
522	395	5
523	394	5
524	393	5
525	392	5
526	391	5
527	390	5
528	389	5
529	388	5
530	387	5
531	386	5
532	385	5
533	384	5
534	383	5
535	382	5
536	381	5
537	380	5
538	379	5
539	378	5
540	377	5
541	376	5
542	375	5
543	374	5
544	373	5
545	372	5
546	371	5
547	370	5
548	369	5
549	368	5
550	367	5
551	366	5
552	365	5
553	364	5
554	363	5
555	362	5
556	361	5
557	360	5
558	359	5
559	358	5
560	357	5
561	356	5
562	355	5
563	354	5
564	353	5
565	352	5
566	351	5
567	350	5
568	349	5
569	348	5
570	347	5
571	346	5
572	345	5
573	344	5
574	343	5
575	342	5
576	341	5
577	340	5
578	339	5
579	338	5
580	337	5
581	336	5
582	335	5
583	334	5
584	333	5
585	332	5
586	331	5
587	330	5
588	329	5
589	328	5
590	327	5
591	326	5
592	325	5
593	324	5
594	323	5
595	322	5
596	321	5
597	320	5
598	319	5
599	318	5
600	317	5
601	316	5
602	315	5
603	314	5
604	313	5
605	312	5
606	311	5
607	310	5
608	309	5
609	308	5
610	307	5
611	306	5
612	305	5
613	304	5
614	303	5
615	302	5
616	301	5
617	300	5
618	299	5
619	298	5
620	297	5
621	296	5
622	295	5
623	294	5
624	293	5
625	292	5
626	291	5
627	290	5
628	289	5
629	288	5
630	287	5
631	286	5
632	285	5
633	284	5
634	283	5
635	282	5
636	281	5
637	280	5
638	279	5
639	278	5
640	277	5
641	276	5
642	275	5
643	274	5
644	273	5
645	272	5
646	271	5
647	270	5
648	269	5
649	268	5
650	267	5
651	266	5
652	265	5
653	264	5
654	263	5
655	262	5
656	261	5
657	260	5
658	259	5
659	258	5
660	257	5
661	256	5
662	255	5
663	254	5
664	253	5
665	252	5
666	251	5
667	250	5
668	249	5
669	248	5
670	247	5
671	246	5
672	245	5
673	244	5
674	243	5
675	242	5
676	241	5
677	240	5
678	239	5
679	238	5
680	237	5
681	236	5
682	235	5
683	234	5
684	233	5
685	232	5
686	231	5
687	230	5
688	229	5
689	228	5
690	227	5
691	226	5
692	225	5
693	224	5
694	223	5
695	222	5
696	221	5
697	220	5
698	219	5
699	218	5
700	217	5
701	216	5
702	215	5
703	214	5
704	213	5
705	212	5
706	211	5
707	210	5
708	209	5
709	208	5
710	207	5
711	206	5
712	205	5
713	204	5
714	203	5
715	202	5
716	201	5
717	200	5
718	199	5
719	198	5
720	197	5
721	196	5
722	195	5
723	194	5
724	193	5
725	192	5
726	191	5
727	190	5
728	189	5
729	188	5
730	187	5
731	186	5
732	185	5
733	184	5
734	183	5
735	182	5
736	181	5
737	180	5
738	179	5
739	178	5
740	177	5
741	176	5
742	175	5
743	174	5
744	173	5
745	172	5
746	171	5
747	170	5
748	169	5
749	168	5
750	167	5
751	166	5
752	165	5
753	164	5
754	163	5
755	162	5
756	161	5
757	160	5
758	159	5
759	158	5
760	157	5
761	156	5
762	155	5
763	154	5
764	153	5
765	152	5
766	151	5
767	150	5
768	149	5
769	148	5
770	147	5
771	146	5
772	145	5
773	144	5
774	143	5
775	142	5
776	141	5
777	140	5
778	139	5
779	138	5
780	137	5
781	136	5
782	135	5
783	134	5
784	133	5
785	132	5
786	131	5
787	130	5
788	129	5
789	128	5
790	127	5
791	126	5
792	125	5
793	124	5
794	123	5
795	122	5
796	121	5
797	120	5
798	119	5
799	118	5
800	117	5
801	116	5
802	115	5
803	114	5
804	113	5
805	112	5
806	111	5
807	110	5
808	109	5
809	108	5
810	107	5
811	106	5
812	105	5
813	104	5
814	506	5
\.


--
-- Data for Name: organizaciones_usuariosbeneficiarios; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY organizaciones_usuariosbeneficiarios (id, sistema_id, anho, numero) FROM stdin;
1	28	2012	4308
\.


--
-- Data for Name: south_migrationhistory; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY south_migrationhistory (id, app_name, migration, applied) FROM stdin;
1	organizaciones	0001_initial	2012-02-18 00:25:54.310869+00
2	organizaciones	0002_initial	2012-02-18 00:27:50.859304+00
3	organizaciones	0003_auto__add_field_organizacion_renovable__add_field_organizacion_asociac	2012-02-21 23:01:55.852269+00
4	indicadores	0001_initial	2012-02-29 22:14:27.999692+00
5	indicadores	0002_auto__add_field_indicadoressectoriales_empleos	2012-02-29 22:14:28.245647+00
6	indicadores	0003_auto__chg_field_indicadoressectoriales_poblacion__chg_field_indicadore	2012-03-01 01:12:26.266048+00
7	indicadores	0004_auto__chg_field_indicadoressectoriales_poblacion__chg_field_indicadore	2012-03-01 01:12:26.31854+00
8	nomencladores	0001_initial	2012-03-06 21:28:58.207739+00
9	nomencladores	0002_initial	2012-03-06 21:28:58.335664+00
10	nomencladores	0003_initial	2012-03-06 21:28:58.338349+00
11	organizaciones	0004_initial	2012-03-06 21:29:03.532469+00
12	indicadores	0005_auto__add_valorindicador__add_tipoindicador__add_indicador__del_field_	2012-03-14 21:17:38.952359+00
13	organizaciones	0005_auto__del_field_organizacion_actividad__add_field_proyecto_enlace__add	2012-03-15 20:45:43.303769+00
14	organizaciones	0006_auto__chg_field_proyecto_donante	2012-03-15 23:23:09.494996+00
15	organizaciones	0007_auto__chg_field_organizacion_tipo	2012-03-16 02:42:42.223899+00
17	organizaciones	0009_auto__chg_field_contacto_categoria	2012-03-16 02:56:58.859964+00
19	organizaciones	0008_auto__add_field_contacto_emails	2012-03-31 02:25:30.059227+00
20	organizaciones	0010_auto__del_field_contacto_emails	2012-03-31 02:25:30.10407+00
21	organizaciones	0011_auto__add_field_organizacion_user_id	2012-03-31 02:26:37.52975+00
\.


--
-- Data for Name: thumbnail_kvstore; Type: TABLE DATA; Schema: public; Owner: renovable
--

COPY thumbnail_kvstore (key, value) FROM stdin;
sorl-thumbnail||image||73442eb7b9c4daf1130d1d183f119a37	{"storage": "django.core.files.storage.FileSystemStorage", "name": "/var/www/renovables/media/organizacion/adm_1.png", "size": [143, 142]}
sorl-thumbnail||image||31c2a912d7c77884962df4f29ab7260e	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/8a/3a/8a3a195872c07747061657d12f93b30b.jpg", "size": [81, 80]}
sorl-thumbnail||thumbnails||73442eb7b9c4daf1130d1d183f119a37	["31c2a912d7c77884962df4f29ab7260e"]
sorl-thumbnail||image||684a5456e54658b509468b48823a4d86	{"storage": "django.core.files.storage.FileSystemStorage", "name": "/var/www/renovables/media/organizacion/PCH_Bilampi.jpg", "size": [55, 55]}
sorl-thumbnail||image||f34ee42b70fde80d158da707b0c7aeef	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/fd/78/fd783829c4c4c1baa6ab60650f1e1512.jpg", "size": [55, 55]}
sorl-thumbnail||thumbnails||1e4eb90a16cc4f903d2d977c0da4cb6e	["c6662b28565b6aa21310cec99338c6ab", "400547bacb22862bb4a02520c0fe7a85", "d369afaa30bb897f6d8a4b1e5913135d", "069d03170e1b511eb99a6e26d9556f49", "c97ab50e0633fe57780b65ea294f0dcc", "ecf76d4bd3c5a86e408b35a70dfbdd40"]
sorl-thumbnail||image||0bbd8037b9ad0614f8dfbaaa73d3dc7e	{"storage": "django.core.files.storage.FileSystemStorage", "name": "/var/www/renovables/media/organizacion/UNA.jpg", "size": [60, 63]}
sorl-thumbnail||image||922dc4f43ca01fdfb9981a88978bb353	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/85/f0/85f02c7a7866e3af1830664dc4e7ecae.jpg", "size": [60, 63]}
sorl-thumbnail||thumbnails||e213f213558cfc3baf3a95a9c6bd44de	["c6868457748eeb0e116d08e26d4b7e5c", "20cec68593b987f0e87be76d7b7cd4ff", "3009bbb1a333d8aa3a5eecd73fd1c6e0", "da40a7417947b69573b0b1e6fab32223", "c8f5f9072c5e05bf387a98f568944d86", "ae8b1b1c47673279e7d267338a345929"]
sorl-thumbnail||image||48716b5dd015013575d4f8a9ec4356c1	{"storage": "django.core.files.storage.FileSystemStorage", "name": "/var/www/renovables/media/organizacion/GFenix.jpg", "size": [93, 108]}
sorl-thumbnail||image||a4b07885f1ab1406279a02ef6b63d212	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/b7/d3/b7d390d4b9ecb2b1cdb1fba42ab44a54.jpg", "size": [69, 80]}
sorl-thumbnail||image||643ec88f3a1450e79e959e41a2b0f5ae	{"storage": "django.core.files.storage.FileSystemStorage", "name": "/var/www/renovables/media/organizacion/AsoFenix.jpg", "size": [108, 45]}
sorl-thumbnail||image||315efa41374c95dd917546b027d34403	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/d3/c7/d3c78a8ae397c4f1e646e3a78a12db8f.jpg", "size": [108, 45]}
sorl-thumbnail||thumbnails||684a5456e54658b509468b48823a4d86	["35f7b32146b4c356603f51f20e06fa35", "f34ee42b70fde80d158da707b0c7aeef", "689640cd61259cce2403092e0ee25f49", "d43e0a65e9d1298384074d8bd8974be0", "5ef11ffabc0ac9106c111ab6649f2dc8", "97595a79e3263a9d9486b3bc853679a1"]
sorl-thumbnail||image||9106e6d2a62c9cc3411ada53fb33629d	{"storage": "django.core.files.storage.FileSystemStorage", "name": "/var/www/renovables/media/organizacion/Amayo.jpeg", "size": [105, 47]}
sorl-thumbnail||image||b0523b6274c18cd44487ec1e44316b6b	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/7a/22/7a2214b7e16e07fdb84d90c0146bfe3d.jpg", "size": [105, 47]}
sorl-thumbnail||image||e213f213558cfc3baf3a95a9c6bd44de	{"storage": "django.core.files.storage.FileSystemStorage", "name": "/var/www/renovables/media/organizacion/ATDER.jpg", "size": [49, 91]}
sorl-thumbnail||image||c8f5f9072c5e05bf387a98f568944d86	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/68/8e/688eb588cb1a7da73d1f23bb22cf72da.jpg", "size": [43, 80]}
sorl-thumbnail||thumbnails||16c6395f92f495be698eaeff14100b4d	["820807e35b55b9acb18e39653ecb1605", "ee0edb1632856277e6bdab69110302aa", "b95c988c787a71b5c2383cc038c0c3c1"]
sorl-thumbnail||image||13c02a6eb6018836901bb08780fb185c	{"storage": "django.core.files.storage.FileSystemStorage", "name": "/var/www/renovables/media/organizacion/bE.jpg", "size": [240, 73]}
sorl-thumbnail||image||dbc4a91b03ccbe4857d84794becd35f3	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/37/66/3766413a043af1f68c43391ac4efbb0f.jpg", "size": [240, 73]}
sorl-thumbnail||image||1e4eb90a16cc4f903d2d977c0da4cb6e	{"storage": "django.core.files.storage.FileSystemStorage", "name": "/var/www/renovables/media/organizacion/CHN.jpg", "size": [130, 53]}
sorl-thumbnail||image||c6662b28565b6aa21310cec99338c6ab	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/14/a4/14a49451b245976d96bc718620e49390.jpg", "size": [130, 53]}
sorl-thumbnail||image||f0c7ec90e827330f6d15923a658fce62	{"storage": "django.core.files.storage.FileSystemStorage", "name": "/var/www/renovables/media/organizacion/logo_eolonica.jpg", "size": [292, 524]}
sorl-thumbnail||image||1e97c1ac9bcfd2cbe70571f28cd49586	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/9b/02/9b02668d83ba4f58fc861b2881ba0508.jpg", "size": [45, 80]}
sorl-thumbnail||image||b8397057b1fa1e0a659e6f944d26642a	{"storage": "django.core.files.storage.FileSystemStorage", "name": "/var/www/renovables/media/organizacion/GIER.jpg", "size": [250, 122]}
sorl-thumbnail||image||634a6bc30e646f1298bef91057a4b4b6	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/60/a7/60a73191322c83500f212ce419c6b205.jpg", "size": [164, 80]}
sorl-thumbnail||image||798e74b0f41fae046a0a80aec75561ae	{"storage": "django.core.files.storage.FileSystemStorage", "name": "/var/www/renovables/media/organizacion/HISMOW.jpg", "size": [41, 42]}
sorl-thumbnail||image||6c5c8846716a4b04b403b4b0ea5640df	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/cc/73/cc73fb099540e0737f028be6b8f6f12a.jpg", "size": [41, 42]}
sorl-thumbnail||image||16c6395f92f495be698eaeff14100b4d	{"storage": "django.core.files.storage.FileSystemStorage", "name": "/var/www/renovables/media/organizacion/IPLS.png", "size": [1025, 1034]}
sorl-thumbnail||image||ee0edb1632856277e6bdab69110302aa	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/fe/a8/fea8d97f9fedaacd0fb12c8802f5c4ba.jpg", "size": [79, 80]}
sorl-thumbnail||image||d306ad40c16afd31220f14353f227c1a	{"storage": "django.core.files.storage.FileSystemStorage", "name": "/var/www/renovables/media/organizacion/GLobeleq_Logo_CMYK.jpg", "size": [1720, 576]}
sorl-thumbnail||image||ec99ed5d6300645b4614e02c4a0cddac	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/d3/9c/d39cf32bcc8ba145aaa09c4da24e0436.jpg", "size": [239, 80]}
sorl-thumbnail||thumbnails||35593861fe31e2b10231372c7f086b55	["836e3b9c389547e4a351af71121a4e36", "c3fe16aa01a9d5340fc117a1ae653f73", "aea9e02ccfda6ce952efe5f7013ea967", "6ac2f4a24b20cc04f2f00dbde58c2063", "dbc22c986a1a93010b342cbebc79553d", "cba62545207c19ef165eb6dd02f4004f"]
sorl-thumbnail||image||8627b3ad33055312df1b9fa3205e4df8	{"storage": "django.core.files.storage.FileSystemStorage", "name": "/var/www/renovables/media/organizacion/LOGO_MUJERES_SOLARES.png", "size": [301, 301]}
sorl-thumbnail||image||1fc656dad2484ef833b9594dbbaa6d66	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/5a/a3/5aa362d173a8887f24975bbb51c76368.jpg", "size": [80, 80]}
sorl-thumbnail||thumbnails||8715606132625eb9a31a2997f6c401cb	["daa641300efb740913562d0caefe6227", "c607993264bd36d38facf79458d615aa", "084a8a562f11c5a777f19cea14caafae", "3a1a011d1c82c2c529894f293ea16253", "d1279476991d9a0f311eb87758d52f44", "fcacd711bd6d1570da0f6ede2b7c6f5d"]
sorl-thumbnail||image||f5c33f6909bd04207e2f9bc5bbca8075	{"storage": "django.core.files.storage.FileSystemStorage", "name": "/var/www/renovables/media/organizacion/LOGO_POLARIS.gif", "size": [3544, 1994]}
sorl-thumbnail||image||604173fe688efd934b4bb411513bfa88	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/23/56/235603efdcb718f0ff8ddf12786afdfd.jpg", "size": [142, 80]}
sorl-thumbnail||image||c3bbea23d9845fed0ba0c23cc5fb157a	{"storage": "django.core.files.storage.FileSystemStorage", "name": "/var/www/renovables/media/organizacion/prolena.jpg", "size": [226, 82]}
sorl-thumbnail||image||1c2c4f2db0dd157cfeb4bcd955931a38	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/b7/d7/b7d7bdfce586aa961d85b3e903e8f65c.jpg", "size": [220, 80]}
sorl-thumbnail||thumbnails||9106e6d2a62c9cc3411ada53fb33629d	["08089172a735fa552d06174eadd9693e", "5108eebf97aed777b13e219f457f69c8", "40c775c9b549e3b8ac71c3faa0bba115", "bd910af4c868c8ebe26c5bd897e37ea7", "3e58f976a2a148b6cd137b0b283406e5", "bcc4277061e226020cc65c699a9c970c", "b0523b6274c18cd44487ec1e44316b6b"]
sorl-thumbnail||image||da750ada9df8f1529d6fb7e993ae7a68	{"storage": "django.core.files.storage.FileSystemStorage", "name": "/var/www/renovables/media/organizacion/Logo_chico.jpg", "size": [689, 198]}
sorl-thumbnail||image||d089104c994dfa8fe74f4c195b52ad25	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/fa/d2/fad2f9a558661b901103bccb294548f6.jpg", "size": [278, 80]}
sorl-thumbnail||thumbnails||c3bbea23d9845fed0ba0c23cc5fb157a	["eed0f6aaa98984239fddff6e99b7cca8", "1c2c4f2db0dd157cfeb4bcd955931a38", "45b2566602c853a47cf3b7f991d98dc3", "1def4b14a742292e932849ca62270fcc", "e03a688e7ef3a2d95449380a14fd7228", "6acddc6217090c44a73a55ca760da162"]
sorl-thumbnail||image||8715606132625eb9a31a2997f6c401cb	{"storage": "django.core.files.storage.FileSystemStorage", "name": "/var/www/renovables/media/organizacion/SuniSolar.jpeg", "size": [100, 60]}
sorl-thumbnail||image||084a8a562f11c5a777f19cea14caafae	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/32/f9/32f9ea96c857dde50ffb5a3d5645389d.jpg", "size": [100, 60]}
sorl-thumbnail||thumbnails||85f1ab0947920d4fd63e45b75ee37f73	["67f0c7d5433a409aa79a0af5131c7251", "2dad85e9c7f00b00f2c14a34c85b7e66", "c4365f1d495340f7a41a574c52d774cb"]
sorl-thumbnail||image||35593861fe31e2b10231372c7f086b55	{"storage": "django.core.files.storage.FileSystemStorage", "name": "/var/www/renovables/media/organizacion/ULSA.png", "size": [1181, 1182]}
sorl-thumbnail||image||836e3b9c389547e4a351af71121a4e36	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/23/b6/23b6bf413bb5a2b220a92ca34b7b822e.jpg", "size": [80, 80]}
sorl-thumbnail||image||85f1ab0947920d4fd63e45b75ee37f73	{"storage": "django.core.files.storage.FileSystemStorage", "name": "/var/www/renovables/media/organizacion/UNI.jpg", "size": [74, 48]}
sorl-thumbnail||image||67f0c7d5433a409aa79a0af5131c7251	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/ae/7d/ae7d32c765f2903a0e1d588ed5841c6f.jpg", "size": [74, 48]}
sorl-thumbnail||image||389fb79be218b172545f01fae85be653	{"storage": "django.core.files.storage.FileSystemStorage", "name": "/var/www/renovables/media/organizacion/UTN.jpg", "size": [315, 255]}
sorl-thumbnail||image||884025f6b7a765ddb891fcc0aca2a63e	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/19/4a/194a4c125193aaba2392de15b2630042.jpg", "size": [99, 80]}
sorl-thumbnail||image||2077b0eb3746d184e91e3c8c0b844c39	{"storage": "django.core.files.storage.FileSystemStorage", "name": "/var/www/renovables/media/organizacion/enicalsa.jpg", "size": [146, 37]}
sorl-thumbnail||image||a3f9a3622d348bf9001eb78fbc027e27	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/21/ac/21ac1c834cbe920622c2dcb13caefc63.jpg", "size": [146, 37]}
sorl-thumbnail||image||4109c6f885c24d2ebdf0188a3eb6dcf2	{"storage": "django.core.files.storage.FileSystemStorage", "name": "/var/www/renovables/media/organizacion/Aprodelbo.jpg", "size": [345, 337]}
sorl-thumbnail||image||e6d51068016e89d6c41dcdabcc4755de	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/af/23/af23d4c3aba4964a5f41aaaadcd81b93.jpg", "size": [82, 80]}
sorl-thumbnail||image||f6712701e34dc77e41ede93f5223d886	{"storage": "django.core.files.storage.FileSystemStorage", "name": "/var/www/renovables/media/organizacion/Ecami.jpg", "size": [108, 96]}
sorl-thumbnail||image||469fa32672aa718219c554ce31507d0b	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/e2/34/e2345654dab5c58bc7e54c7228f56486.jpg", "size": [90, 80]}
sorl-thumbnail||image||bd910af4c868c8ebe26c5bd897e37ea7	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/ef/d8/efd89970e6c2de18ea8cdc041e740e8f.jpg", "size": [50, 70]}
sorl-thumbnail||image||20cec68593b987f0e87be76d7b7cd4ff	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/d4/2a/d42a99e87f03b58aac2f57866949762e.jpg", "size": [50, 70]}
sorl-thumbnail||image||3e1984b0ac58208fa716415dec24a10e	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/84/2d/842dd1fc281cdb6216075376602d0eb3.jpg", "size": [50, 70]}
sorl-thumbnail||image||38f1d41582939623baa86dfeaa4201f6	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/1b/4a/1b4a649b2b98b268243454ec896d3664.jpg", "size": [50, 70]}
sorl-thumbnail||image||85b7cdc390f2843897b9a6fc05b87bc8	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/09/b0/09b085858932b06bf5891c4e7d38f008.jpg", "size": [50, 70]}
sorl-thumbnail||image||2555b29e052a7328259f6b3e2f75c76e	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/2f/23/2f23a9439acf4b9a3a4af966f4e651d4.jpg", "size": [50, 70]}
sorl-thumbnail||image||71dd97684e83d4ee4a30ec48521f13cf	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/75/7a/757a1928e119478e25c7562f413a53d1.jpg", "size": [50, 70]}
sorl-thumbnail||image||45b2566602c853a47cf3b7f991d98dc3	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/48/3b/483b833c8775f237c5b5071de6233b42.jpg", "size": [50, 70]}
sorl-thumbnail||image||dbc22c986a1a93010b342cbebc79553d	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/09/ab/09ab5e842bdb658be9ec7d009c812df8.jpg", "size": [50, 70]}
sorl-thumbnail||image||db8ee4793381e0006b879ab41ea552a0	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/cf/c9/cfc9ca5f43e3965cae147ac84f2d3878.jpg", "size": [50, 70]}
sorl-thumbnail||image||c619b2551d763b3f9a901a90dfd912ae	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/d5/1d/d51ded78442c49144f8d34b735c0976d.jpg", "size": [50, 70]}
sorl-thumbnail||image||bdac8932ad5d40e335ea5ab39365a189	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/6a/c6/6ac6f3f62974ac51ab71e77286accb4e.jpg", "size": [50, 70]}
sorl-thumbnail||image||374043ec8fe7b4d13a8fbc421190c485	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/fc/ba/fcba6d8d19a15ecb4db648c9774d4c96.jpg", "size": [50, 70]}
sorl-thumbnail||image||d369afaa30bb897f6d8a4b1e5913135d	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/ff/d5/ffd5e8966c222ed27fc680edd760b4bd.jpg", "size": [50, 70]}
sorl-thumbnail||image||9cfeeac35e0c7fad5f774b163004fc5a	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/94/d6/94d61fe38a8bb96724f5727507c68e98.jpg", "size": [50, 70]}
sorl-thumbnail||image||8b0defe929c6a2c92d765792aed8b007	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/12/04/1204b520bd6b829b8faa986be85d3979.jpg", "size": [50, 70]}
sorl-thumbnail||image||23c30c8ff68b0bc79d4a9c78c6be0eaa	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/b3/e9/b3e90bf5c45179abaf6f95c2beef9b52.jpg", "size": [50, 70]}
sorl-thumbnail||image||e9e483ec0e98df67f72dd04ceec040b7	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/76/4b/764b8ed2321e5afaaf9b87da4185f114.jpg", "size": [50, 70]}
sorl-thumbnail||image||71ea1abdd7a0f062189f986a7691447b	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/5e/97/5e972239a545ae13ae563063250f39d5.jpg", "size": [50, 70]}
sorl-thumbnail||image||393b329202d0edbe448da03952eb1467	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/ca/4f/ca4fc15bf0288483195b66cb284e474d.jpg", "size": [50, 70]}
sorl-thumbnail||image||97595a79e3263a9d9486b3bc853679a1	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/e5/ca/e5cad666e78c0a0d554f0ed24be98cb1.jpg", "size": [50, 70]}
sorl-thumbnail||image||c607993264bd36d38facf79458d615aa	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/d8/d9/d8d9dc094d92167de67783c93085e252.jpg", "size": [50, 70]}
sorl-thumbnail||image||08089172a735fa552d06174eadd9693e	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/6d/51/6d51cd0575ceaf7cdc38ffea3e083492.jpg", "size": [70, 31]}
sorl-thumbnail||image||c6868457748eeb0e116d08e26d4b7e5c	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/e2/31/e231704e7edf7bb83d6fb0c73cf03873.jpg", "size": [38, 70]}
sorl-thumbnail||image||bcb1f3293f4d84f3007360fa672b7cf3	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/15/55/15558b8da24363054b17cd0ec8767b72.jpg", "size": [70, 29]}
sorl-thumbnail||image||dbbc3e1d146899a3cbc63ac149e36cf7	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/18/c5/18c570d443cbee1ca0b2c60554cfb43e.jpg", "size": [70, 18]}
sorl-thumbnail||image||636430840574cd06f6570170380bf08e	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/99/f2/99f26c2ac6aa4f30ac136ded0e403eb7.jpg", "size": [60, 70]}
sorl-thumbnail||image||36aa0e143c88b1c19f99dfc970a1d5ea	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/77/96/77966d4e30f9739bbbddf0e78a621add.jpg", "size": [70, 34]}
sorl-thumbnail||thumbnails||f6712701e34dc77e41ede93f5223d886	["469fa32672aa718219c554ce31507d0b", "db377a5a9269746a5e88e104f8975fab", "393b329202d0edbe448da03952eb1467", "6f135658964ddd477c784e0e00b75488", "41be95b9d40cfd3d0da2f1304c29ed0b", "0fbaa399eedbe4153cc0f75e5196ae59"]
sorl-thumbnail||image||84e1fabb7930913778760b6337431edd	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/60/4b/604ba9a411c4ad97f00e228e3dc6eca4.jpg", "size": [70, 70]}
sorl-thumbnail||image||1def4b14a742292e932849ca62270fcc	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/02/6b/026bceab57bcc0bf3a6e534bfbd22976.jpg", "size": [70, 25]}
sorl-thumbnail||image||6ac2f4a24b20cc04f2f00dbde58c2063	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/4e/a6/4ea6fb57d0a68618e5cd6a05a8bce32e.jpg", "size": [70, 70]}
sorl-thumbnail||image||457e53defe4744459de0a0500fd6f855	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/9d/b9/9db918a45e93238b8846121c5e3738e9.jpg", "size": [67, 70]}
sorl-thumbnail||image||c09d0a72404bdf38a4a47992c65d8c60	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/a6/60/a660813eac60046d481505db98bc2fd1.jpg", "size": [70, 57]}
sorl-thumbnail||thumbnails||8627b3ad33055312df1b9fa3205e4df8	["71dd97684e83d4ee4a30ec48521f13cf", "eb1a084264f954301c1e3546041e2d38", "1fc656dad2484ef833b9594dbbaa6d66", "84e1fabb7930913778760b6337431edd", "114f035b8cd7383337f0d4259760fb35", "22a8321b0e9b38027982d97317f21b59"]
sorl-thumbnail||image||4d3f47d6c79724ea37b8db2387b05691	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/d9/36/d936ed72b1dbc25ddc8454d19aa98843.jpg", "size": [68, 70]}
sorl-thumbnail||image||9b5ee4e28b706ad056d0b121114fc2c0	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/e8/85/e885f2bc1697a9d9e9c833b04bfbc3dd.jpg", "size": [70, 20]}
sorl-thumbnail||image||400547bacb22862bb4a02520c0fe7a85	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/31/cc/31cc0f052592dbcb81a9c0b6664c6f0d.jpg", "size": [70, 29]}
sorl-thumbnail||image||73e71c77c97483d380afda5ea17aa6cf	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/23/b8/23b877513bf5d5a9264ce651e519c80d.jpg", "size": [70, 39]}
sorl-thumbnail||image||5b3cb34e6e779bcc980ef6f4f3c5837d	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/c7/0d/c70d54a02aba0f99cfa7808cd3983e24.jpg", "size": [70, 21]}
sorl-thumbnail||image||76b11b5e60db0235ffe10bc8a7a07049	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/37/3b/373bf5b97b99057144c669cd710a6bb0.jpg", "size": [70, 68]}
sorl-thumbnail||image||8e12a11109967b152b421d0d0f4c66fe	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/d0/05/d005792d99fcec4164efcf17fcde0579.jpg", "size": [70, 23]}
sorl-thumbnail||image||5c545a1b6ef6453481941238d95c1049	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/7f/97/7f9764fd3efb4b54574ea24dd6038eff.jpg", "size": [39, 70]}
sorl-thumbnail||image||db377a5a9269746a5e88e104f8975fab	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/1f/3d/1f3df5dbf05e31c280c047d0c9fdbb60.jpg", "size": [70, 62]}
sorl-thumbnail||image||689640cd61259cce2403092e0ee25f49	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/5a/e1/5ae1cbd77377d9ee534264976c2f4a8c.jpg", "size": [70, 70]}
sorl-thumbnail||image||3a1a011d1c82c2c529894f293ea16253	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/c4/81/c4819fcee9bd3ef42ace48b4d4e6f70d.jpg", "size": [70, 42]}
sorl-thumbnail||image||bcc4277061e226020cc65c699a9c970c	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/59/02/5902ddace7a4cee15b5114a0f87b5d65.png", "size": [223, 100]}
sorl-thumbnail||image||ae8b1b1c47673279e7d267338a345929	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/59/76/5976743f279131f75563ae25deccd04b.png", "size": [54, 100]}
sorl-thumbnail||image||3c35692022f366de3847dce9f95bcb8a	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/b5/3e/b53ee160751a990df1707dd08cbd331b.png", "size": [240, 100]}
sorl-thumbnail||image||e8857aaf0ccb896d0a52f3fb9e517a61	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/4e/84/4e8466bfe2f88e61ad75be51dad9c745.png", "size": [395, 100]}
sorl-thumbnail||image||972684bd81de8c5271954524c03a741d	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/8d/d7/8dd785acb31fb19f8ecff3f903a36541.png", "size": [86, 100]}
sorl-thumbnail||image||145540500a7a061cdd82a264a7082201	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/90/62/9062b1b5c80c839b5d7d690973ee0100.png", "size": [205, 100]}
sorl-thumbnail||image||b95c988c787a71b5c2383cc038c0c3c1	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/9e/06/9e068da82fdaceb404031fbe8229b30e.png", "size": [99, 100]}
sorl-thumbnail||image||eb1a084264f954301c1e3546041e2d38	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/44/06/44066d64d9b00fb6242436eb8c852eb2.png", "size": [100, 100]}
sorl-thumbnail||image||e03a688e7ef3a2d95449380a14fd7228	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/8d/de/8ddecd12bd11cf5c52576d9e05742578.png", "size": [276, 100]}
sorl-thumbnail||image||cba62545207c19ef165eb6dd02f4004f	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/a6/6d/a66dec99f9e130100eb6021580217898.png", "size": [100, 100]}
sorl-thumbnail||image||2dad85e9c7f00b00f2c14a34c85b7e66	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/0c/6d/0c6d6db172ec3ca73c8ad8431430a950.png", "size": [154, 100]}
sorl-thumbnail||image||30b4c49073e2efedc0a396f3e06babce	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/ae/d3/aed351a8274dc2c8508fcf53bdf265a4.png", "size": [95, 100]}
sorl-thumbnail||image||42048a17c3acbbbf20284eccc96bae26	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/12/00/12008f351eb3ac0cedab2c51cec144de.png", "size": [124, 100]}
sorl-thumbnail||image||c9548faa991731d1148e428fad5d5a3d	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/e4/56/e4566e316a27df2d2c8089697078734e.png", "size": [98, 100]}
sorl-thumbnail||image||7284b87f28689dd2a54292d766986ada	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/1e/24/1e24e776188a6a8f5fbc0ebbd9ceeb93.png", "size": [348, 100]}
sorl-thumbnail||image||069d03170e1b511eb99a6e26d9556f49	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/cc/00/cc001a194338772aef6d9380e5f59e0b.png", "size": [245, 100]}
sorl-thumbnail||image||feaa1ff878da9b0dd9ce4f79f6131f9b	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/3f/a1/3fa1ec260c1d5a171b2bb2d5145c7949.png", "size": [178, 100]}
sorl-thumbnail||thumbnails||2077b0eb3746d184e91e3c8c0b844c39	["ab1c13c509899a8967a576102181dda4", "e8857aaf0ccb896d0a52f3fb9e517a61", "4b76732fdfdbc578e8f0ef8c9219fe30", "a3f9a3622d348bf9001eb78fbc027e27", "dbbc3e1d146899a3cbc63ac149e36cf7", "38f1d41582939623baa86dfeaa4201f6"]
sorl-thumbnail||thumbnails||0bbd8037b9ad0614f8dfbaaa73d3dc7e	["30b4c49073e2efedc0a396f3e06babce", "457e53defe4744459de0a0500fd6f855", "922dc4f43ca01fdfb9981a88978bb353", "db8ee4793381e0006b879ab41ea552a0", "42f1bc8b882f264f7e9a87fab0cce2fc", "59629b9e3b983e24309d68982aed5534"]
sorl-thumbnail||thumbnails||389fb79be218b172545f01fae85be653	["c09d0a72404bdf38a4a47992c65d8c60", "c619b2551d763b3f9a901a90dfd912ae", "cfc978e47bd699c7372bc32dbf6ba72e", "42048a17c3acbbbf20284eccc96bae26", "a68784dbba2485eed0711963d14318a7", "884025f6b7a765ddb891fcc0aca2a63e"]
sorl-thumbnail||thumbnails||13c02a6eb6018836901bb08780fb185c	["1ab386f13381e9bd68880f6422f30418", "428e499b6c8440f8934c355711ae702f", "4d82e40c683bcd1ed24db4ddd1a68b6a", "5b3cb34e6e779bcc980ef6f4f3c5837d", "8b0defe929c6a2c92d765792aed8b007", "dbc4a91b03ccbe4857d84794becd35f3"]
sorl-thumbnail||image||4d82e40c683bcd1ed24db4ddd1a68b6a	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/ed/f4/edf476459904d387c75d0dd6baffa7d8.png", "size": [329, 100]}
sorl-thumbnail||image||a33bfaee104bd1354bb55183ce092581	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/36/90/36909fad289838a692e32d434861c8ad.png", "size": [102, 100]}
sorl-thumbnail||image||8058edc0edca1e3862930f6bd734a317	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/72/2f/722f3fcb4372fc804f92e4b6d48ab149.png", "size": [299, 100]}
sorl-thumbnail||image||8f624f181c9cc2f255e162dbeb833db6	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/92/a8/92a8630758c74c39d74bfd904620d8e5.png", "size": [56, 100]}
sorl-thumbnail||image||6f135658964ddd477c784e0e00b75488	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/26/1f/261f48d9dc501521e0f61bd64c5d29db.png", "size": [113, 100]}
sorl-thumbnail||image||5ef11ffabc0ac9106c111ab6649f2dc8	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/39/b6/39b6f27cebb56e935cf5f5b67a6570fc.png", "size": [100, 100]}
sorl-thumbnail||image||fcacd711bd6d1570da0f6ede2b7c6f5d	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/a6/32/a632ef675cc0d6eb5bc861ba6bebb63f.png", "size": [167, 100]}
sorl-thumbnail||image||5108eebf97aed777b13e219f457f69c8	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/89/27/89277e2a596b9a6a786e81ab42d1d20a.jpg", "size": [100, 45]}
sorl-thumbnail||image||da40a7417947b69573b0b1e6fab32223	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/46/89/4689798242f13e7e874dbd4716b76782.jpg", "size": [54, 100]}
sorl-thumbnail||image||cda1709b34206ec7d8c10e23cb961fb1	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/59/d8/59d8fe0f7a8d859be3670feaea5565d0.jpg", "size": [100, 42]}
sorl-thumbnail||image||4b76732fdfdbc578e8f0ef8c9219fe30	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/47/77/4777f8e1a9472f2fcd316f9cd7b64489.jpg", "size": [100, 25]}
sorl-thumbnail||image||58f3560193ca3a3dc0c2a5e49368982a	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/39/cd/39cd03263342505b23f41ed41273f488.jpg", "size": [86, 100]}
sorl-thumbnail||image||2f07301d5d4d40b38c65bb609196fd5e	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/07/5d/075dc07032b97a78ab738b3776796125.jpg", "size": [100, 49]}
sorl-thumbnail||image||22a8321b0e9b38027982d97317f21b59	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/a2/37/a2375bdb972d706d5f48cc81bd2f8ff0.jpg", "size": [100, 100]}
sorl-thumbnail||image||eed0f6aaa98984239fddff6e99b7cca8	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/f1/8a/f18a5eff7dd3f137dbc03686bde80255.jpg", "size": [100, 36]}
sorl-thumbnail||image||c3fe16aa01a9d5340fc117a1ae653f73	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/ff/b7/ffb714ef2c37e4a5243b7644e74dc99d.jpg", "size": [100, 100]}
sorl-thumbnail||image||59629b9e3b983e24309d68982aed5534	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/ad/52/ad520e61a84846001636276b3ecdd83c.jpg", "size": [95, 100]}
sorl-thumbnail||image||cfc978e47bd699c7372bc32dbf6ba72e	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/d9/c3/d9c3e36804559f73e6266c03ccd69558.jpg", "size": [100, 81]}
sorl-thumbnail||image||88ac19c3fa7ec8a1e03b86ed5e732eb8	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/95/4e/954e946b6d316fd3f807cda39d29b308.jpg", "size": [98, 100]}
sorl-thumbnail||image||fd7860c55e60a4190ee9b5457fc562aa	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/9a/98/9a9846ec9f79e07d97d906343c628f27.jpg", "size": [100, 29]}
sorl-thumbnail||image||c97ab50e0633fe57780b65ea294f0dcc	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/82/e6/82e6317b47cd839cd14b032b3544b0fc.jpg", "size": [100, 41]}
sorl-thumbnail||image||b3fe23abf18d344226b26ce311f75329	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/1d/de/1dde6c353accfc6a8e71feb253e2f1c4.jpg", "size": [100, 56]}
sorl-thumbnail||image||428e499b6c8440f8934c355711ae702f	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/c7/a5/c7a5a1787fb8c4248a3b55555bb47fc5.jpg", "size": [100, 30]}
sorl-thumbnail||image||b89bce12c457e03e4de45521bff0c82b	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/a6/6b/a66bea0f2bf5640937c8098ee203042c.jpg", "size": [100, 98]}
sorl-thumbnail||thumbnails||d306ad40c16afd31220f14353f227c1a	["ec99ed5d6300645b4614e02c4a0cddac", "8058edc0edca1e3862930f6bd734a317", "e9e483ec0e98df67f72dd04ceec040b7", "7c6966e7a6bc29dd28fd15de62e93c03", "8e12a11109967b152b421d0d0f4c66fe", "0e95cc1455388bbc4fe1ec652608c883"]
sorl-thumbnail||image||0e95cc1455388bbc4fe1ec652608c883	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/34/4d/344d997a80dbb446b9e84a80517b6880.jpg", "size": [100, 33]}
sorl-thumbnail||image||bbd89a766388c12464b924d214040d2b	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/e0/b8/e0b8e8c2657530c0e7ded7f73d51499d.jpg", "size": [56, 100]}
sorl-thumbnail||image||41be95b9d40cfd3d0da2f1304c29ed0b	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/9b/c3/9bc32063e57c33047718ca887bc12824.jpg", "size": [100, 89]}
sorl-thumbnail||image||35f7b32146b4c356603f51f20e06fa35	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/d2/46/d246a137e686c780b962549e6e634ff3.jpg", "size": [100, 100]}
sorl-thumbnail||thumbnails||b8397057b1fa1e0a659e6f944d26642a	["2555b29e052a7328259f6b3e2f75c76e", "36aa0e143c88b1c19f99dfc970a1d5ea", "2f07301d5d4d40b38c65bb609196fd5e", "634a6bc30e646f1298bef91057a4b4b6", "145540500a7a061cdd82a264a7082201", "c5ce47e091a9b24d89c4b32ea3df6341"]
sorl-thumbnail||thumbnails||f5c33f6909bd04207e2f9bc5bbca8075	["feaa1ff878da9b0dd9ce4f79f6131f9b", "b3fe23abf18d344226b26ce311f75329", "9cfeeac35e0c7fad5f774b163004fc5a", "604173fe688efd934b4bb411513bfa88", "73e71c77c97483d380afda5ea17aa6cf", "8ca90ce68d0d416cd9121b4d9cf1f3d5"]
sorl-thumbnail||thumbnails||4109c6f885c24d2ebdf0188a3eb6dcf2	["23c30c8ff68b0bc79d4a9c78c6be0eaa", "b89bce12c457e03e4de45521bff0c82b", "76b11b5e60db0235ffe10bc8a7a07049", "e6d51068016e89d6c41dcdabcc4755de", "0863cb5f3024fdc615db7a4a28c24196", "a33bfaee104bd1354bb55183ce092581"]
sorl-thumbnail||image||d1279476991d9a0f311eb87758d52f44	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/0b/88/0b880d0cdba8ba05716d1c41f15e2e7d.jpg", "size": [100, 60]}
sorl-thumbnail||image||40c775c9b549e3b8ac71c3faa0bba115	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/b4/b4/b4b4bcd27c7d00112e0701689e00a223.png", "size": [200, 90]}
sorl-thumbnail||image||3009bbb1a333d8aa3a5eecd73fd1c6e0	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/de/c3/dec3146822f16ece267b1eaa49e5d784.png", "size": [108, 200]}
sorl-thumbnail||image||74659dad4099a366ee53238ca0093ca1	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/9c/73/9c737cbe259d22feaa1866eb7a508bdb.png", "size": [200, 83]}
sorl-thumbnail||thumbnails||643ec88f3a1450e79e959e41a2b0f5ae	["3c35692022f366de3847dce9f95bcb8a", "315efa41374c95dd917546b027d34403", "bcb1f3293f4d84f3007360fa672b7cf3", "3e1984b0ac58208fa716415dec24a10e", "cda1709b34206ec7d8c10e23cb961fb1", "74659dad4099a366ee53238ca0093ca1"]
sorl-thumbnail||image||ab1c13c509899a8967a576102181dda4	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/6f/72/6f723be8a0652661c43631baafdc73e8.png", "size": [200, 51]}
sorl-thumbnail||image||31bdabbc7a6319ca1446057992b249c0	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/83/de/83dee661c2e68fb241e5c66d109a16b1.png", "size": [172, 200]}
sorl-thumbnail||thumbnails||48716b5dd015013575d4f8a9ec4356c1	["31bdabbc7a6319ca1446057992b249c0", "85b7cdc390f2843897b9a6fc05b87bc8", "58f3560193ca3a3dc0c2a5e49368982a", "636430840574cd06f6570170380bf08e", "a4b07885f1ab1406279a02ef6b63d212", "972684bd81de8c5271954524c03a741d"]
sorl-thumbnail||image||c5ce47e091a9b24d89c4b32ea3df6341	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/16/e8/16e80ad3dc43a5825dbcfb3f99a11c6d.png", "size": [200, 98]}
sorl-thumbnail||image||820807e35b55b9acb18e39653ecb1605	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/84/2c/842cb9ef36c14ea1d10dc18f409c9280.png", "size": [198, 200]}
sorl-thumbnail||image||114f035b8cd7383337f0d4259760fb35	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/5c/cd/5ccd703f7c3ec32c043838a86fa7b4e4.png", "size": [200, 200]}
sorl-thumbnail||image||6acddc6217090c44a73a55ca760da162	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/e3/46/e3467a4431091b9267a0e5d7c4733675.png", "size": [200, 73]}
sorl-thumbnail||image||aea9e02ccfda6ce952efe5f7013ea967	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/09/09/0909aa061cc34badab7df524fda62c63.png", "size": [200, 200]}
sorl-thumbnail||image||c4365f1d495340f7a41a574c52d774cb	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/e8/62/e8622abdfd9c0bd2b40a97ac97b53c71.png", "size": [200, 130]}
sorl-thumbnail||image||42f1bc8b882f264f7e9a87fab0cce2fc	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/f3/29/f329ce7944e5ebbf491c1292c757202c.png", "size": [190, 200]}
sorl-thumbnail||image||a68784dbba2485eed0711963d14318a7	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/ee/7b/ee7bc71bd42eb98e2e620ea958534159.png", "size": [200, 162]}
sorl-thumbnail||image||7b7ec0363a0fcaf5ae0c41103c819e96	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/b3/fb/b3fb3e4d5eec121c6f484ce10e01793f.png", "size": [195, 200]}
sorl-thumbnail||thumbnails||798e74b0f41fae046a0a80aec75561ae	["88ac19c3fa7ec8a1e03b86ed5e732eb8", "4d3f47d6c79724ea37b8db2387b05691", "6c5c8846716a4b04b403b4b0ea5640df", "7b7ec0363a0fcaf5ae0c41103c819e96", "bdac8932ad5d40e335ea5ab39365a189", "c9548faa991731d1148e428fad5d5a3d"]
sorl-thumbnail||image||2a796cb37d84f300feef7fee0d390a9e	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/9a/90/9a900fce9b46754dd309a713061042e8.png", "size": [200, 57]}
sorl-thumbnail||thumbnails||da750ada9df8f1529d6fb7e993ae7a68	["fd7860c55e60a4190ee9b5457fc562aa", "d089104c994dfa8fe74f4c195b52ad25", "374043ec8fe7b4d13a8fbc421190c485", "9b5ee4e28b706ad056d0b121114fc2c0", "7284b87f28689dd2a54292d766986ada", "2a796cb37d84f300feef7fee0d390a9e"]
sorl-thumbnail||image||ecf76d4bd3c5a86e408b35a70dfbdd40	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/b7/82/b782a58ac049a30f5ed8f972aa2272e7.png", "size": [200, 82]}
sorl-thumbnail||image||8ca90ce68d0d416cd9121b4d9cf1f3d5	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/ef/f3/eff329588e0377b49ab98b22b8ee9d1d.png", "size": [200, 113]}
sorl-thumbnail||image||1ab386f13381e9bd68880f6422f30418	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/2a/22/2a221478f07f2b7f9c926e175dbfecf9.png", "size": [200, 61]}
sorl-thumbnail||image||0863cb5f3024fdc615db7a4a28c24196	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/c4/6c/c46c5d9a20220248d9c1b96ea7840e77.png", "size": [200, 195]}
sorl-thumbnail||image||7c6966e7a6bc29dd28fd15de62e93c03	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/d0/cd/d0cd1ba4f2bbfdc94ea56e1b1a0be23b.png", "size": [200, 67]}
sorl-thumbnail||image||cb491bbc1ecb4c32e2ed768f836efdfe	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/17/11/171196a17971ca9035d4ae88229e30e7.png", "size": [111, 200]}
sorl-thumbnail||thumbnails||f0c7ec90e827330f6d15923a658fce62	["71ea1abdd7a0f062189f986a7691447b", "cb491bbc1ecb4c32e2ed768f836efdfe", "8f624f181c9cc2f255e162dbeb833db6", "bbd89a766388c12464b924d214040d2b", "5c545a1b6ef6453481941238d95c1049", "1e97c1ac9bcfd2cbe70571f28cd49586"]
sorl-thumbnail||image||0fbaa399eedbe4153cc0f75e5196ae59	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/e0/63/e06392723c81258c699af241b7c29f05.png", "size": [200, 178]}
sorl-thumbnail||image||d43e0a65e9d1298384074d8bd8974be0	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/66/85/6685ebdae979f33be8be2d8f0ae81480.png", "size": [200, 200]}
sorl-thumbnail||image||daa641300efb740913562d0caefe6227	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/4e/07/4e07655c780ab1a90ff710452108621f.png", "size": [200, 120]}
sorl-thumbnail||image||3e58f976a2a148b6cd137b0b283406e5	{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/20/fc/20fc4d8f4b53e1dad366af427fad72fc.png", "size": [300, 134]}
\.


--
-- Name: auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions_group_id_key; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_key UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_message_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY auth_message
    ADD CONSTRAINT auth_message_pkey PRIMARY KEY (id);


--
-- Name: auth_permission_content_type_id_key; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_key UNIQUE (content_type_id, codename);


--
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_user_id_key; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_key UNIQUE (user_id, group_id);


--
-- Name: auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_user_id_key; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_key UNIQUE (user_id, permission_id);


--
-- Name: auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: csvimport_csvimport_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY csvimport_csvimport
    ADD CONSTRAINT csvimport_csvimport_pkey PRIMARY KEY (id);


--
-- Name: csvimport_importmodel_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY csvimport_importmodel
    ADD CONSTRAINT csvimport_importmodel_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type_app_label_key; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_key UNIQUE (app_label, model);


--
-- Name: django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: django_site_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY django_site
    ADD CONSTRAINT django_site_pkey PRIMARY KEY (id);


--
-- Name: indicadores_indicador_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY indicadores_indicador
    ADD CONSTRAINT indicadores_indicador_pkey PRIMARY KEY (id);


--
-- Name: indicadores_indicadoressectoriales_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY indicadores_indicadoressectoriales
    ADD CONSTRAINT indicadores_indicadoressectoriales_pkey PRIMARY KEY (anho);


--
-- Name: indicadores_tipoindicador_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY indicadores_tipoindicador
    ADD CONSTRAINT indicadores_tipoindicador_pkey PRIMARY KEY (id);


--
-- Name: indicadores_valorindicador_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY indicadores_valorindicador
    ADD CONSTRAINT indicadores_valorindicador_pkey PRIMARY KEY (id);


--
-- Name: nomencladores_actividad_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY nomencladores_actividad
    ADD CONSTRAINT nomencladores_actividad_pkey PRIMARY KEY (id);


--
-- Name: nomencladores_categoria_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY nomencladores_categoria
    ADD CONSTRAINT nomencladores_categoria_pkey PRIMARY KEY (id);


--
-- Name: nomencladores_fuenterenovable_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY nomencladores_fuenterenovable
    ADD CONSTRAINT nomencladores_fuenterenovable_pkey PRIMARY KEY (id);


--
-- Name: nomencladores_materiaprima_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY nomencladores_materiaprima
    ADD CONSTRAINT nomencladores_materiaprima_pkey PRIMARY KEY (id);


--
-- Name: nomencladores_pais_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY nomencladores_pais
    ADD CONSTRAINT nomencladores_pais_pkey PRIMARY KEY (id);


--
-- Name: nomencladores_tipoentidad_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY nomencladores_tipoentidad
    ADD CONSTRAINT nomencladores_tipoentidad_pkey PRIMARY KEY (id);


--
-- Name: nomencladores_vegetal_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY nomencladores_vegetal
    ADD CONSTRAINT nomencladores_vegetal_pkey PRIMARY KEY (id);


--
-- Name: organizaciones_beneficiarios_anho_key; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY organizaciones_beneficiarios
    ADD CONSTRAINT organizaciones_beneficiarios_anho_key UNIQUE (anho);


--
-- Name: organizaciones_beneficiarios_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY organizaciones_beneficiarios
    ADD CONSTRAINT organizaciones_beneficiarios_pkey PRIMARY KEY (id);


--
-- Name: organizaciones_beneficiariosproyecto_anho_key; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY organizaciones_beneficiariosproyecto
    ADD CONSTRAINT organizaciones_beneficiariosproyecto_anho_key UNIQUE (anho);


--
-- Name: organizaciones_beneficiariosproyecto_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY organizaciones_beneficiariosproyecto
    ADD CONSTRAINT organizaciones_beneficiariosproyecto_pkey PRIMARY KEY (id);


--
-- Name: organizaciones_contacto_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY organizaciones_contacto
    ADD CONSTRAINT organizaciones_contacto_pkey PRIMARY KEY (id);


--
-- Name: organizaciones_email_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY organizaciones_email
    ADD CONSTRAINT organizaciones_email_pkey PRIMARY KEY (id);


--
-- Name: organizaciones_empleados_anho_key; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY organizaciones_empleados
    ADD CONSTRAINT organizaciones_empleados_anho_key UNIQUE (anho);


--
-- Name: organizaciones_empleados_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY organizaciones_empleados
    ADD CONSTRAINT organizaciones_empleados_pkey PRIMARY KEY (id);


--
-- Name: organizaciones_galeriafotosproyecto_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY organizaciones_galeriafotosproyecto
    ADD CONSTRAINT organizaciones_galeriafotosproyecto_pkey PRIMARY KEY (id);


--
-- Name: organizaciones_galeriafotossistema_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY organizaciones_galeriafotossistema
    ADD CONSTRAINT organizaciones_galeriafotossistema_pkey PRIMARY KEY (id);


--
-- Name: organizaciones_organizaci_organizacion_id_6c502874a051fe3c_uniq; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY organizaciones_organizacion_actividad
    ADD CONSTRAINT organizaciones_organizaci_organizacion_id_6c502874a051fe3c_uniq UNIQUE (organizacion_id, actividad_id);


--
-- Name: organizaciones_organizaci_organizacion_id_7f30f2327a7f8f33_uniq; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY organizaciones_organizacion_fuente
    ADD CONSTRAINT organizaciones_organizaci_organizacion_id_7f30f2327a7f8f33_uniq UNIQUE (organizacion_id, fuenterenovable_id);


--
-- Name: organizaciones_organizacion_actividad_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY organizaciones_organizacion_actividad
    ADD CONSTRAINT organizaciones_organizacion_actividad_pkey PRIMARY KEY (id);


--
-- Name: organizaciones_organizacion_fuente_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY organizaciones_organizacion_fuente
    ADD CONSTRAINT organizaciones_organizacion_fuente_pkey PRIMARY KEY (id);


--
-- Name: organizaciones_organizacion_nombre_key; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY organizaciones_organizacion
    ADD CONSTRAINT organizaciones_organizacion_nombre_key UNIQUE (nombre);


--
-- Name: organizaciones_organizacion_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY organizaciones_organizacion
    ADD CONSTRAINT organizaciones_organizacion_pkey PRIMARY KEY (id);


--
-- Name: organizaciones_proyecto_fuent_proyecto_id_6ebea3a9c5ec6929_uniq; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY organizaciones_proyecto_fuente
    ADD CONSTRAINT organizaciones_proyecto_fuent_proyecto_id_6ebea3a9c5ec6929_uniq UNIQUE (proyecto_id, fuenterenovable_id);


--
-- Name: organizaciones_proyecto_fuente_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY organizaciones_proyecto_fuente
    ADD CONSTRAINT organizaciones_proyecto_fuente_pkey PRIMARY KEY (id);


--
-- Name: organizaciones_proyecto_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY organizaciones_proyecto
    ADD CONSTRAINT organizaciones_proyecto_pkey PRIMARY KEY (organizacion_id);


--
-- Name: organizaciones_responsable_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY organizaciones_responsable
    ADD CONSTRAINT organizaciones_responsable_pkey PRIMARY KEY (organizacion_id);


--
-- Name: organizaciones_sistem_sistemaejecucion_id_1c81da7e7a8668c3_uniq; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY organizaciones_sistemaejecucion_fuente
    ADD CONSTRAINT organizaciones_sistem_sistemaejecucion_id_1c81da7e7a8668c3_uniq UNIQUE (sistemaejecucion_id, fuenterenovable_id);


--
-- Name: organizaciones_sistemaejecucion_fuente_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY organizaciones_sistemaejecucion_fuente
    ADD CONSTRAINT organizaciones_sistemaejecucion_fuente_pkey PRIMARY KEY (id);


--
-- Name: organizaciones_sistemaejecucion_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY organizaciones_sistemaejecucion
    ADD CONSTRAINT organizaciones_sistemaejecucion_pkey PRIMARY KEY (id);


--
-- Name: organizaciones_usuariosbeneficiarios_anho_key; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY organizaciones_usuariosbeneficiarios
    ADD CONSTRAINT organizaciones_usuariosbeneficiarios_anho_key UNIQUE (anho);


--
-- Name: organizaciones_usuariosbeneficiarios_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY organizaciones_usuariosbeneficiarios
    ADD CONSTRAINT organizaciones_usuariosbeneficiarios_pkey PRIMARY KEY (id);


--
-- Name: south_migrationhistory_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY south_migrationhistory
    ADD CONSTRAINT south_migrationhistory_pkey PRIMARY KEY (id);


--
-- Name: thumbnail_kvstore_pkey; Type: CONSTRAINT; Schema: public; Owner: renovable; Tablespace: 
--

ALTER TABLE ONLY thumbnail_kvstore
    ADD CONSTRAINT thumbnail_kvstore_pkey PRIMARY KEY (key);


--
-- Name: auth_group_permissions_group_id; Type: INDEX; Schema: public; Owner: renovable; Tablespace: 
--

CREATE INDEX auth_group_permissions_group_id ON auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id; Type: INDEX; Schema: public; Owner: renovable; Tablespace: 
--

CREATE INDEX auth_group_permissions_permission_id ON auth_group_permissions USING btree (permission_id);


--
-- Name: auth_message_user_id; Type: INDEX; Schema: public; Owner: renovable; Tablespace: 
--

CREATE INDEX auth_message_user_id ON auth_message USING btree (user_id);


--
-- Name: auth_permission_content_type_id; Type: INDEX; Schema: public; Owner: renovable; Tablespace: 
--

CREATE INDEX auth_permission_content_type_id ON auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id; Type: INDEX; Schema: public; Owner: renovable; Tablespace: 
--

CREATE INDEX auth_user_groups_group_id ON auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id; Type: INDEX; Schema: public; Owner: renovable; Tablespace: 
--

CREATE INDEX auth_user_groups_user_id ON auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id; Type: INDEX; Schema: public; Owner: renovable; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_permission_id ON auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id; Type: INDEX; Schema: public; Owner: renovable; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_user_id ON auth_user_user_permissions USING btree (user_id);


--
-- Name: csvimport_importmodel_csvimport_id; Type: INDEX; Schema: public; Owner: renovable; Tablespace: 
--

CREATE INDEX csvimport_importmodel_csvimport_id ON csvimport_importmodel USING btree (csvimport_id);


--
-- Name: django_admin_log_content_type_id; Type: INDEX; Schema: public; Owner: renovable; Tablespace: 
--

CREATE INDEX django_admin_log_content_type_id ON django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id; Type: INDEX; Schema: public; Owner: renovable; Tablespace: 
--

CREATE INDEX django_admin_log_user_id ON django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date; Type: INDEX; Schema: public; Owner: renovable; Tablespace: 
--

CREATE INDEX django_session_expire_date ON django_session USING btree (expire_date);


--
-- Name: indicadores_indicador_tipoindicador_id; Type: INDEX; Schema: public; Owner: renovable; Tablespace: 
--

CREATE INDEX indicadores_indicador_tipoindicador_id ON indicadores_indicador USING btree (tipoindicador_id);


--
-- Name: indicadores_valorindicador_anho_id; Type: INDEX; Schema: public; Owner: renovable; Tablespace: 
--

CREATE INDEX indicadores_valorindicador_anho_id ON indicadores_valorindicador USING btree (anho_id);


--
-- Name: indicadores_valorindicador_indicador_id; Type: INDEX; Schema: public; Owner: renovable; Tablespace: 
--

CREATE INDEX indicadores_valorindicador_indicador_id ON indicadores_valorindicador USING btree (indicador_id);


--
-- Name: organizaciones_beneficiarios_organizacion_id; Type: INDEX; Schema: public; Owner: renovable; Tablespace: 
--

CREATE INDEX organizaciones_beneficiarios_organizacion_id ON organizaciones_beneficiarios USING btree (organizacion_id);


--
-- Name: organizaciones_beneficiariosproyecto_proyecto_id; Type: INDEX; Schema: public; Owner: renovable; Tablespace: 
--

CREATE INDEX organizaciones_beneficiariosproyecto_proyecto_id ON organizaciones_beneficiariosproyecto USING btree (proyecto_id);


--
-- Name: organizaciones_contacto_categoria_id; Type: INDEX; Schema: public; Owner: renovable; Tablespace: 
--

CREATE INDEX organizaciones_contacto_categoria_id ON organizaciones_contacto USING btree (categoria_id);


--
-- Name: organizaciones_contacto_organizacion_id; Type: INDEX; Schema: public; Owner: renovable; Tablespace: 
--

CREATE INDEX organizaciones_contacto_organizacion_id ON organizaciones_contacto USING btree (organizacion_id);


--
-- Name: organizaciones_contacto_pais_id; Type: INDEX; Schema: public; Owner: renovable; Tablespace: 
--

CREATE INDEX organizaciones_contacto_pais_id ON organizaciones_contacto USING btree (pais_id);


--
-- Name: organizaciones_email_contacto_id; Type: INDEX; Schema: public; Owner: renovable; Tablespace: 
--

CREATE INDEX organizaciones_email_contacto_id ON organizaciones_email USING btree (contacto_id);


--
-- Name: organizaciones_empleados_organizacion_id; Type: INDEX; Schema: public; Owner: renovable; Tablespace: 
--

CREATE INDEX organizaciones_empleados_organizacion_id ON organizaciones_empleados USING btree (organizacion_id);


--
-- Name: organizaciones_galeriafotosproyecto_proyecto_id; Type: INDEX; Schema: public; Owner: renovable; Tablespace: 
--

CREATE INDEX organizaciones_galeriafotosproyecto_proyecto_id ON organizaciones_galeriafotosproyecto USING btree (proyecto_id);


--
-- Name: organizaciones_galeriafotossistema_sistema_id; Type: INDEX; Schema: public; Owner: renovable; Tablespace: 
--

CREATE INDEX organizaciones_galeriafotossistema_sistema_id ON organizaciones_galeriafotossistema USING btree (sistema_id);


--
-- Name: organizaciones_organizacion_actividad_actividad_id; Type: INDEX; Schema: public; Owner: renovable; Tablespace: 
--

CREATE INDEX organizaciones_organizacion_actividad_actividad_id ON organizaciones_organizacion_actividad USING btree (actividad_id);


--
-- Name: organizaciones_organizacion_actividad_organizacion_id; Type: INDEX; Schema: public; Owner: renovable; Tablespace: 
--

CREATE INDEX organizaciones_organizacion_actividad_organizacion_id ON organizaciones_organizacion_actividad USING btree (organizacion_id);


--
-- Name: organizaciones_organizacion_fuente_fuenterenovable_id; Type: INDEX; Schema: public; Owner: renovable; Tablespace: 
--

CREATE INDEX organizaciones_organizacion_fuente_fuenterenovable_id ON organizaciones_organizacion_fuente USING btree (fuenterenovable_id);


--
-- Name: organizaciones_organizacion_fuente_organizacion_id; Type: INDEX; Schema: public; Owner: renovable; Tablespace: 
--

CREATE INDEX organizaciones_organizacion_fuente_organizacion_id ON organizaciones_organizacion_fuente USING btree (organizacion_id);


--
-- Name: organizaciones_organizacion_tipo_id; Type: INDEX; Schema: public; Owner: renovable; Tablespace: 
--

CREATE INDEX organizaciones_organizacion_tipo_id ON organizaciones_organizacion USING btree (tipo_id);


--
-- Name: organizaciones_organizacion_user_id_id; Type: INDEX; Schema: public; Owner: renovable; Tablespace: 
--

CREATE INDEX organizaciones_organizacion_user_id_id ON organizaciones_organizacion USING btree (user_id_id);


--
-- Name: organizaciones_proyecto_fuente_fuenterenovable_id; Type: INDEX; Schema: public; Owner: renovable; Tablespace: 
--

CREATE INDEX organizaciones_proyecto_fuente_fuenterenovable_id ON organizaciones_proyecto_fuente USING btree (fuenterenovable_id);


--
-- Name: organizaciones_proyecto_fuente_proyecto_id; Type: INDEX; Schema: public; Owner: renovable; Tablespace: 
--

CREATE INDEX organizaciones_proyecto_fuente_proyecto_id ON organizaciones_proyecto_fuente USING btree (proyecto_id);


--
-- Name: organizaciones_responsable_contacto_id; Type: INDEX; Schema: public; Owner: renovable; Tablespace: 
--

CREATE INDEX organizaciones_responsable_contacto_id ON organizaciones_responsable USING btree (contacto_id);


--
-- Name: organizaciones_sistemaejecucion_fuente_fuenterenovable_id; Type: INDEX; Schema: public; Owner: renovable; Tablespace: 
--

CREATE INDEX organizaciones_sistemaejecucion_fuente_fuenterenovable_id ON organizaciones_sistemaejecucion_fuente USING btree (fuenterenovable_id);


--
-- Name: organizaciones_sistemaejecucion_fuente_sistemaejecucion_id; Type: INDEX; Schema: public; Owner: renovable; Tablespace: 
--

CREATE INDEX organizaciones_sistemaejecucion_fuente_sistemaejecucion_id ON organizaciones_sistemaejecucion_fuente USING btree (sistemaejecucion_id);


--
-- Name: organizaciones_sistemaejecucion_materia_prima_id; Type: INDEX; Schema: public; Owner: renovable; Tablespace: 
--

CREATE INDEX organizaciones_sistemaejecucion_materia_prima_id ON organizaciones_sistemaejecucion USING btree (materia_prima_id);


--
-- Name: organizaciones_sistemaejecucion_organizacion_id; Type: INDEX; Schema: public; Owner: renovable; Tablespace: 
--

CREATE INDEX organizaciones_sistemaejecucion_organizacion_id ON organizaciones_sistemaejecucion USING btree (organizacion_id);


--
-- Name: organizaciones_sistemaejecucion_vegetal_id; Type: INDEX; Schema: public; Owner: renovable; Tablespace: 
--

CREATE INDEX organizaciones_sistemaejecucion_vegetal_id ON organizaciones_sistemaejecucion USING btree (vegetal_id);


--
-- Name: organizaciones_usuariosbeneficiarios_sistema_id; Type: INDEX; Schema: public; Owner: renovable; Tablespace: 
--

CREATE INDEX organizaciones_usuariosbeneficiarios_sistema_id ON organizaciones_usuariosbeneficiarios USING btree (sistema_id);


--
-- Name: actividad_id_refs_id_7162b16773f2347e; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY organizaciones_organizacion_actividad
    ADD CONSTRAINT actividad_id_refs_id_7162b16773f2347e FOREIGN KEY (actividad_id) REFERENCES nomencladores_actividad(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: anho_id_refs_anho_590ba1ed9f3f4e05; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY indicadores_valorindicador
    ADD CONSTRAINT anho_id_refs_anho_590ba1ed9f3f4e05 FOREIGN KEY (anho_id) REFERENCES indicadores_indicadoressectoriales(anho) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions_permission_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_permission_id_fkey FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_message_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY auth_message
    ADD CONSTRAINT auth_message_user_id_fkey FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_fkey FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions_permission_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_permission_id_fkey FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: categoria_id_refs_id_32a0302e7cb40235; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY organizaciones_contacto
    ADD CONSTRAINT categoria_id_refs_id_32a0302e7cb40235 FOREIGN KEY (categoria_id) REFERENCES nomencladores_categoria(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contacto_id_refs_id_417c605e7cfb32b3; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY organizaciones_email
    ADD CONSTRAINT contacto_id_refs_id_417c605e7cfb32b3 FOREIGN KEY (contacto_id) REFERENCES organizaciones_contacto(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contacto_id_refs_id_5749fc196adf4d3d; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY organizaciones_responsable
    ADD CONSTRAINT contacto_id_refs_id_5749fc196adf4d3d FOREIGN KEY (contacto_id) REFERENCES organizaciones_contacto(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: content_type_id_refs_id_728de91f; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT content_type_id_refs_id_728de91f FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: csvimport_importmodel_csvimport_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY csvimport_importmodel
    ADD CONSTRAINT csvimport_importmodel_csvimport_id_fkey FOREIGN KEY (csvimport_id) REFERENCES csvimport_csvimport(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_content_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_fkey FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_fkey FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fuenterenovable_id_refs_id_4a2e33381179229e; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY organizaciones_sistemaejecucion_fuente
    ADD CONSTRAINT fuenterenovable_id_refs_id_4a2e33381179229e FOREIGN KEY (fuenterenovable_id) REFERENCES nomencladores_fuenterenovable(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fuenterenovable_id_refs_id_62c55aaf2ea6f987; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY organizaciones_organizacion_fuente
    ADD CONSTRAINT fuenterenovable_id_refs_id_62c55aaf2ea6f987 FOREIGN KEY (fuenterenovable_id) REFERENCES nomencladores_fuenterenovable(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fuenterenovable_id_refs_id_66dc10bed799b526; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY organizaciones_proyecto_fuente
    ADD CONSTRAINT fuenterenovable_id_refs_id_66dc10bed799b526 FOREIGN KEY (fuenterenovable_id) REFERENCES nomencladores_fuenterenovable(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: group_id_refs_id_3cea63fe; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT group_id_refs_id_3cea63fe FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: indicador_id_refs_id_5d8055b13ecc2166; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY indicadores_valorindicador
    ADD CONSTRAINT indicador_id_refs_id_5d8055b13ecc2166 FOREIGN KEY (indicador_id) REFERENCES indicadores_indicador(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: materia_prima_id_refs_id_5eafdf1ffb67c477; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY organizaciones_sistemaejecucion
    ADD CONSTRAINT materia_prima_id_refs_id_5eafdf1ffb67c477 FOREIGN KEY (materia_prima_id) REFERENCES nomencladores_materiaprima(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: organizacion_id_refs_id_4cebea782dde55b0; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY organizaciones_responsable
    ADD CONSTRAINT organizacion_id_refs_id_4cebea782dde55b0 FOREIGN KEY (organizacion_id) REFERENCES organizaciones_organizacion(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: organizacion_id_refs_id_50dbef423155bf88; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY organizaciones_contacto
    ADD CONSTRAINT organizacion_id_refs_id_50dbef423155bf88 FOREIGN KEY (organizacion_id) REFERENCES organizaciones_organizacion(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: organizacion_id_refs_id_576152a0c7315be5; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY organizaciones_organizacion_actividad
    ADD CONSTRAINT organizacion_id_refs_id_576152a0c7315be5 FOREIGN KEY (organizacion_id) REFERENCES organizaciones_organizacion(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: organizacion_id_refs_id_5adda44e5bd751a; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY organizaciones_sistemaejecucion
    ADD CONSTRAINT organizacion_id_refs_id_5adda44e5bd751a FOREIGN KEY (organizacion_id) REFERENCES organizaciones_organizacion(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: organizacion_id_refs_id_62ce72c329ef572c; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY organizaciones_organizacion_fuente
    ADD CONSTRAINT organizacion_id_refs_id_62ce72c329ef572c FOREIGN KEY (organizacion_id) REFERENCES organizaciones_organizacion(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: organizacion_id_refs_id_643dd9129f37cf89; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY organizaciones_beneficiarios
    ADD CONSTRAINT organizacion_id_refs_id_643dd9129f37cf89 FOREIGN KEY (organizacion_id) REFERENCES organizaciones_organizacion(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: organizacion_id_refs_id_68fdbfb93413b0e6; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY organizaciones_proyecto
    ADD CONSTRAINT organizacion_id_refs_id_68fdbfb93413b0e6 FOREIGN KEY (organizacion_id) REFERENCES organizaciones_organizacion(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: organizacion_id_refs_id_6c6bd1d3aaa35f98; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY organizaciones_empleados
    ADD CONSTRAINT organizacion_id_refs_id_6c6bd1d3aaa35f98 FOREIGN KEY (organizacion_id) REFERENCES organizaciones_organizacion(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pais_id_refs_id_6a719dc120ec439e; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY organizaciones_contacto
    ADD CONSTRAINT pais_id_refs_id_6a719dc120ec439e FOREIGN KEY (pais_id) REFERENCES nomencladores_pais(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: proyecto_id_refs_organizacion_id_3733713df63272d9; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY organizaciones_galeriafotosproyecto
    ADD CONSTRAINT proyecto_id_refs_organizacion_id_3733713df63272d9 FOREIGN KEY (proyecto_id) REFERENCES organizaciones_proyecto(organizacion_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: proyecto_id_refs_organizacion_id_5136654725973f4c; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY organizaciones_proyecto_fuente
    ADD CONSTRAINT proyecto_id_refs_organizacion_id_5136654725973f4c FOREIGN KEY (proyecto_id) REFERENCES organizaciones_proyecto(organizacion_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: proyecto_id_refs_organizacion_id_e26d00954d8445b; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY organizaciones_beneficiariosproyecto
    ADD CONSTRAINT proyecto_id_refs_organizacion_id_e26d00954d8445b FOREIGN KEY (proyecto_id) REFERENCES organizaciones_proyecto(organizacion_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sistema_id_refs_id_3d283f9cce43348b; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY organizaciones_usuariosbeneficiarios
    ADD CONSTRAINT sistema_id_refs_id_3d283f9cce43348b FOREIGN KEY (sistema_id) REFERENCES organizaciones_sistemaejecucion(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sistema_id_refs_id_53e1cbdaa158931; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY organizaciones_galeriafotossistema
    ADD CONSTRAINT sistema_id_refs_id_53e1cbdaa158931 FOREIGN KEY (sistema_id) REFERENCES organizaciones_sistemaejecucion(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sistemaejecucion_id_refs_id_184bb47ea133c934; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY organizaciones_sistemaejecucion_fuente
    ADD CONSTRAINT sistemaejecucion_id_refs_id_184bb47ea133c934 FOREIGN KEY (sistemaejecucion_id) REFERENCES organizaciones_sistemaejecucion(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tipo_id_refs_id_56345143c561c43e; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY organizaciones_organizacion
    ADD CONSTRAINT tipo_id_refs_id_56345143c561c43e FOREIGN KEY (tipo_id) REFERENCES nomencladores_tipoentidad(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tipoindicador_id_refs_id_7d4f7d37944a5b51; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY indicadores_indicador
    ADD CONSTRAINT tipoindicador_id_refs_id_7d4f7d37944a5b51 FOREIGN KEY (tipoindicador_id) REFERENCES indicadores_tipoindicador(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_id_id_refs_id_363d5d2ae7e6ed28; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY organizaciones_organizacion
    ADD CONSTRAINT user_id_id_refs_id_363d5d2ae7e6ed28 FOREIGN KEY (user_id_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_id_refs_id_831107f1; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT user_id_refs_id_831107f1 FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_id_refs_id_f2045483; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT user_id_refs_id_f2045483 FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: vegetal_id_refs_id_5cdff6b86f85c9ce; Type: FK CONSTRAINT; Schema: public; Owner: renovable
--

ALTER TABLE ONLY organizaciones_sistemaejecucion
    ADD CONSTRAINT vegetal_id_refs_id_5cdff6b86f85c9ce FOREIGN KEY (vegetal_id) REFERENCES nomencladores_vegetal(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

