$(document).ready(function() {
    /* 
    * Coordenada del centroide aproximado de nicaragua
    */
    var latlng = new google.maps.LatLng(default_lat,default_lan);
    /*
     * Opcines de la instalcia del mapa
     */
    var myOptions = {
        zoom: 7,
        center: latlng,
        disableDefaultUI: true,
        panControl: true,
        zoomControl: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false
    };
    /*
     * definiendo limites del area navegable del mapa
     */
    map = new google.maps.Map(document.getElementById("mapa"),myOptions);
    /* 
     * Bounds for central america
     */
    var mapBounds = new google.maps.LatLngBounds(
        new google.maps.LatLng(7.5855991905, -95.0122070312), 
        new google.maps.LatLng(21.3352686127, -75.7641601562)
    );
    /*
     * Evaluando si el centro del mapa se encuentra dentro de los limite establecidos
     */
    var dragStartCenter;
    google.maps.event.addListener(map, 'dragstart', function(){
        dragStartCenter = map.getCenter();
    });
    google.maps.event.addListener(map, 'dragend', function(){
        if (mapBounds.contains(map.getCenter())) return;
        map.setCenter(dragStartCenter);
    });
    /*
     * Evaluando si el nivel de zoom no es mayor que el permitido
     */
    var minZoomLevel = 5;
    google.maps.event.addListener(map, 'zoom_changed', function() {
        if (map.getZoom() < minZoomLevel) map.setZoom(minZoomLevel);
    });
    markerCluster = new MarkerClusterer(map, markers, mcOptions);
    /*
     * Solicitud y procesamiento de datos
     * las llamadas se realizan de forma sincrona
     */
    var resource = $("#resource").val();
    var id= $("#id").val();
    parametros= {format: "json", id: id};
    switch(resource){
        case 'system':
            /*
             * Datos de sistemas
             */
            markers_system = loadSystemMarkers('system', parametros, markers_system, markerCluster, markers, true);    
            break;
        case 'organization':
            /*
             * Datos de organizaciones
             */
            markers_organization = loadOrganizationMarkers('organization', parametros, markers_organization, markerCluster, markers);    
            break;
    case 'project':
        /*
         * Datos de proyectos
         */
        markers_project = loadProjectMarkers('project', parametros, markers_project, markerCluster, markers);    
        break;
    }
    /* ocultando y mostrando pantalla de espera */
    $("#cargadormapa").ajaxStart(function() {
        $(this).show("fast");
    });
    $("#cargadormapa").ajaxStop(function() {
        $(this).hide();
    });
});
