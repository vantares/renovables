/*
 * Instancia del google map
 */
var map;
/*
 * Ruta donde se realizan los requisitos a la api
 */
//var api_url= "http://127.0.0.1:8000/api/v1/";
//var api_url= "http://127.0.0.1:8006/api/v1/";
var api_url= "/simernic/api/v1/";
var STATIC_URL= "/simernic/static/";
/*
 * url de direccion para marcadores
 */
//sistemas
var sys_url= "/simernic/detallesistema/";
var prj_url= "/simernic/detalleproyecto/ %}";

/*
 * Listado de todos los marcadores
 */
var markers= [];
var markers_system = [];
var markers_organization = [];
var markers_project = [];
var markers_project = [];
var markers_by_source= [];
var layers = [];
/*
 * Cluster de Marcadores
 */
var mcOptions = {
    gridSize: 50,
    maxZoom: 7,
    minimumClusterSize: 2
}
var markerCluster;
/*
 * coordenadas por defecto
 */
var default_lat= 12.865416;
var default_lan= -84.207229;
/*
 * Ventana de informacion
 */
var infowindow = new google.maps.InfoWindow();
/*
 * Opcines del infobox
 */
var myOptions = {
    disableAutoPan: false,
    maxWidth: 0,
    pixelOffset: new google.maps.Size(-140, 0),
    zIndex: null,
    boxStyle: { 
        background: "",
        opacity: 0.8,
        width: "150px"
    },
    closeBoxMargin: "5px",
    closeBoxURL: "",
    infoBoxClearance: new google.maps.Size(1, 1),
    isHidden: false,
    pane: "overlayMouseTarget",
    enableEventPropagation: false
};
var ib = new InfoBox(myOptions);

/*
 * Load Markers Function
 */
function loadSystemMarkers(resource_name, params, return_result, cluster, full_list, by_parts, url){
    var url_request="";
    if(url){
       url_request= url;
    }
    else{
        url_request=  api_url + resource_name +'/';
    }
    $.ajax({
    dataType: 'json', 
    async: true, 
    url: url_request,
    data: params,
    beforeSend: function(){
    },
    complete: function(){
    },
    success: function(data) {
        var objects= data.objects;
        var metainfo= data.meta;
        $.each(objects, function(index, element){
            var lat_lng = new google.maps.LatLng (element.latitud, element.longitud);
            var icon;
            if(element.fuentes.length > 0){
                icon = element.fuentes[0].imagen;
            }
            else{
                icon = STATIC_URL + "imagenes/varios.png";
            }
            var marker = new google.maps.Marker({
                position: lat_lng,
                draggable: false,
                raiseOnDrag: true,
                map: map,
        //        title: element.nombre ,
                icon: icon
            });
            return_result.push(marker);
            if(cluster){
                cluster.addMarker(marker, false);
            }
            var popupcontent = system_getpopup(element);
            createInfoWindow(marker, popupcontent);
            cantidadsistemas = "<b>" + element.nombre + "</b>" 
            if(element.cantidad_sistemas >0){
                cantidadsistemas= cantidadsistemas + "<br/><b>" + element.cantidad_sistemas + " sistemas</b>"
            }
            createBoxLayer(marker, cantidadsistemas);
        });
        if(full_list){
            full_list = full_list.concat(return_result);
        }
        if(by_parts && metainfo.next){
            var offset= parseInt(metainfo.offset)+ parseInt(metainfo.limit);
            var limit= parseInt(metainfo.limit);
            local_params= { offset: offset, limit: limit };
            $.extend(local_params, params);
            loadSystemMarkers(resource_name, null, return_result, cluster, full_list, by_parts, metainfo.next);    
        }
    }
    });
    return return_result; 
}

function loadOrganizationMarkers(resource_name, params, return_result, cluster, full_list, by_parts, url){
    var url_requst="";
    if(url){
       url_request= url;
    console.log("primer caso");
    }
    else{
        url_request=  api_url + resource_name +'/';
    console.log("segundo caso");
    }
    
    console.log(url_request);
    $.ajax({
    dataType: 'json', 
    //async: false, 
    async: true, 
    url: url_request,
    data: params,
    beforeSend: function(){
    },
    complete: function(){
    },
    success: function(data) {
        var objects= data.objects;
        var metainfo= data.meta;
        $.each(objects, function(index, element){
            var lat_lng = new google.maps.LatLng (element.latitud, element.longitud);
            var icon;
            if(element.fuentes.length > 0){
                icon = element.fuentes[0].imagen;
            }
            else{
                icon = STATIC_URL + "imagenes/varios.png";
            }
            var marker = new google.maps.Marker({
                position: lat_lng,
                draggable: false,
                raiseOnDrag: true,
                map: map,
                title: element.nombre ,
                icon: icon
            });
            return_result.push(marker);
            if(cluster){
                cluster.addMarker(marker, false);
            }
            var popupcontent = organization_getpopup(element);
            createInfoWindow(marker, popupcontent);
        });
        if(full_list){
            full_list = full_list.concat(return_result);
        }
        if(by_parts && metainfo.next){
            var offset= parseInt(metainfo.offset)+ parseInt(metainfo.limit);
            var limit= parseInt(metainfo.limit);
            local_params= { offset: offset, limit: limit };
            $.extend(local_params, params);
            loadOrganizationMarkers(resource_name, null, return_result, cluster, full_list, by_parts, metainfo.next);    
        }
    }
    });
   return return_result; 
}

function loadProjectMarkers(resource_name, params, return_result, cluster, full_list, by_parts, url){
    var url_requst="";
    if(url){
       url_request= url;
    }
    else{
        url_request=  api_url + resource_name +'/';
    }
    $.ajax({
    dataType: 'json', 
    async: true, 
    url: url_request,
    data: params,
    beforeSend: function(){
    },
    complete: function(){
    },
    success: function(data) {
        var objects= data.objects;
        var metainfo= data.meta;
        $.each(objects, function(index, element){
            var lat_lng = new google.maps.LatLng (element.latitud, element.longitud);
            var icon;
            if(element.fuentes.length > 0){
                icon = element.fuentes[0].imagen;
            }
            else{
                icon = STATIC_URL + "imagenes/varios.png";
            }
            var marker = new google.maps.Marker({
                position: lat_lng,
                draggable: false,
                raiseOnDrag: true,
                map: map,
                title: element.nombre ,
                icon: icon
            });
            return_result.push(marker);
            if(cluster){
                cluster.addMarker(marker, false);
            }
            var popupcontent = project_getpopup(element);
            createInfoWindow(marker, popupcontent);
        });
        if(full_list){
            full_list = full_list.concat(return_result);
        }
        if(by_parts && metainfo.next){
            var offset= parseInt(metainfo.offset)+ parseInt(metainfo.limit);
            var limit= parseInt(metainfo.limit);
            local_params= { offset: offset, limit: limit };
            $.extend(local_params, params);
            loadProjectMarkers(resource_name, params, return_result, cluster, full_list, by_parts, metainfo.next);    
        }
    }
    });
   return return_result; 
}

function loadMenuKML(){
    var externalkml= [];
    var parametros= {format: "json"};
    loadExternalKML(false,'externalkml', parametros, null , null, externalkml);
    $("#menukml").html('<ul class="item"></li>');
    $.each(externalkml, function(i, layer){
        $("#menukml ul.item").append('<li class="check"><input class="checkbuscar" id="layer'+layer.id+'" name="kml" type="checkbox" value="'+layer.id+'"  /><label class="texto">'+layer.nombre+'</label> </li>');
    });
}

function loadExternalKML(async, resource_name, params, return_result, full_list, return_elements){
    $.ajax({
    dataType: 'json', 
    //async: false, 
    async: async, 
    url: api_url + resource_name +'/',
    data: params,
    beforeSend: function(){
    },
    complete: function(){
    },
    success: function(data) {
        var objects= data.objects;
        $.each(objects, function(index, element){
            if(return_elements){
                // si hay donde retornar entonce no se despliega en el mapa
                return_elements.push(element);
            }
            else {
                // si no hay donde retornar se muestra en el mapa
                var layer = new google.maps.KmlLayer(element.cargo);
                if(return_result){
                    return_result.push(layer);
                }
                layer.setMap(map);
            }
        });
        if(full_list){
            full_list = full_list.concat(return_result);
        }
    }
    });
}

function displayinfo(event, content) {
    //infowindow.close();
    var infocontent='ventana informativa '+ content;
    infowindow.setContent(infocontent);
    infowindow.setPosition(event.latLng);
    infowindow.open(map, this);
}

function createInfoWindow(marker, popupContent) {
    google.maps.event.addListener(marker, 'click', function (event) {
        infowindow.setContent(popupContent);
        infowindow.setPosition(event.latLng);
        infowindow.open(map, this);
    });
}

function createBoxLayer(marker, boxContent) {
    var boxText = document.createElement("div");
    boxText.style.cssText = "border: none; margin-top: 5px; background: #0068C2; color: #ffffff; padding: 5px; font-size: 12px; -webkit-border-radius: 4px;-moz-border-radius: 4px;border-radius: 4px;";
    boxText.innerHTML = boxContent;
    google.maps.event.addListener(marker, "mouseover", function (e) {
        ib.setContent(boxText);
        ib.open(map, this);
    });
    google.maps.event.addListener(marker, "mouseout", function (e) {
        ib.close(map, this);
    });
}

function system_getpopup(system){
    var popupcontent= '<div class="toltipmapa">';
    if(system.popupimage != ''){
    popupcontent+= '<div style="float:left; width:130px; text-align:center; ">'
     popupcontent+= '<img src="' + system.popupimage + '" >'      
     popupcontent+= '</div>'}

    popupcontent+= '<div style="float: right;width:250px;">';
    popupcontent+= '<img align="left" src="' + STATIC_URL + 'imagenes/sismapa.png" alt="SISTEMAS"><p style="margin-top: 5px"><b><a href="'+sys_url+system.id+'" target=new>'+system.nombre+'</a></b></p>';
    popupcontent+= '<div class="clear"></div>';
    popupcontent+= '<div class="clear"></div><p><b>Fuente(s) Renovable(s):</b> <p>';
    if(system.fuentes.length>0){
        $.each(system.fuentes, function(i,fuente){
            popupcontent+= '<img align="left" style="margin-right: 15px" src="'+ fuente.imagen +'" ><p style="margin-top: 5px"> '+ fuente.nombre +'.</p>';
            popupcontent += '<div class="clear"></div>';
        });
    };
    popupcontent += '<div class="clear"></div>';
    if(system.organizacion){
        popupcontent += '<p><b>Organización:</b> '+ system.organizacion.nombre +'</p>';
    }
    popupcontent += '<p><b>Tarifa promedio anual por MWh:</b> '+ system.tarifa +' Usd$</p>';
    popupcontent += '<p><b>Potencia:</b> '+ system.potencia +' W</p>';
    popupcontent += '</div>';
    popupcontent += '</div>';
    return popupcontent;
}

function organization_getpopup(org){
    var popupcontent = '<div class="toltipmapa">';
    if(org.popupimage != ''){
    popupcontent+= '<div style="float:left; width:130px; text-align:center;">'
     popupcontent+= '<img src="' + org.popupimage + '" >'      
     popupcontent+= '</div>'}
    popupcontent += '<div style="float: right;width:250px;">';
    popupcontent += '<img align="left" src="' + STATIC_URL + 'imagenes/orgmapa.png" alt="Organizaciones"><p style="margin-top: 5px"><b><a href="'+ org.get_absolute_url + '" target=new>' + org.nombre +'</b></p>';

    popupcontent += '<div class="clear"></div>';
    popupcontent += '<p><b>Web:</b> <a href="'+ org.web +'" target=new>'+ org.web +'</a></p>';
    popupcontent += '<div class="clear"></div><p><b>Fuente(s) Renovable(s):</b> <p>';
    if(org.fuentes.length>0){
        $.each(org.fuentes, function(i,fuente){
            popupcontent+= '<img align="left" style="margin-right: 15px" src="'+ fuente.imagen +'" ><p style="margin-top: 5px"> '+ fuente.nombre +'.</p>';
            popupcontent += '<div class="clear"></div>';
        });
    };
    popupcontent+= '<div class="clear"></div><p><b>Actividades:</b><p>';
    if(org.actividades.length>0){
        popupcontent+= '<p>';
        $.each(org.actividades, function(i, actividad){
            popupcontent+= actividad.nombre +', ';
        });
        popupcontent+= '</p>';
    };
    popupcontent += '</div>';
    popupcontent += '</div>';
    return popupcontent;
}

function project_getpopup(project){
    var popupcontent= '<div class="toltipmapa">';
    if(project.popupimage != ''){
    popupcontent+= '<div style="float:left; width:130px; text-align:center; ">'
     popupcontent+= '<img src="' + project.popupimage + '" >'      
     popupcontent+= '</div>'}

    popupcontent+= '<div style="float: right;width:250px;">';
    popupcontent+= '<img align="left" src="' + STATIC_URL + 'imagenes/promapa.png" alt="Proyectos"><p style="margin-top: 5px"><b><a href="'+project.get_absolute_url+'" target=new>'+project.nombre+'</a></b></p>';
    popupcontent+= '<div class="clear"></div>';
    popupcontent+= '<div class="clear"></div><p><b>Fuente(s) Renovable(s):</b> <p>';
    if(project.fuentes.length>0){
        $.each(project.fuentes, function(i,fuente){
            popupcontent+= '<img align="left" style="margin-right: 15px" src="'+ fuente.imagen +'" ><p style="margin-top: 5px"> '+ fuente.nombre +'.</p>';
            popupcontent += '<div class="clear"></div>';
        });
    };
    popupcontent += '<div class="clear"></div>';
    popupcontent += '<p><b>Organización:</b> '+ project.organizacion.nombre +'</p>';
    // incluir los beneficiarios
    popupcontent += '</div>';
    popupcontent += '</div>';
    return popupcontent;
}

function loadMapData(map, show_systems, show_organizations, show_projects, sources, kmllayers){
    if (markerCluster) {
        markerCluster.clearMarkers();
    }
    if(sources){
        $.each(markers, function(i, marker){
            marker.setMap(null);
        });
        markers= [];
        markers_system= [];
        markers_organization = [];
        markers_project = [];
        parametros= {format: "json", src: sources};
    }
    else{
        parametros= {format: "json"};
    }

    if(show_systems){
        markers_system = loadSystemMarkers('system', parametros , markers_system, markerCluster, markers, true);    
    }
    if(show_organizations){
        markers_organization = loadOrganizationMarkers('organization', parametros, markers_organization, markerCluster, markers, true);    
    }
    if(show_projects){
        markers_project = loadProjectMarkers('project', parametros , markers_project, markerCluster, markers, true);    
    }
    if(kmllayers){
        $.each(layers, function(i,layer){
            layer.setMap(null);
        });
        var parametros= {format: "json", lyrs: kmllayers };
        active_layers= [];
        loadExternalKML(true, 'externalkml', parametros, active_layers, null, null);
        layers= active_layers;
    }
}

function reloadMap(){
    var organizacion= false;
    var proyecto= false;
    var sistema= false;
    var arrkml = ""+$("input[id^='layer']:checked").getCheckboxVal();
    if(arrkml == ""){
        var arrkml=0;
    }
    var arrfuente = ""+$("input[name='fuenterenovable']:checked").getCheckboxVal();
    if(arrfuente == ""){ 
        var arrfuente=0;
    }
    if($("#organizaciones").is(":checked")){
        organizacion= true;
    }
    if($("#proyectos").is(":checked")){
        proyecto= true;
    }
    if($("#sistemas").is(":checked")){
        sistema= true;
    }
    loadMapData(map, sistema, organizacion, proyecto, arrfuente, arrkml); 
}
