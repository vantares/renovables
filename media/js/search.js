/**
 * Funciones de busqueda avanzada y carga de resultados
 * Filtos, interacción de la interface, peticiones y paso de parametros.  
 */

$(document).ready(function() {
    $("#fltr_src").multiselect({header: "Seleccione una o varias fuentes", minWidth:'190px'}); 
    $("#fltr_org").multiselect({header: "Seleccione una o varias Organizaciones", minWidth:'190px'}).multiselectfilter({ width: '190px'}); 
    $("#fltr_act").multiselect({header: "Seleccione una o varias actividades", minWidth:'190px'}); 
    $("#presupuesto").css({"width":"190px"}).multiselect({header: "Seleccione un rango", minWidth:'150px', multiple: false, noneSelectedText: "Seleccione un rango", selectedList: 1}); 
});
