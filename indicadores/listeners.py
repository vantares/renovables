# -*- coding: UTF-8 -*-
'''
Created on Nov 11, 2012

@author: luis
'''
from indicadores.utils import obtener_valor_indicador_anual
from django.conf import settings

def proyeccionanual_post_save_handler(sender, *args, **kwargs):
    from indicadores.models import ValorIndicador
    instance = kwargs.get('instance')   
    print "FIXME" 
    try:
        vi = ValorIndicador.objects.get(indicador=instance.indicador,
                                        anho=instance.anho
                                        )
        vi.valor = obtener_valor_indicador_anual(
                                             instance.indicador.pk,
                                             instance.anho
                                             )
        vi.save()

    except ValorIndicador.DoesNotExist:
        pass


    

def valorindicador_pre_save_handler(sender, *args, **kwargs):
    instance = kwargs.get('instance')    
    if instance.automatico and instance.indicador.pk != settings.PORCENTAJE_RENOVABLES :
        instance.valor = obtener_valor_indicador_anual(
                                             instance.indicador.pk,
                                             instance.anho.anho
                                             )
    
def indicadoressectoriales_post_save_handler(sender, *args, **kwargs):
    instance = kwargs.get('instance')
    from indicadores.models import Indicador, ValorIndicador
    ids = [i.indicador.pk for i in instance.valorindicador_set.filter(automatico=False)]
    for indicador in Indicador.objects.exclude(pk__in=ids).order_by('id'):
        #print u"calculando indicador %s"%indicador.nombre
        valor = obtener_valor_indicador_anual(indicador.pk,
                                              instance.anho
                                              )
        inst, created = ValorIndicador.objects.get_or_create(
                                             indicador=indicador,
                                             anho=instance,
                                             defaults={'automatico':True,
                                                       'valor':valor
                                                       }
                                             )
