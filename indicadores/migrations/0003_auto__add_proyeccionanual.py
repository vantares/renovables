# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'ProyeccionAnual'
        db.create_table('indicadores_proyeccionanual', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('indicador', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['indicadores.Indicador'])),
            ('anho', self.gf('django.db.models.fields.IntegerField')()),
            ('valor1', self.gf('django.db.models.fields.DecimalField')(max_digits=16, decimal_places=2)),
            ('valor2', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=16, decimal_places=2, blank=True)),
            ('valor3', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=16, decimal_places=2, blank=True)),
            ('valor4', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=16, decimal_places=2, blank=True)),
        ))
        db.send_create_signal('indicadores', ['ProyeccionAnual'])


    def backwards(self, orm):
        # Deleting model 'ProyeccionAnual'
        db.delete_table('indicadores_proyeccionanual')


    models = {
        'indicadores.indicador': {
            'Meta': {'object_name': 'Indicador'},
            'explicacion': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'fuentes': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'icono': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'nombrelargo': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'orden': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'tipoindicador': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['indicadores.TipoIndicador']"}),
            'unidad': ('django.db.models.fields.CharField', [], {'max_length': '500'})
        },
        'indicadores.indicadoressectoriales': {
            'Meta': {'object_name': 'IndicadoresSectoriales'},
            'anho': ('django.db.models.fields.IntegerField', [], {'unique': 'True', 'primary_key': 'True'}),
            'indicador': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['indicadores.Indicador']", 'symmetrical': 'False', 'through': "orm['indicadores.ValorIndicador']", 'blank': 'True'})
        },
        'indicadores.proyeccionanual': {
            'Meta': {'object_name': 'ProyeccionAnual'},
            'anho': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'indicador': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['indicadores.Indicador']"}),
            'valor1': ('django.db.models.fields.DecimalField', [], {'max_digits': '16', 'decimal_places': '2'}),
            'valor2': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '16', 'decimal_places': '2', 'blank': 'True'}),
            'valor3': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '16', 'decimal_places': '2', 'blank': 'True'}),
            'valor4': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '16', 'decimal_places': '2', 'blank': 'True'})
        },
        'indicadores.tipoindicador': {
            'Meta': {'object_name': 'TipoIndicador'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'indicadores.valorindicador': {
            'Meta': {'object_name': 'ValorIndicador'},
            'anho': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['indicadores.IndicadoresSectoriales']"}),
            'automatico': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'indicador': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['indicadores.Indicador']"}),
            'valor': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '16', 'decimal_places': '4', 'blank': 'True'})
        }
    }

    complete_apps = ['indicadores']