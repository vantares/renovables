# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'TipoIndicador'
        db.create_table('indicadores_tipoindicador', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal('indicadores', ['TipoIndicador'])

        # Adding model 'Indicador'
        db.create_table('indicadores_indicador', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('orden', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('tipoindicador', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['indicadores.TipoIndicador'])),
            ('nombrelargo', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('explicacion', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('fuentes', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('unidad', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('icono', self.gf('sorl.thumbnail.fields.ImageField')(max_length=100, null=True, blank=True)),
        ))
        db.send_create_signal('indicadores', ['Indicador'])

        # Adding model 'IndicadoresSectoriales'
        db.create_table('indicadores_indicadoressectoriales', (
            ('anho', self.gf('django.db.models.fields.IntegerField')(unique=True, primary_key=True)),
        ))
        db.send_create_signal('indicadores', ['IndicadoresSectoriales'])

        # Adding model 'ValorIndicador'
        db.create_table('indicadores_valorindicador', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('anho', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['indicadores.IndicadoresSectoriales'])),
            ('indicador', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['indicadores.Indicador'])),
            ('valor', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=16, decimal_places=4, blank=True)),
        ))
        db.send_create_signal('indicadores', ['ValorIndicador'])


    def backwards(self, orm):
        # Deleting model 'TipoIndicador'
        db.delete_table('indicadores_tipoindicador')

        # Deleting model 'Indicador'
        db.delete_table('indicadores_indicador')

        # Deleting model 'IndicadoresSectoriales'
        db.delete_table('indicadores_indicadoressectoriales')

        # Deleting model 'ValorIndicador'
        db.delete_table('indicadores_valorindicador')


    models = {
        'indicadores.indicador': {
            'Meta': {'object_name': 'Indicador'},
            'explicacion': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'fuentes': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'icono': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'nombrelargo': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'orden': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'tipoindicador': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['indicadores.TipoIndicador']"}),
            'unidad': ('django.db.models.fields.CharField', [], {'max_length': '500'})
        },
        'indicadores.indicadoressectoriales': {
            'Meta': {'object_name': 'IndicadoresSectoriales'},
            'anho': ('django.db.models.fields.IntegerField', [], {'unique': 'True', 'primary_key': 'True'}),
            'indicador': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['indicadores.Indicador']", 'symmetrical': 'False', 'through': "orm['indicadores.ValorIndicador']", 'blank': 'True'})
        },
        'indicadores.tipoindicador': {
            'Meta': {'object_name': 'TipoIndicador'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'indicadores.valorindicador': {
            'Meta': {'object_name': 'ValorIndicador'},
            'anho': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['indicadores.IndicadoresSectoriales']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'indicador': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['indicadores.Indicador']"}),
            'valor': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '16', 'decimal_places': '4', 'blank': 'True'})
        }
    }

    complete_apps = ['indicadores']