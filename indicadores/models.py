# -*- coding: UTF-8 -*-
from django.db import models
from sorl.thumbnail import ImageField
from django.conf import settings
from django.db.models.signals import post_save, pre_save
from indicadores import listeners

# Create your models here.
class TipoIndicador(models.Model):
    nombre = models.CharField(max_length=200,verbose_name="Nombre")

    class Meta:
        verbose_name = "Tipo de Indicador"
        verbose_name_plural = "Tipos de Indicadores"

    def __unicode__(self):
        return unicode(self.nombre)

class Indicador(models.Model):
    orden = models.IntegerField(null=True,blank=True,help_text="Orden en pagina de inicio")
    nombre = models.CharField(max_length=200,verbose_name="Nombre corto")
    tipoindicador=models.ForeignKey(TipoIndicador)
    nombrelargo = models.CharField(max_length=500,verbose_name="nombre completo")
    explicacion = models.TextField(verbose_name= u"Explicación (de las formulas)",blank=True,null=True)
    fuentes = models.TextField(verbose_name="Fuente(s) de los datos",blank=True,null=True)
    unidad = models.CharField(max_length=500,verbose_name="Unidad")
    icono = ImageField(upload_to='indicador',blank=True,null=True)

    class Meta:
        verbose_name = "Indicador"
        verbose_name_plural = "Indicadores"


    def __unicode__(self):
        return unicode(self.nombre)

class IndicadoresSectoriales(models.Model):
    anho = models.IntegerField(primary_key=True,unique=True,verbose_name= u'Año')
    indicador = models.ManyToManyField(Indicador, through='ValorIndicador', blank=True)

    class Meta:
        verbose_name = u"Indicador sectorial por año"
        verbose_name_plural = u"Indicadores Sectoriales por año"


    def __unicode__(self):
        return str(self.anho)

    def get_valueindicadores(self):
        valor =ValorIndicador.objects.filter(anho=self.pk).order_by('indicador__orden')
        return valor


class ValorIndicador(models.Model):
    anho = models.ForeignKey(IndicadoresSectoriales)
    indicador = models.ForeignKey(Indicador)
    valor = models.DecimalField(max_digits=16,decimal_places=4,blank=True, null=True)
    orden = models.IntegerField(default=0)
    automatico = models.BooleanField(default=False,help_text='Si este campo esta marcado, el valor se calculará en base a las fórmulas preestablecidas')
    
    class Meta:
        verbose_name = u"Valor Indicador"
        verbose_name_plural = "Valores Indicador"
        unique_together = ('indicador','anho')
            
    def __unicode__(self):
        return unicode(self.valor)
    
#    def save(self, force_insert=False, force_update=False, using=None):
#        if self.pk is not None and self.automatico:
#            print self.nombre
#            self.valor = utils.obtener_valor_indicador_anual(
#                                         self.indicador.pk,
#                                         self.anho.pk)    
#
#        models.Model.save(self, force_insert=force_insert, force_update=force_update, using=using)
        
class ProyeccionAnual(models.Model):
    indicador = models.ForeignKey(Indicador)
    
    anho = models.IntegerField(verbose_name=u'Año')    
    valor1 = models.DecimalField(max_digits=16,decimal_places=2                                 
                                 )
    valor2 = models.DecimalField(max_digits=16,decimal_places=2,
                                 blank=True, null=True
                                 )
    valor3 = models.DecimalField(max_digits=16,decimal_places=2,
                                 blank=True, null=True                                                                  
                                 )
    valor4 = models.DecimalField(max_digits=16,decimal_places=2,
                                 blank=True, null=True                                 
                                 )
    factor = models.DecimalField(max_digits=16,decimal_places=2,
                                 blank=True, null=True
                                 )

    @property
    def total(self):
        valor = sum([self.valor1,
                        (self.valor2 or 0),
                        (self.valor3 or 0),
                        (self.valor4 or 0)
                        ])
        if self.factor is not None:
            valor*=self.factor
            
        return valor
    
    class Meta:
        verbose_name = u"Proyección anual"
        verbose_name_plural = "Proyecciones Anuales"
        unique_together = ('indicador','anho')        
    
post_save.connect(
              listeners.indicadoressectoriales_post_save_handler, 
              IndicadoresSectoriales,
              dispatch_uid='actualizar_indicadores')

pre_save.connect(listeners.valorindicador_pre_save_handler,
                  ValorIndicador,
                  dispatch_uid='actualizar_valor_indicador')
post_save.connect(listeners.proyeccionanual_post_save_handler,
                  ProyeccionAnual,
                  dispatch_uid='actualizar_valor_indicador')
