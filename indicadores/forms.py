# -*- coding: UTF-8 -*-
'''
Created on Nov 25, 2012

@author: luis
'''
from django import forms
from indicadores.models import ProyeccionAnual

class ProyeccionAnualForm(forms.ModelForm):
    valor1_label="Valor1"
    class Meta:
        model = ProyeccionAnual
        fields = ['anho','valor1']
            
    def __init__(self,*args,**kwargs):
        super(ProyeccionAnualForm,self).__init__(*args,**kwargs)
        self.base_fields['valor1'].label=self.valor1_label            
        
class PoblacionAnualForm(ProyeccionAnualForm):
    valor1_label = u"Población Nacional"    
    
class ValorGEIForm(ProyeccionAnualForm):        
    valor1_label = u"USD $"
    
    
class AhorroFacturaPetroleraForm(ProyeccionAnualForm):    
    valor1_label = u"Precio del petroleo en USD $"    
        
        
class ReduccionGEIForm(forms.ModelForm):
    class Meta:
        model = ProyeccionAnual
        
    def __init__(self,*args,**kwargs):
        super(ReduccionGEIForm,self).__init__(*args,**kwargs)
        self.base_fields['valor1'].label= u"Hidroeléctricas (SIN + Aisladas)"
        self.base_fields['valor2'].label= u"Geotérmicas"
        self.base_fields['valor3'].label= u"Eólicas (SIN + Aisladas)"
        self.base_fields['valor4'].label= u"Térmicas de biomasa"
        self.base_fields['factor'].label= u"Factor kgC02/GWh"         
        
        

    