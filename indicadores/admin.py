# -*- coding: UTF-8 -*-
from django.contrib import admin
from indicadores.models import IndicadoresSectoriales,Indicador,TipoIndicador,ValorIndicador,\
    ProyeccionAnual
from django.utils.functional import curry
from django import forms
from django.conf import settings
from django.forms.models import inlineformset_factory
from indicadores.forms import PoblacionAnualForm, ReduccionGEIForm,\
    ProyeccionAnualForm, ValorGEIForm, AhorroFacturaPetroleraForm



class ValorIndicadorFormSet(forms.models.BaseInlineFormSet):
    def clean(self):
        # get forms that actually have valid data
        validar_indicador_principal = True
        count = 0
        for form in self.forms:            
            try:
                                    
                if form.cleaned_data:
                    instance = form.save(commit=False)
                    if instance.valor is None:
                        raise forms.ValidationError('Debe especificar el valor del indicador %s'
                                                    %instance.indicador.nombre
                                                    )
                    if validar_indicador_principal:
                        validar_indicador_principal = not( instance.indicador.nombre == settings.INDICADOR_PRINCIPAL)
                    count += 1
                    
            except AttributeError:
                pass
            
        if count < 1:
            raise forms.ValidationError('Debe especificar al menos un Indicador')
        elif validar_indicador_principal:            
            raise forms.ValidationError('Debe seleccionar y especificar el valor del indicador %s'%settings.INDICADOR_PRINCIPAL)
            
            


class ValorIndicadorInline(admin.TabularInline):
#    formset = ValorIndicadorFormSet
    model = ValorIndicador    
    extra = 0
    
    def has_add_permission(self, request):
        return False
    
    def has_delete_permission(self, request, obj=None):
        return False

#    def get_formset(self, request, obj=None, **kwargs):
#        """
#        Pre-populating formset using GET params        
#        """     
#        if obj is None:
#            self.extra = 1
#            initial = []
#            if request.method == "GET":
#                # Populate initial based on request
#                try:
#                    i = Indicador.objects.get(nombre=settings.INDICADOR_PRINCIPAL)
#                    initial.append({ 'indicador': i.pk })      
#                except (Indicador.DoesNotExist,Indicador.MultipleObjectsReturned):
#                    pass
#                      
#            formset = super(ValorIndicadorInline, self).get_formset(request,
#                                                         obj, **kwargs)
#            formset.__init__ = curry(formset.__init__, initial=initial)
#            return formset
#        
#        else:
#            self.extra = 0
#            return super(ValorIndicadorInline, self).get_formset(request,
#                                                      obj, **kwargs)    

class IndicadoresSectorialesAdmin(admin.ModelAdmin):
    inlines = (ValorIndicadorInline,)
    
    def get_formsets(self, request, obj=None):        
        if obj is not None:
            return super(IndicadoresSectorialesAdmin,self).get_formsets(
                                                    request, obj=obj)
        else:
            return []     

class ProyeccionAnualInline(admin.TabularInline):
    model = ProyeccionAnual 
    extra = 0
    
    def get_formset(self, request, obj=None, **kwargs):
        formset =  super(ProyeccionAnualInline,self).get_formset(
                                     request, obj=obj, **kwargs)
        formset.form = ProyeccionAnualForm
                        
        if obj is not None:
            if obj.pk == settings.REDUCCION_GEI:
                self.verbose_name_plural= u"Generación bruta por tipo de generación (GWh)"
                formset.form = ReduccionGEIForm

            elif obj.pk == settings.POBLACION_ENERGIA_RENOVABLE:                             
                formset.form = PoblacionAnualForm
                
            elif obj.pk == settings.VALOR_GEI_AHORRADOS:
                formset.form = ValorGEIForm
                
            elif obj.pk == settings.AHORRO_FACTURA_PETROLERA:
                formset.form = AhorroFacturaPetroleraForm
                
                
        return formset
    
class IndicadorAdmin(admin.ModelAdmin):
    inlines = (ProyeccionAnualInline,)
    list_display = ['nombre','orden']
    
    def get_formsets(self, request, obj=None):        
        if obj is not None and obj.pk in [settings.POBLACION_ENERGIA_RENOVABLE,
                                         settings.REDUCCION_GEI,
                                         settings.VALOR_GEI_AHORRADOS,
                                         settings.AHORRO_FACTURA_PETROLERA,
                                         ]:
            return super(IndicadorAdmin,self).get_formsets(
                                                    request, obj=obj)
        else:
            return []    

class ValorIndicadorAdmin(admin.ModelAdmin):
    list_display = ['indicador','anho','valor']
    list_filter = ['indicador','anho']
    
    

admin.site.register(IndicadoresSectoriales,IndicadoresSectorialesAdmin)
admin.site.register(Indicador,IndicadorAdmin)
admin.site.register(TipoIndicador)
admin.site.register(ValorIndicador,ValorIndicadorAdmin)
