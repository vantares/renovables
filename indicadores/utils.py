# -*- coding: UTF-8 -*-
'''
Created on Nov 11, 2012

@author: luis
'''
from django.db.models.aggregates import Sum
from django.conf import settings
from decimal import Decimal

def actualizar_indicador(sender, instance, *args, **kwargs):
    from indicadores.models import  ValorIndicador, IndicadoresSectoriales
    indicador_sectorial, created = IndicadoresSectoriales.objects.get_or_create(
        anho=instance.anho
    )
    vi, created = ValorIndicador.objects.get_or_create(
        indicador=sender.INDICADOR_ID,
        anho=indicador_sectorial
        )
    vi.valor = obtener_valor_indicador_anual(
        sender.INDICADOR_ID,
        instance.anho
        )
    vi.save()


def obtener_valor_indicador_anual(indicador_id,anho):
    from indicadores.models import ProyeccionAnual , ValorIndicador
    from organizaciones.models import (Beneficiarios,
                                    BeneficiariosProyecto,
                                    UsuariosBeneficiarios,
                                    Empleados)

    if indicador_id == settings.POBLACION_BENEFICIADA:
        valor = sum([ (Beneficiarios.objects.filter(anho=anho).aggregate(
                                         total=Sum('numero'))['total'] or 0),
                   (BeneficiariosProyecto.objects.filter(
                                                 anho=anho
                                                 ).aggregate(
                                         total=Sum('numero'))['total'] or 0),

                    (UsuariosBeneficiarios.objects.filter(
                                                  anho=anho
                                                  ).aggregate(
                                         total=Sum('numero'))['total'] or 0)
                   ])

    elif indicador_id == settings.EMPLEOS_GENERADOS:
        valor = Empleados.objects.filter(anho=anho).aggregate(
                                         total=Sum('numero'))['total'] or 0

    elif indicador_id in [settings.POBLACION_ENERGIA_RENOVABLE,
                          settings.AHORRO_FACTURA_PETROLERA
                          ]:
        try:
            valor = ValorIndicador.objects.get(indicador=settings.PORCENTAJE_RENOVABLES,
                                               anho=anho
                                           ).valor
            valor = (valor/ Decimal('100')) * (ProyeccionAnual.objects.filter(
                                                 anho=anho,
                                                 indicador=indicador_id
                                                  ).aggregate(
                                         total=Sum('valor1'))['total'] or 0)
        except ValorIndicador.DoesNotExist:
            valor = 0

    elif indicador_id == settings.REDUCCION_GEI:
        try:
            valor = ProyeccionAnual.objects.get(anho=anho,
                                             indicador=settings.REDUCCION_GEI
                                             ).total
        except ProyeccionAnual.DoesNotExist:
            valor = 0

    elif indicador_id == settings.VALOR_GEI_AHORRADOS:
        try:
            valor =  ProyeccionAnual.objects.get(anho=anho,
                                                 indicador=settings.REDUCCION_GEI
                             ).total * ProyeccionAnual.objects.get(
                                                 anho=anho,
                                                 indicador=settings.VALOR_GEI_AHORRADOS
                                                 ).total
        except ProyeccionAnual.DoesNotExist:
            valor = 0

    elif indicador_id == settings.PORCENTAJE_RENOVABLES:
        try:
            valor = ValorIndicador.objects.get(indicador=settings.PORCENTAJE_RENOVABLES,
                                           anho=anho
                                       ).valor
        except ValorIndicador.DoesNotExist:
            valor = 0
    else:
        valor =  0

    print "valor calculado %s"%valor
    return valor
