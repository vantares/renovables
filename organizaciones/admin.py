from organizaciones.models import Contacto,Organizacion,Email,SistemaEjecucion,Responsable,Beneficiarios,Empleados,UsuariosBeneficiarios,GaleriaFotosSistema,Proyecto,GaleriaFotosProyecto,BeneficiariosProyecto,EmailOrganizacion
#from django.contrib import admin
from django.contrib.gis import admin
from sorl.thumbnail.admin import AdminImageMixin
from gmap_admin.fields import GeoPt
from organizaciones.forms import GMapPolygonWidget
from renovable.forms import AdminGmapPolygonWidget
from django.contrib.gis.db.models import PolygonField
from django import forms
import floppyforms as gisforms

class emailInline(admin.TabularInline ):
    model= Email
    extra= 1

class EmailOrganizacionInline(admin.TabularInline ):
    model= EmailOrganizacion
    extra= 1

class ResponsableInline(admin.TabularInline ):
    model= Responsable
    extra= 1

class BeneficiariosInline(admin.TabularInline ):
    model= Beneficiarios
    extra= 1

class EmpleadosInline(admin.TabularInline ):
    model= Empleados
    extra= 1

class ContactoAdmin(admin.ModelAdmin):
    inlines = [emailInline]
    list_display = ('nombre', 'organizacion', )
    list_filter = ('organizacion__nombre','categoria__nombre','area')
    search_fields = ['nombre']

    def queryset(self, request):
        exclude = ['organizacion',]
        qs = super(ContactoAdmin, self).queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(organizacion__user=request.user)

    def change_view(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            self.exclude=('organizacion', )
        return super(ContactoAdmin, self).change_view(request,*args, **kwargs)

    def add_view(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            self.exclude=('organizacion', )
        return super(ContactoAdmin, self).add_view(request,*args, **kwargs)

    def save_model(self, request, obj, form, change):
        if not request.user.is_superuser:
            obj.organizacion= Organizacion.objects.get(user=request.user)
        super(ContactoAdmin,self).save_model(request, obj, form, change)


class UsuariosBeneficiariosInline(admin.TabularInline ):
    model= UsuariosBeneficiarios
    extra= 1

class GaleriaFotosSistemaInline(AdminImageMixin,admin.TabularInline ):
    model= GaleriaFotosSistema
    extra= 1

class SistemaEjecucionAdmin(admin.ModelAdmin):
    search_fields = ['nombre',]
    inlines = [UsuariosBeneficiariosInline,GaleriaFotosSistemaInline]
    list_filter = ('fuente','organizacion__nombre',)
    
    def queryset(self, request):
        exclude = ['organizacion',]
        qs = super(SistemaEjecucionAdmin, self).queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(organizacion__user=request.user)

    def change_view(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            self.exclude=('organizacion', )
        return super(SistemaEjecucionAdmin, self).change_view(request,*args, **kwargs)

    def add_view(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            self.exclude=('organizacion', )
        return super(SistemaEjecucionAdmin, self).add_view(request,*args, **kwargs)

    def save_model(self, request, obj, form, change):
        if not request.user.is_superuser:
            obj.organizacion= Organizacion.objects.get(pk=request.user.id)
        if obj.geolocation:
            obj.latitud=obj.geolocation.lat
            obj.longitud=obj.geolocation.lon
        else:
            if obj.latitud and obj.longitud:
                obj.geolocation= GeoPt(obj.latitud,obj.longitud)
        obj.save()

class BeneficiariosProyectosInline(admin.TabularInline ):
    model= BeneficiariosProyecto
    extra= 1

class GaleriaFotosProyectoInline(AdminImageMixin,admin.TabularInline ):
    model= GaleriaFotosProyecto
    extra= 1

class ProyectoAdminForm(forms.ModelForm):
    poligono= gisforms.gis.PolygonField(widget=AdminGmapPolygonWidget,required=False)
    class Meta:
        model= Proyecto

class ProyectoAdmin(admin.GeoModelAdmin):
    form = ProyectoAdminForm
    inlines = [BeneficiariosProyectosInline,GaleriaFotosProyectoInline]
    list_display = ['nombre','organizacion']
    search_fields = ['nombre','organizacion__nombre']
    list_filter = ['departamento']
   

    def queryset(self, request):
        exclude = ['organizacion',]
        qs = super(ProyectoAdmin, self).queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(organizacion__user=request.user)

    def change_view(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            self.exclude=('organizacion', )
        return super(ProyectoAdmin, self).change_view(request,*args, **kwargs)

    def add_view(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            self.exclude=('organizacion', )
        return super(ProyectoAdmin, self).add_view(request,*args, **kwargs)

    def save_model(self, request, obj, form, change):
        if not request.user.is_superuser:
            obj.organizacion= Organizacion.objects.get(user=request.user)
        if obj.geolocation and obj.geolocation!="":
            obj.latitud=obj.geolocation.lat
            obj.longitud=obj.geolocation.lon
        else:
            if obj.latitud and obj.longitud:
                obj.geolocation= GeoPt(obj.latitud,obj.longitud)
        super(ProyectoAdmin,self).save_model(request, obj, form, change)



class OrganizacionAdmin(AdminImageMixin,admin.ModelAdmin):
    inlines = (
        EmailOrganizacionInline,
        ResponsableInline,
        BeneficiariosInline,
        EmpleadosInline
    )
    search_fields = ['nombre']
    list_filter = ['renovable','tipo']
 

    def queryset(self, request):
        qs = super(OrganizacionAdmin, self).queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(user=request.user)

    def change_view(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            self.exclude=('user', )
        return super(OrganizacionAdmin, self).change_view(request,*args, **kwargs)

    def add_view(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            self.exclude=('user', )
        return super(OrganizacionAdmin, self).add_view(request,*args, **kwargs)

    def save_model(self, request, obj, form, change):
        if obj.geolocation:
            obj.latitud=obj.geolocation.lat
            obj.longitud=obj.geolocation.lon
        else:
            if obj.latitud and obj.longitud:
                obj.geolocation= GeoPt(obj.latitud,obj.longitud)

        if not request.user.is_superuser:
            obj.user= request.user
        super(OrganizacionAdmin,self).save_model(request, obj, form, change)



admin.site.register(Organizacion,OrganizacionAdmin)
admin.site.register(Contacto,ContactoAdmin)
admin.site.register(SistemaEjecucion,SistemaEjecucionAdmin)
admin.site.register(Proyecto,ProyectoAdmin)



