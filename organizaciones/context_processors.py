from django.conf import settings

def default_geolocation(context):
    return {'DEFAULT_GEOLOCATION': settings.GOOGLE_POSITION}
