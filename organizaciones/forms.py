# forms.py
import floppyforms as forms

class BaseGMapWidget(forms.gis.BaseGeometryWidget):
    """A Google Maps base widget"""
    #map_srid = 900913
    map_srid = 3857
    #map_srid = 4326
    template_name = 'floppyforms/gis/custom_gmap.html'
    display_wkt= False

    class Media:
        js = (
            'http://openlayers.org/api/OpenLayers.js',  # FIXME: use 2.11
            #'http://dev.openlayers.org/releases/OpenLayers-2.11/lib/OpenLayers.js',
            'floppyforms/js/MapWidget.js',
            'http://maps.google.com/maps/api/js?v=3.6&sensor=false',
        )

class GMapPolygonWidget(forms.gis.PolygonWidget, BaseGMapWidget):
    pass

