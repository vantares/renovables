# -*- coding: UTF-8 -*-
#from django.db import models
from django.contrib.gis.db import models
from smart_selects.db_fields import ChainedForeignKey
from nomencladores.models import TipoEntidad, Vegetal, Categoria, FuenteRenovable, MateriaPrima, Pais, Actividad, Departamento
from django.conf import settings
from django.contrib.auth.models import User
from sorl.thumbnail import ImageField
from django.db.models import Q
from django.contrib.auth.models import User
from gmap_admin.fields import GeoLocationField
from django.core.exceptions import ValidationError
from sorl.thumbnail import get_thumbnail
from django.db.models.signals import post_save
from indicadores import utils

class Organizacion(models.Model):
    user=models.ForeignKey(User,verbose_name="Usuario",unique=True)
    tipo=models.ForeignKey(TipoEntidad, blank=True, null=True)
    nombre = models.CharField(max_length=400,verbose_name='Nombre de la Organización',unique=True)
    razon = models.CharField(max_length=400,verbose_name='Razón Social',blank=True)
    fuente = models.ManyToManyField(FuenteRenovable,verbose_name="Fuentes Renovables")
    actividad=models.ManyToManyField(Actividad,blank=True, null=True)
    nic=models.BooleanField(verbose_name='Es consesionario  en Nicaragua?')
    renovable=models.BooleanField(verbose_name='Es miembro  en Renovables?')
    gremial=models.BooleanField(verbose_name='Es miembro de asociación GREMIAL')
    asociacion=models.CharField(max_length=400,verbose_name='Otra asociación',blank=True, null=True)
    telefono=models.CharField(max_length=400,verbose_name='Teléfono',blank=True, null=True)
    web=models.URLField(verbose_name='Web',blank=True, null=True)
    web2=models.URLField(verbose_name='Web adicional',blank=True, null=True)
    direccion=models.CharField(max_length=400,verbose_name='Dirección',blank=True, null=True)
    pais=models.CharField(max_length=400,verbose_name='Pais',blank=True, null=True)
    longitud = models.DecimalField(max_digits=15,decimal_places=10,blank=True, null=True,verbose_name='Longitud')
    latitud = models.DecimalField(max_digits=15,decimal_places=10,blank=True, null=True, verbose_name='Latitud')
    logo = ImageField(upload_to='organizacion',blank=True,null=True)
    geolocation = GeoLocationField(max_length=100,blank=True,null=True,verbose_name='Geolocalización')
    departamento = models.ForeignKey('nomencladores.Departamento', blank=True, null=True, on_delete=models.SET_NULL)

    class Meta:
        verbose_name = "Organización"
        verbose_name_plural = "Organizaciones"
        ordering = ['nombre','id']

    def __unicode__(self):
        return unicode(self.nombre)

    @models.permalink
    def get_absolute_url(self):
        return ('organizaciones_organizacion_detail',(),{
                         'pk': self.pk
                                 })


    @property
    def popupimage(self):
        try:
            imagen = self.logo
            imagen = get_thumbnail(imagen,"140x100")
            return imagen.url
        except Exception as e:
            imagen = ''
        return imagen

    def get_sistemas(self):
        sistemas=SistemaEjecucion.objects.filter(organizacion=self)
        if sistemas.count()>0:
            return sistemas
        else:
            return False

    def get_email(self):
        emails=EmailOrganizacion.objects.filter(organizacion=self)
        if emails.count()>0:
            return emails
        else:
            return False

    def get_proyectos(self):
        proyectos=Proyecto.objects.filter(organizacion=self)
        if proyectos.count()>0:
            return proyectos
        else:
            return False

    def get_contactos(self):
        responsable=self.get_responsable()
        if responsable:
            contactos=Contacto.objects.filter(organizacion=self).exclude(id=responsable.contacto.id)
            if contactos.count()>0:
                return contactos
            else:
                return False
        else:
            contactos=Contacto.objects.filter(organizacion=self)
            if contactos.count()>0:
                return contactos
            else:
                return False
    def get_responsable(self):
        try:
            responsable=Responsable.objects.get(organizacion=self)
            return responsable
        except:
            return False

    def get_lat(self):
        latitud= settings.GOOGLE_POSITION['latitude']
        if(self.latitud):
            latitud= self.latitud
        else:
            if(self.poligono):
                latitud= self.poligono.centroid.y
            else:
                if(self.departamento):
                    latitud= self.departamento.latitud
        return latitud

    def get_lan(self):
        longitud= settings.GOOGLE_POSITION['longitude']
        if(self.longitud):
            longitud= self.longitud
        else:
            if(self.poligono):
                longitud= self.poligono.centroid.x
            else:
                if(self.departamento):
                    longitud= self.departamento.longitud
        return longitud


class EmailOrganizacion(models.Model):
    organizacion=models.ForeignKey(Organizacion)
    email = models.EmailField()

    def __unicode__(self):
        return unicode(self.email)

    class Meta:
        verbose_name = "Email Organización"
        verbose_name_plural = "Email Organización"

class Empleados(models.Model):
    INDICADOR_ID = settings.EMPLEOS_GENERADOS
    organizacion=models.ForeignKey(Organizacion)
    anho = models.IntegerField(verbose_name="Año",max_length=4, help_text="Ejemplo: 2011, 2010, etc.")
    numero=models.IntegerField(verbose_name="Cantidad de Empleados")

    class Meta:
        verbose_name = "Empleados"
        verbose_name_plural = "Empleados"

    def __unicode__(self):
        return unicode(self.anho)

class Contacto(models.Model):
    organizacion=models.ForeignKey(Organizacion,blank=True,null=True)
    organizacion.custom_filter_spec = True
    categoria=models.ForeignKey(Categoria, blank=True, null=True)
    nombre = models.CharField(max_length=200, blank=False, verbose_name='Nombre')
    cargo = models.CharField(max_length=200,verbose_name='Cargo', blank=True, null=True)
    skype = models.CharField(max_length=200,verbose_name='Skype', blank=True, null=True)
    web = models.URLField(verbose_name='Web', blank=True, null=True)
    direccion = models.CharField(max_length=200,verbose_name='Dirección', blank=True, null=True)
    telefono = models.CharField(max_length=300,verbose_name='Teléfonos',blank=True,null=True)
    area = models.CharField(max_length=200,verbose_name='Área de Trabajo', blank=True, null=True)
    pais=models.ForeignKey(Pais, blank=True, null=True)

    class Meta:
        verbose_name = "Contacto"
        verbose_name_plural = "Contactos"
        ordering = ('nombre','id')

    def __unicode__(self):
        return unicode(self.nombre)

    @models.permalink
    def get_absolute_url(self):
        return ('organizaciones_contacto_detail',(),{
                         'pk': self.pk
                                 })


    def get_mail(self):
        mails=Email.objects.filter(contacto=self)
        if mails.count()>0:
            return mails[0]
        else:
            return False

    def get_mails(self):
        mails=Email.objects.filter(contacto=self)
        if mails.count()>0:
            return mails
        else:
            return False

class Responsable(models.Model):
    organizacion=models.ForeignKey(Organizacion,primary_key=True)
    contacto= ChainedForeignKey(
        Contacto,
        chained_field="organizacion",
        chained_model_field="organizacion",
        show_all=False,
        auto_choose=True,
    )

    class Meta:
        verbose_name = "Responsable"
        verbose_name_plural = "Responsables"

    def __unicode__(self):
        return unicode(self.organizacion)

    def get_mail(self):
        mails=Email.objects.filter(contacto=self)
        if mails.count()>0:
            return mails[0]
        else:
            return False

class Email(models.Model):
    contacto=models.ForeignKey(Contacto)
    email = models.EmailField()

    def __unicode__(self):
        return unicode(self.email)


class Proyecto(models.Model):
    nombre = models.CharField(max_length=200,verbose_name='Nombre del proyecto')
    organizacion=models.ForeignKey(Organizacion)
    monto=models.IntegerField(verbose_name='Monto total en US$')
    fuente = models.ManyToManyField(FuenteRenovable,verbose_name="Fuentes Renovables")
    longitud = models.DecimalField(max_digits=15,decimal_places=10,blank=True, null=True,verbose_name='Longitud')
    latitud = models.DecimalField(max_digits=15,decimal_places=10,blank=True, null=True, verbose_name='Latitud')
    enlace=models.URLField(verbose_name='Página Web',blank=True, null=True)
    donante = models.CharField(max_length=400,verbose_name='Donante',blank=True, null=True)
    geolocation = GeoLocationField(max_length=100,blank=True,null=True,verbose_name='Geolocalización')
    poligono= models.PolygonField(null=True,blank=True)
    departamento = models.ForeignKey('nomencladores.Departamento', blank=True, null=True, on_delete=models.SET_NULL)

    class Meta:
        verbose_name = "Proyecto"
        verbose_name_plural = "Proyectos"
        ordering = ['nombre','id']

    def __unicode__(self):
        return unicode(self.nombre)


    @models.permalink
    def get_absolute_url(self):
        return ('organizaciones_proyecto_detail',(),{
                         'pk': self.pk
                                 })


    def clean(self):
        if self.poligono is None:
            raise ValidationError('Por favor seleccione el poligono donde esta ubicado el proyecto')

    def get_imagen(self):
        imagen=GaleriaFotosProyecto.objects.filter(proyecto=self)[:1]
        if imagen.count()>0:
            return imagen[0].imagen
        else:
            return False

    @property
    def popupimage(self):
        try:
            imagen = GaleriaFotosProyecto.objects.filter(proyecto=self)[:1][0]
            imagen = get_thumbnail(imagen,"140x100")
            return imagen.url
        except:
            imagen = ''
        return imagen

    def get_beneficiarios(self):
        beneficiario=BeneficiariosProyecto.objects.filter(proyecto=self)
        if beneficiario.count()>0:
            return beneficiario
        else:
            return False

    def get_lat(self):
        latitud= settings.GOOGLE_POSITION['latitude']
        if(self.latitud):
            latitud= self.latitud
        else:
            if(self.poligono):
                latitud= self.poligono.centroid.y
            else:
                if(self.departamento):
                    latitud= self.departamento.latitud
        return latitud

    def get_lan(self):
        longitud= settings.GOOGLE_POSITION['longitude']
        if(self.longitud):
            longitud= self.longitud
        else:
            if(self.poligono):
                longitud= self.poligono.centroid.x
            else:
                if(self.departamento):
                    longitud= self.departamento.longitud
        return longitud

class GaleriaFotosProyecto(models.Model):
    proyecto=models.ForeignKey(Proyecto)
    imagen = ImageField(upload_to='proyecto')

    class Meta:
        verbose_name = "Imagen"
        verbose_name_plural = "Galeria de Imagenes del Proyecto"

    def __unicode__(self):
        return unicode(self.imagen)

class SistemaEjecucion(models.Model):
    nombre = models.CharField(max_length=200,verbose_name='Identificador del sistema ')
    organizacion=models.ForeignKey(Organizacion,null=True,blank=True,verbose_name='Organización')
    #instancias = models.IntegerField(verbose_name='Cantidad de instancias en este sistema ')
    fuente = models.ManyToManyField(FuenteRenovable,verbose_name="Fuentes Renovables", related_name="sistemas")
    vegetal = models.ForeignKey(Vegetal,null=True,blank=True,verbose_name='Vegetal que usa')
    materia_prima=models.ForeignKey(MateriaPrima,null=True,blank=True,verbose_name='Materia Prima')
    biocombustible=models.BooleanField(verbose_name='Produce Biocombustible')
    longitud = models.DecimalField(max_digits=15,decimal_places=10,blank=True, null=True,verbose_name='Longitud')
    latitud = models.DecimalField(max_digits=15,decimal_places=10,blank=True, null=True, verbose_name='Latitud')
    potencia = models.CharField(max_length=200,null=True,blank=True, verbose_name="Potencia en W")
    interconexion=models.BooleanField(verbose_name='Interconexión a SIN')
    energia_producida=models.IntegerField(verbose_name='Energia producida anualmente en MWh',default=0)
    cantidad_sistemas= models.IntegerField(verbose_name='Cantidad de sistemas instalados', null=True, blank= True)
    tarifa=models.IntegerField(verbose_name='Tarifa promedio anual por $/MWh',default=0)
    calor=models.BooleanField(verbose_name='Produce Calor')
    geolocation = GeoLocationField(max_length=100,blank=True,null=True,verbose_name='Geolocalización')
    departamento = models.ForeignKey('nomencladores.Departamento', blank=True, null=True, on_delete=models.SET_NULL)

    class Meta:
        verbose_name = "Sistemas de Generación"
        verbose_name_plural = "Sistemas de Generación"
        ordering = ['nombre','id']

    def __unicode__(self):
        return unicode(self.nombre)


    @models.permalink
    def get_absolute_url(self):
        return ('organizaciones_sistemaejecucion_detail',(),{
                         'pk': self.pk
                                 })


    def get_imagen(self):
        imagen=GaleriaFotosSistema.objects.filter(sistema=self)[:1]
        if imagen.count()>0:
            return imagen[0].imagen
        else:
            return False

    @property
    def popupimage(self):
        try:
            imagen = GaleriaFotosSistema.objects.filter(sistema=self)[:1][0]
            imagen = get_thumbnail(imagen,"140x100")
            return imagen.url
        except:
            imagen = ''
        return imagen

    def get_lat(self):
        latitud= settings.GOOGLE_POSITION['latitude']
        if(self.latitud):
            latitud= self.latitud
        else:
            if(self.departamento):
                latitud= self.departamento.latitud
        return latitud

    def get_lan(self):
        longitud= settings.GOOGLE_POSITION['longitude']
        if(self.longitud):
            longitud= self.longitud
        else:
            if(self.departamento):
                longitud= self.departamento.longitud
        return longitud

class GaleriaFotosSistema(models.Model):
    sistema=models.ForeignKey(SistemaEjecucion)
    imagen = ImageField(upload_to='sistemas')

    class Meta:
        verbose_name = "Imagen"
        verbose_name_plural = "Galeria de Imagenes"

    def __unicode__(self):
        return unicode(self.imagen)

##########################
#Beneficiarios
#########################
class BaseBeneficiario(models.Model):
    INDICADOR_ID = settings.POBLACION_BENEFICIADA
    anho = models.IntegerField(verbose_name=u"Año", max_length=4, help_text="Ejemplo: 2011, 2010, etc.")
    numero=models.IntegerField(verbose_name="Cantidad de Usuarios Beneficiados")

    class Meta:
        abstract = True
        verbose_name = "Beneficiarios"
        verbose_name_plural = "Beneficiarios"

    def __unicode__(self):
        return unicode(self.anho)

class UsuariosBeneficiarios(BaseBeneficiario):
    sistema=models.ForeignKey(SistemaEjecucion)

    class Meta:
        verbose_name = "Usuarios Beneficiados"
        verbose_name_plural = "Usuarios Beneficiados"

class Beneficiarios(BaseBeneficiario):
    organizacion=models.ForeignKey(Organizacion)

class BeneficiariosProyecto(BaseBeneficiario):
    proyecto=models.ForeignKey(Proyecto)


post_save.connect(utils.actualizar_indicador, Empleados,dispatch_uid='actualiar_empleos')
post_save.connect(utils.actualizar_indicador, UsuariosBeneficiarios,dispatch_uid='actualiar_poblacion')
