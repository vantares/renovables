from django.http import HttpResponse
from django.utils import simplejson
from django.core import serializers
from organizaciones.models import Organizacion, Proyecto
import json

def Organizacionjson(request):
    title = 'Organizaciones'
    data = Organizacion.objects.all().order_by('nombre')
    return HttpResponse(json.dumps(data),content_type='application.json')

def MiembrosRenovablesJSON(request):
    title = 'Organizaciones'
    data = Organizacion.objects.filter(renovable=True).order_by('nombre')
    json_data= serializers.serialize('json', data, fields=('id','nombre','asociacion','telefono','web','direccion','longitud','latitud'), ensure_ascii=False)
    return HttpResponse("var miembros= "+json_data,content_type = 'application/javascript; charset=utf8')
    
def ProyectosRenovablesJSON(request):
    title = 'Proyectos'
    proyectos = Proyecto.objects.all()
    data= []
    latitud = 12.865416
    longitud = -85.207229
    for proyecto in proyectos:
        if(proyecto.latitud and proyecto.longitud ):
            latitud = proyecto.latitud
            longitud = proyecto.longitud
        else:
            if(proyecto.poligono):
                latitud = proyecto.poligono.centroid.x
                longitud = proyecto.poligono.centroid.y
            else:
                if(proyecto.departamento):
                    latitud = proyecto.departamento.latitud
                    longitud = proyecto.departamento.longitud
                
        row = {'pk': proyecto.pk, 'fields': {'id': proyecto.pk, 'nombre': proyecto.nombre, 'donante': proyecto.donante, 'monto': proyecto.monto, 'latitud': latitud, 'longitud': longitud}}
        data.append(row)
    json_data= simplejson.dumps(data, use_decimal=True)
    #json_data= serializers.serialize('json', data, fields=('id','nombre','organizacion','monto','donante','fuente','departamento','longitud','latitud'), ensure_ascii=False)
    return HttpResponse("var proyectos= "+json_data,content_type = 'application/javascript; charset=utf8')
