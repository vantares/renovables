# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Organizacion'
        db.create_table('organizaciones_organizacion', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], unique=True)),
            ('tipo', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['nomencladores.TipoEntidad'], null=True, blank=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(unique=True, max_length=400)),
            ('razon', self.gf('django.db.models.fields.CharField')(max_length=400, blank=True)),
            ('nic', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('renovable', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('gremial', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('asociacion', self.gf('django.db.models.fields.CharField')(max_length=400, null=True, blank=True)),
            ('telefono', self.gf('django.db.models.fields.CharField')(max_length=400, null=True, blank=True)),
            ('web', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('web2', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('direccion', self.gf('django.db.models.fields.CharField')(max_length=400, null=True, blank=True)),
            ('pais', self.gf('django.db.models.fields.CharField')(max_length=400, null=True, blank=True)),
            ('longitud', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=15, decimal_places=10, blank=True)),
            ('latitud', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=15, decimal_places=10, blank=True)),
            ('logo', self.gf('sorl.thumbnail.fields.ImageField')(max_length=100, null=True, blank=True)),
            ('geolocation', self.gf('gmap_admin.fields.GeoLocationField')(max_length=100, null=True, blank=True)),
        ))
        db.send_create_signal('organizaciones', ['Organizacion'])

        # Adding M2M table for field fuente on 'Organizacion'
        db.create_table('organizaciones_organizacion_fuente', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('organizacion', models.ForeignKey(orm['organizaciones.organizacion'], null=False)),
            ('fuenterenovable', models.ForeignKey(orm['nomencladores.fuenterenovable'], null=False))
        ))
        db.create_unique('organizaciones_organizacion_fuente', ['organizacion_id', 'fuenterenovable_id'])

        # Adding M2M table for field actividad on 'Organizacion'
        db.create_table('organizaciones_organizacion_actividad', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('organizacion', models.ForeignKey(orm['organizaciones.organizacion'], null=False)),
            ('actividad', models.ForeignKey(orm['nomencladores.actividad'], null=False))
        ))
        db.create_unique('organizaciones_organizacion_actividad', ['organizacion_id', 'actividad_id'])

        # Adding model 'EmailOrganizacion'
        db.create_table('organizaciones_emailorganizacion', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('organizacion', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['organizaciones.Organizacion'])),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
        ))
        db.send_create_signal('organizaciones', ['EmailOrganizacion'])

        # Adding model 'Beneficiarios'
        db.create_table('organizaciones_beneficiarios', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('organizacion', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['organizaciones.Organizacion'])),
            ('anho', self.gf('django.db.models.fields.IntegerField')(max_length=4)),
            ('numero', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('organizaciones', ['Beneficiarios'])

        # Adding model 'Empleados'
        db.create_table('organizaciones_empleados', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('organizacion', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['organizaciones.Organizacion'])),
            ('anho', self.gf('django.db.models.fields.IntegerField')(max_length=4)),
            ('numero', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('organizaciones', ['Empleados'])

        # Adding model 'Contacto'
        db.create_table('organizaciones_contacto', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('organizacion', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['organizaciones.Organizacion'], null=True, blank=True)),
            ('categoria', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['nomencladores.Categoria'], null=True, blank=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('cargo', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('skype', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('web', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('direccion', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('telefono', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('area', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('pais', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['nomencladores.Pais'], null=True, blank=True)),
        ))
        db.send_create_signal('organizaciones', ['Contacto'])

        # Adding model 'Responsable'
        db.create_table('organizaciones_responsable', (
            ('organizacion', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['organizaciones.Organizacion'], primary_key=True)),
            ('contacto', self.gf('smart_selects.db_fields.ChainedForeignKey')(to=orm['organizaciones.Contacto'])),
        ))
        db.send_create_signal('organizaciones', ['Responsable'])

        # Adding model 'Email'
        db.create_table('organizaciones_email', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('contacto', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['organizaciones.Contacto'])),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
        ))
        db.send_create_signal('organizaciones', ['Email'])

        # Adding model 'Proyecto'
        db.create_table('organizaciones_proyecto', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('organizacion', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['organizaciones.Organizacion'])),
            ('monto', self.gf('django.db.models.fields.IntegerField')()),
            ('longitud', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=15, decimal_places=10, blank=True)),
            ('latitud', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=15, decimal_places=10, blank=True)),
            ('enlace', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('donante', self.gf('django.db.models.fields.CharField')(max_length=400, null=True, blank=True)),
            ('geolocation', self.gf('gmap_admin.fields.GeoLocationField')(max_length=100, null=True, blank=True)),
            ('poligono', self.gf('django.contrib.gis.db.models.fields.PolygonField')(null=True, blank=True)),
            ('departamento', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['nomencladores.Departamento'], null=True, on_delete=models.SET_NULL, blank=True)),
        ))
        db.send_create_signal('organizaciones', ['Proyecto'])

        # Adding M2M table for field fuente on 'Proyecto'
        db.create_table('organizaciones_proyecto_fuente', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('proyecto', models.ForeignKey(orm['organizaciones.proyecto'], null=False)),
            ('fuenterenovable', models.ForeignKey(orm['nomencladores.fuenterenovable'], null=False))
        ))
        db.create_unique('organizaciones_proyecto_fuente', ['proyecto_id', 'fuenterenovable_id'])

        # Adding model 'GaleriaFotosProyecto'
        db.create_table('organizaciones_galeriafotosproyecto', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('proyecto', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['organizaciones.Proyecto'])),
            ('imagen', self.gf('sorl.thumbnail.fields.ImageField')(max_length=100)),
        ))
        db.send_create_signal('organizaciones', ['GaleriaFotosProyecto'])

        # Adding model 'BeneficiariosProyecto'
        db.create_table('organizaciones_beneficiariosproyecto', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('proyecto', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['organizaciones.Proyecto'])),
            ('anho', self.gf('django.db.models.fields.IntegerField')(unique=True, max_length=4)),
            ('numero', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('organizaciones', ['BeneficiariosProyecto'])

        # Adding model 'SistemaEjecucion'
        db.create_table('organizaciones_sistemaejecucion', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('organizacion', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['organizaciones.Organizacion'], null=True, blank=True)),
            ('vegetal', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['nomencladores.Vegetal'], null=True, blank=True)),
            ('materia_prima', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['nomencladores.MateriaPrima'], null=True, blank=True)),
            ('biocombustible', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('longitud', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=15, decimal_places=10, blank=True)),
            ('latitud', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=15, decimal_places=10, blank=True)),
            ('potencia', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('interconexion', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('energia_producida', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('cantidad_sistemas', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('tarifa', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('calor', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('geolocation', self.gf('gmap_admin.fields.GeoLocationField')(max_length=100, null=True, blank=True)),
            ('departamento', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['nomencladores.Departamento'], null=True, on_delete=models.SET_NULL, blank=True)),
        ))
        db.send_create_signal('organizaciones', ['SistemaEjecucion'])

        # Adding M2M table for field fuente on 'SistemaEjecucion'
        db.create_table('organizaciones_sistemaejecucion_fuente', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('sistemaejecucion', models.ForeignKey(orm['organizaciones.sistemaejecucion'], null=False)),
            ('fuenterenovable', models.ForeignKey(orm['nomencladores.fuenterenovable'], null=False))
        ))
        db.create_unique('organizaciones_sistemaejecucion_fuente', ['sistemaejecucion_id', 'fuenterenovable_id'])

        # Adding model 'GaleriaFotosSistema'
        db.create_table('organizaciones_galeriafotossistema', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('sistema', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['organizaciones.SistemaEjecucion'])),
            ('imagen', self.gf('sorl.thumbnail.fields.ImageField')(max_length=100)),
        ))
        db.send_create_signal('organizaciones', ['GaleriaFotosSistema'])

        # Adding model 'UsuariosBeneficiarios'
        db.create_table('organizaciones_usuariosbeneficiarios', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('sistema', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['organizaciones.SistemaEjecucion'])),
            ('anho', self.gf('django.db.models.fields.IntegerField')(max_length=4)),
            ('numero', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('organizaciones', ['UsuariosBeneficiarios'])


    def backwards(self, orm):
        # Deleting model 'Organizacion'
        db.delete_table('organizaciones_organizacion')

        # Removing M2M table for field fuente on 'Organizacion'
        db.delete_table('organizaciones_organizacion_fuente')

        # Removing M2M table for field actividad on 'Organizacion'
        db.delete_table('organizaciones_organizacion_actividad')

        # Deleting model 'EmailOrganizacion'
        db.delete_table('organizaciones_emailorganizacion')

        # Deleting model 'Beneficiarios'
        db.delete_table('organizaciones_beneficiarios')

        # Deleting model 'Empleados'
        db.delete_table('organizaciones_empleados')

        # Deleting model 'Contacto'
        db.delete_table('organizaciones_contacto')

        # Deleting model 'Responsable'
        db.delete_table('organizaciones_responsable')

        # Deleting model 'Email'
        db.delete_table('organizaciones_email')

        # Deleting model 'Proyecto'
        db.delete_table('organizaciones_proyecto')

        # Removing M2M table for field fuente on 'Proyecto'
        db.delete_table('organizaciones_proyecto_fuente')

        # Deleting model 'GaleriaFotosProyecto'
        db.delete_table('organizaciones_galeriafotosproyecto')

        # Deleting model 'BeneficiariosProyecto'
        db.delete_table('organizaciones_beneficiariosproyecto')

        # Deleting model 'SistemaEjecucion'
        db.delete_table('organizaciones_sistemaejecucion')

        # Removing M2M table for field fuente on 'SistemaEjecucion'
        db.delete_table('organizaciones_sistemaejecucion_fuente')

        # Deleting model 'GaleriaFotosSistema'
        db.delete_table('organizaciones_galeriafotossistema')

        # Deleting model 'UsuariosBeneficiarios'
        db.delete_table('organizaciones_usuariosbeneficiarios')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'nomencladores.actividad': {
            'Meta': {'object_name': 'Actividad'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'nomencladores.categoria': {
            'Meta': {'object_name': 'Categoria'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'nomencladores.departamento': {
            'Meta': {'ordering': "['nombre']", 'object_name': 'Departamento', 'db_table': "'nomencladores_depto'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latitud': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '15', 'decimal_places': '10', 'blank': 'True'}),
            'layerkml': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'longitud': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '15', 'decimal_places': '10', 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'nomencladores.fuenterenovable': {
            'Meta': {'object_name': 'FuenteRenovable'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imagen': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'nomencladores.materiaprima': {
            'Meta': {'object_name': 'MateriaPrima'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'nomencladores.pais': {
            'Meta': {'object_name': 'Pais'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'nomencladores.tipoentidad': {
            'Meta': {'object_name': 'TipoEntidad'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'nomencladores.vegetal': {
            'Meta': {'object_name': 'Vegetal'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'organizaciones.beneficiarios': {
            'Meta': {'object_name': 'Beneficiarios'},
            'anho': ('django.db.models.fields.IntegerField', [], {'max_length': '4'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'numero': ('django.db.models.fields.IntegerField', [], {}),
            'organizacion': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['organizaciones.Organizacion']"})
        },
        'organizaciones.beneficiariosproyecto': {
            'Meta': {'object_name': 'BeneficiariosProyecto'},
            'anho': ('django.db.models.fields.IntegerField', [], {'unique': 'True', 'max_length': '4'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'numero': ('django.db.models.fields.IntegerField', [], {}),
            'proyecto': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['organizaciones.Proyecto']"})
        },
        'organizaciones.contacto': {
            'Meta': {'ordering': "('nombre',)", 'object_name': 'Contacto'},
            'area': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'cargo': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'categoria': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['nomencladores.Categoria']", 'null': 'True', 'blank': 'True'}),
            'direccion': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'organizacion': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['organizaciones.Organizacion']", 'null': 'True', 'blank': 'True'}),
            'pais': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['nomencladores.Pais']", 'null': 'True', 'blank': 'True'}),
            'skype': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'web': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        'organizaciones.email': {
            'Meta': {'object_name': 'Email'},
            'contacto': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['organizaciones.Contacto']"}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'organizaciones.emailorganizacion': {
            'Meta': {'object_name': 'EmailOrganizacion'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'organizacion': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['organizaciones.Organizacion']"})
        },
        'organizaciones.empleados': {
            'Meta': {'object_name': 'Empleados'},
            'anho': ('django.db.models.fields.IntegerField', [], {'max_length': '4'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'numero': ('django.db.models.fields.IntegerField', [], {}),
            'organizacion': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['organizaciones.Organizacion']"})
        },
        'organizaciones.galeriafotosproyecto': {
            'Meta': {'object_name': 'GaleriaFotosProyecto'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imagen': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'proyecto': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['organizaciones.Proyecto']"})
        },
        'organizaciones.galeriafotossistema': {
            'Meta': {'object_name': 'GaleriaFotosSistema'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imagen': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'sistema': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['organizaciones.SistemaEjecucion']"})
        },
        'organizaciones.organizacion': {
            'Meta': {'ordering': "['nombre']", 'object_name': 'Organizacion'},
            'actividad': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['nomencladores.Actividad']", 'null': 'True', 'blank': 'True'}),
            'asociacion': ('django.db.models.fields.CharField', [], {'max_length': '400', 'null': 'True', 'blank': 'True'}),
            'direccion': ('django.db.models.fields.CharField', [], {'max_length': '400', 'null': 'True', 'blank': 'True'}),
            'fuente': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['nomencladores.FuenteRenovable']", 'symmetrical': 'False'}),
            'geolocation': ('gmap_admin.fields.GeoLocationField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'gremial': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latitud': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '15', 'decimal_places': '10', 'blank': 'True'}),
            'logo': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'longitud': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '15', 'decimal_places': '10', 'blank': 'True'}),
            'nic': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '400'}),
            'pais': ('django.db.models.fields.CharField', [], {'max_length': '400', 'null': 'True', 'blank': 'True'}),
            'razon': ('django.db.models.fields.CharField', [], {'max_length': '400', 'blank': 'True'}),
            'renovable': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '400', 'null': 'True', 'blank': 'True'}),
            'tipo': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['nomencladores.TipoEntidad']", 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'unique': 'True'}),
            'web': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'web2': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        'organizaciones.proyecto': {
            'Meta': {'ordering': "['nombre']", 'object_name': 'Proyecto'},
            'departamento': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['nomencladores.Departamento']", 'null': 'True', 'on_delete': 'models.SET_NULL', 'blank': 'True'}),
            'donante': ('django.db.models.fields.CharField', [], {'max_length': '400', 'null': 'True', 'blank': 'True'}),
            'enlace': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'fuente': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['nomencladores.FuenteRenovable']", 'symmetrical': 'False'}),
            'geolocation': ('gmap_admin.fields.GeoLocationField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latitud': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '15', 'decimal_places': '10', 'blank': 'True'}),
            'longitud': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '15', 'decimal_places': '10', 'blank': 'True'}),
            'monto': ('django.db.models.fields.IntegerField', [], {}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'organizacion': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['organizaciones.Organizacion']"}),
            'poligono': ('django.contrib.gis.db.models.fields.PolygonField', [], {'null': 'True', 'blank': 'True'})
        },
        'organizaciones.responsable': {
            'Meta': {'object_name': 'Responsable'},
            'contacto': ('smart_selects.db_fields.ChainedForeignKey', [], {'to': "orm['organizaciones.Contacto']"}),
            'organizacion': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['organizaciones.Organizacion']", 'primary_key': 'True'})
        },
        'organizaciones.sistemaejecucion': {
            'Meta': {'ordering': "['nombre']", 'object_name': 'SistemaEjecucion'},
            'biocombustible': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'calor': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'cantidad_sistemas': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'departamento': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['nomencladores.Departamento']", 'null': 'True', 'on_delete': 'models.SET_NULL', 'blank': 'True'}),
            'energia_producida': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'fuente': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'sistemas'", 'symmetrical': 'False', 'to': "orm['nomencladores.FuenteRenovable']"}),
            'geolocation': ('gmap_admin.fields.GeoLocationField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'interconexion': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'latitud': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '15', 'decimal_places': '10', 'blank': 'True'}),
            'longitud': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '15', 'decimal_places': '10', 'blank': 'True'}),
            'materia_prima': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['nomencladores.MateriaPrima']", 'null': 'True', 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'organizacion': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['organizaciones.Organizacion']", 'null': 'True', 'blank': 'True'}),
            'potencia': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'tarifa': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'vegetal': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['nomencladores.Vegetal']", 'null': 'True', 'blank': 'True'})
        },
        'organizaciones.usuariosbeneficiarios': {
            'Meta': {'object_name': 'UsuariosBeneficiarios'},
            'anho': ('django.db.models.fields.IntegerField', [], {'max_length': '4'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'numero': ('django.db.models.fields.IntegerField', [], {}),
            'sistema': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['organizaciones.SistemaEjecucion']"})
        }
    }

    complete_apps = ['organizaciones']