from nomencladores.models import Categoria,FuenteRenovable,MateriaPrima,Pais,TipoEntidad,Vegetal,Actividad,Departamento
from django.contrib import admin

admin.site.register(Categoria)
admin.site.register(Actividad)
admin.site.register(FuenteRenovable)
admin.site.register(MateriaPrima)
admin.site.register(Pais)
admin.site.register(TipoEntidad)
admin.site.register(Vegetal)
admin.site.register(Departamento)
