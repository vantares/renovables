# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Pais'
        db.create_table('nomencladores_pais', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal('nomencladores', ['Pais'])

        # Adding model 'Categoria'
        db.create_table('nomencladores_categoria', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal('nomencladores', ['Categoria'])

        # Adding model 'TipoEntidad'
        db.create_table('nomencladores_tipoentidad', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal('nomencladores', ['TipoEntidad'])

        # Adding model 'MateriaPrima'
        db.create_table('nomencladores_materiaprima', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal('nomencladores', ['MateriaPrima'])

        # Adding model 'Vegetal'
        db.create_table('nomencladores_vegetal', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal('nomencladores', ['Vegetal'])

        # Adding model 'FuenteRenovable'
        db.create_table('nomencladores_fuenterenovable', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('imagen', self.gf('sorl.thumbnail.fields.ImageField')(max_length=100, null=True, blank=True)),
        ))
        db.send_create_signal('nomencladores', ['FuenteRenovable'])

        # Adding model 'Actividad'
        db.create_table('nomencladores_actividad', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal('nomencladores', ['Actividad'])

        # Adding model 'Departamento'
        db.create_table('nomencladores_depto', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('latitud', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=15, decimal_places=10, blank=True)),
            ('longitud', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=15, decimal_places=10, blank=True)),
            ('layerkml', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
        ))
        db.send_create_signal('nomencladores', ['Departamento'])


    def backwards(self, orm):
        # Deleting model 'Pais'
        db.delete_table('nomencladores_pais')

        # Deleting model 'Categoria'
        db.delete_table('nomencladores_categoria')

        # Deleting model 'TipoEntidad'
        db.delete_table('nomencladores_tipoentidad')

        # Deleting model 'MateriaPrima'
        db.delete_table('nomencladores_materiaprima')

        # Deleting model 'Vegetal'
        db.delete_table('nomencladores_vegetal')

        # Deleting model 'FuenteRenovable'
        db.delete_table('nomencladores_fuenterenovable')

        # Deleting model 'Actividad'
        db.delete_table('nomencladores_actividad')

        # Deleting model 'Departamento'
        db.delete_table('nomencladores_depto')


    models = {
        'nomencladores.actividad': {
            'Meta': {'object_name': 'Actividad'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'nomencladores.categoria': {
            'Meta': {'object_name': 'Categoria'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'nomencladores.departamento': {
            'Meta': {'ordering': "['nombre']", 'object_name': 'Departamento', 'db_table': "'nomencladores_depto'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latitud': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '15', 'decimal_places': '10', 'blank': 'True'}),
            'layerkml': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'longitud': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '15', 'decimal_places': '10', 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'nomencladores.fuenterenovable': {
            'Meta': {'object_name': 'FuenteRenovable'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imagen': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'nomencladores.materiaprima': {
            'Meta': {'object_name': 'MateriaPrima'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'nomencladores.pais': {
            'Meta': {'object_name': 'Pais'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'nomencladores.tipoentidad': {
            'Meta': {'object_name': 'TipoEntidad'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'nomencladores.vegetal': {
            'Meta': {'object_name': 'Vegetal'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['nomencladores']