 # -*- coding: UTF-8 -*-
from django.db import models
from django.conf import settings
from sorl.thumbnail import ImageField

SMALL_SIZE = 100

class Pais(models.Model):
    nombre = models.CharField(max_length=200)

    class Meta:
        verbose_name = "Pais"
        verbose_name_plural = "Paises"

    def __unicode__(self):
        return self.nombre

class Categoria(models.Model):
    nombre = models.CharField(max_length=200,verbose_name="Nombre de la Categoria")

    class Meta:
        verbose_name = "Categoria"
        verbose_name_plural = "Categorias"

    def __unicode__(self):
        return unicode(self.nombre)

class TipoEntidad(models.Model):
    nombre = models.CharField(max_length=200)
    class Meta:
        verbose_name = "Tipo de Entidad"
        verbose_name_plural = "Tipos de Entidad"

    def __unicode__(self):
        return self.nombre

class MateriaPrima(models.Model):
    nombre = models.CharField(max_length=200)
    class Meta:
        verbose_name = "Materia Prima"
        verbose_name_plural = "Materias Prima"

    def __unicode__(self):
        return self.nombre

class Vegetal(models.Model):
    nombre = models.CharField(max_length=200)
    class Meta:
        verbose_name = "Vegetal"
        verbose_name_plural = "Vegetales"

    def __unicode__(self):
        return self.nombre

class FuenteRenovable(models.Model):
    nombre = models.CharField(max_length=200)
    imagen = ImageField(upload_to='fuente-renovable',blank=True,null=True)
    class Meta:
        verbose_name = "Fuente Renovable"
        verbose_name_plural = "Fuentes renovables"

    def __unicode__(self):
        return self.nombre

class Actividad(models.Model):
    nombre = models.CharField(max_length=200)
    class Meta:
        verbose_name = "Tipo de Actividad"
        verbose_name_plural = "Tipos de Actividades"

    def __unicode__(self):
        return self.nombre

class Departamento(models.Model):
    nombre = models.CharField(max_length=SMALL_SIZE)
    latitud = models.DecimalField(max_digits=15,decimal_places=10,blank=True, null=True)
    longitud = models.DecimalField(max_digits=15,decimal_places=10,blank=True, null=True)
    layerkml = models.FileField(upload_to="departamento", null= True, blank= True, verbose_name=u"KML")
    class Meta:
        db_table= "nomencladores_depto"
        ordering = ["nombre"]
        verbose_name = "Departamento"
        verbose_name_plural = "Departamentos"

    def __unicode__(self):
        return self.nombre

