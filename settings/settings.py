# Django settings for renovable project.
import django.conf.global_settings as DEFAULT_SETTINGS
import os
SITE_ROOT = os.path.join( os.path.dirname( os.path.realpath(__file__) ) ,'..' )

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'renovables',                      # Or path to database file if using sqlite3.
        'USER': 'renovables',                      # Not used with sqlite3.
        'PASSWORD': 'Fabric*Relic',                  # Not used with sqlite3.
        'HOST': 'karinka.guegue.com',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}
# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'America/Managua'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'es-es'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
#USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
#USE_L10N = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = os.path.join(SITE_ROOT,'media')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = os.path.join(SITE_ROOT,'static')

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# URL prefix for admin static files -- CSS, JavaScript and images.
# Make sure to use a trailing slash.
# Examples: "http://foo.com/static/admin/", "/static/admin/".
ADMIN_MEDIA_PREFIX = '/static/admin/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
 os.path.join(SITE_ROOT,'media'),
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'pt8pr52pe#bls45p67ccx7y9!!umpak=3)1z875ky@86-n(rj_'

TEMPLATE_CONTEXT_PROCESSORS = DEFAULT_SETTINGS.TEMPLATE_CONTEXT_PROCESSORS + (
"organizaciones.context_processors.default_geolocation",
"publicaciones.context_processors.recientes_pub",
)

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
)

#GEOS_LIBRARY_PATH = '/home/invitado/Documents/repositorios/renovables/env/lib/python2.7/site-packages/django/contrib/gis/geos/libgeos.py'

ROOT_URLCONF = 'urls'

TEMPLATE_DIRS = (os.path.join(SITE_ROOT,'templates'),)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    'django.contrib.gis',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
    'renovables.organizaciones',
    'renovables.smart_selects',
    'renovables.nomencladores',
    'renovables.indicadores',
    'sorl.thumbnail',
    'south',
    'django.contrib.flatpages',
    'renovable',
    'django.contrib.humanize',
    'csvimport',
    'raven.contrib.django',
    'gmap_admin',
    'floppyforms',
    'tinymce',
    'sorl.thumbnail',
    'tastypie',
    'gunicorn',
    'publicaciones',
)

SENTRY_DSN = 'http://public:secret@example.com/1'

ADMIN_MEDIA_PREFIX = '/static/admin/'

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}
# default location google map
GOOGLE_POSITION = {
    'latitude': 12.865416,
	'longitude': -85.207229,
}

# TinyMCE editor for flatpages
TINYMCE_DEFAULT_CONFIG = {
    'theme': "advanced",
    'skin': "o2k7",
    'skin_variant': "silver",
    'theme_advanced_buttons1': "bold,italic,strikethrough,|,bullist,numlist,blockquote,|,justifyleft,justifycenter,justifyright,|,link,unlink,selectall,|,fullscreen,wp_adv,|,formatselect",
    'theme_advanced_buttons2': "underline,justifyfull,forecolor,|,pastetext,pasteword,removeformat,|,image,media,charmap,|,outdent,indent,|,undo,redo",
    'theme_advanced_buttons3': "",
    'theme_advanced_buttons4': "",
    'language': "es",
    'theme_advanced_toolbar_location': "top",
    'theme_advanced_toolbar_align': "left",
    'theme_advanced_statusbar_location': "bottom",
    'theme_advanced_resizing': 'true',
    'theme_advanced_resize_horizontal': 'false',
    'dialog_type': "modal",
    'relative_urls': "",
    'remove_script_host': "",
    'convert_urls': "",
    'remove_linebreaks': "1",
    'gecko_spellcheck': "1",
    'width': "500",
    'height': "350",
    'extended_valid_elements': "iframe[src|width|height|name|align]",
    'plugins': "safari,inlinepopups,paste,fullscreen"
}

DEFAULT_CHARSET = 'utf-8' 
FILE_CHARSET = 'utf-8'

# Tastypi API settings
# API_LIMIT_PER_PAGE = 0
API_LIMIT_PER_PAGE = 20

PORCENTAJE_RENOVABLES = 1
POBLACION_ENERGIA_RENOVABLE = 2
REDUCCION_GEI = 3
VALOR_GEI_AHORRADOS = 4
AHORRO_FACTURA_PETROLERA = 5
EMPLEOS_GENERADOS = 6   
POBLACION_BENEFICIADA = 7

#FORCE_SCRIPT_NAME = '/simernic'
LOGIN_URL = '/simernic/accounts/login/'

INSTALLED_APPS = INSTALLED_APPS + (
# ...
'raven.contrib.django',
)
ALLOWED_HOSTS = ['localhost', '127.0.0.1']

if DEBUG:
    INSTALLED_APPS += (
        'debug_toolbar',
    )

    INTERNAL_IPS = (
        '127.0.0.1',
    )

    MIDDLEWARE_CLASSES += (
        'debug_toolbar.middleware.DebugToolbarMiddleware',
    )
    DEBUG_TOOLBAR_CONFIG = {
        'INTERCEPT_REDIRECTS': False,
    }

