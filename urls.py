from django.conf.urls.defaults import patterns, include, url
from django.contrib.auth.views import login, logout
from renovable.views import OrganizacionList
from publicaciones.views import search
from tastypie.api import Api
from django.views.generic import DetailView
from publicaciones.models import Publicacion
from renovable.api import SystemResource, OrganizationResource, DepartmentResource, ProjectResource, ExternalKMLResource, SourceResource

v1_api = Api(api_name='v1')    
# instancias de recursos
v1_api.register(SystemResource())
v1_api.register(SourceResource())
v1_api.register(OrganizationResource())
v1_api.register(DepartmentResource())
v1_api.register(ProjectResource())
v1_api.register(ExternalKMLResource())



# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'renovable.views.home', name='home'),
    # url(r'^renovable/', include('renovable.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^accounts/login/$', 'django.contrib.auth.views.login',name='login'),
    url(r'^accounts/logout/$', logout,name='logout'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^chaining/', include('smart_selects.urls')),
    url(r'^tinymce/', include('tinymce.urls')),
    url(r'^inicio', 'renovable.views.home',name='simernic_home'),
    url(r'^cargarindicador', 'renovable.views.cargar_indicador'),
    url(r'^buscadoravanzado', 'renovable.views.buscador', name='advanced_search'),
        
    url(r'^exportarorganizaciones', 'renovable.views.exportarorganizaciones',
                    name='organization_export'),
    url(r'^exportarproyecto', 'renovable.views.exportarproyecto',
                    name='project_export'),
    url(r'^exportarsistemas', 'renovable.views.exportarsistemas',
                    name='system_export'),
    url(r'^exportarcontactos', 'renovable.views.exportarcontactos',
                    name='contact_export'),
                       
    url(r'^filtrarorganizaciones', 'renovable.views.filtrarorganizaciones',
                    name='organization_filter'),                       
    url(r'^filtrarsistemas', 'renovable.views.filtrarsistemas',
                    name='system_filter'),
    url(r'^filtrarproyectos', 'renovable.views.filtrarproyecto',
                    name='project_filter'),
    url(r'^filtrarcontactos', 'renovable.views.filtrarcontacto',
                    name='contact_filter'),
    
    url(r'^busquedaorganizacion', 'renovable.views.busquedaorganizacion',
                    name='organization_search'),
    url(r'^busquedacontacto', 'renovable.views.busquedacontacto', 
                    name='contact_search'),
    url(r'^busquedasistemas', 'renovable.views.busquedasistemas', 
                    name='system_search' ),
    url(r'^busquedaproyecto', 'renovable.views.busquedaproyecto', 
                    name='project_search' ),
    
    url(r'^cargarfuentes', 'renovable.views.cargar_fuentesrenovables'),
    url(r'^cargarkml', 'renovable.views.cargar_kmlexternos'),
    url(r'^loadkml', 'renovable.views.loadkml',name='loadkml'),
    
    url(r'^detalleorganizacion/(?P<pk>\d+)/$', 'renovable.views.detalleorganizacion',name='organizaciones_organizacion_detail'),
    url(r'^detalleproyecto/(?P<pk>\d+)/$', 'renovable.views.detalleproyecto',
                name='organizaciones_proyecto_detail'),
    url(r'^detallesistema/(?P<pk>\d+)/$', 'renovable.views.detallesistema',
                name='organizaciones_sistemaejecucion_detail'),
    url(r'^detallecontacto/(?P<pk>\d+)/$', 'renovable.views.detallecontacto',
                    name='organizaciones_contacto_detail'),
    url(r'^buscar', 'renovable.views.buscar'),
    url(r'^$', 'renovable.views.home'),
    url(r'^organizaciones_list/$', OrganizacionList.as_view(),name='organizaciones'), 
    url(r'^miembrosrenovables.js', 'organizaciones.views.MiembrosRenovablesJSON'),
    url(r'^proyectosrenovables.js', 'organizaciones.views.ProyectosRenovablesJSON'),
    # biblioteca virtual
    url(r'^biblioteca', 'publicaciones.views.search'),
    url(r'^publicacion/(?P<pk>\d+)/(?P<slug>[-\w]+)/$', DetailView.as_view(model=Publicacion,
        template_name='publicaciones/publicacion_detail.html'),
        name="publicacion_detail"),
    #api recursos
    (r'^api/', include(v1_api.urls)),
)

from django.conf import settings
if settings.DEBUG:
    urlpatterns += patterns( '',
        url( r'^simernic/static/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.STATIC_ROOT,
            } ),
        url( r'^simernic/media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
            } ),
    )
